<%-- 
    Document   : register_rent
    Created on : Jan 24, 2018, 3:29:54 PM
    Author     : yitagesu
--%>
<%@page import="com.intaps.nrlais.model.*" %>
<%@page import="com.intaps.nrlais.repo.*" %>
<%@page import="com.intaps.nrlais.controller.*" %>
<%@page import="com.intaps.nrlais.controller.tran.*" %>
<script src="/assets/JS/wizard_pages.js"></script>
<script src="/assets/JS/ejs.js"></script>
<script src="/party/party_list.js"></script>
<script src="/party/register_party.js"></script>
<%
    ExchangeViewController controller=new ExchangeViewController(request,response,true);
    if(controller.holding1.holdingUID!=null){ 
%>
<script>
    var exchange_tran_data =<%=controller.json(controller.tran)%>;
</script>
<%}else{%>
<script>
    var exchange_tran_data = null;
</script>
<%}%>
<div class="stepwizard">
    <div class="stepwizard-row setup-panel" id='tab-buttons'>
        
    </div>
</div>
<form role="form" action="" method="post">
    <div class="row setup-content" id="step-app">
        <div class="col-md-12">
            <h4><small><%=controller.text("Set Application")%></small></h4>
            <hr>
            <div class="form-group col-md-6">
                <label class="control-label"><%=controller.text("Application Type")%>:</label>
                <input  type="text"  class="form-control" disabled="disabled" id="app_type" value="<%=controller.tranTypeText()%>"/>
</div>
<div class="form-group col-md-6">
    <label class=""><%=controller.text("Application Date")%>:</label>
    <input max="5" type="text" class="form-control" placeholder="pick application Date"  id="app_date" dateVal="<%=controller.grigDate(controller.tran.time)%>"/>
</div>
<div class="form-group col-md-12">
    <label class=""><%=controller.text("Application Description")%>:</label>
    <textarea rows="4"  class="form-control" placeholder="<%=controller.text("Write application description")%>" id="app_description"><%=controller.escapeHtml(controller.tran.notes)%></textarea>
</div>
<div class="form-group col-md-12" id="holder_content">

</div>
<div class="col-md-12">
    <div class="pull-left">
        <div class="error-panel">

        </div>
    </div>
    <div class="pull-right">
        <div class="btn-group">
            <button class="btn btn-nrlais" type="button" data-toggle='modal' data-target='#pick_person' data-backdrop='static' ><i class="fa fa-hand-o-up"></i><%=controller.text("Load First Holding Parcel")%> </button>
            <button class="btn btn-nrlais" type="button" onclick="wizardPages.gotoNext()" id="nextToSecond"><%=controller.text("Next")%> <i class="fa fa-angle-double-right"></i></button>
        </div>
    </div>

</div>
</div>
</div>
<div class="row setup-content" id="step-holding1-applicants">
    <div class="col-md-12">
        <h4><small><%=controller.text("Enter Application Information for first Holders")%></small></h4>
        <hr/>
        <div id="exchange_applicant1">
        </div>
        <div class="align-right pull-right">
            <div class="btn-group">
                <button class="btn btn-nrlais nextBtn" type="button" id="doc_nextbtn" onclick="wizardPages.gotoNext()"><%=controller.text("Next")%> <i class="fa fa-angle-double-right"></i></button>
            </div>
        </div>
        <div class="pull-left">
            <div class="btn-group">
                <div class="id-error-panel">
                    
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row setup-content" id="step-parcel1">
    <div class="col-md-12">
        <h4><small><%=controller.text("Select Parcels for Exchange from the First Holding")%></small></h4>
        <hr>
        <div id="exchange_parcels1">
        </div>

        <div class="pull-right">
            <div class="btn-group">
                <button class="btn btn-nrlais" type="button" onclick="wizardPages.gotoNext()"><%=controller.text("Next")%> <i class="fa fa-angle-double-right"></i></button>
            </div>
        </div>
        <div class="pull-left">
            <div class="parcel1-error-panel error_panel">
            </div>
        </div>
    </div>
</div>

<div class="row setup-content" id="step-holding2">
    <div class="col-md-12">
        <h4><small><%=controller.text("Select the Second Holding and Enter Application Information for Second Holding")%></small></h4>
        <hr/>
        <div id="exchange_holding2">
        </div>
        <div id="exchange_applicant2">
        </div>
        

        <div class="align-right pull-right">
            <div class="btn-group">
                <button class="btn btn-nrlais" type="button" data-toggle='modal' data-target='#pick_person' data-backdrop='static' ><i class="fa fa-hand-o-up"></i> <%=controller.text("Load Second Holding Parcel")%> </button>
                <button class="btn btn-nrlais nextBtn" type="button" id="doc_nextbtn" onclick="wizardPages.gotoNext()"><%=controller.text("Next")%>  <i class="fa fa-angle-double-right"></i></button>
            </div>
        </div>
        <div class="pull-left">
            <div class="btn-group">
                <div class="id-error-panel">

                </div>
                <div class="Holder2_error error_panel"></div>
            </div>
        </div>
    </div>
</div>

<div class="row setup-content" id="step-parcel2">
    <div class="col-md-12">
        <h4><small><%=controller.text("Select Parcels for Exchange from the Second Holding")%></small></h4>
        <hr>
        <div id="exchange_parcels2">
        </div>

        <div class="pull-right">
            <div class="btn-group">
                <button class="btn btn-nrlais" type="button" onclick="wizardPages.gotoNext()"><%=controller.text("Next")%> <i class="fa fa-angle-double-right"></i></button>
            </div>
        </div>
        <div class="pull-left">
            <div class="parcel2-error-panel error_panel">
            </div>
        </div>
    </div>
</div>
<div class="row setup-content" id="step-docs">
    <div class="col-md-12">
        <h4><small><%=controller.text("Add Required Documents")%></small></h4>
        <hr/>
        <table class="table table-striped jambo_table">
            <thead>
                <tr>
                    <td><%=controller.text("Document Type")%></td>
                    <td><%=controller.text("Document Reference")%></td>
                    <td><%=controller.text("Document Description")%></td>
                    <td><%=controller.text("Document File")%></td>
                    <td></td>
                </tr>
            </thead>
            <tbody id="documentTableBody">                    
                <tr doc_type="<%=DocumentTypeInfo.DOC_TYPE_DIVORCE_CLAIM_RESOLUTION%>">
                    <td><%=controller.text("Claim Resolution Document")%>:</td>
                    <td id="doc_ref"></td>
                    <td id="doc_desc"></td>
                    <td id="doc_File"></td>
                    <td><a  class="btn btn-nrlais"><i class="fa fa-plus"></i></a> </td>
                </tr><tr doc_type="<%=DocumentTypeInfo.DOC_TYPE_LAND_HOLDING_CERTIFICATE%>">
                    <td><%=controller.text("Land Holding Certificate of the First Holder")%>:</td>
                    <td id="doc_ref"></td>
                    <td id="doc_desc"></td>
                    <td id="doc_File"></td>
                    <td><a  class="btn btn-nrlais"><i class="fa fa-plus"></i></a> </td>
                </tr>
                <tr doc_type="<%=DocumentTypeInfo.DOC_TYPE_LAND_HOLDING_CERTIFICATE%>">
                    <td><%=controller.text("Land Holding Certificate of the Second Holder")%>:</td>
                    <td id="doc_ref"></td>
                    <td id="doc_desc"></td>
                    <td id="doc_File"></td>
                    <td><a  class="btn btn-nrlais"><i class="fa fa-plus"></i></a> </td>
                </tr>
            </tbody>
        </table>

        <div class="align-right pull-right">
            <div class="btn-group">
                <button class="btn btn-nrlais" type="button" data-toggle='modal' data-target='#add_document' data-backdrop='static'><i class="fa fa-plus"></i><%=controller.text("Add")%> </button>
                <button class="btn btn-nrlais nextBtn" type="button" id="doc_nextbtn" onclick="wizardPages.gotoNext()"><%=controller.text("Next")%> <i class="fa fa-angle-double-right"></i></button>
            </div>
        </div>
        <div class="pull-left">
            <div class="btn-group">
                <div class="error-req-doc-panel">

                </div>
            </div>
        </div>
    </div>
</div>
<div class="row setup-content" id="step-finish">
    <div class="col-md-12">
        <h4><small><%=controller.text("Review and Submit Application")%></small></h4>
        <hr>
        <div class="print_slip" id="print_slip">
             <iframe id="printSlip"  frameborder="0" width="100%" height="150px" scrolling="no"></iframe>
        </div>
        <div class="align-right">
            <div class="btn-group">
                <button class="btn btn-nrlais" type="button" id="print" onclick="pritSlip()"><i class="fa fa-print"></i> <%=controller.text("Print")%></button>
                <button class="btn btn-nrlais nextBtn" type="button" id="exchange_save_btn" ><%=controller.text("Register Transaction")%> <i class="fa fa-exclamation"></i></button>
            </div>
        </div>
    </div>
</div>
</form>
