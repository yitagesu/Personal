/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var consolidation_holding_data;
var consolidation_holders;
var consolidation_holdingID;
var consolidation_combined;
var consolidation_docs = [];
function consolidation_buildTransactionObject()
{
    if (wizardPages.current == "#step-docs-tab")
    {
        consolidation_docs = documentPicker.getAllDocs();
    }
    var data = {};
    data.holdingUID = consolidation_holdingID;
    data.parcels = [];
    parcelTransferTable.getTransfers().forEach(function (p) {
        data.parcels.push(p.parcelUID);
    });
    data.applicants = applicantPicker.getApplicants();
    data.landCertDocs = consolidation_docs;
   // data.otherDocument = documentPicker.getAllAddtionalDocument();
    var tranData = transaction_getTransactionInfo(TRAN_TYPE_CONSOLIDATION);
    tranData.nrlais_kebeleid = consolidation_holding_data.nrlais_kebeleid;
    tranData.data = data;
    consolidation_combined = tranData;
}

function consolidation_addHolding(holdingUID)
{
    consolidation_holdingID = holdingUID;
    transaction_map.zoomHolding('nrlais_inventory', holdingUID);
    //show holding information
    $.ajax('/holding/min_holding_search_result.jsp?holding_uid=' + holdingUID, {
        'data': {'method': 'GET'},
        'success': function (data, textStatus, jqXHR) {
            $('#holder_content').html(data);
            $('#pick_person').modal('hide');
        }
    });


    //load holding data from database
    $.ajax('/api/get_holding?schema=nrlais_inventory&holding_uid=' + holdingUID,
            {
                method: 'GET',
                success: function (data)
                {
                    //alert(JSON.stringify(data));
                    consolidation_holding_data = data.res;
                    var party_done = [];
                    consolidation_holders = [];
                    consolidation_holding_data.parcels.forEach(function (parcel)
                    {
                        parcel.rights.forEach(function (right)
                        {
                            if (right.rightType != RIGHT_TYPE_PUR)
                                return;
                            if (party_done[right.partyUID])
                                return;
                            party_done[right.partyUID] = true;
                            consolidation_holders.push(right);
                        });
                    });
                },
                dataType: 'json'
            });

    //setup applicant table
    var setting = {
        pickPOM: true,
        pickShare: false,
        selectable: false
    };

    if (consolidation_combined)
        applicantPicker.load("#holder_with_representative", holdingUID, consolidation_combined.data.applicants, setting);
    else
        applicantPicker.load("#holder_with_representative", holdingUID, null, setting);

    //setup parcel tansfer table
    var parcelSetting = {
        split: false,
        transferArea: false,
        transferShare: false,
        singleParcel: false,
        transferLabel: lang('Select')
    };
    if (consolidation_combined == null)
        parcelTransferTable.load('#holder_parcel', holdingUID, null, parcelSetting);
    else
    {
        var t = [];
        consolidation_combined.data.parcels.forEach(function (p)
        {
            t.push({parcelUID: p});
        });
        parcelTransferTable.load('#holder_parcel', holdingUID, t, parcelSetting);
    }


    //load required documents

    consolidation_holdingID = holdingUID;
}
function consolidation_validateApplicantPage() {
    return applicantPicker.validate(consolidation_holders);
}
function consolidation_validateApplicationPage()
{
    return transaction_validateApplication() && validatePageOne();
}
function condolidation_validateParcelPage() {
    var valid = true;
    var validator = validatorPanel('.parcel-error-panel');
    var parcel = $('#holder_parcel input[field=select]:checked').length > 1;
    if (!parcel) {
        validator.removeError("selectParcel");
        validator.addError("selectParcel", lang("Please select at least two parcel"));
        valid = false;
    } else {
        validator.removeError("selectParcel");
    }
    return valid;
}
function consolidation_SaveToServer()
{
    transaction_saveTransaction(consolidation_combined, false, function (ternID) {
        bootbox.alert({
            message: lang("You Have Successfully Registered a Transaction")+"!",
            callback: function () {
                window.location.href = "/dashboard.jsp";
            }
        });
    }, function (err) {
        bootbox.alert(lang("Transaction Registration Failed")+". \n" + err);
    });
}
function consolidation_showPreview() {
    consolidation_buildTransactionObject();
    $.ajax({type: 'POST',
        url: "/tran/consolidation/full_conslidation_viewer_embeded.jsp",
        contentType: "application/json",
        data: JSON.stringify(consolidation_combined),
        async: true,
        error: function (errorThrown) {
            alert(lang("error"));
        },
        success: function (data) {
            var iframe = $('iframe#printSlip').contents();
            iframe.find('body').html(data);
            $('#printSlip').attr('height', document.getElementById("printSlip").contentWindow.document.body.scrollHeight + "px");
        }

    });
}


function consolidation_init() {

    wizardPages.buildNavHtml("#tab-buttons",
            [{tab: "#step-app-tab", cont: "#step-app", enable: true},
                {tab: "#step-applicant-tab", cont: "#step-applicant", enable: true},
                {tab: "#step-parcel-tab", cont: "#step-parcel", enable: true},
                {tab: "#step-docs-tab", cont: "#step-docs", enable: true},
                {tab: "#step-finish-tab", cont: "#step-finish", enable: true},
            ]);
    wizardPages.beforePageChange = function (show, hide)
    {
        var ret = true;
        if (hide == "#step-app-tab") {
            ret = consolidation_validateApplicationPage();
        } else if (hide == "#step-applicant-tab") {
            ret = consolidation_validateApplicantPage();
        } else if (hide == "#step-parcel-tab") {
            ret = condolidation_validateParcelPage();
        }
        if (show == "#step-docs-tab") {
            var types = [];
            var adj = parcelTransferTable.getTransfers();
            for (var i in adj)
                types.push({typeID: DOC_TYPE_LAND_HOLDING_CERTIFICATE, description: lang("Land Certificate for Parcel")+" - " + parcelTransferTable.seqNo(adj[i].parcelUID)});
            documentPicker.load("#documentTableBody", types, function () {
                for (var i = 0; i < adj.length; i++)
                {
                    documentPicker.setRequiredDocument(i, consolidation_docs[i]);
                }
            });
            ret = condolidation_validateParcelPage();
            consolidation_showPreview();
        }
        if (hide == "#step-docs-tab")
        {
            consolidation_docs = documentPicker.getAllDocs();
            var valid = true;
            var validator = validatorPanel('.error-req-doc-panel');
            for (var i = 0; i < consolidation_docs.length; i++) {
                if (consolidation_docs[i].refText=="") {
                    validator.removeError("landHolding");
                    validator.addError("landHolding", lang("Please add Land Holding Certificate"));
                    valid = false;
                } else {
                    validator.removeError("landHolding");
                }
            }
            return valid;
        }
        if (show == "#step-finish-tab") {
            consolidation_showPreview();
        }

        return ret;
    }
    $("#app_type").val(lang("Consolidation Transaction"));

    app_holdingSearch.onHoldingPicked = consolidation_addHolding;

    documentPicker.load("#documentTableBody");

    //initialize neighbourds list picker
    consolidation_adjacent_parcels = adjacentParcelsPicker("#consolidation_neighbors");

    //load existing data
    if (consolidation_holdingID != null)
    {
        consolidation_addHolding(consolidation_holdingID);
    }

    transaction_saveInitial = function ()
    {
        if (consolidation_holdingID == null)
        {
            bootbox.alert(lang('You must at least select a holding to save the consolidation transaction'));
            return;
        }
        consolidation_buildTransactionObject();
        transaction_saveTransaction(consolidation_combined, true, function (tranUID) {
            bootbox.alert({
                message: lang("You Have Saved a Transaction Data Temporarily")+"!",
                callback: function () {
                    window.location.href = "/dashboard.jsp";
                }
            });
        }, function (err) {
            bootbox.alert(lang("Saving Transaction Data Failed")+".\n" + err);
        });
    };




    //load required documents
    if (consolidation_combined)
    {
        consolidation_docs = consolidation_combined.data.landCertDocs;
    }

}
