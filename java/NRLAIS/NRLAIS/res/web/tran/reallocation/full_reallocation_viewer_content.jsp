<table class="table table-striped">
    <tbody id="">
        <%@ include file="/tran/tran_viewer_header.jsp" %>
        <tr><td colspan="2" class="table_separetor"><label><%=controller.text("Information of Holder")%></label></td></tr>
        <%for(LADM.Right r:controller.holding.getHolders()){
            String holderROLE=controller.roleText(r.party.mreg_familyrole);
            String holderSEX=controller.sexText(r.party.sex);
            Applicant ap=controller.partyApplicant(r.party.partyUID);
        %>
        <tr id="final_<%=controller.holding.holdingUID%>" class="holder">
            <td class="many" colspan="2">
                <label><%=holderROLE%><%=controller.text("Info")%></label>
                <ul>
                    <li><label><%=controller.text("Name")%>: </label> <%=r.party.getFullName()%></li>
                    <li><label><%=controller.text("Sex")%>: </label> <%=holderSEX%></li>
                    <li><label><%=controller.text("Age")%>: </label> <span id="ageOfApp">  <%=controller.age(r.party.dateOfBirth)%> (<%=EthiopianCalendar.ToEth(r.party.dateOfBirth).toString()+controller.text(" EC")%>)</span></li>
                    <li><label><%=controller.text("Share")%>: </label> <%=r.share.num%>/<%=r.share.denum%></li>
                </ul>
            </td>
        </tr>
        <%}//for%>
        <tr><td colspan="2" class="table_separetor"><label>Information of Allocated Party</label></td></tr>
            <%for(PartyItem b:controller.beneficiaries()){
                String benfiROLE=controller.roleText(b.party.mreg_familyrole);
                String benfiSEX=controller.sexText(b.party.sex);
                Applicant ap=controller.partyApplicant(b.party.partyUID);
            %>
            <tr id="benfi_">
                <td class="many">
                    <ul>
                        <li><label><%=controller.text("Name")%>: </label> <%=b.party.getFullName()%></li>
                        <li><label><%=controller.text("Sex")%>: </label> <%=benfiSEX%></li>
                        <li><label><%=controller.text("Age")%>: </label> <%=controller.age(b.party.dateOfBirth)%> (<%=EthiopianCalendar.ToEth(b.party.dateOfBirth).toString()+controller.text(" EC")%>)</li>
                        <li><label><%=controller.text("Family Role")%>: </label> <%=benfiROLE%></li>
                        <li><label><%=controller.text("Share")%>: </label> <%=controller.shareText(b.share)%></li>
                        <li><label><%=controller.text("ID")%>: </label> <%if(ap.idDocument.fileImage ==null){%>
                        <%=controller.applicantIDTypeText(ap)%> (<%=controller.applicantIDText(ap)%>)
                        <%}else{%>
                        <a href="<%=controller.applicantIDLink(ap)%>"><%=controller.applicantIDTypeText(ap)%> (<%=controller.applicantIDText(ap)%>)</a>
                        <%}%>
                    </li>
                    <li><label><%=controller.text("Proof of Marital Status")%>: </label>  <%if(ap.pomDocument.fileImage ==null){%>
                        <%=controller.applicantPOMText(ap)%> 
                        <%}else{%>
                        <a href="<%=controller.applicantPOMLink(ap)%>"><%=controller.applicantPOMText(ap)%></a>
                        <%}%>
                    </li>
                    </ul>
                </td>
            </tr>
            <%}%>
            <tr><td colspan="2" class="table_separetor"><label><%=controller.text("Parcel for Reallocation")%></label> </td></tr>
        <%for(ParcelTransfer p:controller.transfers()){
               LADM.Parcel pr=controller.transferParcel(p);
        %>
        <tr>
            <td class="many" colspan="2">
                <ul>
                    <li><label><%=controller.text("Parcel No")%>: </label> <%=pr.seqNo%></li>
                    <%if(p.splitParcel==true){%>
                    <li><label><%=controller.text("Area")%>: </label><%=pr.areaGeom%> <%=controller.text("M")%><sup>2</sup> (<%=controller.text("Parcel Split")%>)</li>
                    <%}else{%>
                    <li><label><%=controller.text("Area")%>: </label><%=pr.areaGeom%> <%=controller.text("M")%><sup>2</sup></li>
                    <%}%>
                </ul>
            </td>
        </tr>
        <%}//for parcel%>
        <tr><td colspan="2" class="table_separetor"><label><%=controller.text("Document Provides")%></label></td></tr>
        <tr>
            <td colspan="2">
                <ol>
                    <li><%=controller.text("Reallocation Decision")%>: (<a href="<%=controller.showDocumentLink(controller.data.adminDecisionDoc)%>"><%=controller.showDocumentText(controller.data.adminDecisionDoc)%></a>)</li>
                    <li><%=controller.text("Claim Resolution")%>: (<a href="<%=controller.showDocumentLink(controller.data.claimResolution)%>"><%=controller.showDocumentText(controller.data.claimResolution)%></a>)</li>
                    <li><%=controller.text("Land Holding Certificate")%>: (<a href="<%=controller.showDocumentLink(controller.data.landHoldingCertificateDoc)%>"><%=controller.showDocumentText(controller.data.landHoldingCertificateDoc)%></a>)</li>
                    <%for(SourceDocument doc:controller.data.otherDocument){%>
                        <li><%=controller.adminSourceText(DocumentTypeInfo.DOC_TYPE_OTHER)%>: <%=doc.description%> (<%=doc.refText%>)</li>
                    <%}%>
                </ol>
            </td>
        </tr>
    </tbody>

</table>

