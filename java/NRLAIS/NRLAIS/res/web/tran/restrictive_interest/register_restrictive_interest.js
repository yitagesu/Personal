/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function restrictiveIntersetController(restrictionData)
{


    var kebeleID;
    var holdingUID;
    var restrictorsPicker;
    var contract_doc_image = null;
    var contract_doc_imie = null;
    var start_date_picker;
    var end_date_picker
    function buildTransactionObject()
    {
        var restData = getRestrictiveInterestContract();

        restData.holdingUID = holdingUID;
        restData.applicants = applicantPicker.getApplicants();
        restData.restrictedParcels = parcelTransferTable.getTransfers();
        restData.restrictionParties = restrictorsPicker.parties;

        var tranData = transaction_getTransactionInfo(TRAN_TYPE_RESTRICTIVE_INTEREST);
        if (trancation_type == TRAN_TYPE_RESTRICTIVE_INTEREST)
        {
            restData.courtDoc = documentPicker.getRequiredDocument(0);
            restData.landHoldingCertificateDoc = documentPicker.getRequiredDocument(1);
        } else
        {
            restData.landHoldingCertificateDoc = documentPicker.getRequiredDocument(0);
        }
        restData.otherDocument = documentPicker.getAllAddtionalDocument();
        var tranData = transaction_getTransactionInfo(trancation_type);
        tranData.nrlais_kebeleid = kebeleID;
        tranData.data = restData;
        return tranData;
    }

    function setHoldingUID(huid)
    {
        holdingUID = huid;
        transaction_map.zoomHolding('nrlais_inventory', holdingUID);
        //show holding information
        $.ajax('/holding/min_holding_search_result.jsp?holding_uid=' + holdingUID, {
            'data': {'method': 'GET'},
            'success': function (data, textStatus, jqXHR) {
                $('#holder_content').html(data);
                $('#pick_person').modal('hide');
            }
        });


        //load holding data from database
        $.ajax('/api/get_holding?schema=nrlais_inventory&holding_uid=' + holdingUID,
                {
                    method: 'GET',
                    success: function (data)
                    {
                        //alert(JSON.stringify(data));
                        kebeleID = data.res.nrlais_kebeleid;
                    },
                    dataType: 'json'
                });

        //setup applicant table
        if (restrictionData == null)
            applicantPicker.load("#holder_with_representative", holdingUID, null, {pickPOM: true});
        else
        {
            applicantPicker.load("#holder_with_representative", holdingUID, restrictionData.data.applicants, {pickPOM: true});
        }

        //setup parcel tansfer table
        var parcelSetting = {
            split: false,
            transferArea: false,
            transferShare: false,
            transferLabel: lang("Restrict")
        }
        if (restrictionData == null)
            parcelTransferTable.load('#holder_parcel', holdingUID, null, parcelSetting);
        else
        {
            parcelTransferTable.load('#holder_parcel', holdingUID, restrictionData.data.restrictedParcels, parcelSetting);
        }

    }
    function validateApplicantPage() {
        return applicantPicker.validate();
    }
    function validateApplicationPage()
    {
        return transaction_validateApplication() && validatePageOne();
    }
    function saveToServer()
    {
        restrictionData = buildTransactionObject();
        transaction_saveTransaction(restrictionData, false, function (ternID) {
            bootbox.alert({
                message: lang("You Have Successfully Registered a Transaction") + "!",
                callback: function () {
                    window.location.href = "/dashboard.jsp";
                }
            });
        }, function (err) {
            bootbox.alert(lang("Transaction Registration Failed") + " .\n" + err);
        });
    }
    function showPreview() {
        restrictionData = buildTransactionObject();
        $.ajax({type: 'POST',
            url: "/tran/restrictive_interest/full_restrictive_interest_viewer_embeded.jsp?tran_type=" + trancation_type,
            contentType: "application/json",
            data: JSON.stringify(restrictionData),
            async: true,
            error: function (err) {
                bootbox.alert(lang("Error") + "\n" + err);
            },
            success: function (data) {
                $("#print_slip").html(data);
            }

        });
    }
    function getRestrictiveInterestContract()
    {
        var ret = {};
        ret.restrictionType = $("#restriction_type").val();
        ret.startDate = getLongDateFromCal($("#restrictive_interest_date_start"));
        ret.endDate = getLongDateFromCal($("#restrictive_interest_date_end"));

        ret.agreementDoc =
                {
                    docfile: $("#restrictive_interest_agreement_doc").val(),
                    description: $("#restrictive_interest_agreement_disc").val(),
                    refText: $("#restrictive_interest_agreement_doc_ref").val(),
                    fileImage: contract_doc_image,
                    mimeType: contract_doc_imie,
                    archiveType: contract_doc_image == null ? 2 : 1
                };
        return ret;
    }
    function populateContractPicker()
    {
        start_date_picker.setDate(new Date(restrictionData.data.startDate));
        end_date_picker.setDate(new Date(restrictionData.data.endDate));
        if (restrictionData.data.isRent)
            $("#restrictive_interest_restrictive_interest_type").attr("checked", "checked");
        else
            $("#restrictive_interest_lease_type").attr("checked", "checked");
        if (restrictionData.data.agreementDoc)
        {
            contract_doc_image = restrictionData.data.agreementDoc.fileImage;
            contract_doc_imie = restrictionData.data.agreementDoc.mimeType;
            $("#restrictive_interest_agreement_disc").val(restrictionData.data.agreementDoc.description);
            $("#restrictive_interest_agreement_doc_ref").val(restrictionData.data.agreementDoc.refText);
        }

    }
    function agreementDocAttached()
    {

        if (attachment.hasAttachment()) {
            contract_doc_imie = attachment.mimeType;
            contract_doc_image = attachment.imageBytes;
        } else {
            contract_doc_image = null;
            contract_doc_imie = null
        }
    }

    function validateParcelPage() {
        var valid = true;
        var validator = validatorPanel('.parcel-error-panel');
        var parcel = $('#holder_parcel  input[field=select]:checked').length > 0;
        if (!parcel) {

            validator.addError("selectParcel", lang("Please select at least one parcel"));
            valid = false;
        } else {
            validator.removeError("selectParcel");
        }
        return valid;
    }
    function validateRestricationPage() {
        var valid = true;
        var oneValid = true;
        var requiredField = ['restriction_type', 'restrictive_interest_date_start', 'restrictive_interest_agreement_doc_ref'];
        for (var i = 0; i < requiredField.length; i++) {
            oneValid = ($('#' + requiredField[i]).val() != "");
            setValidationStyle($('#' + requiredField[i]), oneValid);
            valid = valid && oneValid;
        }
        return valid;
    }

    function validateThirdPartyPage() {
        var validApplication = true;
        var validator = validatorPanel(".error-tenant-party-panel");
        if (!$.trim($('#restrictive_interest_parties tbody').html()).length) {
            validator.removeError("partyPanel");
            validator.addError("partyPanel", lang("Please add Third Party Information"));
            validApplication = false;
        } else {
            validator.removeError("partyPanel");
        }
        return validApplication;
    }
    
    function validateReqDocumentPage(){
        var valid = true;
    var validator = validatorPanel('.error-req-doc-panel');

    if (!documentPicker.getRequiredDocument(0))
    {
        validator.removeError("claimResolution");
        validator.addError("claimResolution", lang("Please add court decision document"));
        valid = false;
    } else {
        validator.removeError("claimResolution");
    }

    if (!documentPicker.getRequiredDocument(1))
    {
        validator.removeError("landHoldingCertificate");
        validator.addError("landHoldingCertificate", lang("Please add land holding certificate"));
        valid = false;
    } else {
        validator.removeError("landHoldingCertificate");
    }
    return valid;
    }
    //setup wizard
    wizardPages.load([
        {tab: "#step-1-tab", cont: "#step-1", enable: true},
        {tab: "#step-2-tab", cont: "#step-2", enable: true},
        {tab: "#step-3-tab", cont: "#step-3", enable: true},
        {tab: "#step-4-tab", cont: "#step-4", enable: true},
        {tab: "#step-5-tab", cont: "#step-5", enable: true},
        {tab: "#step-6-tab", cont: "#step-6", enable: true},
        {tab: "#step-7-tab", cont: "#step-7", enable: true},
    ]);
    wizardPages.beforePageChange = function (show, hide)
    {
        var ret = true;
        if (hide == "#step-1-tab")
        {
            //ret=restrictive_interest_validateApplicationPage();   
            ret = validateApplicationPage();
        } else if (hide == "#step-2-tab")
        {
            //ret=restrictive_interest_validateApplicantPage();
            ret = validateApplicantPage();
        } else if (hide == "#step-3-tab")
        {
            ret = validateParcelPage();//NOTE: no validation for parcel page?
        } else if (hide == "#step-4-tab")
        {
            ret =validateThirdPartyPage();
        } else if (hide == "step-5-tab") {
            ret = validateRestricationPage();
        } else if (hide == "step-6-tab") {
            ret = validateReqDocumentPage();
        }
        if (show == "#step-7-tab")
        {
            showPreview();
        }
        return ret;
    };
    $("#restrictive_interest_save_btn").click(function () {
        saveToServer();
    });
    if (window.location.href.search("tran_type=11") > 0) {
        $("#app_type").val(lang("Restrictive Interest Transaction"));
    }
    if (window.location.href.search("tran_type=10") > 0) {
        $("#app_type").val(lang("Easment/Servitude Transaction"));
    }
    app_holdingSearch.onHoldingPicked = setHoldingUID;

    documentPicker.load("#documentTableBody");

    restrictiveIntersetController.onContractAttached = function ()
    {
        agreementDocAttached();
    };


    transaction_saveInitial = function ()
    {
        if (!holdingUID)
        {
            bootbox.alert(lang("You must at least select holding to save this transaction"));
            return;
        }
        transaction_saveTransaction(buildTransactionObject(), true, function (tranUID) {
            bootbox.alert({
                message: lang("You Have Saved a Transaction Data Temporarily") + "!",
                callback: function () {
                    window.location.href = "/dashboard.jsp";
                }
            });
        }, function (err) {
            bootbox.alert(lang("Saving Transaction Data Failed") + ".\n" + err);
        });
    }

    //initialize party list picker
    restrictorsPicker = getPartyListPicker({
        containerEl: "restrictive_interest_parties",
        addButtonEl: "restrictive_interest_add",
        share: false,
        idDoc: true,
        edit: true,
        delete: true,
    });
    if (restrictionData)
    {

    }
    //initailize ethiopian dates
    start_date_picker = new EthiopianDualCalendar("#restrictive_interest_date_start");
    end_date_picker = new EthiopianDualCalendar("#restrictive_interest_date_end");

    //load existing data
    if (restrictionData != null)
    {
        setHoldingUID(restrictionData.data.holdingUID);
        restrictorsPicker.setParyItems(restrictionData.data.restrictionParties);
        documentPicker.setRequiredDocument(0, restrictionData.data.courtDoc);
        documentPicker.setRequiredDocument(1, restrictionData.data.landHoldingCertificateDoc);
        populateContractPicker();
    }

}