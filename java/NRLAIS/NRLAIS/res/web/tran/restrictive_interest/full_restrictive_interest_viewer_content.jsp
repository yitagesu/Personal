<table class="table table-striped">
    <tbody id="">
        <%@ include file="/tran/tran_viewer_header.jsp" %>
        <%for(LADM.Right r:controller.holding.getHolders()){
            String holderROLE=controller.roleText(r.party.mreg_familyrole);
            String holderSEX=controller.sexText(r.party.sex);
            Applicant ap=controller.partyApplicant(r.party.partyUID);
        %>
        <tr id="final_<%=controller.holding.holdingUID%>" class="holder">
            <td class="many" colspan="2">
                <label><%=holderROLE%> Info</label>
                <ul>
                    <li><label><%=controller.text("Name")%>: </label> <%=r.party.getFullName()%></li>
                    <li><label><%=controller.text("Sex")%>: </label> <%=holderSEX%></li>
                    <li><label><%=controller.text("Age")%>: </label> <span id="ageOfApp"><%=controller.age(r.party.dateOfBirth)%> (<%=EthiopianCalendar.ToEth(r.party.dateOfBirth).toString()+" EC"%>)</span></li>
                    <li><label><%=controller.text("Share")%>: </label> <%=r.share.num%>/<%=r.share.denum%></li>
                    <li><label><%=controller.text("ID")%>: </label> <%if(ap.idDocument.fileImage ==null){%>
                        <%=controller.applicantIDTypeText(ap)%> (<%=controller.applicantIDText(ap)%>)
                        <%}else{%>
                        <a href="<%=controller.applicantIDLink(ap)%>"><%=controller.applicantIDTypeText(ap)%> (<%=controller.applicantIDText(ap)%>)</a>
                        <%}%>
                    </li>
                    <li><label><%=controller.text("Proof of Marital Status")%>: </label>  <%if(ap.pomDocument.fileImage ==null){%>
                        <%=controller.applicantPOMText(ap)%> 
                        <%}else{%>
                        <a href="<%=controller.applicantPOMLink(ap)%>"><%=controller.applicantPOMText(ap)%></a>
                        <%}%>
                    </li>
                </ul>
            </td>
            
        </tr>
        <%}%>
        <tr><td class="table_separetor" colspan="2"><%=controller.text("Information of Third Party")%>  </td></tr>
       <%for(Object _heir:controller.thirdParty()){
           PartyItem heir=(PartyItem)_heir;  
           String heirROLE=controller.roleText(heir.party.mreg_familyrole);
            String heirSEX=controller.sexText(heir.party.sex);%>
        <tr>
            <td class="many">
                <ul>
                    <li><label><%=controller.text("Name")%>: </label> <%=heir.party.getFullName()%></li>
                    <li><label><%=controller.text("Sex")%>: </label> <%=heirSEX%></li>
                    <li><label><%=controller.text("Age")%>: </label> <span id="ageOfApp"><%=controller.age(heir.party.dateOfBirth)%> (<%=EthiopianCalendar.ToEth(heir.party.dateOfBirth).toString()+" EC"%>)</span></li>
                    <li><label><%=controller.text("Family Role")%>: </label> <%=heirROLE%></li>
                    <li><label><%=controller.text("ID")%>: </label>  <%if(heir.idDoc.fileImage ==null){%>
                        <%=controller.applicantIDTypeText(heir)%> (<%=controller.applicantIDText(heir)%>)
                        <%}else{%>
                        <a href="<%=controller.applicantIDLink(heir)%>"><%=controller.applicantIDTypeText(heir)%> (<%=controller.applicantIDText(heir)%>)</a>
                        <%}%>
                    </li>
                    
                </ul>
            </td>
            <td></td>
        </tr>  
        <%}%>
        <tr><td colspan="2" class="table_separetor"><label><%=controller.text("Parcels for Restriction")%></label> </td></tr>
        
        <%for(Object _p:controller.transfers()){
            ParcelTransfer p=(ParcelTransfer)_p;
            LADM.Parcel ptr=controller.restrictedParcel(p);
            
        %>
        <tr>
            <td class="many" colspan="2">
                <ul>
                    <li><label><%=controller.text("Parcel No")%>: </label> <%=ptr.seqNo%></li>
                    <li><label><%=controller.text("Area")%>: </label> <%=ptr.areaGeom%> M<sup>2</sup></li>
                    
                </ul>
            </td>
        </tr>
        <%}%>
        
        <tr><td colspan="2" class="table_separetor"><label><%=controller.text("Restriction Details")%></label> </td></tr>
        <tr>
            <td colspan="2">
                <ul>
                    <li><label><%=controller.text("Restriction Type")%>: </label> <%=controller.restrictionType()%></li>
                    <li><label><%=controller.text("Start Date")%>:</label><%=controller.startDate()%></li>
                    <li><label><%=controller.text("End Date")%>:</label> <%=controller.endDate()%></li>
                    
                </ul>
            </td>
        </tr>
        <tr><td colspan="2" class="table_separetor"><label><%=controller.text("Document Provides")%></label></td></tr>
        <tr>
            <td colspan="2">
                <ol>
                    <li><%=controller.adminSourceText(DocumentTypeInfo.DOC_TYPE_LAND_HOLDING_CERTIFICATE)%> (<a href="<%=controller.landHoldingCertificateLink()%>"><%=controller.landHoldingCertificateText()%></a>)</li>
                    <%for(SourceDocument doc:controller.data.otherDocument){%>
                        <li><%=controller.adminSourceText(DocumentTypeInfo.DOC_TYPE_OTHER)%>: <%=doc.description%> (<%=doc.refText%>)</li>
                    <%}%>
                </ol>
            </td>
        </tr>
    </tbody>
</table>
