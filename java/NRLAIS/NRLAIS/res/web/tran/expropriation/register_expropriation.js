var expropriation_holding_data;
var expropriation_holders;
var expropriation_holdingID;
var expropriation_combined;
var expropriation_woreda_damin;

function expropriation_buildTransactionObject() {
    var expropriationData = {
        holdingUID: expropriation_holdingID,
        applicants: applicantPicker.getApplicants(),
        transfers: parcelTransferTable.getTransfers(expropriation_holding_data),
        wordaAdministrtion: expropriation_woreda_damin.parties,
        decisionOfWoredaAdministration: documentPicker.getRequiredDocument(0),
        proofOfpaymentOfCompensation: documentPicker.getRequiredDocument(1),
        landHoldingCertificate: documentPicker.getRequiredDocument(2),
        otherDocument:documentPicker.getAllAddtionalDocument()
    };
    var tranData = transaction_getTransactionInfo(TRAN_TYPE_EXPROPRIATION);
    tranData.nrlais_kebeleid = expropriation_holding_data.nrlais_kebeleid;
    tranData.data = expropriationData;
    expropriation_combined = tranData;

}
function expropriation_validateApplicationPage() {
    return transaction_validateApplication() && validatePageOne();
}
function expropration_validateApplicantPage() {
    var validApplication = true;
    var validator = validatorPanel(".exproporationWoreda");
    if (!$.trim($('#woreda_administration tbody').html()).length) {
        validator.removeError("woredaPanel");
        validator.addError("woredaPanel", lang("Please Add State Information"));
        validApplication = false;
    } else {
        validator.removeError("woredaPanel");
    }
    return validApplication;
}
function expropriation_addExpropriationHolding(holdingUID) {
    expropriation_holdingID = holdingUID;
    transaction_map.zoomHolding('nrlais_inventory', holdingUID);
    $.ajax('/holding/min_holding_search_result.jsp?holding_uid=' + holdingUID, {
        'data': {'method': 'GET'},
        'success': function (data, textStatus, jqXHR) {
            $('#holder_content').html(data);
            $('#pick_person').modal('hide');
        }
    });

    $.ajax('/api/get_holding?schema=nrlais_inventory&holding_uid=' + holdingUID,
            {
                method: 'GET',
                success: function (data)
                {
                    //alert(JSON.stringify(data));
                    expropriation_holding_data = data.res;
                    var party_done = [];
                    expropriation_holders = [];
                    expropriation_holding_data.parcels.forEach(function (parcel)
                    {
                        parcel.rights.forEach(function (right)
                        {
                            if (right.rightType != RIGHT_TYPE_PUR)
                                return;
                            if (party_done[right.partyUID])
                                return;
                            party_done[right.partyUID] = true;
                            expropriation_holders.push(right);
                        });
                    });
                },
                dataType: 'json'
            });

var setting = {
        pickID: false,
        pickRepresentative: false,
        pickPOM: false,
        selectable: false
    };
    if (expropriation_combined == null)
        applicantPicker.load("#holder_with_representative", holdingUID, null, setting);
    else
    {
        applicantPicker.load("#holder_with_representative", holdingUID, expropriation_combined.data.applicants, setting);
    }
    var parcelSetting = {
        split: true,
        transferArea: false,
        transferShare: false,
        transferLabel: lang("Expropriate")
    }
    if (expropriation_combined == null) {
        parcelTransferTable.load('#holder_parcel', holdingUID, null, parcelSetting);
    } else {
        parcelTransferTable.load('#holder_parcel', holdingUID, expropriation_combined.data.transfers, parcelSetting);
    }

}
function expropriation_validateParcelPage() {
    var valid = true;
    var validator = validatorPanel('.parcel-error-panel');
    var parcel = $('#holder_parcel input[field=select]:checked').length > 0;
    if (!parcel) {
        validator.removeError("selectParcel");
        validator.addError("selectParcel", lang("Please select at least one parcel"));
        valid = false;
    } else {
        validator.removeError("selectParcel");
    }
    return valid;
}
function expropriation_SaveToServer() {
    transaction_saveTransaction(expropriation_combined, false, function (ternID) {
       bootbox.alert({
            message: lang("You Have Successfully Registered a Transaction")+"!",
            callback: function () {
                window.location.href = "/dashboard.jsp";
            }
        });
    }, function (err) {
        bootbox.alert(lang("Transaction Registration Failed")+".\n" + err);
    });
}
function expropriation_ShowPreview() {
    expropriation_buildTransactionObject();
    $.ajax({type: 'POST',
        url: "/tran/expropriation/full_expropriation_viewer_embeded.jsp",
        contentType: "application/json",
        data: JSON.stringify(expropriation_combined),
        async: true,
        error: function (errorThrown) {
            alert("error");
        },
        success: function (data) {
           var iframe=$('iframe#printSlip').contents();
            iframe.find('body').html(data);
            $('#printSlip').attr('height', document.getElementById("printSlip").contentWindow.document.body.scrollHeight + "px");
        }

    });
}
function expropriation_validateReqDocument() {
    var valid = true;
    var validator = validatorPanel('.error-req-doc-panel');

    if (!documentPicker.getRequiredDocument(0))
    {
        validator.removeError("decisionOfWoreda");
        validator.addError("decisionOfWoreda", lang("Please add Decision by woreda administration"));
        valid = false;
    } else {
        validator.removeError("decisionOfWoreda");
    }
    if (!documentPicker.getRequiredDocument(1)) {
        validator.removeError("paymentOfCompensation");
        validator.addError("paymentOfCompensation", lang("Please add Prove of payment of compensation"));
        valid = false;
    } else {
        validator.removeError("paymentOfCompensation");
    }
    if (!documentPicker.getRequiredDocument(2)) {
        validator.removeError("landHolding");
        validator.addError("landHolding", lang("Please add Land Holding Certificate"));
        valid = false;
    } else {
        validator.removeError("landHolding");
    }
    return valid;
}
function expropriation_init() {
    //setup wizard
    wizardPages.load([
        {tab: "#step-1-tab", cont: "#step-1", enable: true},
        {tab: "#step-2-tab", cont: "#step-2", enable: true},
        {tab: "#step-3-tab", cont: "#step-3", enable: true},
        {tab: "#step-4-tab", cont: "#step-4", enable: true},
        {tab: "#step-5-tab", cont: "#step-5", enable: true},
        {tab: "#step-6-tab", cont: "#step-6", enable: true},
    ]);
    wizardPages.beforePageChange = function (show, hide)
    {
        var ret = true;
        if (hide == "#step-1-tab")
        {
            ret = expropriation_validateApplicationPage();
        } else if (hide == "#step-2-tab")
        {
            ret = true;
        } else if (hide == "#step-3-tab")
        {
            ret = expropriation_validateParcelPage();//NOTE: no validation for parcel page?
        } else if (hide == "#step-4-tab")
        {
            ret = expropration_validateApplicantPage();
        } else if (hide == "#step-5-tab")
        {
            ret = expropriation_validateReqDocument();
        }
        if (show == "#step-6-tab")
        {
            expropriation_ShowPreview();
        }
        return ret;
    }
    $("#app_type").val(lang("Expropriation Transaction"));

    app_holdingSearch.onHoldingPicked = expropriation_addExpropriationHolding;
    //setup required document picker
    documentPicker.load("#documentTableBody");

    //load existing data
    if (expropriation_holdingID != null) {
        expropriation_addExpropriationHolding(expropriation_holdingID);
    }

    //override save initial transaction
    transaction_saveInitial = function ()
    {
        if (!expropriation_holding_data)
        {
            bootbox.alert(lang('You must at least select holding to save divorce transaction'));
            return;
        }
        expropriation_buildTransactionObject();
        transaction_saveTransaction(expropriation_combined, true, function (tranUID) {
            bootbox.alert({
                message: lang("You Have Saved a Transaction Data Temporarily")+"!",
                callback: function () {
                    window.location.href = "/dashboard.jsp";
                }
            });
        }, function (err) {
            bootbox.alert(lang("Saving Transaction Data Failed")+".\n" + err);
        });
    }
    expropriation_woreda_damin = getPartyListPicker({
        containerEl: "woreda_administration",
        addButtonEl: "expro_beneficiary_add",
        partyType: 3,
        share: false,
        idDoc: false,
        edit: true,
        delete: true,
    });

    if (expropriation_combined)
    {
        expropriation_woreda_damin.setParyItems(expropriation_combined.data.wordaAdministrtion);
    }
    if (expropriation_combined) {
        documentPicker.setRequiredDocument(0, expropriation_combined.data.decisionOfWoredaAdministration);
        documentPicker.setRequiredDocument(1, expropriation_combined.data.proofOfpaymentOfCompensation);
        documentPicker.setRequiredDocument(2, expropriation_combined.data.landHoldingCertificate);
         if(expropriation_combined.data.otherDocument.length>0){
            documentPicker.setAddtionalDocument(expropriation_combined.data.otherDocument);
        }
    }
}

