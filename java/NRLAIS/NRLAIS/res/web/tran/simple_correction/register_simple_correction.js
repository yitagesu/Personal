/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var simple_correction_holding_data;
var simple_correction_holdingID;
var simple_correction_combined;
var simple_correction_holders;
var simple_correction_applicant;
var simple_correction_parcels;

function simple_correction_buildTransactionObject() {
    var simple_correctionData = {
        holdingUID: simple_correction_holdingID,
        applicant: applicantPicker.getApplicants(),
        landHoldingCertifcateDocument: documentPicker.getRequiredDocument(0),
        otherDocument:documentPicker.getAllAddtionalDocument()
    }
    var tranData = transaction_getTransactionInfo(TRAN_TYPE_SIMPLE_CORRECTION);
    tranData.nrlais_kebeleid = simple_correction_holding_data.nrlais_kebeleid;
    tranData.data = simple_correctionData;
    simple_correction_combined = tranData;
}
function simple_correction_SaveToServer() {
    transaction_saveTransaction(simple_correction_combined, false, function (ternID) {
       bootbox.alert({
            message: lang("You Have Successfully Registered a Transaction")+"!",
            callback: function () {
                window.location.href = "/dashboard.jsp";
            }
        });
    }, function (err) {
        bootbox.alert(lang("Transaction Registration Failed")+ ".\n" + err);
    });
}
function simple_correction_addSimpleCorrectionHolding(holdingUID) {
    transaction_map.zoomHolding('nrlais_inventory', holdingUID);
    $.ajax('/holding/min_holding_search_result.jsp?holding_uid=' + holdingUID, {
        'data': {'method': 'GET'},
        'success': function (data, textStatus, jqXHR) {
            $('#holder_content').html(data);
            $('#pick_person').modal('hide');
        }
    });
    $.ajax('/api/get_holding?schema=nrlais_inventory&holding_uid=' + holdingUID,
            {
                method: 'GET',
                success: function (data)
                {
                    //alert(JSON.stringify(data));
                    simple_correction_holding_data = data.res;
                    var party_done = [];
                    simple_correction_holders = [];
                    simple_correction_holding_data.parcels.forEach(function (parcel)
                    {
                        parcel.rights.forEach(function (right)
                        {
                            if (right.rightType != RIGHT_TYPE_PUR)
                                return;
                            if (party_done[right.partyUID])
                                return;
                            party_done[right.partyUID] = true;
                            simple_correction_holders.push(right);
                        });
                    });
                },
                dataType: 'json'
            });

    var setting = {
        pickPOM: true,
        pickShare: false,
        selectable: false
    };
    if (simple_correction_combined == null)
        applicantPicker.load("#holder_with_representative", holdingUID, null, setting);
    else
    {
        applicantPicker.load("#holder_with_representative", holdingUID, simple_correction_combined.data.applicant, setting);
    }
    if (simple_correction_combined)
    {
        documentPicker.setRequiredDocument(0, simple_correction_combined.data.landHoldingCertifcateDocument);
    }
    simple_correction_holdingID = holdingUID;
}
function simple_correction_validateApplicationPage()
{
    return transaction_validateApplication() && validatePageOne();
}
function simple_correction_validateApplicantPage(){
    return applicantPicker.validate();
}
function simple_correction_validateReqDocument(){
     var valid = true;
    var validator = validatorPanel('.error-req-doc-panel');

    if (!documentPicker.getRequiredDocument(0))
    {
        validator.removeError("landHoldingCertificate");
        validator.addError("landHoldingCertificate", lang("Please add land holding certificate"));
        valid = false;
    } else {
        validator.removeError("landHoldingCertificate");
    }
    return valid;
}
function simple_correction_showPreview() {

    simple_correction_buildTransactionObject();
    $.ajax({type: 'POST',
        url: "/tran/simple_correction/full_simple_correction_viewer_embeded.jsp",
        contentType: "application/json",
        data: JSON.stringify(simple_correction_combined),
        async: true,
        error: function (errorThrown) {
            bootbox.alert(lang("error"));
        },
        success: function (data) {
            var iframe = $('iframe#printSlip').contents();
            iframe.find('body').html(data);
            $('#printSlip').attr('height', document.getElementById("printSlip").contentWindow.document.body.scrollHeight + "px");
        }

    });
}
function simpleCorrection_init() {
    wizardPages.load([
        {tab: "#step-1-tab", cont: "#step-1", enable: true},
        {tab: "#step-2-tab", cont: "#step-2", enable: true},
        {tab: "#step-3-tab", cont: "#step-3", enable: true},
        {tab: "#step-4-tab", cont: "#step-4", enable: true},
    ]);
    wizardPages.beforePageChange = function (show, hide)
    {
        var ret = true;
        if (hide == "#step-1-tab")
        {
            //ret=rent_validateApplicationPage();   
            ret = simple_correction_validateApplicationPage();
        } else if (hide == "#step-2-tab")
        {
            //ret=rent_validateApplicantPage();
            ret = simple_correction_validateApplicantPage();
        } else if (hide == "#step-3-tab")
        {
            //ret=rent_validateApplicantPage();
            ret = simple_correction_validateReqDocument();
        }
        if (show == "#step-4-tab") {
            simple_correction_showPreview();
        }
        return ret;
    }
    $("#app_type").val(lang("Simple Correction Transaction"));


    app_holdingSearch.onHoldingPicked = simple_correction_addSimpleCorrectionHolding;
    documentPicker.load("#documentTableBody");
    if (simple_correction_holdingID != null)
    {
        simple_correction_addSimpleCorrectionHolding(simple_correction_holdingID);
    }
    transaction_saveInitial = function ()
    {
        if (!simple_correction_holding_data)
        {
            bootbox.alert(lang('You must at least select holding to save simple correction transaction'));
            return;
        }
        simple_correction_buildTransactionObject();
        transaction_saveTransaction(simple_correction_combined, true, function (tranUID) {

            bootbox.alert({
                message: lang("You Have Saved a Transaction Data Temporarily")+"!",
                callback: function () {
                    window.location.href = "/dashboard.jsp";
                }
            });
        }, function (err) {
            bootbox.alert(lang("Saving Transaction Data Failed")+ ".\n" + err);
        });
    }

    if (simple_correction_combined)
    {
        documentPicker.setRequiredDocument(0, simple_correction_combined.data.landHoldingCertifcateDocument);
        if (simple_correction_combined.data.otherDocument.length > 0) {
            documentPicker.setAddtionalDocument(simple_correction_combined.data.otherDocument);
        }
    }
}
