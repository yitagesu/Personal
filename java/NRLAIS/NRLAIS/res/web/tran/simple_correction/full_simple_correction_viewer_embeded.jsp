<%-- 
    Document   : full_simple_correction_viewer_embeded
    Created on : Feb 27, 2018, 9:35:39 AM
    Author     : yitagesu
--%>

<%@page import="com.intaps.nrlais.*"%>
<%@page import="com.intaps.nrlais.api.*"%>
<%@page import="com.intaps.nrlais.model.*"%>
<%@page import="com.intaps.nrlais.viewmodel.*"%>
<%@page import="com.intaps.nrlais.model.tran.*"%>
<%@page import="com.intaps.nrlais.repo.*"%>
<%@page import="com.intaps.nrlais.tran.*"%>
<%@page import="com.intaps.nrlais.util.*"%>
<%@page import="com.intaps.nrlais.controller.*"%>
<%@page import="com.intaps.nrlais.controller.tran.*"%>
<%@page pageEncoding="UTF-8" %>
<%
    SimpleCorrectionViewController controller=new SimpleCorrectionViewController(request, response, true);  
%>
<link rel="stylesheet" href="/assets/CSS/bootstrap.min.css"/>
<link rel="stylesheet" href="/assets/CSS/bootstrap-popover-x.min.css"/>
<link rel="stylesheet" href="/assets/CSS/custom.css"/>
<%@ include file="/tran/simple_correction/full_simple_correction_viewer_content.jsp" %>
