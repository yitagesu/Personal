/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var split_holding_data;
var split_holders;
var split_holdingID;
var split_combined;


function split_buildTransactionObject()
{
    var splitData =
            {
                holdingUID: split_holdingID,
                applicants: applicantPicker.getApplicants(),
                partitions: parcelSplitTable.getPartitions(split_holding_data),
                landHoldingCertifcateDocument: documentPicker.getRequiredDocument(0),
                otherDocument: documentPicker.getAllAddtionalDocument()
            };
    var tranData = transaction_getTransactionInfo(TRAN_TYPE_SPLIT);
    tranData.nrlais_kebeleid = split_holding_data.nrlais_kebeleid;
    tranData.data = splitData;
    split_combined = tranData;
    //alert(JSON.stringify(split_combined));

}
function split_SaveToServer() {
    transaction_saveTransaction(split_combined, false, function (ternID) {
        bootbox.alert({
            message: lang("You Have Successfully Registered a Transaction")+"!",
            callback: function () {
                window.location.href = "/dashboard.jsp";
            }
        });
    }, function (err) {
        bootbox.alert(lang("Transaction Registration Failed")+". \n" + err);
    });
}
function split_showPreview() {
    split_buildTransactionObject();
    $.ajax({type: 'POST',
        url: "/tran/split/full_split_viewer_embeded.jsp",
        contentType: "application/json",
        data: JSON.stringify(split_combined),
        async: true,
        error: function (errorThrown) {
            bootbox.alert(lang("error"));
        },
        success: function (data) {
            var iframe = $('iframe#printSlip').contents();
            iframe.find('body').html(data);
            $('#printSlip').attr('height', document.getElementById("printSlip").contentWindow.document.body.scrollHeight + "px");
        }

    });
}
function split_addSplitHolding(holdingUID)
{

    //zoom map
    transaction_map.zoomHolding('nrlais_inventory', holdingUID);

    //show holding information
    $.ajax('/holding/min_holding_search_result.jsp?holding_uid=' + holdingUID, {
        'data': {'method': 'GET'},
        'success': function (data, textStatus, jqXHR) {
            $('#holder_content').html(data);
            $('#pick_person').modal('hide');
        }
    });


    //load holding data from database
    $.ajax('/api/get_holding?schema=nrlais_inventory&holding_uid=' + holdingUID,
            {
                method: 'GET',
                success: function (data)
                {
                    //alert(JSON.stringify(data));
                    split_holding_data = data.res;
                    var party_done = [];
                    split_holders = [];
                    split_holding_data.parcels.forEach(function (parcel)
                    {
                        parcel.rights.forEach(function (right)
                        {
                            if (right.rightType != RIGHT_TYPE_PUR)
                                return;
                            if (party_done[right.partyUID])
                                return;
                            party_done[right.partyUID] = true;
                            split_holders.push(right);
                        });
                    });
                },
                dataType: 'json'
            });

    //setup applicant table
    if (split_combined == null)
        applicantPicker.load("#holder_with_representative", holdingUID, null, {});
    else
    {
        applicantPicker.load("#holder_with_representative", holdingUID, split_combined.data.applicants, {});
    }

    //setup parcel sharing table

    if (split_combined == null)
        parcelSplitTable.load('#holder_parcel', holdingUID);
    else
    {
        parcelSplitTable.load('#holder_parcel', holdingUID, split_combined.data.partitions);
    }

    //load required documents
    if (split_combined)
    {
        documentPicker.setRequiredDocument(0, split_combined.data.landHoldingCertifcateDocument);
    }
    split_holdingID = holdingUID;
}

function split_validateApplicationPage()
{
    return transaction_validateApplication() && validatePageOne();
}
function split_validateApplicantsPage() {
    return applicantPicker.validate(split_holders);
}

function split_validReqDocument() {
    var valid = true;
    $("#uploadLandCertificate").remove();
    if (!documentPicker.getRequiredDocument(0)) {
        $(".error-req-doc-panel").append("<span id='uploadLandCertificate'><i class='fa fa-warning faa-flash animated'></i>"+lang("Please Add Land Holding Certificate")+"<br/></span>");
        valid = false;
    } else {
        $("#uploadLandCertificate").remove();
    }
    return valid;
}



function split_initSplit() {
    //setup wizard
    wizardPages.load([
        {tab: "#step-1-tab", cont: "#step-1", enable: true},
        {tab: "#step-2-tab", cont: "#step-2", enable: true},
        {tab: "#step-3-tab", cont: "#step-3", enable: true},
        {tab: "#step-4-tab", cont: "#step-4", enable: true},
        {tab: "#step-5-tab", cont: "#step-5", enable: true},
    ]);
    wizardPages.beforePageChange = function (show, hide)
    {
        var ret = true;
        if (hide == "#step-1-tab")
        {
            ret = split_validateApplicationPage();
        } else if (hide == "#step-2-tab")
        {
            ret = split_validateApplicantsPage();
        } else if (hide == "#step-3-tab")
        {
            ret = true;//NOTE: no validation for parcel page?
        } else if (hide == "#step-4-tab")
        {
            ret = split_validReqDocument();
        }
        if (show == "#step-5-tab")
        {
            split_showPreview();
        }
        return ret;
    }
    $("#app_type").val(lang("Split Transcation"));
    app_holdingSearch.onHoldingPicked = split_addSplitHolding;

    //setup required document picker
    documentPicker.load("#documentTableBody");

    //load existing data
    if (split_holdingID != null)
    {
        split_addSplitHolding(split_holdingID);
    }

    //override save initial transaction
    transaction_saveInitial = function ()
    {
        if (!split_holding_data)
        {
            bootbox.alert(lang('You must at least select holding to save Split transaction'));
            return;
        }
        split_buildTransactionObject();
        transaction_saveTransaction(split_combined, true, function (tranUID) {
            bootbox.alert({
                message: lang("You Have Saved a Transaction Data Temporarily")+"!",
                callback: function () {
                    window.location.href = "/dashboard.jsp";
                }
            });
        }, function (err) {
            bootbox.alert(lang("Saving Transaction Data Failed")+".\n" + err);
        });
    }
}






