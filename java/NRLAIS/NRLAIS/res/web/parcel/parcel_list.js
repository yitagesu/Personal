/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var parcelListPicker =
        {
            instances: [],
            settings: null,
            addButtonClicked: function (container, existing)
            {
                this.instances[container].addButtonClicked(existing);
            },
            removeItem: function (container, tag)
            {
                this.instances[container].removeItem(tag);
            },
            editItem: function (container, tag)
            {
                this.instances[container].editItem(tag);
            },
            splitItem: function (container, tag)
            {
                this.instances[container].splitItem(tag);
            },
            showPopover: function (container, tag) {
                this.instances[container].showPopover(tag);
            },
            
            selectionChanged: function (container, tag) {
                this.instances[container].selectionChanged(tag);
            }
            ,showOnMap:function(container,parcelUID)
            {
                this.instances[container].showOnMap(parcelUID);
            }
            
        };

function getParcelListPicker(setting)
{
    if (parcelListPicker.instances[setting.containerEl])
        return parcelListPicker.instances[setting.containerEl];
    if (setting.allowExisting)
        $("#" + setting.addExistingButtonEl).attr("onclick", "parcelListPicker.addButtonClicked('" + setting.containerEl + "',true)");
    if (setting.allowNew)
        $("#" + setting.addNewButtonEl).attr("onclick", "parcelListPicker.addButtonClicked('" + setting.containerEl + "',false)");
    var row_template = "";

    var pickerID = setting.containerEl + '_parcel_picker';
    var pacel_picker = holdingBrowser(pickerID, true, {pickHolding: false, pickParcel: true, pickParty: false});
    pacel_picker.onParcelPicked = function (parcelUID)
    {
        $.ajax({url: '/api/get_parcel?parcel_uid=' + parcelUID, dataType: 'json', success: function (data) {
                state.addParcelRow({parcel: data.res});
            }});
    };
    var state =
            {
                ready: false,
                parcelEditor: null,
                setting: setting,
                parcels: [],
                parcels_tag: [],
                tag_counter: 0,
                edit_row_index: -1,
                onChanged: null,
                beforeSplit: null,
                beforeSave: null,
                onSelectionChanged:null,
                beforeDelete:null,
                callOnChanged: function ()
                {
                    if (this.onChanged)
                        this.onChanged();
                },
                clearSelection:function()
                {
                    $("#" + setting.containerEl+" tbody input[type=checkbox]").removeAttr("checked");
                },
                selectionChanged:function()
                {
                    if(this.onSelectionChanged)
                        this.onSelectionChanged();
                },
                
                addParcelRow: function (parcel)
                {
                    this.tag_counter++;
                    this.parcels_tag.push(this.tag_counter);
                    var html = ejs.render(row_template, {locals: {p: parcel, setting: setting, tag: this.tag_counter}});
                    $("#" + setting.containerEl + "_parcel_list").append(html);
                },
                setParcelRow: function (index, parcel)
                {
                    var html = ejs.render(row_template, {locals: {p: parcel, setting: setting, tag: this.parcels_tag[index]}});
                    $("#" + setting.containerEl + "_parcel_list").children().eq(index).replaceWith(html);
                },
                doDelete:function(i)
                {
                     $("#" + setting.containerEl + "_parcel_list").children().eq(i).remove();
                    this.parcels.splice(i,1);
                    this.parcels_tag.splice(i,1);
                    this.callOnChanged();
                },
                removeItem: function (tag)
                {
                    var i = this.parcels_tag.indexOf(parseInt(tag));
                    if(this.beforeDelete!=null)
                    {
                        var that=this;
                        this.beforeDelete(this.parcels[i],function(success)
                        {
                            if(success)
                                that.doDelete(i);
                        });
                    }
                    else
                        this.doDelete(i);                    
                   
                },
                editItem: function (tag)
                {
                    this.edit_row_mode = true;
                    var i = this.parcels_tag.indexOf(parseInt(tag));
                    if (i == -1)
                    {
                        bootbox.alert('Element not found');
                        return;
                    }
                    this.edit_row_index = i;
                    this.parcelEditor.showModal(this.parcels[i]);
                },
                splitItem: function (tag) {

                },
                doSplit: function (qr, p)
                {
                    
                    var n = parseInt(qr.val());
                    qr.val("Procesing...");
                    qr.prop('disabled', true);
                    if (n > 1)
                    {
                        if (this.beforeSplit)
                        {
                            busyClue.show('Spliting parcel..');
                            this.beforeSplit(p, n, function(success){
                                busyClue.hide();
                                if (success)
                                    qr.hide();
                                qr.prop('disabled', false);
                            })
                        } else
                        {
                            qr.prop('disabled', false);
                        }
                    }
                },
                showPopover: function (tag) {
                    var i = this.parcels_tag.indexOf(parseInt(tag));
                    var qr = $("#" + setting.containerEl + " tbody tr[parcel_uid=" + this.parcels[i].parcel.parcelUID + "] input[type=text]");
                    if (qr.css("display") != "none")
                    {

                        this.doSplit(qr, this.parcels[i]);

                    } else
                    {
                        var that = this;
                        qr.show();
                        qr.focus();
                        qr.keydown(
                                function (event) {
                                    if (event.keyCode == 13)
                                    {
                                        that.doSplit(qr, that.parcels[i]);
                                    }
                                }
                        );
                        qr.focusout(function(){
                            qr.hide();
                        });
                    }
                },
                onParcelSave: function (parcel)
                {
                    if (this.edit_row_index > -1)
                    {
                        this.setParcelRow(this.edit_row_index, parcel);
                        this.parcels[this.edit_row_index] = parcel;
                    } else
                    {
                        this.addParcelRow(parcel);
                        this.parcels.push(parcel);
                    }
                    this.callOnChanged();
                },
                populate: function ()
                {
                    $('#'+this.setting.containerEl+" tbody tr").remove();
                    for (var i = 0; i < this.parcels.length; i++)
                        this.addParcelRow(this.parcels[i]);
                },
                setParcelItems: function (items)
                {
                    this.parcels_tag=[];
                    this.parcels = items;
                    if (this.ready)
                        this.populate();
                },
                loadParcelPicker: function (done)
                {
                    this.parcelEditor = getParcelPicker({containerEl: this.setting.containerEl + '_new_parcel_picker'});
                    var b = this.onParcelSave.bind(this);
                    this.parcelEditor.onParcelSave = b;
                    this.parcelEditor.beforeSave = this.beforeSave;

                },
                addButtonClicked: function (existing)
                {
                    if (existing)
                    {
                        $("#" + pickerID).modal('show');
                    } else
                    {
                        this.parcelEditor.showModal();
                    }
                },
                tr: function (parcelUID)
                {
                    return $("#" + setting.containerEl + " tr[parcel_uid=" + parcelUID + "]");
                },
                getSelected: function ()
                {
                    var ret = [];
                    var that=this;
                    var i=0;
                    $("#" + setting.containerEl+" tbody [parcel_uid]").each(function()
                    {
                        if($(this).find("input[type=checkbox]").is(":checked"))
                        {
                            ret.push(that.parcels[i]);
                        }
                        i++;
                    });
                    return ret;

                },
                showOnMap:function(parcelUID)
                {
                    this.setting.showOnMap(parcelUID);
                },
                load: function ()
                {

                    var that = this;
                    $.ajax({
                        url: '/parcel/parcel_list.jsp?setting=' + encodeURIComponent(JSON.stringify(setting)),
                        success: function (html)
                        {
                            $("#" + setting.containerEl).html(html);                            
                            $.ajax({
                                url: '/parcel/parcel_list_row.ejs',
                                success: function (html)
                                {
                                    row_template = html;
                                    that.ready = true;
                                    that.populate();
                                    that.loadParcelPicker();
                                    $('[data-toggle="popover"]').popover();
                                    
                                }
                            });
                        }
                    });
                }
            };
    state.load();
    parcelListPicker.instances[setting.containerEl] = state;
    return state;
}
