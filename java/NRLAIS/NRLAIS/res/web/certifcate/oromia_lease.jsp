<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<%@page import="org.massreg.dao.*"%>
<%@page import="org.massreg.dto.*"%>
<%@page import="java.util.ArrayList"%>
<%@page import="org.massreg.util.*"%>
<%@page import="org.massreg.entity.*"%>
<%@page import="org.massreg.api.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% 
    ParcelRegistrationData data=Repository2.getParcel(request.getParameter("upi"));
    String wordea=MasterRepository.getInstance().getWoredaName(data.parcel.upi.substring(0,6),"amharic");
    String zone=MasterRepository.getInstance().getZoneName(data.parcel.upi.substring(0,6),"amharic");
    String region=MasterRepository.getInstance().getRegionName(data.parcel.upi.substring(0,6),"amharic");
    String kebeles=MasterRepository.getInstance().getKebeleName(data.parcel.upi.substring(0,9), "amharic");
    double area=Repository2.getArea(data.parcel.upi);
    MapperEngineModel.MEPoint center=Repository2.getCentroid(data.parcel.upi);
    String centerX;
    String centerY;
    
    if(center==null){
        centerX="_";
        centerY="_";
    }
    else{
        centerX=Double.toString(center.x);
        centerY=Double.toString(center.y);
    }
    ParcelRegistrationData.ParcelStage[] stages2=Repository2.getParcelStages(request.getParameter("upi"));
    String approval="-";
    String checker="-";
    String issuedDate="-";
    for(ParcelRegistrationData.ParcelStage stage:stages2)
    {
        if(stage.stage==ParcelRegistrationData.Parcel.STAGE_DATA_CHECKED)
        {
            checker=stage.audit.userName;
        }
        if(stage.stage==ParcelRegistrationData.Parcel.STAGE_DATA_APPROVED)
        {
            approval=stage.audit.userName;
        }
        if(stage.stage==ParcelRegistrationData.Parcel.STAGE_DATA_CERTIFICATE_ISSUED)
        {
            
            //issuedDate=new Date(stage.audit.time).toString();
        }
    }
    User checkeruser=MasterRepository.getInstance().getUser(checker);
    User approvaluser=MasterRepository.getInstance().getUser(approval);
    MASSREGLookups lu=new MASSREGLookups(DBUtil.getConnection());
    if(data!=null)
    {
        if(data.holding.person!=null)
        {
            data.holding.group=new ParcelRegistrationData.GroupMember[]{new ParcelRegistrationData.GroupMember()};
            data.holding.group[0].numerator=1;
            data.holding.group[0].denumerator=1;
            data.holding.group[0].member=data.holding.person;
            data.holding.person=null;
        }   
    }
%>
<html>
    <head>
        <title>Oromia Land Certificate</title>
        <meta charset="utf-8">
        <meta charset="windows-1252">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/external.css" rel="stylesheet"/>
        <link media="print" rel="Alternate" href="print.pdf">
    </head>
    <body id="amhara">
            <div class="certificate-body">
                <table>
                    <tr>
                        <td colspan="2" class="align-center">
                            <h2>WARAQAA RAGAA QABIYYEE LAFA BAADIYYAA</h2>
                            <h2>KAARTAA QABIYYEE LAFAA LAKKOOFSA ADDAA 04/_/_/_/_/_</h2>
                        </td>
                        <td>
                            <p>Lakk: _____________</p>
                            <p>Guyyaa: ___________</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Naanoo Oromiyaa Godina: __________</label><br/>
                            <label>Aanaa:_________ Ganda:____________</label><br/>
                            <label>Maqaa Abbaa Qabiyyee/ootaa</label><br/>
                            <label>1. _______________________________</label><br/>
                            <label>2. _______________________________</label><br/>
                            <label>Bali’ina/hek/</label>
                        </td>
                        <td class="align-center">
                            <img src="image/oromia_logo.gif"/>
                        </td>
                        <td class="align-center">
                            <img src="<%=request.getContextPath()%>/assets/images/sample_1.png" width="100"/>
                            <h4>Foyyessuu/edition/________________</h4>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 30%">
                            <div class="align-center">
                                <img src="image/ordirection.png"/>
                            </div>
                            <p><b>Ibsa </b></p>
                            <table >
                                                
                                                <tr>
                                                    <td><img src="image/border-parcel.png"/></td>
                                                    <td>Daangesitoota </td>
                                                </tr>
                                                <tr>
                                                    <td><img src="image/parcel.png"/></td>
                                                    <td>Lafa W/Ragaa itti kenname</td>
                                                </tr>
                                            </table>
                            <label>Iskeelii 1:_________</label>
                        </td>
                        <td style="width: 40%">
                            <div class="map">
                                <img src="/cert?width=1000&height=1000&upi=<%=data.parcel.upi%>"/>
                            </div>
                        </td>
                        <td style="width: 30%" class="cert-desc">
                            <div class="holding-description">
                                <h4 class="align-center">Hubachiisa</h4>
                                <ul>
                                    <li>
                                       Labsii bulchiinsa fi itti fayyadam lafa baadiyyaa naannoo oromiyaa lakk. 130/1999 raawwachisuuf aangoo BLBENO tiif kennameen waraqaa raga qabiyyee lafaa qopha’ee kennameera. 
                                    </li>
                                    <li>Abbaan qabiyyee lafaa kamiyyuu akkaataa labsii fi dabii bulchiinsa fi itti fayyadama lafa baddiyaa naannoo oromiyaa baheen dirqama irra eegamuu bahu yoo dhabee/de mirga itti fayyadama qabiyyee lafaa dhabuu ni danda’a/dandeesi.</li>
                                    <li>Qabiyee lafaa haalaan itti fayyadamuu dhabuun laf ofi fi olloota irratti badii geessisuun seeraan gaagachisa.</li>
                                    <li>Abbaan qabiyyee laffa mirga itti fayyadama lafaa akka jalaa hafu kan itti murtaa’e kamiyyuu waraqaa ragaa qabiyyee lafaa WLBEN annaaf deebissuf ni dirqama.</li>
                                    <li>Abbaa warraa fi haadha warraa waliin waraqaa raga argatan mirga itti fayyadama laffa qiixa qabu.</li>
                                    <li>Namnii mirga itti fayyadamaa qabu akkaataa seeraatiin mirga itti fayyadamaa dabarsuu ni danda’a/dandessi.</li>
                                    <li>Qabiyyee lafaa yoo dabarfamu waraqaan ragaa kan foyya’u ta’a </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Kan Qopheesse:</label><br/>
                            <label>Maqaa:_____________</label><br/>
                            <label>Mallattoo:____________</label><br/>
                            <label>Guyyaa:______________</label><br/>
                        </td>
                        <td>
                            <label>Kan Mirkaneesse:</label><br/>
                            <label>Maqaa:_____________</label><br/>
                            <label>Mallattoo:____________</label><br/>
                            <label>Guyyaa:______________</label><br/>
                        </td>
                        <td>
                            <label>Kan Raggaasise:</label><br/>
                            <label>Maqaa:_____________</label><br/>
                            <label>Mallattoo:____________</label><br/>
                            <label>Guyyaa:______________</label><br/>
                        </td>
                    </tr>
                </table>
            </div>
    </body>
    <script>
    </script>
</html>
