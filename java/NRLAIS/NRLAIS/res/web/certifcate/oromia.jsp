<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<%@page import="com.intaps.nrlais.repo.*"%>
<%@page import="com.intaps.nrlais.*"%>
<%@page import="com.intaps.nrlais.controller.*"%>
<%@page import="com.intaps.nrlais.controller.tran.*"%>
<%@page import="com.intaps.nrlais.model.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% 
    CertficateViewController controller=new CertficateViewController(request,response);
    int numHolder = 0;
%>
<html>
    <head>
        <title>Oromia Land Certificate</title>
        <meta charset="utf-8">
        <meta charset="windows-1252">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/external.css" rel="stylesheet"/>
        <link media="print" rel="Alternate" href="print.pdf">
    </head>
    <body id="amhara">
            <div class="certificate-body">
                <table>
                    <tr>
                        <td colspan="2" class="align-center">
                            <h2>WARAQAA RAGAA QABIYYEE LAFA BAADIYYAA</h2>
                            <h2>KAARTAA QABIYYEE LAFAA LAKKOOFSA ADDAA 04/_/_/_/_/_</h2>
                        </td>
                        <td>
                            <p>Lakk: _____________</p>
                            <p>Guyyaa: ___________</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Naanoo Oromiyaa Godina: </label><ins><%=controller.zone%></ins><br/>
                            <label>Aanaa: </label><ins><%=controller.woreda%></ins>____<label>Ganda: </label><ins><%=controller.kebele%></ins><br/>
                            <label>Maqaa Abbaa Qabiyyee/ootaa</label><br/>
                             <% for(String name:controller.nameList()){numHolder++;%>
                             <%=numHolder%>. <ins><%=name%></ins><br/>
                                        <%}%>
                                        <label>Bali’ina/hek/: </label><ins><%=((double)Math.round(controller.area))/10000%></ins><br/>
                        </td>
                        <td class="align-center">
                            <img src="image/oromia_logo.gif"/>
                        </td>
                        <td class="align-center">
                            <img src="image/sample_1.png" width="100"/>
                            <h4>Foyyessuu/edition/________________</h4>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 30%">
                            <div class="align-center">
                                <img src="image/ordirection.png"/>
                            </div>
                            <p><b>Ibsa </b></p>
                            <table >
                                                
                                                <tr>
                                                    <td><img src="image/border-parcel.png"/></td>
                                                    <td>Daangesitoota </td>
                                                </tr>
                                                <tr>
                                                    <td><img src="image/parcel.png"/></td>
                                                    <td>Lafa W/Ragaa itti kenname</td>
                                                </tr>
                                            </table>
                            <label>Iskeelii 1:_________</label>
                        </td>
                        <td style="width: 40%">
                            <div class="map">
                                 <img src="/api/map/cert?width=1000&height=1000&tran_uid=<%=controller.tran.transactionUID%>&parcel_uid=<%=controller.parcel.parcelUID%>"/>
                            </div>
                        </td>
                        <td style="width: 30%" class="cert-desc">
                            <div class="holding-description">
                                <h4 class="align-center">Hubachiisa</h4>
                                <ul>
                                    <li>
                                       Labsii bulchiinsa fi itti fayyadam lafa baadiyyaa naannoo oromiyaa lakk. 130/1999 raawwachisuuf aangoo BLBENO tiif kennameen waraqaa raga qabiyyee lafaa qopha’ee kennameera. 
                                    </li>
                                    <li>Abbaan qabiyyee lafaa kamiyyuu akkaataa labsii fi dabii bulchiinsa fi itti fayyadama lafa baddiyaa naannoo oromiyaa baheen dirqama irra eegamuu bahu yoo dhabee/de mirga itti fayyadama qabiyyee lafaa dhabuu ni danda’a/dandeesi.</li>
                                    <li>Qabiyee lafaa haalaan itti fayyadamuu dhabuun laf ofi fi olloota irratti badii geessisuun seeraan gaagachisa.</li>
                                    <li>Abbaan qabiyyee laffa mirga itti fayyadama lafaa akka jalaa hafu kan itti murtaa’e kamiyyuu waraqaa ragaa qabiyyee lafaa WLBEN annaaf deebissuf ni dirqama.</li>
                                    <li>Abbaa warraa fi haadha warraa waliin waraqaa raga argatan mirga itti fayyadama laffa qiixa qabu.</li>
                                    <li>Namnii mirga itti fayyadamaa qabu akkaataa seeraatiin mirga itti fayyadamaa dabarsuu ni danda’a/dandessi.</li>
                                    <li>Qabiyyee lafaa yoo dabarfamu waraqaan ragaa kan foyya’u ta’a </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Kan Qopheesse:</label><br/>
                            <label>Maqaa: </label><ins><%=controller.intiator%></ins><br/>
                            <label>Mallattoo:_____________</label><br/>
                            <label>Guyyaa: </label><ins><%=controller.intiatedDate%></ins><br/>
                        </td>
                        <td>
                            <label>Kan Mirkaneesse:</label><br/>
                            <label>Maqaa: </label><ins><%=controller.checker%></ins><br/>
                            <label>Mallattoo:______________</label><br/>
                            <label>Guyyaa: </label><ins><%=controller.checkeDate%></ins><br/>
                        </td>
                        <td>
                            <label>Kan Raggaasise:</label><br/>
                            <label>Maqaa: </label><ins><%=controller.approval%></ins><br/>
                            <label>Mallattoo:_______________</label><br/>
                            <label>Guyyaa: </label><ins><%=controller.approvedDate%></ins><br/>
                        </td>
                    </tr>
                </table>
            </div>
    </body>
    <script>
    </script>
</html>
