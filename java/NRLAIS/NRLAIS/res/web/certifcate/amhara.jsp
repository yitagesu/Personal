<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<%@page import="com.intaps.nrlais.repo.*"%>
<%@page import="com.intaps.nrlais.*"%>
<%@page import="com.intaps.nrlais.controller.*"%>
<%@page import="com.intaps.nrlais.controller.tran.*"%>
<%@page import="com.intaps.nrlais.model.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% 
    CertficateViewController controller=new CertficateViewController(request,response);
%>
        
<html>
    <head>
        <title>Amhara Land Certificate</title>
        <meta charset="utf-8">
        <meta charset="windows-1252">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/external.css" rel="stylesheet"/>
        <link media="print" rel="Alternate" href="print.pdf">
    </head>
    <body id="amhara">
        <div class="certificate-body">
            <table style="width:100%">
                <tr>
                    <td style="width:60%">
                        <div class="map">
                            <img src="/api/map/cert?width=1000&height=1000&tran_uid=<%=controller.tran.transactionUID%>&parcel_uid=<%=controller.parcel.parcelUID%>"/>
                        </div>
                    </td>
                    <td class="cert-desc">
                        <div class="holding-description">
                            <table class="table-title">
                                <tr>
                                    <td colspan="3" class="align-right"><h1>የማሳ ካርታ</h1></td>
                                </tr>
                                <tr>
                                    <td>
                                        <h3>መግለጫ</h3>
                                        <table class="legend-table">
                                            <tr>
                                                <td><img src="image/river.png"/></td>
                                                <td>ወንዝ</td>
                                            </tr>
                                            <tr>
                                                <td><img src="image/rode.png"/></td>
                                                <td>መንገድ</td>
                                            </tr>
                                            <tr>
                                                <td><img src="image/kebele-border.png"/></td>
                                                <td>የቀበሌ ድንበር</td>
                                            </tr>
                                            <tr>
                                                <td><img src="image/border-parcel.png"/></td>
                                                <td>አዋሳኝ ማሳ</td>
                                            </tr>
                                            <tr>
                                                <td><img src="image/parcel.png"/></td>
                                                <td>የባለይዞታው ማሳ</td>
                                            </tr>
                                        </table></td>
                                    <td><img src="image/dirction.png"/></td>
                                    <td><img src="image/logo.png"/></td>
                                </tr>
                            </table>
                            <table class="holder-info">
                                <tr>
                                    <th>
                                        የባለይዞታው/ዋ/ዎች ስም：
                                    </th>
                                </tr>

                                <tr>
                                    <td>
                                        <% for(String name:controller.nameList()){%>
                                            <%=name%><br/>
                                        <%}%>
                                    </td>
                                </tr>

                            </table>
                            <table class="holder-parcel">
                                <tr>
                                    <th colspan="2" class="align-center">
                                        <h3>የማሳው አድራሻ</h3>
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        <b>ክልል:</b> <%=controller.region%>
                                        

                                    </td>
                                    <td>
                                        <b>ዞን :</b> <%=controller.zone%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>ወራዳ :</b> <%=controller.woreda%>
                                    </td>
                                    <td>                                        
                                        <b>ቀበሌ :</b> <%=controller.kebele%>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <b>የማሳ መለያ ቁጥር :</b> <%=controller.parcel.upid%>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <b>የማሳው ስፋት (ሄ/ር) :</b> <%=((double)Math.round(controller.area))/10000%>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <b>ማዕከላዊ የምስራቅ ኮርዲኔት :</b> <%=controller.centerX%>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <b>ማዕከላዊ የሰሜን ኮርዲኔት :</b> <%=controller.centerY%>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <b>የመሬት አጠቃቀም :</b> <%=controller.currentLandUse%>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <b>የለምነት ደረጃ :</b> <%=controller.soilFertility%>
                                    </td>
                                </tr>
                            </table>
                            <table class="parcel-confirm">
                                <tr>
                                    <td colspan="2">
                                        <b>የተሰጠበት ቀን：</b> <span id="issueDate"></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <b>ያረጋገጠው ባለሙያ ስም:</b><%=controller.checker%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>                                        
                                        <b>ፊርማ：</b>________________________
                                    </td>
                                    <td><b>ቀን：</b> <span id="checkDate"><%=controller.checkeDate%></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <b>ያጸደቀው ባለሙያ ስም:</b><%=controller.approval%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>ፊርማ :</b>________________________
                                    </td>
                                    <td><b>ቀን :</b> <span id="approveDate"><%=controller.approvedDate%></span>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </body>
    <script>
    </script>
</html>
