<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<%@page import="com.intaps.nrlais.repo.*"%>
<%@page import="com.intaps.nrlais.*"%>
<%@page import="com.intaps.nrlais.controller.*"%>
<%@page import="com.intaps.nrlais.controller.tran.*"%>
<%@page import="com.intaps.nrlais.model.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% 
    CertficateViewController controller=new CertficateViewController(request,response);
%>
<html>
    <head>
        <title>Tigray Land Certificate</title>
        <meta charset="utf-8">
        <meta charset="windows-1252">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/external.css" rel="stylesheet">
        <link media="print" rel="Alternate" href="print.pdf">
    </head>
    <body id="tigray">
        <div class="certificate-body">
            <table>
                <tr>
                    <td style="width: 55%">
                        <div class="map-body">
                            <div class="certificate-header ">
                                <table>
                                    <tr>
                                        <td><p class="title">ኤጀንሲ ሓለዋ ስነከባብን ኣጠቃቍማን ምምሕዳርን ገፀር መሬት ክልል ትግራይ</p></td>
                                        <td><img src="image/tigray-logo.png"/></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="align-center title"><h3><u>ናይ ገፀር ትሕዝቶ  መሬት ጠቋሚ ካርታ</u></h3></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"><img src="image/tigray-direction.png"/></td>
                                    </tr>
                                </table>
                                
                                
                                
                            </div>
                            <div class="map">
                                 <img src="/api/map/cert?width=1000&height=1000&tran_uid=<%=controller.tran.transactionUID%>&parcel_uid=<%=controller.parcel.parcelUID%>"/>
                            </div>
                        </div></td>
                    <td style="width: 45%" class="cert-desc"><div class="holding-description">
                <table class="holder-parcel">
                    <tr>
                        <th colspan="2">
                            <u class="title">1. መግለፂ ትሕዝቶ መሬት <br/>ኣብ ናይ ትሕዝቶ  መሬት መቋሚ ካርታ ተጠቊሱ ዘሎ ትሕዝቶ መሬት ዝስዕቡ መለለይታት ይህልዉዎ።</u>
                        </th>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <p><u></u></p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <%if(controller.region!=null){%>
                            <b>ክልል :</b> <label> <%=controller.region%>  </label> <label class="empty">__________</label>
                                            <%} else {%>
                                            <b>ክልል :</b> <label class="empty">_____________________</label>
                                            <%}%>
                        </td>
                        <td>
                            <%if(controller.zone!=null){%>
                                            <b>ዞባ :</b> <label><%=controller.zone%> </label> <label class="empty">__________</label>
                                            <%} else {%>
                                            <b>ዞባ :</b> <label class="empty">_____________________</label>
                                            <%}%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <%if(controller.woreda!=null){%>
                                            <b>ወራዳ :</b> <label> <%=controller.woreda%> </label> <label class="empty">__________</label>
                                            <%} else {%>
                                            <b>ወራዳ :</b> <label class="empty">_____________________</label>
                                            <%}%>
                        </td>
                        <td>
                             <%if(controller.kebele!=null){%>
                                            <b>ጣብያ :</b> <label> <%=controller.kebele%> </label> <label class="empty">__________</label>
                                            <%} else {%>
                                            <b>ጣብያ :</b><label class="empty">_____________________</label> 
                                            <%}%>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <%
                                if(false){%>
                                            <b>ቁሸት :</b> <label>  </label> <label class="empty">__________</label>
                                            <%} else {%>
                                            <b>ቁሸት :</b><label class="empty">_____________________</label>
                                            <%}%>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <%if(controller.parcel.upid!=null){%>
                                            <b>ቁፅሪ መለለይ ትሕዝቶ መሬት :</b> <label> <%=controller.parcel.upid%> </label> <label class="empty">__________</label>
                                            <%} else {%>
                                            <b>ቁፅሪ መለለይ ትሕዝቶ መሬት :</b> <label class="empty">_____________________</label>
                                            <%}%>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <%if(controller.holding.holdingType!=-1){%>
                            <b>ዓይነት ትሕዝቶ :</b> <label><%=controller.holding.holdingType%></label><label class="empty">__________</label>
                                            <%} else {%>
                                            <b>ዓይነት ትሕዝቶ :</b><label class="empty">_____________________</label>
                                            <%}%>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                                        <b>የመሬት አጠቃቀም :</b> <%=controller.currentLandUse%>
                                    </td>
                    </tr>
                    <tr>
                          <td colspan="2">
                                        <b>የማሳው ስፋት (ሄ/ር) :</b> <%=((double)Math.round(controller.area))/10000%>
                           </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="holder-name">
                            <b>ሽም በዓል/ሰብ/ ትሕዝቶ：</b>

                             <% for(String name:controller.nameList()){%>
                                            <%=name%><br/>
                                        <%}%>
                            
                        </td>
                    </tr>
                </table>
                <table class="holder-parcel2">
                    
                    <tr>
                        <td>
                            <table class="legend-table">
                                <tr>
                                    <td colspan="2"><u>መፍትሕ</u></td>
                                </tr>
                                <tr>
                                    <td><img src="image/parcel.png"/></td>
                                    <td>ግራት በዓል/ሰብ/ ትሕዝቶ</td>
                                </tr>
                                <tr>
                                    <td><img src="image/border-parcel.png"/></td>
                                    <td>መዳበውቲ ግራት</td>
                                </tr>
                                
                            </table>
                        </td>
                    </tr>
                </table>

                <table class="parcel-confirm">
                    <tr>
                        <td colspan="2">
                            <b>ዝተውሃበሉ ዕለት :</b><label><span id="issueDate"><%=controller.issuedDate%></span></label><label class="empty">__________</label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <%if(controller.checker!=null){%>
                            <b>ዘረጋገፀ ሸም:</b><label><%=controller.checker%></label><label class="empty">__________</label>
                                            <%} else {%>
                                            <b>ዘረጋገፀ ሸም :</b><label class="empty">_____________________</label>
                                            <%}%>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <%if(false){%>
                            <b>ሓላፍነት :</b><label></label><label class="empty">__________</label>
                                            <%} else {%>
                                            <b>ዘሓላፍነት :</b><label class="empty">_____________________</label>
                                            <%}%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                                            <b>ፈርማ :</b><label class="empty">_____________________</label>
                          
                        </td>
                        <td>
                            <b>ዕለት:</b> <label><span id="checkDate"><%=controller.checkeDate%></span></label><label class="empty">__________</label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <%if(false){%>
                            <b>ዘፅደቀ ሸም:</b><label></label><label class="empty">__________</label>
                                             <%} else {%>
                                              <b>ዘፅደቀ ሸም:</b><label class="empty">_____________________</label> 
                                            <%}%>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <%if(controller.approval != null){%>
                            <b>ሓላፍነት :</b><label><%=controller.approval%></label><label class="empty">__________</label>
                                            <%} else {%>
                                            <b>ዘሓላፍነት :</b><label class="empty">_____________________</label>
                                            <%}%>
                        </td>
                    </tr>
                    <tr>
                        <td>

                                            <b>ፈርማ：</b><label class="empty">_____________________</label>
                                          
                        </td>
                        <td><b>ዕለት:</b> <label> <span id="approveDate"><%=controller.approvedDate%></span></label><label class="empty">__________</label>
                        </td>
                    </tr>
                </table>
            </div></td>
                </tr>
            </table>
            
        </div>
        <div class="break"></div>
        <div class="certificate-back">
            <table>
                <tr>
                    <td style="width:50%; vertical-align: top">
                        
                       <div class="section-1">
                           <p>2. <u>መግለፂ በዓል/ሰብ/ ትሕዝቶ </u></p>
                        <table>
                            <tr>
                                <th>
                                    ሽም በዓል ሰብ ትሕዝቶ   
                                </th>
                                <th class="align-center">ፌርማ</th>
                            </tr>

                            <% for(String name:controller.nameList()){%>
                                            <%=name%><br/>
                                        <%}%>
                            
                            
                        </table>
                        <p>እዚ ኣን ላዕሊ ተገሊፁ ዘሎ ትሕዝቶ መሬት ናይ ቶም ተጠቂሶም ዘለው በዓል/ሰብ/ትሕዝቶ ምኳኑ ሙሉእ ናይ ምጥቃም ግቡኡን ተረጋጊፁ ኣሎ።</p>
                        
                        <p>ሕጋዊ ማሕተም </p>
                        
                        <p><b><u>መተሓሳሰቢ</u></b></p>
                        <p>መሰልን ግቡኡን ባዓል/ሰብ/ትሕዝቶ መሬት </p>
                        <p><b>ሀ. <u>ውልቀ ገባር/ራት </u></b></p>
                        <p>    ኣብ ኣዋጅ ገጠር መሬት ቁፅሪ 239/2006 ከምኡውን ኣብ ደንቢ ቁፅሪ 85/2006 ዝተቀመጡ ግቡኣትን መሰላትን ከምዘለው ኮይኖም </p>
                        <ol>
                            <li>
                                <p>በዓል ትሕዝቶ  ገጠር መሬት ብዘይጊዜ ገደብ ናይ ምጥቃም ፣ መሬት ንዘይብሎም ብሕጊ ንዝተገቀደሎም ናይ ምውራስ  ንቤተሰብ ኣባላት ብውህብቶ ናይ ምትሕልላፍ፣ናይ ምክራይ ፣ ናይ ምልዋጥን ኣብ መሬት ንዘፍለይዎም ፍርያት ናይ ምጥቃም  መሰል አለዎ።</p>
                            </li>
                        </ol>

                    </div></td>
                    <td>
                          <div class="section-2">
                              <ol start="2">
                        <li>
                            <p>ብመሰረት ደንቢ 85/2006 ዓንቀፅ 20 ኣብ መሬት ንዘብፅሖ ጉድኣት ማለት ካብ ዝተፈቀደሉ ዕላማ ወፃኢ እቲ መሬት እንተተጠቂሙ ንኣብነት ሑፃ ምውፃእ እምኒ ምፍንቃል፣ ልዕሊ 2ተ ጊዜ እንተዘይኣፍርዩ ፣ መሬት ጉድኣት እናበፅሖ  ብሽለልተኝነት መስተኻኸሊ ስጉምቲ ከይወሰደ እንተተሪፋን ዘይግቡእ ኣጠቃቅማ  እንትህሊ ብመሬት ናይ ምጥቃም መሰሉ ክስእን ይኸእል እዩ።</p>
                        </li>
                        <li>
                            <p>ብመሰረት ደንቢ 85/2006 ዓንቀፅ 29 ቁፅሪ 4 በዓል ትሕዝቶ ኣብ መሬት ንፍርያት ዘራእቲ ዘይፃብኡ ተኽልታት ናይ ዕቀባ  ሓመድን ማይን ስራሕቲ ናይ ምስራሕ ግቡእ ኣለዎ።</p>
                        </li>
                        <li>
                            <p>ብመሬት ናይ ምጥቃም መሰል ብድልየቱ ዘቋረፀ ሰብ ቅድሚ ሓደ ወርሒ ንቁሸት ኮሚቴ ምም/ር መሬት ኣፍሊጡ ከረክብ ኣለዎ።</p>
                        </li>
                        <li>
                            <p>ብመሰረት ደንቢ ቁፅሪ 85/2006 ዓብቀፅ 47 ቁፅሪ 8<sup>ን</sup> 10<sup>ን</sup> ካብ ሕጊ ወፃኢ ዝተወሰደ ወይ ፀገም ዘለዎ  ትሕዝቶ መረጋረፂ ደፍተር ተመሊሱ  ተስተኻኺሉ ክወሃብ  ብምግባርን ናይ ትሕዝቶ ለውጢ እንትግበር እቲ ኣቀዲሙ ዝተውሃቦ  ሰርተፍኬት ናይ ምምላስ ግቡእ ኣለዎ።</p>
                        </li>
                    </ol>
                              <p><b>ለ. <u>ናይ ሓባር መሬት ብዝምልከት ዝህልው መሰልን ግቡእን</u></b></p>
                    <ol>
                        <li>
                            <p>ብመሰረት ኣዋጅ ገጠር መሬት ቁፅሪ 2 ዓንቀፅ 31 ኣመሓድራ ናይ ሕዛእቲ /ጋህፂ/ ቅድም ክብል እቲ ኸባቢ ይመሓደረሉ ዝነበረ ልማድ መሰረት ይኸውን ኣድላይ ኮይኑ እንትርከብ ናይቲ ከባቢ ሕ/ሰብ በቲ ጣብያ ቤ/ምኽሪ ኣቢሉ መተሓዳደሪ ስሪት ከውዕእን  ከተግብርን ይኽእል እዩ።</p>
                        </li>
                        <li>
                            <p>ናይ ሓባር መሬት ተባሂሉ ብሕጊ ተኽሊሉ ካብ ዝተውሃበ መሬት ወፃእ ደሪብካ ኣስፊሕካ ምሓዝን ምጥቃምን ኣይከኣልን።<br/> ብመሰረት ደንቢ ቁፅሪ 85/2006 ዓንቀፅ 29 ቁፅሪ 6 ዝኾነ ናይ ሓናር መሬት ተጠቃሚ ሰብ መሬት ንምክንኻን  ብዝወፀ ናይቲ ከባቢ ቤት ምኽሪ ወረዳ ወይ ጣብያ መምርሒ መሰረት ብፅሒቱ ክፍፅም ኣለዎ።</p>
                        </li>
                    </ol>
                </div>
                    </td>
                </tr>
            </table>
            
        </div>
                                             <div class="certificate-footer align-center">
                <label>መሬት ምክንኻንን ብዘላቅነት ምልማዕን  ናይ ባዓል ትሕዝቶ መሬት መሰልን ግቡእን እዩ</label>
            </div>
    </body>
</html>
