/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var parcelTransferTable = {
    el: null,
    settings: null,
    onChanged:null,
    callOnChanged:function()
    {
        if(this.onChanged)
            this.onChanged();
    },
    tr:function(parcelUID)
    {
        return $(this.el+" tr[parcel_uid="+parcelUID+"]");
    },
    enableShare:function(parcelUID,enable)
    {
        var q=this.tr(parcelUID).find('input[type=text]');
        q.css("color",enable?"black":"transparent");
        q.prop("disabled",!enable);
    },
    updateShareEditorStatus:function(parcelUID)
    {
        var isTansfered = this.isSelected(parcelUID);
        var isSplit=false;
        if(this.settings.split)
        {
            isSplit = this.isSplit(parcelUID);
        }
        if(this.settings.transferShare)
        {
            this.enableShare(parcelUID,isTansfered&&!isSplit);
        }
    },
    isSelected:function(parcelUID)
    {
        return this.tr(parcelUID).find("input[field='select']").is(":checked");
    },
    isSplit:function(parcelUID)
    {
        return this.tr(parcelUID).find("input[field='split']").is(":checked");
    },
    tansferCheckChanged:function(parcelUID)
    {
        var isTansfered = this.isSelected(parcelUID);
        this.updateShareEditorStatus(parcelUID);
        if(this.settings.singleParcel && isTansfered)
        {
            var that=this;
            $(this.el+" tbody tr").each(function()
            {
                var thisParcelUID=$(this).attr('parcel_uid');
                if(thisParcelUID!=parcelUID)
                {
                    $(this).find("input[field='select']").removeAttr("checked");
                    that.updateShareEditorStatus(thisParcelUID);
                }
            });
        }
        this.callOnChanged();
    },
    afterLoad:function()
    {
        var that=this;
        $(this.el+" tbody tr").each(function()
        {
            var parcelUID=$(this).attr('parcel_uid');
            that.updateShareEditorStatus(parcelUID);
        });
    },
    seqNo:function(parcelUID)
    {
      return this.tr(parcelUID).find("td").eq(1).text();  
    },
    load: function (containerElement, holdingUID, data, settings)
    {
        this.settings = settings;
        this.el = containerElement;
        var url = '/holding/parcel_transfer_table.jsp?holding_uid=' + holdingUID;
        if (settings)
            url = url + "&settings=" + encodeURIComponent(JSON.stringify(settings));
        var that=this;
        if (data)
        {
            $.ajax({
                url: url,
                method: 'POST',
                contentType: "application/json;charset=utf-8",
                data: JSON.stringify(data),
                success: function (data) {
                    $(containerElement).html(data);
                    that.afterLoad();
                }
            });
        } else
        {
            $.ajax({
                url: url,
                method: 'GET',
                success: function (data) {
                    $(containerElement).html(data);
                    that.afterLoad();
                }
            });
        }
    },
    getTransfers: function ()
    {
        var ret = [];
        var that=this;
        $(this.el+" tbody tr").each(function()
        {
            var parcelUID=$(this).attr("parcel_uid");
            var transfered=that.isSelected(parcelUID);
            if(!transfered)
                return;
            var oneTransfer = {};
            oneTransfer.parcelUID = parcelUID;
            
            oneTransfer.splitParcel = that.settings.split && that.isSplit(parcelUID);
            if(that.settings.transferArea)
                oneTransfer.area=parseFloat(that.tr(parcelUID).find("input[field=area]").val());
            else
                oneTransfer.area=0;
            if(that.settings.transferShare)
                oneTransfer.share = common_parseFraction(that.tr(parcelUID).find("input[field=share]").val());
            else
                oneTransfer.share=null;
            
            ret.push(oneTransfer);
        });
        return ret;
    }

};