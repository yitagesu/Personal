/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var parcelSplitTable={
    el:null,
    splitCheckChanged:function(parcelUID)
    {
        var isSplit = $("#sh_" + parcelUID + " #shSplit").is(":checked");
        if(isSplit)
            $("#sh_" + parcelUID+" input[type=text]").css("color","transparent");
        else
            $("#sh_" + parcelUID+" input[type=text]").css("color","black");
        $("#sh_" + parcelUID+" input[type=text]").prop("disabled",isSplit);
    },
    load:function(containerElement,holdingUID,data,setting)
    {
        this.el=containerElement;
        var url='/holding/parcel_split_table.jsp?holding_uid=' + holdingUID;
        if (setting)
            url = url + "&setting=" + encodeURIComponent(JSON.stringify(setting));

        if (data)
        {
            $.ajax({
                url: url,
                method: 'POST',
                contentType: "application/json",
                data: JSON.stringify(data),
                success: function (data) {
                    $(containerElement).html(data);
                }
            });
        } 
        else
        {
            $.ajax({
                url: url,
                method: 'GET',
                success: function (data) {
                    $(containerElement).html(data);
                }
            });
        }
    },
    getPartitions:function()
    {
        var ret = [];
        
        $(this.el+" tbody tr").each(function()
        {
            var onePartition = {};
            onePartition.parcelUID = $(this).attr("parcel_uid");
            onePartition.shares = [];
            var isSplit = $("#sh_" + onePartition.parcelUID + " #shSplit").is(":checked");
            $(this).find("input[type=text]").each(function()
            {
                var oneShare = {};
                oneShare.partyUID = $(this).attr("party_uid");
                oneShare.share = common_parseFraction($(this).val());
                oneShare.splitGeom = isSplit;
                onePartition.shares.push(oneShare);
            });
            if(onePartition.shares.length==1)
            {
                onePartition.shares[0].splitGeom=false;
                onePartition.shares[0].share={num:1,debum:1};
            }
            ret.push(onePartition);
        });
        
        return ret;
    }
            
};