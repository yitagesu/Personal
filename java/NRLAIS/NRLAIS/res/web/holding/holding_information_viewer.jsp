<%@page import="com.intaps.nrlais.repo.*"%>
<%@page import="com.intaps.nrlais.*"%>
<%@page import="com.intaps.nrlais.controller.*"%>
<%@page import="com.intaps.nrlais.model.*"%>
<%@page import="java.util.*"%>
<%@page pageEncoding="UTF-8" %>
<link rel="stylesheet" href="/assets/CSS/bootstrap.min.css"/>
<link rel="stylesheet" href="/assets/CSS/custom.css"/>
<%
    HoldingViewController controller=new HoldingViewController(request,response);
%>

<div class="row" id="view-content">
    <div class="col-md-12 bhoechie-tab-container">
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 bhoechie-tab-menu">
            <div class="list-group">

                <a href="#" class="list-group-item active">
                    <h4 class="fa fa-map"></h4> <%=controller.text("Parcel")%>
                </a>
                <a href="#" class="list-group-item">
                    <h4 class="fa fa-user"></h4> <%=controller.text("Holders and Tenants")%>
                </a>
                <a href="#" class="list-group-item">
                    <h4 class="fa fa-user"></h4> <%=controller.text("Restriction")%>
                </a>
                <a href="#" class="list-group-item">
                    <h4 class="fa fa-user"></h4> <%=controller.text("Servitude/Easement")%>
                </a>
                <a href="#" class="list-group-item">
                    <h4 class="fa fa-history"></h4> <%=controller.text("History")%>
                </a>
            </div>
        </div>
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 bhoechie-tab has-scroll">

            <div class="bhoechie-tab-content active">
                <%for(LADM.Parcel p:controller.holding.parcels){%>
                <table class="table table-striped">
                    <tr>
                        <td class="table_separetor" colspan="2"><label><%=controller.text("Parcel")%>:  </label> <%=p.seqNo%></td>
                    </tr>
                    <tr>
                       <td><label><%=controller.text("Region")%>: </label> <%=controller.holding.csaregionid%></td>
                        <td style="width:50%" rowspan="9">
                            <div id="map_<%=p.parcelUID%>" parcel_uid="<%=p.parcelUID%>" style="height:400px"> </div>
                        </td>
                    </tr>
                    <tr>
                        <td><label><%=controller.text("Woreda")%>: </label> <%=controller.holding.nrlais_woredaid%></td>
                    </tr>
                    <tr>
                        <td><label><%=controller.text("Kebele")%>: </label> <%=controller.holding.nrlais_kebeleid%></td>
                    </tr>
                    <tr>
                        <td><label><%=controller.text("Area")%>: </label> <%=p.areaGeom%> M<sup>2</sup></td>
                    </tr>
                    <tr>
                        <td><label><%=controller.text("Adjudication Id")%>: </label> <%=p.adjudicationID==null?"":p.adjudicationID%></td>
                    </tr>
                    <tr>
                        <td><label><%=controller.text("Adjudicated By")%>: </label> <%=p.adjudicatedBy%></td>
                    </tr>
                    <tr>
                        <td><label><%=controller.text("Land Use")%>: </label> <%=controller.lookupText("nrlais_sys.t_cl_landusetype",p.landUse)%></td>
                    </tr>
                    <tr>
                        <td><label><%=controller.text("Notes")%>: </label> <%=p.notes%></td>
                    </tr>
                </table>
                <%}%>
            </div>

            <div class="bhoechie-tab-content">
                <div class="panel panel-success">
                    <div class="panel-heading"><%=controller.text("Holder")%></div>
                    <div class="panel-body">
                        <%for(LADM.Right r:controller.holding.getHolders()){%>

                        <table class="table table-striped">
                            <% 
                        
                                if(r.party.partyType==1){%>

                            <tr>
                                <td class="table_separetor" colspan="2"><label><%=controller.text("Full Name")%>: </label> <%=r.party.getFullName()%></td>
                            </tr>
                            <tr>
                                <td><label><%=controller.text("Sex")%>: </label> <%=controller.sexText(r.party.sex)%></td>
                                <td><label><%=controller.text("is Orphan?")%>: </label> <%=controller.isOrphanText(r.party.isorphan)%></td>
                            </tr>
                            <tr>
                                <td id='holder_<%=r.party.partyUID%>'><label><%=controller.text("Age")%>: </label> <span id='dob'><%=controller.age(r.party.dateOfBirth)%></span></td>
                                <td><label><%=controller.text("Has Physical Impairment")%>: </label> <%=controller.disablityText(r.party.disability)%></td>
                            </tr>
                            <tr>
                                <td><label><%=controller.text("Martial Status")%>: </label> <%=controller.maritalStatusText(r.party.maritalstatus)%></td>
                                <td><label><%=controller.text("Contacts")%>: </label> 
                                    <ul>
                                        <li><%=controller.text("Phone")%>: </li>
                                        <li><%=controller.text("Address")%>: </li>
                                    </ul>
                                </td>
                            </tr>
                            <tr>
                                <td><label><%=controller.text("Family Role")%>: </label> <%=controller.roleText(r.party.mreg_familyrole)%></td>
                                <td><label><%=controller.text("ID/Passport")%>: </label> </td>
                            </tr>
                            <%}
                            
                            if(r.party.partyType == 6){%>
                            <tr>
                                <td class="table_separetor" colspan="2"><label><%=controller.text("Full Name")%>: </label> <%=r.party.getFullName()%></td>
                            </tr>
                            <%
                           
                                    List<LADM.GroupPartyMembership> mems = r.party.members;
                                    for(LADM.GroupPartyMembership gp:mems)
                                {

                            
                            %>
                            <tr>
                                <td  colspan="2">
                                    <ul>
                                        <li><label><%=controller.text("Member")%>: </label><%=gp.member.getFullName()%>
                                            <ul>
                                                <li><label><%=controller.text("Sex")%>: </label> <%=controller.sexText(gp.member.sex)%></li>
                                                <li><label><%=controller.text("is Orphan?")%>: </label> <%=controller.isOrphanText(gp.member.isorphan)%></li>
                                                <li><label><%=controller.text("Age")%>: </label> <span id='dob'><%=controller.age(gp.member.dateOfBirth)%></span></li>
                                                <li><label><%=controller.text("Has Physical Impairment")%>: </label> <%=controller.disablityText(gp.member.disability)%></li>
                                                <li><label><%=controller.text("Martial Status")%>: </label> <%=controller.maritalStatusText(gp.member.maritalstatus)%></li>
                                                <li><label><%=controller.text("Family Role")%>: </label> <%=controller.roleText(gp.member.mreg_familyrole)%></li>
                                                <li><label><%=controller.text("ID/Passport")%>: </label> </li>
                                                <li><label><%=controller.text("Contacts")%>: </label>  
                                                    <ul>
                                                        <li><%=controller.text("Phone")%>: </li>
                                                        <li><%=controller.text("Address")%>: </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul></td>
                            </tr>

                            <%}}%>
                        </table>
                        <%}%>
                    </div>
                </div>

                <%for(HoldingViewController.Rent rent:controller.rents()){%>
                <div class="panel panel-success">
                    <div class="panel-heading"><%=controller.text("Tenant")%></div>
                    <div class="panel-body">
                        <%for (LADM.Right tenant:rent.renter){ %>
                        <table class="table table-striped">


                            <tr>
                                <td class="table_separetor" colspan="2"><label><%=controller.text("Full Name")%>: </label> <%=tenant.party.getFullName()%></td>
                            </tr>
                            <tr>
                                <td><label><%=controller.text("Sex")%>: </label> <%=controller.sexText(tenant.party.sex)%></td>
                                <td><label><%=controller.text("is Orphan?")%>: </label> <%=controller.isOrphanText(tenant.party.isorphan)%></td>
                            </tr>
                            <tr>
                                <td><label><%=controller.text("Age")%>: </label> <span id='dob'><%=controller.age(tenant.party.dateOfBirth)%></span></td>
                                <td><label><%=controller.text("Has Physical Impairment")%>: </label> <%=controller.disablityText(tenant.party.disability)%></td>
                            </tr>
                            <tr>
                                <td><label><%=controller.text("Martial Status")%>: </label> <%=controller.maritalStatusText(tenant.party.maritalstatus)%></td>
                                <td><label><%=controller.text("Contacts")%>: </label> 
                                    <ul>
                                        <li><%=controller.text("Phone")%>: </li>
                                        <li><%=controller.text("Address")%>: </li>
                                    </ul>
                                </td>
                            </tr>
                            <tr>
                                <td><label><%=controller.text("Family Role")%>: </label> <%=controller.roleText(tenant.party.mreg_familyrole)%></td>
                                <td><label><%=controller.text("ID/Passport")%>: </label> </td>
                            </tr>
                        </table>
                        <%}%>
                        <table class="table table-striped">
                            <tr><td class="table_separetor"><label><%=controller.text("Rent/Lease Details")%></label></td></tr>
                            <tr><td><label><%=controller.text("Rent/Lease Start Date")%>: </label>  <%=controller.rentFrom(rent)%></td></tr>
                            <tr><td><label><%=controller.text("Rent/Lease End Date")%>: </label> <%=controller.rentTo(rent)%></td></tr>
                            <tr><td><label><%=controller.text("Rent/Lease Amount")%>: </label> <%=controller.rentAmount(rent)%></td></tr>
                            <tr><td><label><%=controller.text("Rented Parcels")%>:</label>
                                    <%for(LADM.Parcel p:rent.parcels){%>
                                    <ul>
                                        <li><label><%=controller.text("Parcel No")%>: </label>  <%=p.seqNo%></li>
                                        <li><label><%=controller.text("Parcel Area")%>: </label>  <%=p.areaGeom%> <%=controller.text("M")%><sup>2</sup></li>
                                        <li><label><%=controller.text("Rented Area")%>: </label> <%=controller.rentArea(rent, p.upid)%> <%=controller.text("M")%><sup>2</sup> <hr></li>
                                    </ul>
                                    <%}%>
                                </td></tr>
                        </table>
                    </div>
                </div>
                <%}%>

            </div>
            <div class="bhoechie-tab-content">

                <div class="panel panel-warning">
                    <div class="panel-heading"><%=controller.text("Restriction")%></div>
                    <div class="panel-body">
                        <%int restSize=controller.restrictions().size();
                         if(restSize==0){%>
                        <div class="align-center"><h4><%=controller.text("No Restriction Registered")%></h4></div>
                        <%}else{
                        for(HoldingViewController.Restriction rest:controller.restrictions()){
                        for(LADM.Restriction res:rest.restrict){%>
                        <table class="table table-striped">


                            <tr>
                                <td class="table_separetor" colspan="2"><label><%=controller.text("Third Party Name")%>: </label> <%=res.party.getFullName()%></td>
                            </tr>
                            <tr>
                                <td><label><%=controller.text("Sex")%>: </label> <%=controller.sexText(res.party.sex)%></td>
                                <td><label><%=controller.text("is Orphan?")%>: </label> <%=controller.isOrphanText(res.party.isorphan)%></td>
                            </tr>
                            <tr>
                                <td><label><%=controller.text("Age")%>: </label> <span id='dob'><%=controller.age(res.party.dateOfBirth)%></span></td>
                                <td><label><%=controller.text("Has Physical Impairment")%>: </label> <%=controller.disablityText(res.party.disability)%></td>
                            </tr>
                            <tr>
                                <td><label><%=controller.text("Martial Status")%>: </label> <%=controller.maritalStatusText(res.party.maritalstatus)%></td>
                                <td><label><%=controller.text("Contacts")%>: </label> 
                                    <ul>
                                        <li><%=controller.text("Phone")%>: </li>
                                        <li><%=controller.text("Address")%>: </li>
                                    </ul>
                                </td>
                            </tr>
                            <tr>
                                <td><label><%=controller.text("Family Role")%>: </label> <%=controller.roleText(res.party.mreg_familyrole)%></td>
                                <td><label><%=controller.text("ID/Passport")%>: </label> </td>
                            </tr>
                        </table>
                        <%}%>
                        <table class="table table-striped">
                            <tr><td class="table_separetor"><label><%=controller.text("Restriction Details")%></label></td></tr>
                            <%for(LADM.Restriction res:rest.restrict){%>
                            <tr><td><label><%=controller.text("Restriction Type")%>: </label> <%=controller.restrictionTypeText(res.restrictionType)%></td></tr>
                            <%}%>
                            <tr><td><label><%=controller.text("Restriction Start Date")%>: </label>  <%=controller.restrictionFrom(rest)%></td></tr>
                            <tr><td><label><%=controller.text("Restriction End Date")%>: </label> <%=controller.restrictionTo(rest)%></td></tr>

                            <tr><td><label><%=controller.text("Restricted Parcels")%>:</label>
                                    <%for(LADM.Parcel p:rest.parcels){%>
                                    <ul>
                                        <li><label><%=controller.text("Parcel No")%>: </label>  <%=p.seqNo%></li>
                                        <li><label><%=controller.text("Parcel Area")%>: </label>  <%=p.areaGeom%> <%=controller.text("M")%><sup>2</sup><hr></li>
                                    </ul>
                                    <%}%>
                                </td></tr>
                        </table>
                        <%}}%>
                    </div>
                </div>

            </div>
            <div class="bhoechie-tab-content">
                <div class="panel panel-warning">
                    <div class="panel-heading"><%=controller.text("Servitude/Easement")%></div>
                    <div class="panel-body">
                        <%int servSize=controller.servitudes().size();
                             if(servSize==0){%>
                        <div class="align-center"><h4><%=controller.text("No Servitude/Easement Registered")%></h4></div>
                        <%}else{
                        for(HoldingViewController.Servitude servitude:controller.servitudes()){
                            for(LADM.Restriction serv:servitude.servitude){%>
                        <table class="table table-striped">
                            <tr>
                                <td class="table_separetor" colspan="2"><label><%=controller.text("Third Party Name")%>: </label> <%=serv.party.getFullName()%></td>
                            </tr>
                            <tr>
                                <td><label><%=controller.text("Sex")%>: </label> <%=controller.sexText(serv.party.sex)%></td>
                                <td><label><%=controller.text("is Orphan?")%>: </label> <%=controller.isOrphanText(serv.party.isorphan)%></td>
                            </tr>
                            <tr>
                                <td><label><%=controller.text("Age")%>: </label> <span id='dob'><%=controller.age(serv.party.dateOfBirth)%></span></td>
                                <td><label><%=controller.text("Has Physical Impairment")%>: </label> <%=controller.disablityText(serv.party.disability)%></td>
                            </tr>
                            <tr>
                                <td><label><%=controller.text("Martial Status")%>: </label> <%=controller.maritalStatusText(serv.party.maritalstatus)%></td>
                                <td><label><%=controller.text("Contacts")%>: </label> 
                                    <ul>
                                        <li><%=controller.text("Phone")%>: </li>
                                        <li><%=controller.text("Address")%>: </li>
                                    </ul>
                                </td>
                            </tr>
                            <tr>
                                <td><label><%=controller.text("Family Role")%>: </label> <%=controller.roleText(serv.party.mreg_familyrole)%></td>
                                <td><label><%=controller.text("ID/Passport")%>: </label> </td>
                            </tr>
                        </table>
                        <%}%>
                        <table class="table table-striped">
                            <tr><td class="table_separetor"><label><%=controller.text("Servitude/Easement Details")%></label></td></tr>
                            <%for(LADM.Restriction serv:servitude.servitude){%>
                            <tr><td><label><%=controller.text("Servitude/Easement Type")%>: </label> <%=controller.restrictionTypeText(serv.restrictionType)%></td></tr>
                            <%}%>
                            <tr><td><label><%=controller.text("Servitude/Easement Start Date")%>: </label>  <%=controller.servitudeFrom(servitude)%></td></tr>
                            <tr><td><label><%=controller.text("Servitude/Easement End Date")%>: </label> <%=controller.servitudeTo(servitude)%></td></tr>

                            <tr><td><label><%=controller.text("Servitude/Easement Parcels")%>:</label>
                                    <%for(LADM.Parcel p:servitude.parcels){%>
                                    <ul>
                                        <li><label><%=controller.text("Parcel No")%>: </label>  <%=p.seqNo%></li>
                                        <li><label><%=controller.text("Parcel Area")%>: </label>  <%=p.areaGeom%> <%=controller.text("M")%><sup>2</sup><hr></li>
                                    </ul>
                                    <%}%>
                                </td></tr>
                        </table>
                        <%}}%>
                    </div>
                </div>

            </div>
            <div class="bhoechie-tab-content">
                <table class="table table-striped jambo_table table-responsive" id="transcation_table">
                    <thead>
                        <tr>
                            <th><%=controller.text("Date")%></th>
                            <th><%=controller.text("Transaction ID")%></th>
                            <th><%=controller.text("Transaction Type")%></th>
                            <th><%=controller.text("Note")%></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>

                        <%for(HoldingViewController.HoldingHistory h:controller.holdingHistory()){%>
                        <tr>
                            <td><%=h.date%></td>
                            <td><%=h.tranID%></td>
                            <td><%=h.tranType%></td>
                            <td><%=h.note%></td>
                            <td><a href="javascript:transaction_showTranDetail('<%=h.tranUID%>',<%=h.tranTypeID%>)"><i class="fa fa-eye"></i></a></td>
                        </tr>  
                        <%}%>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>