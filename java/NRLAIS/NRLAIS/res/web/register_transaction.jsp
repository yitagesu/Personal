<%-- 
    Document   : register_transaction
    Created on : Dec 25, 2017, 11:23:08 AM
    Author     : yitagesu
--%>
<%@page import="com.intaps.nrlais.repo.*"%>
<%@page import="com.intaps.nrlais.*"%>
<%@page import="com.intaps.nrlais.controller.*"%>
<%@page import="com.intaps.nrlais.controller.tran.*"%>
<%@page import="com.intaps.nrlais.model.*"%>
<%@page import="java.util.*"%>
<%@page pageEncoding="UTF-8" %>
<%

    TransactionContainerViewController containerController=new TransactionContainerViewController(request,response);
    HeaderViewController headerController=new HeaderViewController(request,response,false);
    
%>
<html>
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>NRLAIS</title>
        <link rel="stylesheet" href="/assets/CSS/bootstrap.min.css"/>
        <link rel="stylesheet" href="/assets/CSS/bootstrap-popover-x.min.css"/>
        <link rel="stylesheet" href="/assets/CSS/custom.css"/>
        <link rel="stylesheet" href="/assets/ol/ol.css"/>
        <!--        third party-->
        <script src="/assets/JS/jquery.min.js"></script>
        <script src="/assets/JS/jquery-ui.js"></script>
        <script src="/assets/JS/bootstrap.min.js"></script>
        <script src="/assets/JS/bootstrap-popover-x.min.js"></script>
        <script src="/assets/JS/bootbox.js"></script>
        <script src="/assets/JS/string.js"></script>
        <script src="/assets/JS/uuid-gen.js"></script>

        <!--        INTAPS common-->
        <script src="assets/JS/et_dual_cal.js"></script>
        <script src="/assets/JS/general.js"></script>

        <!--        NRLAIS general-->
        <script src="/assets/JS/js_constants.jsp"></script>
        <script src="assets/JS/document_picker.js"></script>        
        <script src="/holding/searching_holding.js"></script>
        <script src="/assets/JS/transaction.js"></script>
        <script src="/assets/JS/attachment.js"></script>
        <script src="/holding/applicants_picker.js"></script>
        <script src="/holding/parcel_split_table.js"></script>
        <script src="/holding/parcel_transfer_table.js"></script>
        <script src="/party/register_party.js"></script>
        <script src="/assets/JS/nrlais_map.js"></script>
        <script src="/assets/ol/ol-debug.js"></script>
        <script src="/assets/ol/proj4.js"></script>
        <script src="/assets/JS/lang.js.jsp"></script>

        <!--        NRLAIS transaction specific-->
        <script src="/tran/divorce/register_divorce.js"></script>
        <script src="/tran/rent/register_rent.js"></script>
        <script src="/tran/gift/register_gift.js"></script>
        <script src="/tran/inheritance/register_inheritance.js"></script>
        <script src="/tran/reallocation/register_reallocation.js"></script>
        <script src="/tran/boundary_correction/register_boundary_correction.js"></script>
        <script src="/tran/expropriation/register_expropriation.js"></script>
        <script src="/tran/boundary_correction/register_boundary_correction.js"></script>
        <script src="/tran/special_case/register_special_case.js"></script>
        <script src="/tran/consolidation/register_consolidation.js"></script>
        <script src="/tran/exchange/register_exchange.js"></script>
        <script src="/tran/certificate_replacement/register_certificate_replacement.js"></script>
        <script src="/tran/ex_officio/register_ex_officio.js"></script>
        <script src="/tran/restrictive_interest/register_restrictive_interest.js"></script>
        <script src="/tran/simple_correction/register_simple_correction.js"></script>
        <script src="/tran/split/register_split.js"></script>

        <script src="/assets/JS/lookups.js.jsp"></script>

        <script src="/assets/JS/modal_controler.js"></script>


        <script>
            var trancation_type =<%=containerController.tranType%>;
            <%if(containerController.txuid!=null && !containerController.txuid.equals("")){%>
            var transaction_tran_uid = '<%=containerController.txuid%>';
            <%}else{%>
            var transaction_tran_uid = null;
            <%}%>
            var app_dob;
            var app_holdingSearch;
            function transaction_initializeTransaction()
            {
            <%if(containerController.tranType==LATransaction.TRAN_TYPE_BOUNDARY_CORRECTION){%>
                boundary_correction_init();
            <%}else if(containerController.tranType==LATransaction.TRAN_TYPE_DIVORCE){%>
                divorce_initDivorce();
            <%}else if(containerController.tranType==LATransaction.TRAN_TYPE_GIFT){%>
                gift_initGift();
            <%}else if(containerController.tranType==LATransaction.TRAN_TYPE_RENT){%>
                rent_initRent();
            <%}else if(containerController.tranType==LATransaction.TRAN_TYPE_INHERITANCE||containerController.tranType==LATransaction.TRAN_TYPE_INHERITANCE_WITHWILL){%>
                inheritance_init();
            <%}else if(containerController.tranType==LATransaction.TRAN_TYPE_REALLOCATION){%>
                reallocation_init();
            <%}else if(containerController.tranType==LATransaction.TRAN_TYPE_EXPROPRIATION){%>
                expropriation_init();
            <%}else if(containerController.tranType==LATransaction.TRAN_TYPE_SPECIAL_CASE){%>
                special_case_init();
            <%}else if(containerController.tranType==LATransaction.TRAN_TYPE_CONSOLIDATION){%>
                consolidation_init();
            <%}else if(containerController.tranType==LATransaction.TRAN_TYPE_EXCHANGE){%>
                exchange_init(exchange_tran_data);
            <%}else if(containerController.tranType==LATransaction.TRAN_TYPE_EX_OFFICIO){%>
                ex_officio_init();
            <%}else if(containerController.tranType==LATransaction.TRAN_TYPE_RESTRICTIVE_INTEREST){%>
                restrictiveIntersetController(retriction_tran_data);
            <%}else if(containerController.tranType==LATransaction.TRAN_TYPE_SERVITUDE){%>
                restrictiveIntersetController(retriction_tran_data);
            <%}else if(containerController.tranType==LATransaction.TRAN_TYPE_REPLACE_CERTIFICATE){%>
                certificateReplacement_init();
            <%}else if(containerController.tranType==LATransaction.TRAN_TYPE_SIMPLE_CORRECTION){%>
                simpleCorrection_init();
            <%}else if(containerController.tranType==LATransaction.TRAN_TYPE_SPLIT){%>
                split_initSplit();
            
            <%}else{%>
                bootbox.alert('Application type<%=containerController.tranType%> is not supported');
            <%}%>
            }
            function transaction_saveInitial()
            {
                bootbox.alert('Saving without submission is not implemente for this transaction type');
            }
              var map_server = '<%=containerController.mapServerUrl()%>';
              function intializeHoldingSearch(){
                  $.ajax('/holding/holding_search.jsp?setting='+encodeURIComponent(JSON.stringify({containerEl:"pick_person",pickHolding:true,pickParty:false}))
                , {

                    'method': 'GET',
                    'success': function (data) {
                      
                       
                        $('#app_search_holding_container').html(data);
                        app_holdingSearch=holdingBrowser("#pick_person",true,{containerEl:"pick_person",pickHolding:true,pickParty:false});
                        transaction_initializeTransaction();
                    },
                    'error': function (error) {
                        bootbox.alert("error occord:" + error);
                    }
                });
              }
            $(document).ready(function ()
            {
                modalController.load('body');

                $(document).on('click', "div.bhoechie-tab-menu>div.list-group>a", function (e) {
                    e.preventDefault();
                    $(this).siblings('a.active').removeClass("active");
                    $(this).addClass("active");
                    var index = $(this).index();
                    $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
                    $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
                });

                transcation_initMap('<%=containerController.mapServerUrl()%>');
                $('#party_type').val('1');
                app_dob = new EthiopianDualCalendar("#app_dob");
                app_date = new EthiopianDualCalendar("#app_date");
                intializeHoldingSearch();
                serveyDate = new EthiopianDualCalendar("#parcel_surveyDate");
                
                $('iframe').contents().find('body').css('margin', '0');
                $('#holding_view_modal .close').click(function () {
                    $("#pick_person").modal('show');
                });

            });
        </script>
    </head>

    <body id="body">
        <%@ include file="header.jsp" %>
        <div class="dashboard-body">
            <div class="list-of-transc col-md-7" style='height:95%' view="half">
                <div id="reg_application" style='height:100%'>
                    <div class="x_panel " id="" style='height:100%'>
                        <div class="x_title">
                            <div class="pull-left">
                                <h4><%=containerController.text("Transaction Registration")%> <small id="title"></small></h4>
                            </div>
                            <div class="pull-right">
                                <button class="btn btn-nrlais" type="button" onclick="transaction_saveInitial()" ><%=containerController.text("Save Transaction")%></button>
                                <button class="btn btn-nrlais" type="button" onclick="navigateMap()"><i class="fa fa-arrow-right" id="arrow"style="font-size: 17px"></i></button>
                            </div>

                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content has-scroll" style='height:90%'>
                            <div id="appContent" class="col-md-12">
                                <%switch(containerController.tranType){
                                case LATransaction.TRAN_TYPE_BOUNDARY_CORRECTION:{%>
                                <%@ include file="/tran/boundary_correction/register_boundary_correction.jsp" %>
                                <%}break;
                                case LATransaction.TRAN_TYPE_DIVORCE:{%>
                                <%@ include file="/tran/divorce/register_divorce.jsp" %>
                                <%}break;
                                case LATransaction.TRAN_TYPE_GIFT:{%>
                                <%@ include file="/tran/gift/register_gift.jsp" %>
                                <%}break;
                                case LATransaction.TRAN_TYPE_RENT:{%>
                                <%@ include file="/tran/rent/register_rent.jsp" %>
                                <%}break;   
                                case LATransaction.TRAN_TYPE_INHERITANCE_WITHWILL:
                                case LATransaction.TRAN_TYPE_INHERITANCE:{%>
                                <%@ include file="/tran/inheritance/register_inheritance.jsp" %>
                                <%}break;
                                case LATransaction.TRAN_TYPE_REALLOCATION:{%>
                                <%@ include file="/tran/reallocation/register_reallocation.jsp" %>
                                <%}break;
                                case LATransaction.TRAN_TYPE_EXPROPRIATION:{%>
                                <%@ include file="/tran/expropriation/register_expropriation.jsp" %>
                                <%}break;
                                case LATransaction.TRAN_TYPE_SPECIAL_CASE:{%>
                                <%@ include file="/tran/special_case/register_special_case.jsp" %>
                                <%}break;                                
                                case LATransaction.TRAN_TYPE_CONSOLIDATION:{%>
                                <%@ include file="/tran/consolidation/register_consolidation.jsp" %>
                                <%}break;                                
                                case LATransaction.TRAN_TYPE_EXCHANGE:{%>
                                <%@ include file="/tran/exchange/register_exchange.jsp" %>
                                <%}break;   
                                case LATransaction.TRAN_TYPE_EX_OFFICIO:{%>
                                <%@ include file="/tran/ex_officio/register_ex_officio.jsp" %>
                                <%}break; 
                                case LATransaction.TRAN_TYPE_RESTRICTIVE_INTEREST:
                                case LATransaction.TRAN_TYPE_SERVITUDE:{%>
                                <%@ include file="/tran/restrictive_interest/register_restrictive_interest.jsp" %>
                                <%}break; 
                                case LATransaction.TRAN_TYPE_REPLACE_CERTIFICATE:{%>
                                <%@ include file="/tran/certificate_replacement/register_certificate_replacement.jsp" %>
                                <%}break;
                                case LATransaction.TRAN_TYPE_SIMPLE_CORRECTION:{%>
                                <%@ include file="/tran/simple_correction/register_simple_correction.jsp" %>
                                <%}break;
                                case LATransaction.TRAN_TYPE_SPLIT:{%>
                                <%@ include file="/tran/split/register_split.jsp" %>
                                <%}break;
                                default:%>
                                <%="Transaction Type not Supported"%>

                                <% }%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="map col-md-5" style='height:95%'>
                <div class="x_panel" style='height:100%'>
                    <div class="x_content has-scroll" id="map" style='height:100%'>
                    </div>
                    <div class="map_text" id="map_text" style="display: none">
                        <h3>Map Content</h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade " id="register_representative" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4><%=containerController.text("Register Authorized Representative")%></h4>

                    </div>            
                    <!-- /modal-header -->

                    <div class="modal-body has-scroll">
                        <form id="applicant_form">
                            <div class="row">
                                <div class="input-form col-md-12">
                                    <label class="control-label"><%=containerController.text("First Name")%></label>
                                    <input type="text" class="form-control" id="app_name" placeholder="<%=containerController.text("Enter First Name")%>"  />
                                </div>
                                <div class="input-form col-md-12">
                                    <label class="control-label"><%=containerController.text("Father Name")%></label>
                                    <input type="text" class="form-control" id="app_father_name" placeholder="<%=containerController.text("Enter Father Name")%>"  />
                                </div>
                                <div class="input-form col-md-12">
                                    <label class="control-label"><%=containerController.text("Grand Father Name")%></label>
                                    <input type="text" class="form-control" id="app_gfather_name" placeholder="<%=containerController.text("Enter Grand Father Name")%>"  />
                                </div>

                                <div class="input-form col-md-6">
                                    <label class="control-label col-md-12"><%=containerController.text("Sex")%>:</label>
                                    <div class="col-md-6">
                                        <div class="input-group" >
                                            <span class="input-group-addon" id="addon">
                                                <input type="radio" class="radio radio-primary" name="sex" id="app_sex_male" checked="checked"/>
                                            </span>
                                            <label class="form-control" for="app_sex_male" id="app_male" ><%=containerController.text("Male")%></label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="input-group" >
                                            <span class="input-group-addon" id="addon">
                                                <input type="radio" class="radio radio-primary" name="sex" id="app_sex_female">
                                            </span>
                                            <label class="form-control" for="app_sex_female" id="app_female"><%=containerController.text("Female")%></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="input-form col-md-6">
                                    <label class="control-label"><%=containerController.text("Date of Birth")%></label>
                                    <input type="text" class="form-control" placeholder="<%=containerController.text("pick DOB")%>"  id="app_dob"/>
                                </div>
                                <div class="input-form col-md-6">
                                    <label class="control-label"><%=containerController.text("Relationship")%></label>
                                    <select class="form-control" id="app_relation">
                                        <option value=""><%=containerController.text("Please select one")%></option>
                                        <option value="1"><%=containerController.text("Husband")%></option>
                                        <option value="2"><%=containerController.text("Wife")%></option>
                                        <option value="3"><%=containerController.text("sister")%></option>
                                        <option value="4"><%=containerController.text("Brother")%></option>
                                        <option value="5"><%=containerController.text("Friend")%></option>
                                        <option value="6"><%=containerController.text("Attorney")%></option>
                                        <option value="7"><%=containerController.text("other")%></option>
                                    </select>
                                </div>
                                <div class="input-form col-md-6">
                                    <label class="control-label"><%=containerController.text("Phone No")%></label>
                                    <input type="phone" class="form-control" placeholder="<%=containerController.text("Enter phone no")%>"  id="app_phone"/>
                                </div>
                                <div class="input-form col-md-12">
                                    <label class="control-label"><%=containerController.text("Address")%></label>
                                    <textarea class="form-control" rows="4" placeholder="<%=containerController.text("Enter Address")%>"  id="app_address"></textarea>
                                </div>
                                <div class="input-form col-md-6">
                                    <label class="control-label"><%=containerController.text("Authorized Doc")%></label>
                                    <input  type="file" name="uploadimg" class="uploadimg" id="autorized_doc" onchange='attachment.openFile(event)'/>
                                </div>
                                <div class="input-form col-md-6">
                                    <label class="control-label"><%=containerController.text("Doc Reference")%></label>
                                    <input tyep="text" class="form-control" id="doc_reference" placehoder="<%=containerController.text("Enter Reference")%>"/>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <div class="pull-left">
                            <div class="app-error-panel">

                            </div>
                        </div>
                        <div class="input-form">
                            <button class="btn btn-nrlais" onclick="applicantPicker.validateAndSaveRep()"><%=containerController.text("Add")%></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade " id="pick_person" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div id="app_search_holding_container" class="modal-content">
                    
                </div>
            </div>
        </div>
        <div class="modal fade " id="register_holder" tabindex="-1" role="dialog">
        </div>
        <div class="modal fade " id="pick_parcel" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4>Search Parcel</h4>

                    </div>            <!-- /modal-header -->

                    <div class="modal-body">
                        <form id="applicant_form">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group col-md-6" >
                                        <select class="form-control" required="required" id="parcel_kebele">
                                            <option>please Select Kebele</option>
                                        </select>
                                    </div>
                                    <div class="input-group col-md-6" >
                                        <input max="5" type="text" class="form-control" placeholder="Enter Parcel Number"  />
                                        <span class="input-group-addon" id="addon">
                                            <a href="#"><i class="fa fa-search"></i></a>
                                        </span>
                                    </div>
                                </div>
                                <hr/>
                                <table class="table table-striped jambo_table">
                                    <thead>
                                        <tr>
                                            <td>Woreda</td>
                                            <td>Kebele</td>
                                            <td>Parcel No</td>
                                            <td>Area</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Meskan</td>
                                            <td>Meseretwogerama</td>
                                            <td>12457</td>
                                            <td>0.01 HR</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade " id="add_id_content" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4><%=containerController.text("Add ID Information")%></h4>

                    </div>            <!-- /modal-header -->

                    <div class="modal-body">
                        <form id="add_ID_form">
                            <div class="row">
                                <div class="input-form col-md-12">
                                    <input id="id_file" type="file" name="uploadimg" class="uploadimg" onchange='attachment.openFile(event)'/>
                                </div>
                                <div class="input-form col-md-12">
                                    <select id="id_type" class="form-control">
                                        <option value=""><%=containerController.text("Select ID Type")%></option>
                                        <option value="<%=DocumentTypeInfo.DOC_TYPE_KEBELE_ID%>"><%=containerController.text("Kebele ID")%></option>
                                        <option value="<%=DocumentTypeInfo.DOC_TYPE_PASSPORT%>"><%=containerController.text("Passport")%></option>
                                    </select>
                                </div>
                                <div class="input-form col-md-12">
                                    <input type="text"  class="form-control" placeholder="<%=containerController.text("ID Number")%>" id="id_text"/>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <div class="input-form align-right">
                            <a class="btn btn-nrlais" href="javascript:applicantPicker.validateAndSaveID()"><%=containerController.text("Add")%></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade " id="add_pom_content" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 id="add_pom_title"></h4>

                    </div>            <!-- /modal-header -->

                    <div class="modal-body">
                        <form id="add_pom_form">
                            <div class="row">
                                <div class="input-form col-md-12">
                                    <input id="pom_file" type="file" name="uploadimg" class="uploadimg" onchange='attachment.openFile(event)'/>
                                </div>
                                <div class="input-form col-md-12">
                                    <label class="control-label"><%=containerController.text("Document Reference")%></label>
                                    <input type="text"  class="form-control" placeholder="<%=containerController.text("Refernce")%>" id="pom_ref"/>
                                </div>
                                <div class="input-form col-md-12">
                                    <label class="control-label"><%=containerController.text("Document Description")%></label>
                                    <input type="text"  class="form-control" placeholder="Description" id="pom_text"/>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <div class="input-form align-right">
                            <a class="btn btn-nrlais" href="javascript:applicantPicker.validateAndSavePOM()"><%=containerController.text("Add")%></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>                            

        <div class="modal fade " id="add_req_document" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4><%=containerController.text("Add Required Document")%></h4>
                    </div>            <!-- /modal-header -->

                    <div class="modal-body">
                        <form id="req_doc_form">
                            <div class="row">
                                <div class="input-form col-md-12">
                                    <label class="control-label"><%=containerController.text("upload document")%></label>
                                    <input  type="file" name="uploadimg" class="uploadimg" id="upload_doc_req" onchange='attachment.openFile(event)'/>
                                </div>
                                <div class="input-form col-md-12">
                                    <label class="control-label"><%=containerController.text("Refernce")%></label>
                                    <input type="text" class="form-control" placeholder="<%=containerController.text("Refernce")%>"  id="doc_reference_req"/>
                                </div>
                                <div class="input-form col-md-12">
                                    <label class="control-label"><%=containerController.text("Document Description")%></label>
                                    <textarea rows="5" type="text" class="form-control" placeholder="<%=containerController.text("Enter Description")%>"  id="doc_description_req"></textarea>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <div class="input-form">
                            <button class="btn btn-nrlais" onclick="documentPicker.validateAndSaveDocument()" ><%=containerController.text("Add")%></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade " id="document_preview_modal" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                    </div>            <!-- /modal-header -->

                    <div class="modal-body">
                        <iframe style="width: 100%" id="attachment_preview" src=''>

                        </iframe>
                    </div>
                    <div class="modal-footer">
                        <div class="input-form">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="holding_view_modal" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                    </div>            <!-- /modal-header -->

                    <div class="modal-body">
                        <div id="holdingContent">

                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="input-form">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade " id="application_detail" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>            <!-- /modal-header -->

                    <div class="modal-body" id="transactionContent">
                        <iframe id="transactionContent-iframe" name="iframe"></iframe>

                    </div>
                    <div class="modal-footer"> </div>
                </div>
            </div>
        </div>    
        
        <div class="modal fade " id="add_document" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4><%=containerController.text("Add Additional Document")%></h4>
            </div>            <!-- /modal-header -->

            <div class="modal-body">
                <form id="doc_form">
                    <div class="row">
                        <div class="input-form col-md-12">
                            <label class="control-label"><%=containerController.text("upload document")%></label>
                            <input  type="file" name="uploadimg" class="uploadimg" id="upload_doc_add" onchange='attachment.openFile(event)'/>
                        </div>
                        <div class="input-form col-md-12">
                            <label class="control-label"><%=containerController.text("Document Description")%></label>
                            <input type="text" class="form-control" placeholder="<%=containerController.text("Enter Reference")%>"  id="doc_ref_add"/>
                        </div>
                        <div class="input-form col-md-12">
                            <label class="control-label"><%=containerController.text("Document Description")%></label>
                            <textarea cols="8" class="form-control" placeholder="<%=containerController.text("Enter Description")%>"  id="doc_description_add"></textarea>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="input-form">
                    <button class="btn btn-nrlais" onclick="documentPicker.addAdditional()" ><%=containerController.text("Add")%></button>
                </div>
            </div>
        </div>
    </div>
</div>
    </body>

</html>

