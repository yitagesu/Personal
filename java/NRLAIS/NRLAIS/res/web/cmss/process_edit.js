/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var edit_parcels = null;
var edit_task = null;
var edit_geom = null;
var edit_edited_parcel = [];
function bcInit(taskUID)
{
    $.ajax({url: '/api/edit_task?task_uid=' + taskUID + "&session_id=" + sessionid,
        dataType: 'json',
        success: function (data)
        {
            edit_task = data.task;
            edit_parcels = data.parcels;
            edit_geom = data.geoms;
        }
    });
    layerChanged = function (type, id)
    {
        if (type == 1)
        {
            alert('It is not allowed to create new parcels during bounary correction');
            return;
        }
        if (type == 2)
        {
            alert('It is not allowed to delete parcels during bounary correction');
            return;
        }
        getGeomData(function (data)
        {
            for (var i in edit_geom)
            {
                if (edit_geom[i].id == id)
                {
                    $("#edit_new_area_" + edit_geom[i].parcelUID).text(data.area);
                    edit_edited_parcel[id] = data;
                    $("#edit_button_commit").show();
                }
            }
        }, id);
    };
}
function edit_getParcelUID(id)
{
    for(var i in edit_geom)
        if(edit_geom[i].id==id)
            return edit_geom[i].parcelUID;
    return null;
}

function edit_comit()
{
    getGeomData(function (data)
    {
        var as = {
            task_uid: edit_task.taskUID,
            assignments: []
        };
        var labels = [];
        for (var i = 0; i < data.length; i++)
        {
            
            var puid = edit_getParcelUID(data[i].id);
            as.assignments.push(
                    {
                        geom: 'SRID=20137;' + data[i].wkt,
                        parcel_uid: puid
                    });
        }
        $.ajax({
            url: '/api/taskgeom?task_uid=' + edit_task.taskUID + "&session_id=" + sessionid,
            method: 'POST',
            data: JSON.stringify(as),
            contentType: 'application/json',
            dataType: 'json',
            success: function (res)
            {
                if (res.error)
                {
                    alert('Error trying to save change.\n' + res.error);
                } else
                {
                    alert('succesfully saved');
                    unloadTask();
                    openHomePage();
                }
            },
            error: function (err)
            {
                alert('failed to save geometry to the database');
            }
        });
    });

}
function edit_cancel()
{
    unloadTask();
    openHomePage();
}