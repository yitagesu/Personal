<%@page import="com.intaps.nrlais.*"%>
<%@page import="com.intaps.nrlais.api.*"%>
<%@page import="com.intaps.nrlais.model.*"%>
<%@page import="com.intaps.nrlais.viewmodel.*"%>
<%@page import="com.intaps.nrlais.model.tran.*"%>
<%@page import="com.intaps.nrlais.repo.*"%>
<%@page import="com.intaps.nrlais.tran.*"%>
<%@page import="com.intaps.nrlais.util.*"%>
<%@page import="com.intaps.nrlais.controller.*"%>
<%@page import="com.intaps.nrlais.replication.*"%>
<%@page import="java.util.*"%>
<%@page pageEncoding="UTF-8" %>
<%
    ReplicationViewController controller=new ReplicationViewController(request,response);
    HeaderViewController headerController=new HeaderViewController(request,response,false);
    
%>
<html>
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>NRLAIS</title>
        <link rel="stylesheet" href="/assets/CSS/bootstrap.min.css"/>
        <link rel="stylesheet" href="/assets/CSS/bootstrap-popover-x.min.css"/>
        <link rel="stylesheet" href="/assets/CSS/custom.css"/>

        <link href="/assets/ol/ol.css" type="text/css" rel="stylesheet">        
        <script src="/assets/ol/ol-debug.js"></script>
        <script src="/assets/JS/nrlais_map.js"></script>
        <script src="/assets/ol/proj4.js"></script>

        <script src="/assets/JS/jquery.min.js"></script>
        <script src="/assets/JS/jquery-ui.js"></script>
        <script src="/assets/JS/bootstrap.min.js"></script>
        <script src="/assets/JS/bootstrap-popover-x.min.js"></script>
        <script src="/assets/JS/bootbox.js"></script>        

        <script src="/assets/JS/js_constants.jsp"></script>
        <script src="/assets/JS/general.js"></script>
        <script src="/assets/JS/transaction.js"></script>
        <script src="/assets/JS/dashboard.js"></script>
        <script src="/assets/JS/data_browser.js"></script>
        <script src="/assets/JS/server-job.js"></script>
        <script>
            var job;
        $(document).ready(function()
        {
            job=monitorJob("/api/replication_job", {}, "/api/replication_job", null, "#status", null
                            , function ()//succesfully started
                            {
                            }
                    , function (res)//done import
                    {
                        if(res.errorCount>0)
                        {
                            $("#status").text("error");
                        }
                        else
                        {
                            $("#status").text("Replication file ready, downloading..");
                            window.location="/api/replication_job?get_file=true";
                        }                        
                    },"buttonStopTransfer");
            job.attach();
        });        
        </script>
    </head>
    <body id="body">
        <%@ include file="/header.jsp" %>
          <div class="replication">
              <div id="status" class="align-center"></div>
          </div>
    </body>
    
</html>