var listMangerRouter =
        {
            instance_counter: 0,
            instances: [],

            removeItem: function (instance, row_tag)
            {
                this.instances[instance].removeItem(row_tag);
            },
            editItem: function (instance, row_tag)
            {
                this.instances[instance].editItem(row_tag);
            },
            addInstance: function (inst)
            {
                this.instance_counter++;
                this.instances[this.instance_counter] = inst;
                return this.instance_counter;
            }
        }
function listManager(settings)
{
    /*
     tableBodySelector
     addButtonSelector     
     row_template_url
     form_template_url
     getNewRecordTemplateData()
     getEditFormTemplateData(rowData)
     getRowTemplateData(rowData)
     validateAndGetData()
     */
    var row_template = null;
    var form_template = null;
    $.ajax({url: settings.row_template_url, success: function (data)
        {
            row_template = data;

        }, error: settings.error});

    $.ajax({url: settings.form_template_url, success: function (data)
        {
            form_template = data;

        }, error: settings.error});
    var addFormDialog = null;
    var modalId = settings.tableBodySelector + "_form";
    $(settings.addButtonSelector).click(function ()
    {
        editRowIndex = null;
        var formHtml = ejs.render(form_template, {locals: settings.getNewRecordTemplateData()});
        addFormDialog = modalController.createModal(formHtml, modalId);
        settings.formDOMAttached();
        addFormDialog.showModal();
    });

    var tableData
    var row_tag_counter = 0;
    var row_tags = [];
    function addRow(rowData)
    {
        row_tag_counter++;
        var locals = settings.getRowTemplateData(rowData);
        locals.tag = 'row_' + row_tag_counter;
        locals.instanceID = state.instanceId;
        row_tags.push(locals.tag);
        var rowhtml = ejs.render(row_template, {locals: locals});
        $(settings.tableBodySelector).append(rowhtml);
    }
    function setRow(index, rowData)
    {
        var locals = setting.getRowTemplateData(rowData);
        locals.tag = row_tags[index];
        locals.instanceID = state.instanceId;
        var rowhtml = ejs.render(row_template, {locals: locals});
        $(settings.tableBodySelector + " tr").children().eq(index).replaceWith(rowhtml);
    }
    var state = {};
    state.populateData = function (data)
    {
        tableData = data;
        row_tags=[];
        $(settings.tableBodySelector).children().remove();
        for (var i in data)
        {
            addRow(data[i]);
        }
    };
    state.getData = function ()
    {
        return tableData;
    };
    state.hideEditor = function ()
    {
        addFormDialog.hideModal();
    };
    var editRowIndex = null;
    state.editItem = function (row_tag)
    {
        var i = row_tags.indexOf(row_tag);
        var formHtml = ejs.render(form_template, {locals: settings.getEditFormTemplateData(tableData[i])});
        editRowIndex = i;
        addFormDialog = modalController.createModal(formHtml, modalId);
        settings.formDOMAttached();
        addFormDialog.showModal();
    };
    function removeItemInternal(i)
    {
        var tag=row_tags[i];
        $(settings.tableBodySelector +" tr[row_tag="+tag+"]").remove();
        tableData.splice(i,1);
        row_tags.splice(i,1);
    }
    state.beforeDelete = null;
    state.removeItem = function (row_tag)
    {
        var i = row_tags.indexOf(row_tag);
        if (state.beforeDelete)
        {
            this.beforeDelete(tableData[i], function (success)
            {
                if (success)
                {
                    removeItemInternal(i);
                }
            });

        } else
            removeItemInternal(i);
    };
    state.instanceId = listMangerRouter.addInstance(state);
    return state;
}