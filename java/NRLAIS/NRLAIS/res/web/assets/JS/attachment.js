/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var attachment =
        {
            arrayBuffer: null,
            imageBytes: null,
            mimeType: null,
            clear:function()
            {
                this.arrayBuffer=null;
                this.imageBytes=null;
            },
            hasAttachment()
            {
                return this.arrayBuffer!=null;
            },
            openFile: function (event,callbackName)
            {
                if(callbackName)
                    this.callBack=function(){
                        eval(callbackName);
                    }
                var input = event.target;
                var reader = new FileReader();
                reader.onload = function () {
                    attachment.arrayBuffer = reader.result;
                    attachment.imageBytes = [].slice.call(new Uint8Array(attachment.arrayBuffer));
                    var extension = $(input).val().replace(/^.*\./, '');
                    if (extension === 'pdf') {
                        attachment.mimeType = 'application/pdf';
                    } else if (extension === 'jpeg') {
                        attachment.mimeType = 'image/jpeg';
                    } else if (extension === 'jpg') {
                        attachment.mimeType = 'image/jpg';
                    } else if (extension === 'png') {
                        attachment.mimeType = 'image/png';
                    } else {
                        attachment.mimeType = 'application/octet-stream';
                    }
                    if(attachment.callBack)
                        attachment.callBack();
                };
                reader.readAsArrayBuffer(input.files[0]);
            },
            callback:null,
        };