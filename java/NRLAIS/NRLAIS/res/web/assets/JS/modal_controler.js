var modalController =
        {
            sel: null,
            load: function (sel)
            {
                this.sel = sel;
            },
            createModal: function (html, id)
            {
                var _id;
                if (id)
                {
                    if (id.substring(0, 1) == "#")
                        _id = id.substring(1);
                    else
                        _id = id;
                }
                if (_id)
                {
                    $("#" + _id).remove();
                }
                var el = document.createElement("div");
                $(el).attr('class', "modal fade");
                $(el).attr('role', "dialog");
                $(el).html(html);
                if (_id)
                    $(el).attr('id', _id);
                $(this.sel).append(el);
                return  {
                    showModal: function ()
                    {
                        $(el).modal('show');
                    },
                    hideModal: function ()
                    {
                        $(el).modal('hide');
                    },
                    el: el
                };
            },
        };
