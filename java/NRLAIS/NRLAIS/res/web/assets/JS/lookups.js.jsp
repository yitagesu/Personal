<%@page import="com.intaps.nrlais.repo.*"%>
<%@page import="com.intaps.nrlais.*"%>
<%@page import="com.intaps.nrlais.model.*"%>
<%@page import="com.intaps.nrlais.model.tran.*"%>
<%@page import="com.intaps.nrlais.util.*"%>
<%@page import="com.intaps.nrlais.controller.*"%>
<%@page import="com.intaps.nrlais.controller.tran.*"%>
<%@page import="java.util.*"%>
<%@page pageEncoding="UTF-8" %>

var LU_TABLE=[];
var thisTable;
<%
    UserSession s=Startup.getSessionByRequest(request);
    MainFacade facade=new MainFacade(s);
    for(String table:LookupRepo.lookupTables){%>
//start <%=table%>
thisTable=[];
        <%for(IDName n:facade.getAllLookup(table)){%>
thisTable[<%=n.id%>]='<%=n.idNameText(s.lang()).name%>';
        <%}%>
LU_TABLE['<%=table%>']=thisTable;
//end<%=table%>
<%}%>

function lookUPCode(table,code)
{
    var t=LU_TABLE[table];
    if(t==null)
        return "";
    var ret=t[code];
    if(!ret)
        return "";
    return ret;
}