/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Tewelde
 * Created: Mar 19, 2018
 */


DROP TABLE nrlais_inventory.t_replicationlog;

--DROP TABLE nrlais_inventory.t_replication;
CREATE TABLE nrlais_inventory.t_replicationlog
(
    woredaid character varying(10) COLLATE pg_catalog."default" NOT NULL,
    repno integer NOT NULL,
    objuid uuid NOT NULL,
    objecttype integer NOT NULL,
    "operation" character(1) COLLATE pg_catalog."default" NOT NULL,
    dml text,
    log_time bigint,
    CONSTRAINT pk_t_replicationlog PRIMARY KEY (woredaid,repno)
)