/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Tewelde
 * Created: Dec 26, 2017
 */
--add transaction_data table
--DROP TABLE nrlais_inventory.t_transaction_data;
CREATE TABLE nrlais_inventory.t_transaction_data
(
    tx_uid uuid NOT NULL,
    tx_data json NULL,
    CONSTRAINT t_transaction_data_pkey PRIMARY KEY (tx_uid)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE nrlais_inventory.t_transaction_data
    OWNER to postgres;
