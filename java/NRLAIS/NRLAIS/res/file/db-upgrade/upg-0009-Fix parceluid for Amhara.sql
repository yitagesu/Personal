Update nrlais_inventory.t_rights 
set parceluid=(Select  parceluid from nrlais_inventory.v_holding_parcel where holdinguid=t_rights.holdinguid limit 1);
insert into nrlais_inventory.t_rights
Select uuid_generate_v4() as uid, 
r.csaregionid,
r.nrlais_zoneid,
r.nrlais_woredaid,
r.nrlais_kebeleid,
r.syscreateby,
r.syscreatedate,
r.syslastmodby,
r.syslastmoddate,
r.notes,
r.righttype,
r.sharenumerator,
r.sharedenominator,
r.startleasedate,
r.endleasedate,
r.leaseamount,
r.leaseref,
r.startrentdate,
r.endrentdate,
r.rentsize,
r.startcroppingdate,
r.endcroppingdate,
r.holdinguid,
r.partyuid,
r.sourcetxuid,
r.currenttxuid,
x.parceluid 
from 
(Select * from nrlais_inventory.v_holding_parcel 
where parceluid not in (Select parceluid from nrlais_inventory.t_rights)) x
inner join nrlais_inventory.t_rights r 
on r.holdinguid=x.holdinguid;