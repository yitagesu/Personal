/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Tewelde
 * Created: Mar 19, 2018
 */

CREATE OR REPLACE FUNCTION nrlais_tx_pkg.write_rep_log(
	wid character varying,
	ouid uuid,
	otype integer,
	oper character varying)
    RETURNS integer
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

DECLARE
	maxn integer;
BEGIN	
	maxn=(Select max(repno) from nrlais_inventory.t_replicationlog where woredaid=wid);
	if(maxn is null) then
		maxn=0;
	end if;
	INSERT INTO nrlais_inventory.t_replicationlog(
		woredaid, repno, objuid,objecttype , operation)
			VALUES (wid,maxn+1,ouid,otype,oper);
	RETURN maxn+1;
END;

$BODY$;

ALTER FUNCTION nrlais_tx_pkg.write_rep_log(character varying, uuid, integer, character varying)
    OWNER TO postgres;
