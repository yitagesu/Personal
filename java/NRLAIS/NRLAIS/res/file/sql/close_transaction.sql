/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Tewelde
 * Created: Feb 8, 2018
 */

delete from nrlais_inventory.t_transactioncontent where txuid='@txuid'::uuid;
delete from nrlais_transaction.t_partycontactdata where currenttxuid='@txuid'::uuid;
delete from nrlais_transaction.t_partyrole where currenttxuid='@txuid'::uuid;;
delete from nrlais_transaction.t_partyrelationship where currenttxuid='@txuid'::uuid;;
delete from nrlais_transaction.t_restrictions where currenttxuid='@txuid'::uuid;;
delete from nrlais_transaction.t_rights where currenttxuid='@txuid'::uuid;;
delete from nrlais_transaction.t_party where currenttxuid='@txuid'::uuid;;
delete from nrlais_transaction.t_sys_fc_holding where currenttxuid='@txuid'::uuid;;
delete from nrlais_transaction.t_holdings where currenttxuid='@txuid'::uuid;;
delete from nrlais_transaction.fdconnector where currenttxuid='@txuid'::uuid;;
delete from nrlais_transaction.t_parcels where currenttxuid='@txuid'::uuid;

Update nrlais_inventory.t_transaction set syssessionid=null where uid='@txuid'::uuid;
SELECT nrlais_tx_pkg.unlocktx('@txuid');