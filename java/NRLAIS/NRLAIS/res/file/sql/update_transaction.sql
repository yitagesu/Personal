Update nrlais_inventory.t_transaction
set	
    csaregionid=@csaregionid, 
    nrlais_zoneid=@nrlais_zoneid, 
    nrlais_woredaid=@nrlais_woredaid, 
    nrlais_kebeleid=@nrlais_kebeleid, 
    syslastmodby=@syslastmodby, 
    syslastmoddate=@syslastmoddate, 
    notes=@notes, 
    txyear=@txyear, 
    txdate=@txdate, 
    syssessionid=@syssessionid::uuid, 
    spatialtask=@spatialtask
where uid=@uid::uuid