/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Tewelde
 * Created: Jan 25, 2018
 */

INSERT INTO nrlais_historic.t_restrictions(
	id, csaregionid, nrlais_zoneid, nrlais_woredaid, nrlais_kebeleid, inventoryid, sourcetx, archivetx, archivedate, type, startdate, enddate, holdingid, partyid, restriction_attr
)
	VALUES (
	@id::uuid, 
@csaregionid, 
@nrlais_zoneid, 
@nrlais_woredaid, 
@nrlais_kebeleid, 
@inventoryid::uuid, 
@sourcetx::uuid, 
@archivetx::uuid, 
@archivedate::uuid, 
@type, 
@startdate, 
@enddate, 
@holdingid::uuid, 
@partyid::uuid, 
@data::json
);