/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Tewelde
 * Created: Jan 6, 2018
 */

Update nrlais_inventory.t_transaction set 
txstatus=@newstatus,
syslastmodby=@syslastmodby,
syslastmoddate=@syslastmoddate
where uid=@txuid::uuid;

INSERT INTO nrlais_inventory.t_transaction_flow(
	uid, csaregionid, nrlais_zoneid, nrlais_woredaid, nrlais_kebeleid, syscreateby, syscreatedate, syslastmodby, syslastmoddate, notes, txuid, oldstatus, newstatus, flowseq)
	VALUES (
@uid::uuid, 
@csaregionid, 
@nrlais_zoneid, 
@nrlais_woredaid, 
@nrlais_kebeleid, 
@syscreateby, 
@syscreatedate, 
@syslastmodby, 
@syslastmoddate, 
@notes, 
@txuid::uuid, 
@oldstatus, 
@newstatus, 
(Select max(flowseq)+1 from nrlais_inventory.t_transaction_flow where txuid=@txuid::uuid)) ;