/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Tewelde
 * Created: Jan 5, 2018
 */

INSERT INTO nrlais_transaction.t_rights(
	uid, csaregionid, nrlais_zoneid, nrlais_woredaid, nrlais_kebeleid, syscreateby, syscreatedate, syslastmodby, syslastmoddate, 
notes, righttype, sharenumerator, sharedenominator, startleasedate, endleasedate, leaseamount, leaseref, startrentdate, endrentdate, 
rentsize, startcroppingdate, endcroppingdate, holdinguid, partyuid, editstatus, currenttxuid,parceluid)
	VALUES (
@uid::uuid, 
@csaregionid, 
@nrlais_zoneid, 
@nrlais_woredaid, 
@nrlais_kebeleid, 
@syscreateby, 
@syscreatedate, 
@syslastmodby, 
@syslastmoddate, 
@notes, 
@righttype, 
@sharenumerator, 
@sharedenominator, 
@startleasedate, 
@endleasedate, 
@leaseamount, 
@leaseref, 
@startrentdate, 
@endrentdate, 
@rentsize, 
@startcroppingdate, 
@endcroppingdate, 
@holdinguid::uuid, 
@partyuid::uuid, 
'n', 
@currenttxuid::uuid,
@parceluid::uuid
);

INSERT INTO nrlais_inventory.t_transactioncontent(
	uid, csaregionid, nrlais_zoneid, nrlais_woredaid, nrlais_kebeleid, syscreateby, syscreatedate, syslastmodby, syslastmoddate, notes, txuid, inputobjecttype, inputobjectuid, outputobjecttype, outputobjectuid, task, isprimary, status)
	VALUES (
uuid_generate_v4(), 
@csaregionid, 
@nrlais_zoneid, 
@nrlais_woredaid, 
@nrlais_kebeleid, 
@syscreateby, 
@syscreatedate, 
@syslastmodby, 
@syslastmoddate, 
@notes, 
@currenttxuid::uuid, 
10, 
@uid::uuid, 
null,
null, 
'AWFSS Task', 
true, 
2
);