/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.controller.tran;

import com.intaps.nrlais.controller.ViewControllerBase;
import com.intaps.nrlais.model.tran.ManualTransactionData;
import com.intaps.nrlais.repo.ManualLRManipulatorFacade;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Tewelde
 */
public class ManualLRManipulatorViewController extends ViewControllerBase{
    
    String tranUID;
    ManualLRManipulatorFacade lrFacade;
    public ManualLRManipulatorViewController(HttpServletRequest request, HttpServletResponse response) {
        super(request, response);
        tranUID=request.getParameter("tran_uid");
        lrFacade=new ManualLRManipulatorFacade(session);
    }

    public Object processCommand(ManualTransactionData.LrOperation data) throws Exception {
        return lrFacade.executeOpertion(tranUID, data);
    }
    
}
