/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.model.tran;

import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.SourceDocument;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Tewelde
 */
public class RestrictionTransactionData extends LATransaction.TransactionData {

    public String holdingUID = null;
    public int restrictionType;
    public long startDate;
    public long endDate;
    public List<Applicant> applicants = new ArrayList<Applicant>();
    public List<PartyItem> restrictionParties = new ArrayList<PartyItem>();
    public List<ParcelTransfer> restrictedParcels=new ArrayList<ParcelTransfer>();

    public SourceDocument landHoldingCertificateDoc = null;
}
