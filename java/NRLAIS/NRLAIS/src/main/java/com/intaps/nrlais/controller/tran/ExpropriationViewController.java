/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.controller.tran;

import com.intaps.nrlais.Startup;
import com.intaps.nrlais.model.IDName;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.SourceDocument;
import com.intaps.nrlais.model.tran.Applicant;
import com.intaps.nrlais.model.tran.ParcelTransfer;
import com.intaps.nrlais.model.tran.Partition;
import com.intaps.nrlais.model.tran.PartyItem;
import com.intaps.nrlais.model.tran.PartyParcelShare;
import com.intaps.nrlais.model.tran.ExpropriationTransactionData;
import com.intaps.nrlais.repo.MainFacade;
import com.intaps.nrlais.repo.RepoBase;
import com.intaps.nrlais.repo.TransactionFacade;
import com.intaps.nrlais.util.GSONUtil;
import com.intaps.nrlais.util.INTAPSDateUtils;
import com.intaps.nrlais.util.INTAPSLangUtils;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
/**
 *
 * @author yitagesu
 */
public class ExpropriationViewController extends TransactionViewController<ExpropriationTransactionData>{
    
    public LADM.Holding holding;
    public ExpropriationViewController(HttpServletRequest request, HttpServletResponse response) throws Exception {
        super(request, response, ExpropriationTransactionData.class, LATransaction.TRAN_TYPE_EXPROPRIATION);
    }
     public ExpropriationViewController(HttpServletRequest request, HttpServletResponse response, boolean loadData) throws Exception {
        super(request, response, ExpropriationTransactionData.class, LATransaction.TRAN_TYPE_EXPROPRIATION, loadData);
        if(loadData){
            if(data.holdingUID == null){
                holding=new LADM.Holding();
            }else{
                if(tran.status == LATransaction.TRAN_STATUS_ACCEPTED || tran.status==LATransaction.TRAN_STATUS_FINISHED){
                    holding=mainFacade.getFromHistoryByArchiveTx(tran.transactionUID, data.holdingUID).holding;
                }else{
                    holding=mainFacade.getLADM("nrlais_inventory", data.holdingUID).holding;
                }
            }
            kebele=mainFacade.getKebele(holding.nrlais_kebeleid);
            woreda=mainFacade.getWoreda(holding.nrlais_woredaid);
        }
    }
   public Applicant partyApplicant(String partyUID){
       for(Applicant ap: this.data.applicants){
           if(partyUID.equals(ap.selfPartyUID)){
               return ap;
           }
       }
       return null;
   }  
   
   public String letterOfAttroneyText(Applicant ap){
       if(ap==null){
           return "";
       }
       if(ap.letterOfAttorneyDocument==null){
           return "";
       }
       return ap.letterOfAttorneyDocument.refText;
   } 
   public String letterOfAttroneyLink(Applicant ap){
       if(ap==null){
           return "";
       }
       return showDocumentLink(ap.letterOfAttorneyDocument);
   }
   public String decisionOfWoredaAdministrationText(){
       if(this.data.decisionOfWoredaAdministration==null){
           return "";
       }
       return this.data.decisionOfWoredaAdministration.refText;
               
   }
   public String decisionOfWoredaAdministrationLink(){
       return showDocumentLink(this.data.decisionOfWoredaAdministration);
   }
   public String proofOfpaymentOfCompensationText(){
       if(this.data.proofOfpaymentOfCompensation==null){
           return "";
       }
       return this.data.proofOfpaymentOfCompensation.refText;
   }
   public String proofOfpaymentOfCompensationLink(){
       return showDocumentLink(this.data.proofOfpaymentOfCompensation);
   }
   public String landHoldingCertificateText(){
       if(this.data.landHoldingCertificate == null){
           return "";
       }
       return this.data.landHoldingCertificate.refText;
   }
   public String landHoldingCertificateLink(){
       return showDocumentLink(this.data.landHoldingCertificate);
   }
   public ParcelTransfer transfer(String parcelUID) {
        for(ParcelTransfer t: this.data.transfers)
            if(t.parcelUID.equals(parcelUID))
                return t;
        
        return null;
    }
   public LADM.Parcel transferParcel(ParcelTransfer t)
    {
        return holding.getParcel(t.parcelUID);
    }
   
   public String holdingJson()
    {
        return GSONUtil.toJson(this.holding);
    }
   
   public String holdingLink()
    {
        String ret="/holding/holding_information_viewer_page.jsp?holdingUID="+this.holding.holdingUID;
        if(tran.isCommited())
            ret+="&tran_uid="+tran.transactionUID;
        return ret;
    }
}
