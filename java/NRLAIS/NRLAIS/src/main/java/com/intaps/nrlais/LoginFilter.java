/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais;

/**
 *
 * @author user
 */
import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class LoginFilter implements Filter {

    @Override
    public void init(FilterConfig config) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        HttpSession session = request.getSession(false);

     String loginURI = request.getContextPath() + "/login.jsp";
        boolean cmss = false,api = false;
        String requestURI = request.getRequestURI();
        String[] split = requestURI.split("/");
        if(split[1].equals("cmss"))
           cmss = true;
        if(split[1].equals("api"))
            api = true;
        
        boolean loggedIn = session != null;// &&Startup.isSessionActive(Startup.getRequestSessionID(request));
        boolean loginRequest = request.getRequestURI().equals(loginURI);

        if (loggedIn || loginRequest || cmss || api) {
            chain.doFilter(request, response);
        } else {
            response.sendRedirect(loginURI);
        }
    }

    @Override
    public void destroy() {
    }

}
