/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.repo;

import com.intaps.nrlais.UserSession;
import com.intaps.nrlais.model.MultiLangString;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.intaps.nrlais.model.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author yitagesu
 */
public class WoredaRepo extends RepoBase{
    public WoredaRepo(UserSession session) {
        super(session);
    }
 
    public List<WoredaInfo> getAllWoredas(final Connection con) throws SQLException {
        String woredaID = super.session.worlaisSession.getWoredaID(con);
        List<WoredaInfo> ret = new ArrayList<>();
        try (ResultSet rs = con.prepareStatement("Select nrlais_woredaid,woredanameeng,woredanameamharic,woredanametigrinya,woredanameoromifya from nrlais_sys.t_woredas where nrlais_woredaid='" + woredaID + "' order by nrlais_woredaid").executeQuery()) {
            while (rs.next()) {
                WoredaInfo k = new WoredaInfo();
                k.woredaID = rs.getString("nrlais_woredaid");
                k.name = new MultiLangString(rs.getString("woredanameeng"), rs.getString("woredanameamharic"), rs.getString("woredanameoromifya"),
                        rs.getString("woredanametigrinya"));
                ret.add(k);
            }
        }
        return ret;
    }

   
    public WoredaInfo get(final Connection con, String woredaid) throws SQLException {
        try (ResultSet rs = con.prepareStatement("Select nrlais_woredaid,woredanameeng,woredanameamharic,woredanametigrinya,woredanameoromifya from nrlais_sys.t_woredas where nrlais_woredaid='" + woredaid + "'").executeQuery()) {
            if (rs.next()) {
                WoredaInfo w = new WoredaInfo();
                w.woredaID = rs.getString("nrlais_woredaid");
                w.name = new MultiLangString(rs.getString("woredanameeng"), rs.getString("woredanameamharic"), rs.getString("woredanameoromifya"),
                        rs.getString("woredanametigrinya"));
                return w;
            }
            return null;
        }
    }
}
