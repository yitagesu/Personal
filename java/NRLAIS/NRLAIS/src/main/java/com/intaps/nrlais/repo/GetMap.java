/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.repo;

import com.google.gson.internal.LinkedTreeMap;
import com.intaps.nrlais.Startup;
import com.intaps.nrlais.UserSession;
import com.intaps.nrlais.model.KebeleInfo;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.map.GeoJSON;
import com.intaps.nrlais.model.map.MapData;
import com.intaps.nrlais.model.map.MapperEngineModel;
import com.intaps.nrlais.model.map.MapperEngineModel.MEBox;
import com.intaps.nrlais.util.GSONUtil;
import com.intaps.nrlais.util.NamedPreparedStatement;
import com.intaps.nrlais.util.ResourceUtils;
import com.vividsolutions.jts.io.ParseException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Tewelde
 */
public class GetMap extends RepoBase {

    MapData getTranscationParcels(Connection con, String tranUID) throws SQLException, IOException, ParseException {
        GetTransaction tranRepo = new GetTransaction(session);
        List<String> input = tranRepo.getTransactionInputHoldings(con, tranUID);
        List<String> output = tranRepo.getTransactionOutputHoldings(con, tranUID);
        LATransaction tran = tranRepo.get(con, tranUID, false);

        HashMap<String, LADM.Parcel> inputParcel = new HashMap<>();
        HashMap<String, LADM.Parcel> outputParcel = new HashMap<>();
        if (tran.isCommited()) {
            for (String ip : input) {
                LADM.Holding h = new GetLADM(session, "nrlais_historic").getFromHistoryByArchiveTx(con, tranUID, ip).holding;
                h.parcels.forEach((p) -> {
                    inputParcel.put(p.parcelUID, p);
                });
            }
            for (String op : output) {
                LADM.Holding h = new GetLADM(session, "nrlais_historic").getByTx(con, tranUID, op).holding;
                h.parcels.forEach((p) -> {
                    outputParcel.put(p.parcelUID, p);
                });
            }
        } else {
            for (String ip : input) {
                LADM.Holding h = new GetLADM(session, "nrlais_inventory").get(con, ip, LADM.CONTENT_FULL).holding;
                h.parcels.forEach((p) -> {
                    inputParcel.put(p.parcelUID, p);
                });
            }
            for (String op : output) {
                LADM.Holding h = new GetLADM(session, "nrlais_transaction").get(con, op, LADM.CONTENT_FULL).holding;
                h.parcels.forEach((p) -> {
                    outputParcel.put(p.parcelUID, p);
                });
            }
        }
        for (java.util.Map.Entry<String, LADM.Parcel> e : outputParcel.entrySet()) {
            if (inputParcel.containsKey(e.getKey())
                    && inputParcel.get(e.getKey()).geometry.equals(e.getValue().geometry)) {
                inputParcel.remove(e.getKey());
            }
        }
        MapData ret = new MapData();
        ret.description = "Transaction parcels for: " + tranUID;
        org.postgis.Geometry g;
        GeoJSON gj = new GeoJSON();
        gj.type = "FeatureCollection";
        gj.totalFeatures = inputParcel.size() + outputParcel.size();
        gj.crs = GeoJSON.GJCRS.createByCRS("EPSG::20137");
        List<GeoJSON.GJFeature> features = new ArrayList<GeoJSON.GJFeature>();
        for (LADM.Parcel p : inputParcel.values()) {
            features.add(new GeoJSON.GJFeature("d_" + p.parcelUID, p.geometry, new GeoJSON.GJProperty[]{
                new GeoJSON.GJProperty("editStatus", 'd')
            }));
        }
        for (LADM.Parcel p : outputParcel.values()) {
            features.add(new GeoJSON.GJFeature("n_" + p.parcelUID, p.geometry, new GeoJSON.GJProperty[]{
                new GeoJSON.GJProperty("editStatus", 'n')
            }));
        }
        gj.features = features;
        ret.gj = gj;
        GeoJSON.BBox box = ret.gj.box();
        ret.x = (box.x1 + box.x2) / 2;
        ret.y = (box.y1 + box.y2) / 2;
        ret.width = box.x2 - box.x1;
        ret.height = box.y2 - box.y1;
        return ret;
    }

    public static class MapSetting {

        public double x = 462059;
        public double y = 767028;
        public double width = 50000;
        public double height = 50000;
    }

    public GetMap(UserSession session) {
        super(session);
    }

    public MapSetting getDefaultSetting(Connection con) throws SQLException {
        try (ResultSet rs = con.prepareStatement("SELECT ST_Extent(geometry) as bextent FROM nrlais_inventory.t_parcels where nrlais_woredaid='" + session.worlaisSession.getWoredaID(con) + "'").executeQuery()) {
            MapSetting ret = new MapSetting();;
            if (rs.next()) {
                Object obj = rs.getObject(1);
                if (obj instanceof org.postgis.PGbox2d) {
                    org.postgis.PGbox2d box = (org.postgis.PGbox2d) obj;
                    ret.x = (box.getLLB().x + box.getURT().x) / 2;
                    ret.y = (box.getLLB().y + box.getURT().y) / 2;
                    ret.width = box.getURT().x - box.getLLB().x;
                    ret.height = box.getURT().y - box.getLLB().y;
                }
            }

            return ret;
        }
    }

    public MapData getHoldingParcels(Connection con, String schema, String holdingUID) throws IOException, SQLException {

        String sql = ResourceUtils.getSql("get_holding_map.sql");
        sql = StringUtils.replaceEach(sql, new String[]{"@schema", "@holdinguid"}, new String[]{
            schema, holdingUID
        });
        try (ResultSet rs = con.prepareStatement(sql).executeQuery()) {
            if (rs.next()) {
                String json = rs.getString(1);
                org.postgis.PGbox2d box = (org.postgis.PGbox2d) rs.getObject(2);
                MapData ret = new MapData();
                ret.description = "Parcels of holding:" + holdingUID;
                ret.gj = GSONUtil.fromJson(GeoJSON.class, json);
                if (ret.gj != null) {
                    ret.gj.crs = GeoJSON.GJCRS.createByCRS("EPSG::20137");
                }
                ret.x = (box.getLLB().x + box.getURT().x) / 2;
                ret.y = (box.getLLB().y + box.getURT().y) / 2;
                ret.width = box.getURT().x - box.getLLB().x;
                ret.height = box.getURT().y - box.getLLB().y;
                return ret;
            }
            return null;
        }
    }

    public MapData getSingleParcel(Connection con, String schema, String parcelUID) throws IOException, SQLException, ParseException {

        LADM.Parcel p = new GetLADM(session, schema).getParcel(con, parcelUID, LADM.CONTENT_FULL);

        MapData ret = new MapData();
        ret.description = "Parcels :" + parcelUID;

        ret.gj = new GeoJSON();
        ret.gj.features=new ArrayList<>();
        ret.gj.features.add(new GeoJSON.GJFeature(parcelUID, p.geometry, new GeoJSON.GJProperty[0]));
        ret.gj.type = "FeatureCollection";
        ret.gj.totalFeatures = 1;
        ret.gj.crs = GeoJSON.GJCRS.createByCRS("EPSG::20137");

        GeoJSON.BBox box = ret.gj.box();
        ret.x = box.centerX();
        ret.y = box.centerY();
        ret.width = box.width();
        ret.height = box.height();
        return ret;

    }
    
    public MapData getKebeleGeo(Connection con, String schema, String kebeleCode) throws SQLException, ParseException{
        KebeleInfo k = new GetLADM(session, schema).getKeb(con, kebeleCode);
        MapData ret = new MapData();
        ret.description = "kebles :" + kebeleCode;

        ret.gj = new GeoJSON();
        ret.gj.features=new ArrayList<>();
        ret.gj.features.add(new GeoJSON.GJFeature(kebeleCode, k.geometry, new GeoJSON.GJProperty[0]));
        ret.gj.type = "FeatureCollection";
        ret.gj.totalFeatures = 1;
        ret.gj.crs = GeoJSON.GJCRS.createByCRS("EPSG::20137");

        GeoJSON.BBox box = ret.gj.box();
        ret.x = box.centerX();
        ret.y = box.centerY();
        ret.width = box.width();
        ret.height = box.height();
        return ret;
    }
}
