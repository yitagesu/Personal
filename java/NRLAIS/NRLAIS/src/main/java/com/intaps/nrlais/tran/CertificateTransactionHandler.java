/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.tran;

import com.intaps.nrlais.UserSession;
import com.intaps.nrlais.model.DocumentTypeInfo;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.SourceDocument;
import com.intaps.nrlais.model.tran.Applicant;
import com.intaps.nrlais.model.tran.CertificateReplacementTransactionData;
import com.intaps.nrlais.model.tran.ExpropriationTransactionData;
import com.intaps.nrlais.model.tran.ParcelTransfer;
import com.intaps.nrlais.model.tran.PartyItem;
import com.intaps.nrlais.repo.DocumentTypeRepo;

import com.intaps.nrlais.repo.GetLADM;
import com.intaps.nrlais.repo.LRManipulator;
import com.intaps.nrlais.repo.TransactionRepo;
import com.intaps.nrlais.util.EthiopianCalendar;
import com.intaps.nrlais.util.RationalNum;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 *
 * @author user
 */
public class CertificateTransactionHandler extends TransactionHandlerBase implements TransactionHandler {

    void registerTransaction(UserSession session, Connection con, LATransaction theTransaction, CertificateReplacementTransactionData tran) throws SQLException, Exception {
        LRManipulator lrm = new LRManipulator(session);
        TransactionRepo tranRepo = new TransactionRepo(session);
        GetLADM repo = new GetLADM(session, "nrlais_transaction");
        LADM ladm = repo.get(con, tran.holdingUID, LADM.CONTENT_FULL);
        LADM.Holding holding = ladm.holding;

        HashMap<String, Applicant> applicants = new HashMap<>();
        for (Applicant ap : tran.applicant) {
            if (applicants.containsKey(ap.selfPartyUID)) {
                throw new IllegalArgumentException("Duplicate applicant" + ap.selfPartyUID);
            }
            applicants.put(ap.selfPartyUID, ap);
        }
        for (LADM.Right r : holding.getHolders()) {
            if (!applicants.containsKey(r.partyUID)) {
                throw new IllegalArgumentException("Please Provide applicant information for" + r.party.getFullName());
            }
            applicants.get(r.partyUID).share = r.share;
            Applicant ap = applicants.get(r.partyUID);

            if (ap.idDocument == null) {
                throw new IllegalArgumentException("ID Document not provided for" + r.party.getFullName());
            } else {
                ap.idDocument.uid = UUID.randomUUID().toString();
                tranRepo.saveDocument(con, theTransaction, ap.idDocument);
            }
            if (ap.representative != null) {
                lrm.createParty(con, theTransaction, ap.representative);
                if (ap.letterOfAttorneyDocument == null) {
                    throw new IllegalArgumentException("Autorized Representation Document not provided for" + ap.representative.getFullName());

                }
                ap.letterOfAttorneyDocument.uid = UUID.randomUUID().toString();
                ap.letterOfAttorneyDocument.sourceType = DocumentTypeInfo.DOC_TYPE_LETTER_OF_ATTORNEY;
                tranRepo.saveDocument(con, theTransaction, ap.letterOfAttorneyDocument);
            }
            if (ap.pomDocument != null) {
                ap.pomDocument.uid = UUID.randomUUID().toString();
                if (r.party.maritalstatus == LADM.Party.MARITAL_STATUS_MARRIED) {
                    ap.pomDocument.sourceType = DocumentTypeInfo.DOC_TYPE_PROOF_OF_MARRAIGE;
                } else {
                    ap.pomDocument.sourceType = DocumentTypeInfo.DOC_TYPE_PROOF_OF_CELEBACY;
                }
            }

        }

        SourceDocument[] allDocs = new SourceDocument[]{tran.claimResolution};
        int[] allTypes = new int[]{DocumentTypeInfo.DOC_TYPE_ELDERS_WOREDA_RESOLUTION};
        DocumentTypeRepo docTypeRepo = new DocumentTypeRepo(session);
        for (int i = 0; i < allDocs.length; i++) {
            SourceDocument srcDoc = allDocs[i];
            if (srcDoc == null) {
                throw new IllegalArgumentException("Required Document of " + docTypeRepo.getDocumentType(con, srcDoc.sourceType) + "is not provided");
            }
            srcDoc.uid = UUID.randomUUID().toString();
            srcDoc.sourceType = allTypes[i];
            tranRepo.saveDocument(con, theTransaction, srcDoc);
        }

        HashMap<String, ParcelTransfer> transfers = new HashMap<String, ParcelTransfer>();
        tran.parcelUID.forEach((p) -> {
            transfers.put(p.parcelUID, p);
        });

    }

    @Override
    public void saveTransaction(UserSession session, Connection con, LATransaction theTransaction, boolean register) throws Exception {
        try {
            CertificateReplacementTransactionData tran = (CertificateReplacementTransactionData) theTransaction.data;
            LADM existing = new GetLADM(session, "nrlais_inventory").get(con, tran.holdingUID, LADM.CONTENT_FULL);
            String error = existing.getRuleErrorsAsString(theTransaction.time);
            if (!org.apache.commons.lang3.StringUtils.isEmpty(error)) {
                throw new IllegalArgumentException(error);
            }
            if (existing.holding.hasNoneHoldingRight()) {
                throw new IllegalAccessException("certificate replacement transaction can't be execute on a holding with none-holding rights");

            }
            if (!register) {
                return;
            }
            super.transferLandRecordToTransaction(session, con, theTransaction.transactionUID, existing);
            registerTransaction(session, con, theTransaction, tran);
        } catch (CloneNotSupportedException | IOException ex) {
            throw new SQLException("Cant Excute Transaction", ex);
        }
    }
}
