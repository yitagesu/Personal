/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.controller.routes.api;

import com.google.gson.stream.JsonWriter;
import com.intaps.nrlais.Startup;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.repo.GetLADM;
import com.intaps.nrlais.repo.MainFacade;
import com.intaps.nrlais.util.GSONUtil;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author yitagesu
 */
@WebServlet("/api/get_tran_parties")
public class APIGetTransactionParties extends APIBase {

    
    static class PartiesRet extends GSONUtil.JSONRet<List<LADM.Party>>
    {
        
    }
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PartiesRet ret=new PartiesRet();
        try {
            //String userName=assertLogedInUser(req, resp);
            String schema=req.getParameter("schema");
            String tranUID=req.getParameter("tran_uid");
            
            ret.res=new MainFacade(Startup.getSessionByRequest(req)).getPartiesByTran(schema,tranUID);
            ret.error=null;
            
        } catch (Exception ex) {
            Logger.getLogger(PartiesRet.class.getName()).log(Level.SEVERE, null, ex);
            ret.error=ex.getMessage();
            ret.res=null;    
        } 
        try (ServletOutputStream str = resp.getOutputStream()) {
                resp.setContentType("application/json");
                JsonWriter writer=new JsonWriter(new OutputStreamWriter(str,"UTF-8"));
                GSONUtil.getAdapter(PartiesRet.class).write(writer,ret);
                writer.close();
            }    
    }    
    
}
