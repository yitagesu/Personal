/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.repo.lrop;

import com.intaps.nrlais.UserSession;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.tran.ManualTransactionData;
import com.intaps.nrlais.repo.GetLADM;
import com.intaps.nrlais.repo.LRManipulator;
import com.intaps.nrlais.repo.ManualLRManipulatorFacade;
import com.intaps.nrlais.util.RationalNum;
import java.io.IOException;
import java.sql.Connection;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Tewelde
 */
public class LrOpExecTransferHolders implements ManualLRManipulatorFacade.LROPExecutor<ManualTransactionData.LrOpTransferHolders> {

    @Override
    public Object doOperation(UserSession session, Connection con, LATransaction tran, ManualTransactionData.LrOpTransferHolders op) throws Exception {
        LRManipulator lrm = new LRManipulator(session);
        GetLADM ladm = new GetLADM(session, "nrlais_transaction");

        LADM.Holding holding = ladm.get(con, op.sourceHolding, LADM.CONTENT_FULL).holding;
        LADM.Holding destHolding = ladm.get(con, op.destinationHolding, LADM.CONTENT_FULL).holding;
        List<String> holdings = new ArrayList<>();
        holdings.add(op.sourceHolding);
        holdings.add(op.destinationHolding);
        List<LADM.Right> destHolders=destHolding.getHolders();
        lrm.lrStartTransaction(con, tran, holdings);
        if(op.parties.size()==0)
            throw new IllegalArgumentException("No holding selected");
        
        for (String partyUID : op.parties) {
            LADM.Right sourceRight=null;
            
            for(LADM.Parcel parcel:holding.parcels)
            {
                boolean removed=false;
                for(LADM.Right r:parcel.getHolders())
                {
                    if(r.partyUID.equals(partyUID))
                    {
                        lrm.lrRemoveRight(con, tran,op.sourceHolding,parcel.parcelUID,r);
                        removed=true;
                        if(sourceRight==null)
                            sourceRight=r;
                        break;
                    }
                }
                if(!removed)
                    throw new IllegalArgumentException("Party not found as a holder");
            }
            if(sourceRight==null)
                throw new IllegalArgumentException("Party not found as a holder");
            LADM.Right newRight=new LADM.Right();
            newRight.party=sourceRight.party;
            newRight.partyUID=sourceRight.parcelUID;
            newRight.rightType=sourceRight.rightType;
            newRight.share=new RationalNum(1,op.parties.size()+destHolders.size());
            newRight.holdingUID=destHolding.holdingUID;
            
            for(LADM.Parcel parcel:destHolding.parcels)
            {
                newRight.parcelUID=parcel.parcelUID;
                lrm.lrAddRight(con, tran, op.destinationHolding,parcel.parcelUID, newRight);
            }            
        }
        lrm.finishTransaction(con, tran,false);
        return null;
    }
}
