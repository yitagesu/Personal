/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.repo.lrop;

import com.intaps.nrlais.UserSession;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.tran.ManualTransactionData;
import com.intaps.nrlais.repo.GetLADM;
import com.intaps.nrlais.repo.LRManipulator;
import com.intaps.nrlais.repo.ManualLRManipulatorFacade;
import java.io.IOException;
import java.sql.Connection;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Tewelde
 */
public class LrOpExecDeleteParcel implements ManualLRManipulatorFacade.LROPExecutor<ManualTransactionData.LrOpDeleteParcel> {

    @Override
    public Object doOperation(UserSession session, Connection con, LATransaction tran, ManualTransactionData.LrOpDeleteParcel op) throws Exception {
        LRManipulator lrm = new LRManipulator(session);
        GetLADM ladm = new GetLADM(session, "nrlais_transaction");
       
        LADM.Holding holding = ladm.getByParcel(con, op.parcelUID, LADM.CONTENT_FULL).holding;
        
        lrm.lrStartTransaction(con, tran, holding.holdingUID);
        lrm.lrRemoveAndDeleteParcel(con, tran, holding.holdingUID,op.parcelUID);
        lrm.finishTransaction(con, tran,false);
        return null;
    }

}
