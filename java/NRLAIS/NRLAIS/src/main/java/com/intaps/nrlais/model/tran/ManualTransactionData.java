/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.model.tran;

import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.util.RationalNum;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Tewelde
 */
public class ManualTransactionData extends LATransaction.TransactionData {

    public String holdingUID;

    public static class LrOperation {

        public int serialNo;
        public String opType;
    }

    public static class LrOpUpdateHoldingInfo extends LrOperation {

        public LADM.Holding holding;
    }

    public static class LrOpCreateParcel extends LrOperation {

        public List<LADM.Parcel> newParcelUIDs = new ArrayList<LADM.Parcel>();
    }

    public static class LrOpSplitParcel extends LrOperation {

        public String parcelUID;
        public int n;
    }

    public static class LrOpEditParcelBoundary extends LrOperation {

        public List<String> newParcelUIDs = new ArrayList<String>();
    }

    public static class LrOpEditParcelInfo extends LrOperation {

        public LADM.Parcel parcel;
    }

    public static class LrOpDeleteParcel extends LrOperation {

        public String parcelUID;
    }

    public static class LrOpSavePartyInfo extends LrOperation {

        public String holdingUID;
        public LADM.Party party;
    }

    public static class LrOpAddPartyInfo extends LrOperation {

        public String holdingUID;
        public LADM.Party party;
    }

    public static class LrOpDeleteParty extends LrOperation {

        public String holdingUID;
        public String partyUID;
    }

    public static class LrOpAddHolder extends LrOperation {

        public LADM.Right holder;
    }

    public static class LrOpUpdateHolder extends LrOperation {

        public String holdingUID;
        public String partyUID;
        public LADM.Party party;
        public RationalNum share;
    }

    public static class LrOpRemoveHolder extends LrOperation {

        public String holdingUID;
        public String partyUID;
    }

    public static class LrOpSaveRent extends LrOperation {

        public LADM.Right rent;

    }
    
    

    public static class LrOpUpdateRent extends LrOperation {

        public LADM.Right renter;
        public String parcel;
    }

    public static class LrOpRemoveRent extends LrOperation {

        public String rentRightUID;
    }

    public static class LrOpAddRestriction extends LrOperation {

        public ArrayList<LADM.Restriction> restrictors;
        public List<String> parcels;
    }

    public static class LrOpUpdateRestriction extends LrOperation {

        public LADM.Right renter;
        public String parcel;
    }

    public static class LrOpRemoveRestriction extends LrOperation {

        public String restrctionUID;
    }

    public static class LrOpUpateParcel extends LrOperation {

        public String holdingUID = null;
        public LADM.Parcel parcel;
    }

    public static class LrOpStartBoundaryCorrection extends LrOperation {

        public List<String> parcels;
    }

    public static class LrOpDeleteRent extends LrOperation {

        public String rightUID;
    }

    public static class LrOpSaveRestriction extends LrOperation {

        public LADM.Restriction restriction;
    }

    public static class LrOpDeleteRestriction extends LrOperation {

        public String restrictionUID;
    }

    public static class LrOpRemoveHolding extends LrOperation {

        public String holdingUID;
    }

    public static class LrOpTransferParcels extends LrOperation {

        public List<String> parcels;
        public String sourceHolding;
        public String destinationHolding;
    }
    
    public static class LrOpTransferHolders extends LrOperation {

        public List<String> parties;
        public String sourceHolding;
        public String destinationHolding;
    }

}
