/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.controller.tran;

import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.tran.Applicant;
import com.intaps.nrlais.model.tran.BoundaryCorrectionTransactionData;
import com.intaps.nrlais.model.tran.ConsolidationTransactionData;
import com.intaps.nrlais.util.INTAPSLangUtils;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Tewelde
 */
public class ConsolidationViewController extends TransactionViewController<ConsolidationTransactionData> {

    public ConsolidationViewController(HttpServletRequest request, HttpServletResponse response) throws Exception {
        super(request, response, ConsolidationTransactionData.class, LATransaction.TRAN_TYPE_CONSOLIDATION);
    }
    public List<LADM.Parcel> adjecentParcels;
    public LADM.Holding holding;
    public ConsolidationViewController(HttpServletRequest request, HttpServletResponse response, boolean loadData) throws Exception {
        super(request, response, ConsolidationTransactionData.class, LATransaction.TRAN_TYPE_CONSOLIDATION, loadData);
        if (loadData) {
            if (data.holdingUID == null) {
                holding=new LADM.Holding();
                adjecentParcels = new ArrayList<LADM.Parcel>();
            } else {
                
                if (tran.isCommited()) {
                    holding = mainFacade.getFromHistoryByArchiveTx(tran.transactionUID, data.holdingUID).holding;
                } else {
                    holding = mainFacade.getLADM("nrlais_inventory", data.holdingUID).holding;
                }                
                adjecentParcels = new ArrayList<LADM.Parcel>();
                for(String p:data.parcels)
                    adjecentParcels.add(holding.getParcel(p));
                
            }
            kebele = mainFacade.getKebele(holding.nrlais_kebeleid);
            woreda = mainFacade.getWoreda(holding.nrlais_woredaid);
        }
    }

    public Applicant partyApplicant(String partyUID) {
        for (Applicant ap : this.data.applicants) {
            if (partyUID.equals(ap.selfPartyUID)) {
                return ap;
            }
        }
        return null;
    }

    public String letterOfAttroneyText(Applicant ap) {
        if (ap == null) {
            return "";
        }

        if (ap.letterOfAttorneyDocument == null) {
            return "";
        }
        return ap.letterOfAttorneyDocument.refText;
    }

    public String letterOfAttroneyLink(Applicant ap) {
        if (ap == null) {
            return "";
        }
        return showDocumentLink(ap.letterOfAttorneyDocument);
    }
    
}