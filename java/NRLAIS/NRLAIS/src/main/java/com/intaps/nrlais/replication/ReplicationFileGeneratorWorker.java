/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.replication;

import com.google.gson.TypeAdapter;
import com.intaps.nrlais.NRLAISWorker;
import com.intaps.nrlais.Startup;
import com.intaps.nrlais.UserSession;
import com.intaps.nrlais.util.GSONUtil;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.omg.CORBA.portable.OutputStream;

/**
 *
 * @author Tewelde
 */
public class ReplicationFileGeneratorWorker extends NRLAISWorker {

    public static ReplicationFileGeneratorWorker worker = new ReplicationFileGeneratorWorker();

    String fileName = null;

    public String getFileName() {
        return fileName;
}
    public static class ReplicationFileGeneratorPar
    {
        public List<ReplicationFileGeneratorParItem> items;
    }
    public static class ReplicationFileGeneratorParItem {

        public String woredaID;
        public int repNo;
    }

    public static class ReplicationFileGeneratorStatusInfo extends StatusInfo {

        public String fileName;

        public ReplicationFileGeneratorStatusInfo(StatusInfo status, String fileName) {
            this.status = status.status;
            this.progress = status.progress;
            this.status_message = status.status_message;
            this.errorList = status.errorList;
            this.errorCount = status.errorCount;
            this.fileName = fileName;

        }
    }

    public static void zipFiles(ArrayList<String> files, String zipFile) {
        try {
            // create byte buffer
            byte[] buffer = new byte[1024];
            FileOutputStream fos = new FileOutputStream("data/tmp/"+zipFile);
            ZipOutputStream zos = new ZipOutputStream(fos);
            for (int i = 0; i < files.size(); i++) {
                File srcFile = new File("data/tmp/"+files.get(i));
                FileInputStream fis = new FileInputStream(srcFile);
                // begin writing a new ZIP entry, positions the stream to the start of the entry data
                zos.putNextEntry(new ZipEntry(srcFile.getName()));
                int length;
                while ((length = fis.read(buffer)) > 0) {
                    zos.write(buffer, 0, length);
                }
                zos.closeEntry();
                // close the InputStream
                fis.close();
            }
            // close the ZipOutputStream
            zos.close();

        } catch (IOException ioe) {
            System.out.println("Error creating zip file: " + ioe);
        }
    }

    static final DecimalFormat repNoFormat = new DecimalFormat("00000000");

    public void run(UserSession session, final ReplicationFileGeneratorPar pars) throws SQLException {
        synchronized (this) {
            if (status == STATUS_INITIALIZING || status == STATUS_RUNNING) {
                throw new IllegalStateException("Replication file generation is already running. Please wait until it is complete.");
            }

            ReplicationFacade facade = new ReplicationFacade(session);
            status = STATUS_INITIALIZING;
            status_message = "Initializaing";
            progress = 0;
            thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        synchronized (ReplicationFileGeneratorWorker.this) {
                            status = STATUS_RUNNING;
                            status_message = "Running";
                        }

                        int i = 0;

                        ArrayList<String> files = new ArrayList<>();
                        for (final ReplicationFileGeneratorParItem par : pars.items) {

                            List<ReplicationData> data = facade.getReplicationData(par.woredaID, par.repNo, 100);
                            for (ReplicationData d : data) {
                                String fn = "rep_" + d.woredaID + repNoFormat.format(d.data.get(0).repNo) + "_" + repNoFormat.format(d.data.get(d.data.size() - 1).repNo)
                                        + "_" + UUID.randomUUID().toString() + ".json";
                                String json = GSONUtil.toJson(d);
                                try (FileOutputStream output = new FileOutputStream("data/tmp/"+fn)) {
                                    org.apache.commons.io.IOUtils.write(json, output, "UTF-8");
                                }
                                files.add(fn);
                                i++;
                                progress = 0;
                                status_message = i + " files done";
                            }
                        }

                        if (i > 0) {
                            String fileName = "rep_" + UUID.randomUUID().toString() + ".zip";
                            zipFiles(files, fileName);
                            ReplicationFileGeneratorWorker.this.fileName = fileName;
                        }
                        else
                            ReplicationFileGeneratorWorker.this.fileName=null;
                        
                        synchronized (ReplicationFileGeneratorWorker.this) {
                            status = STATUS_DONE;
                            status_message = i + " files created";
                        }

                    } catch (Exception ex) {
                        status = STATUS_ERROR;
                        status_message = "Error\n" + ex.getMessage();
                    }
                }
            });
            thread.start();
        }

    }

    @Override
    public StatusInfo getStatus(int errorIndex, int errorCount) {
        return new ReplicationFileGeneratorStatusInfo(super.getStatus(errorIndex, errorCount), fileName);
    }
}
