/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.controller.routes.api;

import com.google.gson.stream.JsonWriter;
import com.intaps.nrlais.Startup;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.repo.GetLADM;
import com.intaps.nrlais.repo.MainFacade;
import com.intaps.nrlais.util.GSONUtil;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author yitagesu
 */
@WebServlet("/api/get_holding")
public class APIGetHolding extends APIBase {

    static class HoldingData
    {
        public List<LADM.Right> holders;
        public LADM.Holding holding;
        public HoldingData(LADM.Holding hodling)
        {
            this.holding=hodling;
            this.holders=this.holding.getHolders();
        }
    }
    static class HoldingRet extends GSONUtil.JSONRet<LADM.Holding>
    {
        
    }
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HoldingRet ret=new HoldingRet();
        try {
            //String userName=assertLogedInUser(req, resp);
            String schema=req.getParameter("schema");
            String holdingUID=req.getParameter("holding_uid");            
            LADM.Holding data= new MainFacade(Startup.getSessionByRequest(req)).getLADM(schema,holdingUID, LADM.CONTENT_FULL).holding;
            ret.res=data;
            ret.error=null;
            
        } catch (Exception ex) {
            Logger.getLogger(HoldingRet.class.getName()).log(Level.SEVERE, null, ex);
            ret.error=ex.getMessage();
            ret.res=null;    
        } 
        try (ServletOutputStream str = resp.getOutputStream()) {
                resp.setContentType("application/json");
                JsonWriter writer=new JsonWriter(new OutputStreamWriter(str,"UTF-8"));
                GSONUtil.getAdapter(HoldingRet.class).write(writer,ret);
                writer.close();
            }    
    }    
    
}
