/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.controller;

import com.intaps.nrlais.util.GSONUtil;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Tewelde
 */
public class HoldingSearchViewController extends ViewControllerBase{
    static class Settings 
    {
        public String containerEl=null;
        public boolean pickHolding = true;
        public boolean pickParcel = false;
        public boolean pickParty = false;
    }
    Settings setting;
    public HoldingSearchViewController(HttpServletRequest request, HttpServletResponse response) throws IOException {
        super(request, response);
        String s = request.getParameter("setting");
        if (StringUtils.isEmpty(s)) {
            this.setting = new HoldingSearchViewController.Settings();
        } else {
            this.setting = GSONUtil.getAdapter(HoldingSearchViewController.Settings.class).fromJson(s);
        }
    }
    public String objectID()
    {
        if(setting.containerEl==null)
            return "null";
        return "'#"+setting.containerEl+"'";
    }
    
}
