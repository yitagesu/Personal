/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.controller;

import com.intaps.nrlais.Startup;
import com.intaps.nrlais.repo.MainFacade;
import com.intaps.nrlais.worlais.WORLAISClient;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Tewelde
 */
public class LoginViewController {

    public String loginError = null;
    public String userName = "";
    public String password = "";
    public boolean abortView=false;
    public LoginViewController(HttpServletRequest request, HttpServletResponse response) {

        if ("POST".equalsIgnoreCase(request.getMethod())) {
            boolean logout=StringUtils.endsWithIgnoreCase("true",request.getParameter("logout"));
            if(logout)
            {
                Startup.removeSession(request);
                abortView=true;
                return;
            }
            userName = request.getParameter("username");
            password = request.getParameter("password");
            try {

                WORLAISClient client = new WORLAISClient();
                client.login(userName, password);
                Startup.addSession(request, userName,request.getParameter("lang"), client);
                if(client.isAdmin())
                    response.sendRedirect("/replication/replication.jsp");
                else 
                {
                    MainFacade mainFacade=new MainFacade(Startup.getSessionByRequest(request));
                    if("WORLAIS".equals(mainFacade.getSystemLevel()))
                        response.sendRedirect("/dashboard.jsp");
                    else
                        throw new IllegalStateException("Only admin user can login to this application");
                }
                abortView=true;
            } catch (Exception ex) {
                loginError = "Login failed." + ex.getMessage();
            }
        }
    }
}
