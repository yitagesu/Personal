/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.controller.tran;

import com.intaps.nrlais.model.KebeleInfo;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.tran.Applicant;
import com.intaps.nrlais.model.tran.InheritanceTransactionData;
import com.intaps.nrlais.model.tran.ParcelItem;
import com.intaps.nrlais.model.tran.ParcelTransfer;
import com.intaps.nrlais.model.tran.Partition;
import com.intaps.nrlais.model.tran.PartyItem;
import com.intaps.nrlais.model.tran.PartyParcelShare;
import com.intaps.nrlais.model.tran.SpecialCaseTransactionData;
import com.intaps.nrlais.util.GSONUtil;
import com.intaps.nrlais.util.INTAPSLangUtils;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Tewelde
 */
public class SpecialCaseViewController extends TransactionViewController<SpecialCaseTransactionData> {

    public SpecialCaseViewController(HttpServletRequest request, HttpServletResponse response) throws Exception {
        super(request, response, SpecialCaseTransactionData.class, LATransaction.TRAN_TYPE_SPECIAL_CASE);
    }

    public SpecialCaseViewController(HttpServletRequest request, HttpServletResponse response, boolean loadData) throws Exception {
        super(request, response, SpecialCaseTransactionData.class, LATransaction.TRAN_TYPE_SPECIAL_CASE, loadData);
        if (loadData) {
            if (tran.status != LATransaction.TRAN_STATUS_INITIATED && tran.status!=0 ) {
                for (ParcelItem pi : this.data.parcels) {
                     LADM.Parcel p;
                    if (tran.isCommited()) {
                        p= mainFacade.getHistoryParcelBySourceTx(tran.transactionUID, pi.parcel.parcelUID);
                    }
                    else{
                        p = mainFacade.getParcel("nrlais_transaction", pi.parcel.parcelUID);
                    }
                    pi.parcel=p;
                }
            }

            if (data.holding.nrlais_kebeleid != null) {
                kebele = mainFacade.getKebele(data.holding.nrlais_kebeleid);
                woreda = mainFacade.getWoreda(KebeleInfo.getWoredaID(data.holding.nrlais_kebeleid));
            }
        }
    }

    public List<PartyItem> applicants() {
        return this.data.applicants;
    }

    public List<ParcelItem> parcels() {
        return this.data.parcels;
    }

    public String parcelNo(LADM.Parcel parcel) {
        if (parcel.seqNo == 0) {
            return "";
        }
        return LADM.Parcel.formatParcelSeqNo(parcel.seqNo);
    }

    public String claimResolutionText() {
        if (this.data.claimResolutionDoc == null) {
            return "";
        }
        return this.data.claimResolutionDoc.refText;
    }

    public String claimResolutionLink() {
        return showDocumentLink(this.data.claimResolutionDoc);
    }

    public String landHoldingText() {
        if (this.data.firtLevelLandHoldingBook == null) {
            return "";
        }
        return this.data.firtLevelLandHoldingBook.refText;
    }

    public String landHoldingLink() {
        return showDocumentLink(this.data.firtLevelLandHoldingBook);
    }

//    @Override
//    public List<CertificateLink> certficateLinks() throws SQLException, IOException {
//        ArrayList<CertificateLink> ret = new ArrayList<>();
//        for (ParcelItem pi : this.data.parcels) {
//            CertificateLink link = new CertificateLink();
//            link.upid = pi.parcel.upid;
//            LADM.Parcel l = mainFacade.getHistoryParcelBySourceTx(this.tran.transactionUID, pi.parcel.parcelUID);
//            if (l == null || l.rights.size() == 0) {
//                continue;
//            }
//            for (LADM.Right r : l.getHolders()) {
//                link.owner = INTAPSLangUtils.appendStringList(link.owner, ", ", r.party.getFullName());
//            }
//            link.url = "javascript:transcation_showCertiricate('" + tran.transactionUID + "','" + l.rights.get(0).holdingUID + "','" + pi.parcel.parcelUID + "')";
//            link.upid = l.upid;
//            ret.add(link);
//
//        }
//        return ret;
//    }

}
