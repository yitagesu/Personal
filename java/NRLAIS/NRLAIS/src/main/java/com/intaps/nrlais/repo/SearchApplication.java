/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.repo;

import com.intaps.nrlais.UserSession;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.SearchApplicationPars;
import com.intaps.nrlais.util.INTAPSLangUtils;
import com.intaps.nrlais.util.ResourceUtils;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Tewelde
 */
public class SearchApplication extends RepoBase {

    public SearchApplication(UserSession session) {
        super(session);
    }

    String getSql(SearchApplicationPars pars, boolean count) throws IOException {
        String cr = null;
        if (!StringUtils.isEmpty(pars.name1)) {
            cr = INTAPSLangUtils.appendStringList(cr, " and ", "(t_party.name like  '" + pars.name1 + "%' or gparty.name like '" + pars.name1 + "')");
        }
        if (!StringUtils.isEmpty(pars.name2)) {
            cr = INTAPSLangUtils.appendStringList(cr, " and ", "(t_party.fathersname like  '" + pars.name2 + "%' or gparty.fathersname like '" + pars.name2 + "')");
        }
        if (!StringUtils.isEmpty(pars.name3)) {
            cr = INTAPSLangUtils.appendStringList(cr, " and ", "t_party.grandfathersname like  '" + pars.name3 + "%' or gparty.grandfathersname like '" + pars.name3 + "')");
        }
        if (!StringUtils.isEmpty(pars.kebele)) {
            cr = INTAPSLangUtils.appendStringList(cr, " and ", "t_holdings.nrlais_kebeleid like  '" + pars.kebele + "%'");
        }
        if (pars.parcelSeqNo > 0) {
            cr = INTAPSLangUtils.appendStringList(cr, " and ", "t_parcels.seqnr=" + pars.parcelSeqNo);
        }
        if (pars.holdingSeqNo > 0) {
            cr = INTAPSLangUtils.appendStringList(cr, " and ", "t_parcels.seqnr=" + pars.holdingSeqNo);
        }
        if (pars.transactionType > 0) {
            cr = INTAPSLangUtils.appendStringList(cr, " and ", "t_transaction.transactiontype=" + pars.transactionType);
        }
        if (pars.transactionStatus.size()>0) {
            String list=null;
            for(int s:pars.transactionStatus)
            {
                list=INTAPSLangUtils.appendStringList(list,",", Integer.toString(s));
            }
            cr = INTAPSLangUtils.appendStringList(cr, " and ", "t_transaction.txstatus in ("+list+")");
        }
        if (pars.filterDateFrom) {
            cr = INTAPSLangUtils.appendStringList(cr, " and ", "t_transaction.txstatus=" + pars.transactionStatus);
        }
        
         if (pars.applicationSeqNo > 0) {
            cr = INTAPSLangUtils.appendStringList(cr, " and ", "t_transaction.txseqnr=" + pars.applicationSeqNo);
        }
        String sql = ResourceUtils.getSql(count ? "search_tran_count.sql" : "search_tran.sql");
        sql = StringUtils.replaceEach(sql, new String[]{"@cr"}, new String[]{cr});
        return sql;
    }

    public int count(final Connection con, SearchApplicationPars pars) throws SQLException, IOException {
        String sql = getSql(pars, true);
        try (ResultSet rs = con.prepareStatement(sql).executeQuery()) {
            if (rs.next()) {
                return rs.getInt(1);
            }
            return 0;
        }
    }

    public List<LATransaction> searchTransaction(final Connection con, SearchApplicationPars pars, boolean includeDetail, int pageOffset, int pageSize) throws IOException, SQLException {

        List<LATransaction> ret = new ArrayList<>();
        String sql = getSql(pars, false) + " offset " + pageOffset + " limit " + pageSize;
        try (ResultSet rs = con.prepareStatement(sql).executeQuery()) {
            while (rs.next()) {
                LATransaction p = new GetTransaction(super.session).get(con, rs.getString("uid"), includeDetail);
                ret.add(p);
            }
        }
        return ret;

    }
}
