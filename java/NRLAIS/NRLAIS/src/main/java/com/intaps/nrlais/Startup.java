/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.tran.CertificateReplacementTransactionData;
import com.intaps.nrlais.model.tran.BoundaryCorrectionTransactionData;
import com.intaps.nrlais.model.tran.ConsolidationTransactionData;
import com.intaps.nrlais.model.tran.DivorceTransactionData;
import com.intaps.nrlais.model.tran.ExOfficioTransactionData;
import com.intaps.nrlais.model.tran.ExchangeTransactionData;
import com.intaps.nrlais.model.tran.GiftTransactionData;
import com.intaps.nrlais.model.tran.ReallocationTransactionData;
import com.intaps.nrlais.model.tran.RentTransactionData;
import com.intaps.nrlais.model.tran.ExpropriationTransactionData;
import com.intaps.nrlais.model.tran.InheritanceWithOutWillTransactionData;
import com.intaps.nrlais.model.tran.InheritanceWithWillTransactionData;
import com.intaps.nrlais.model.tran.RestrictiveInterestTransactionData;
import com.intaps.nrlais.model.tran.ServitudeTransactionData;
import com.intaps.nrlais.model.tran.SimpleCorrectionData;
import com.intaps.nrlais.model.tran.SpecialCaseTransactionData;
import com.intaps.nrlais.model.tran.SplitHoldingTransactionData;
import com.intaps.nrlais.replication.AutoReplicationWorker;
import com.intaps.nrlais.util.GSONUtil;
import com.intaps.nrlais.worlais.WORLAISClient;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.WebResourceRoot;
import org.apache.catalina.core.StandardContext;
import org.apache.catalina.startup.Tomcat;
import org.apache.catalina.webresources.DirResourceSet;
import org.apache.catalina.webresources.StandardRoot;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author yitagesu
 */
public class Startup {

    static Properties appConfig = null;
    final static HashMap<String, UserSession> activeSesssons = new HashMap<String, UserSession>();

    public static void addSession(HttpServletRequest req, String userName, String lang, WORLAISClient worlaisSession) throws IOException {
        synchronized (activeSesssons) {
            UserSession session = new UserSession();
            req.getSession().setMaxInactiveInterval(60*60);
            session.userName = userName;
            session.setLang(lang);
            session.sessionID = req.getSession().getId();
            session.worlaisSession = worlaisSession;
            activeSesssons.put(session.sessionID, session);
            onUserSessionCreated(session);
        }
    }

    public static void onUserSessionCreated(UserSession session) {
        try {
            //LiveTest.postTransactionTest(session);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static Connection getNRLAISConnection() throws SQLException {
        String url = Startup.getAppConfig("worlais-db");
        String user = Startup.getAppConfig("worlais-user-name");
        String password = Startup.getAppConfig("worlais-password");
        return DriverManager.getConnection(url, user, password);
    }

    public static String getRequestSessionID(HttpServletRequest req) {
        String sessionID = req.getParameter("session_id");
        if (org.apache.commons.lang3.StringUtils.isEmpty(sessionID)) {
            return req.getSession().getId();
        }
        return sessionID;
    }

    public static void removeSession(HttpServletRequest req) {
        synchronized (activeSesssons) {
            activeSesssons.remove(getRequestSessionID(req));
        }
    }

    public static boolean isSessionActive(String sessionID) {
        synchronized (activeSesssons) {
            return activeSesssons.containsKey(sessionID);
        }
    }

    public static UserSession getSessionByJSession(String jsessionid) {
        return getSessionByJSession(jsessionid, true);
    }

    public static UserSession getSessionByJSession(String jsessionid, boolean validate) {
        synchronized (activeSesssons) {
            if (activeSesssons.containsKey(jsessionid)) {
                return activeSesssons.get(jsessionid);
            }
            if (validate) {
                throw new IllegalArgumentException("JSession " + jsessionid + " is not valid");
            }
            return null;
        }
    }

    public static UserSession getSessionByRequest(HttpServletRequest req, boolean validate) {
        return getSessionByJSession(getRequestSessionID(req), validate);
    }

    public static UserSession getSessionByRequest(HttpServletRequest req) {
        return getSessionByJSession(getRequestSessionID(req), true);
    }

    public static List<UserSession> getActiveSesssions(String userName) {
        synchronized (activeSesssons) {
            List<UserSession> ret = new ArrayList<UserSession>();
            for (Map.Entry<String, UserSession> entry : activeSesssons.entrySet()) {
                if (StringUtils.equalsIgnoreCase(entry.getValue().userName, userName)) {
                    ret.add(entry.getValue());
                }
            }
            return ret;
        }
    }
    static void runAutoReplication()
    {
        UserSession session=new UserSession();
        session.worlaisSession=new WORLAISClient();
        session.userName="System";
        AutoReplicationWorker worker=new AutoReplicationWorker(session);
    }
    public static void main(String[] args) {
        try {
            //loading configuration
            logServerAction("Loading configuration");
            loadConfiguration();
            logServerAction("Loading configuration done");

            //initialize wire conneciton
            logServerAction("Establishing write connection");
            initWriteConnection();
            logServerAction("Establishing write connection done");
                        
            logServerAction("Starting autoreplication");
            runAutoReplication();
            logServerAction("Starting autoreplicationdone");
            
            logServerAction("Starting Tomcat embeded webserver");
            setupTomcat();
            logServerAction("Starting Tomcat embeded webserver done");

        } catch (Exception ex) {
            System.out.println("Error initializeing MASSREG PSS Webapp");
            ex.printStackTrace();
        }
    }

    private static void setupTomcat() throws IOException, ServletException, IllegalStateException, LifecycleException {
        Tomcat tomcat = new Tomcat();

        //configure temp directory temporary java classes Tomcat generates
        Path tempPath = Files.createTempDirectory("tomcat-base-dir");
        System.out.println("Tomcat-base-dir: " + tempPath.toString());
        tomcat.setBaseDir(tempPath.toString());

        //configure listening port for Tomcat
        tomcat.setPort(8540);

        //configure web content folder
        File webContentFolder = new File("res/web");
        if (!webContentFolder.exists()) {
            throw new IllegalStateException(webContentFolder.getAbsolutePath() + " not found");
        }

        StandardContext ctx = (StandardContext) tomcat.addWebapp("", webContentFolder.getAbsolutePath());

        // Additions to make @WebServlet work
        String cl = new File("target/classes/com/intaps/nrlais/controller/routes").getAbsolutePath();
        System.out.println("Additional class folder: " + cl);
        WebResourceRoot resources = new StandardRoot(ctx);
        resources.addPreResources(new DirResourceSet(resources, "/WEB-INF/classes", cl, "/"));
        ctx.setResources(resources);

        //Set execution independent of current thread context classloader (compatibility with exec:java mojo)
        ctx.setParentClassLoader(Startup.class.getClassLoader());
        tomcat.start();
        tomcat.getServer().await();
    }

    public static void logError(String error) {
        System.err.println(error);
    }

    public static void logServerAction(String action) {
        System.out.println(action);
    }

    static void loadConfiguration() throws IOException {
        appConfig = new Properties();
        File configFile = new File("res/file/config.properties");
        if (!configFile.exists()) {
            throw new IllegalStateException("Could not find res/file/config.properties");
        }
        try (InputStream resourceAsStream = new FileInputStream(configFile)) {
            appConfig.load(resourceAsStream);
        }
    }

    public static String getAppConfig(String key) {
        return getAppConfig(key, null);
    }

    public static String getAppConfig(String key, String def) {
        if (appConfig == null) {
            throw new IllegalStateException("Configuration not loaded");
        }
        return appConfig.getProperty(key, def);
    }

    //TODO: deserialize from stream instead of json string
    public static <T> T readPostedObject(HttpServletRequest req, Class<T> c) throws IOException {
        try (
                ServletInputStream ins = req.getInputStream();
                JsonReader reader = new JsonReader(new InputStreamReader(ins, "UTF-8"));) {
            if (c == LATransaction.class) {
                return (T) readTransaction(reader);
            }
            //String json = org.apache.commons.io.IOUtils.toString(ins, "UTF-8");
            T data = GSONUtil.getAdapter(c).read(reader);
            //T data = GSONUtil.getAdapter(c).fromJson(json);
            return data;
        }
    }

    public static <T> T[] readPostedArrayObject(HttpServletRequest req, Type t) throws IOException {
        try (
                ServletInputStream ins = req.getInputStream();
                JsonReader reader = new JsonReader(new InputStreamReader(ins, "UTF-8"));) {
            synchronized (GSONUtil.gson) {
                //String json = org.apache.commons.io.IOUtils.toString(ins, "UTF-8");
                TypeToken tt = TypeToken.getArray(t);
                Object data = GSONUtil.gson.getAdapter(tt).read(reader);
                return (T[]) data;
            }
        }
    }

    static LATransaction.TransactionData readFromJsonElement(int tranType, JsonElement el) {
        switch (tranType) {
            case LATransaction.TRAN_TYPE_DIVORCE:
                return GSONUtil.getAdapter(DivorceTransactionData.class).fromJsonTree(el);
            case LATransaction.TRAN_TYPE_RENT:
                return GSONUtil.getAdapter(RentTransactionData.class).fromJsonTree(el);
            case LATransaction.TRAN_TYPE_GIFT:
                return GSONUtil.getAdapter(GiftTransactionData.class).fromJsonTree(el);
            case LATransaction.TRAN_TYPE_INHERITANCE:
                return GSONUtil.getAdapter(InheritanceWithOutWillTransactionData.class).fromJsonTree(el);
            case LATransaction.TRAN_TYPE_INHERITANCE_WITHWILL:
                return GSONUtil.getAdapter(InheritanceWithWillTransactionData.class).fromJsonTree(el);            
            case LATransaction.TRAN_TYPE_REALLOCATION:
                return GSONUtil.getAdapter(ReallocationTransactionData.class).fromJsonTree(el);
            case LATransaction.TRAN_TYPE_REPLACE_CERTIFICATE:
                return GSONUtil.getAdapter(CertificateReplacementTransactionData.class).fromJsonTree(el);
            case LATransaction.TRAN_TYPE_EXPROPRIATION:
                return GSONUtil.getAdapter(ExpropriationTransactionData.class).fromJsonTree(el);
            case LATransaction.TRAN_TYPE_BOUNDARY_CORRECTION:
                return GSONUtil.getAdapter(BoundaryCorrectionTransactionData.class).fromJsonTree(el);
            case LATransaction.TRAN_TYPE_SPECIAL_CASE:
                return GSONUtil.getAdapter(SpecialCaseTransactionData.class).fromJsonTree(el);
            case LATransaction.TRAN_TYPE_CONSOLIDATION:
                return GSONUtil.getAdapter(ConsolidationTransactionData.class).fromJsonTree(el);
            case LATransaction.TRAN_TYPE_EX_OFFICIO:
                return GSONUtil.getAdapter(ExOfficioTransactionData.class).fromJsonTree(el);
            case LATransaction.TRAN_TYPE_EXCHANGE:
                return GSONUtil.getAdapter(ExchangeTransactionData.class).fromJsonTree(el);
            case LATransaction.TRAN_TYPE_RESTRICTIVE_INTEREST:
                return GSONUtil.getAdapter(RestrictiveInterestTransactionData.class).fromJsonTree(el);
            case LATransaction.TRAN_TYPE_SERVITUDE:
                return GSONUtil.getAdapter(ServitudeTransactionData.class).fromJsonTree(el);
            case LATransaction.TRAN_TYPE_SIMPLE_CORRECTION:
                return GSONUtil.getAdapter(SimpleCorrectionData.class).fromJsonTree(el);
            case LATransaction.TRAN_TYPE_SPLIT:
                return GSONUtil.getAdapter(SplitHoldingTransactionData.class).fromJsonTree(el);
            default:
                return null;
        }
    }

    public static LATransaction readTransaction(JsonReader reader) throws IOException {
        JsonElement el = GSONUtil.parse(reader);
        LATransaction test = GSONUtil.getAdapter(LATransaction.class).fromJsonTree(el);
        JsonObject obj = el.getAsJsonObject();
        el = obj.get("data");
        test.data = readFromJsonElement(test.transactionType, el);
        return test;
    }

    public static LATransaction readTransaction(String json) {
        JsonElement el = GSONUtil.parse(json);
        LATransaction test = GSONUtil.getAdapter(LATransaction.class).fromJsonTree(el);
        JsonObject obj = el.getAsJsonObject();
        el = obj.get("data");
        test.data = readFromJsonElement(test.transactionType, el);
        return test;
    }
    static Connection writeConnection = null;

    static void initWriteConnection() throws SQLException {
        String url = Startup.getAppConfig("worlais-db");
        String user = Startup.getAppConfig("worlais-user-name");
        String password = Startup.getAppConfig("worlais-password");
        writeConnection = DriverManager.getConnection(url, user, password);
    }

    public static Connection getWriteConnection() throws SQLException {
        synchronized (Startup.class) {
            if (!writeConnection.isValid(2000)) {
                initWriteConnection();
            }
            return writeConnection;
        }
    }

    public static String regionToLang(String csaregionid) {
        switch (csaregionid) {
            case "01":
                return "ti-et";
            case "04":
                return "om-et";
            default:
                return "am-et";
        }
    }

}
