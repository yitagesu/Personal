/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais;

import com.intaps.nrlais.lang.Lang;
import com.intaps.nrlais.util.Translations;
import com.intaps.nrlais.worlais.WORLAISClient;
import java.io.IOException;

/**
 *
 * @author Tewelde
 */
public class UserSession {
    
    public String userName;
    public String sessionID;
    public WORLAISClient worlaisSession;
    private Translations translations;

    public void setLang(String lang) throws IOException
    {
        translations=new Translations(lang);
    }
    public String lang()
    {
        return translations.lang();
    }
    public String getSessionGUID() {
        int i = worlaisSession.getSesionID().indexOf("-");
        String  ret= worlaisSession.getSesionID().substring(i - 8, i - 8 + 36);
        return ret;
    }

    public String getUserFullName(String userName) {
        return userName;
    }

    public Translations translations() {
        return translations;
    }
    public String text(String text,Object ... pars)
    {
        return Lang.tran(this.lang(), text, pars);
    }
    
}
