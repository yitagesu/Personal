
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.tran;

import com.intaps.nrlais.UserSession;
import com.intaps.nrlais.model.DocumentTypeInfo;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.SourceDocument;
import com.intaps.nrlais.model.tran.Applicant;
import com.intaps.nrlais.model.tran.BoundaryCorrectionTransactionData;
import com.intaps.nrlais.model.tran.ConsolidationTransactionData;
import com.intaps.nrlais.model.tran.SpatialOperationTransactionData;
import com.intaps.nrlais.repo.GetLADM;
import com.intaps.nrlais.repo.LRManipulator;
import com.intaps.nrlais.repo.TransactionRepo;
import com.intaps.nrlais.util.INTAPSLangUtils;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Tewelde
 */
public class ConsolidationTransactionHandler extends TransactionHandlerBase implements TransactionHandler {

    void registerTransaction(UserSession session, Connection con, LATransaction theTransaction, ConsolidationTransactionData tran) throws SQLException, Exception {

        LRManipulator lrm = new LRManipulator(session);
        TransactionRepo tranRepo = new TransactionRepo(session);
        GetLADM repo = new GetLADM(session, "nrlais_transaction");
        LADM ladm = repo.get(con, tran.holdingUID, LADM.CONTENT_FULL);
        LADM.Holding holding = ladm.holding;

        HashMap<String, Applicant> applicants = new HashMap<>();
        for (Applicant ap : tran.applicants) {
            if (applicants.containsKey(ap.selfPartyUID)) {
                throw new IllegalArgumentException(session.text("Duplicate applicant") + ap.selfPartyUID);
            }
            applicants.put(ap.selfPartyUID, ap);
        }
        List<LADM.Right> holders = holding.getHolders();
        for (LADM.Right r : holders) {
            if (!applicants.containsKey(r.partyUID)) {
                throw new IllegalArgumentException(session.text("Please provide applicant information for")+" " + r.party.getFullName());
            }
            Applicant ap = applicants.get(r.partyUID);
            if (ap.idDocument == null) {
                throw new IllegalArgumentException(session.text("ID Document not provided for") + r.party.getFullName());
            } else {
                ap.idDocument.uid = UUID.randomUUID().toString();
                tranRepo.saveDocument(con, theTransaction, ap.idDocument);
            }
            if (ap.representative != null) {
                lrm.createParty(con, theTransaction, ap.representative);
                if (ap.letterOfAttorneyDocument == null) {
                    throw new IllegalArgumentException(session.text("Autorized Representation Document not provided for")+ ap.representative.getFullName());

                }
                ap.letterOfAttorneyDocument.uid = UUID.randomUUID().toString();
                ap.letterOfAttorneyDocument.sourceType = DocumentTypeInfo.DOC_TYPE_LETTER_OF_ATTORNEY;
                tranRepo.saveDocument(con, theTransaction, ap.letterOfAttorneyDocument);
            }
            if (ap.pomDocument != null) {
                ap.pomDocument.uid = UUID.randomUUID().toString();
                if (r.party.maritalstatus == LADM.Party.MARITAL_STATUS_MARRIED) {
                    ap.pomDocument.sourceType = DocumentTypeInfo.DOC_TYPE_PROOF_OF_MARRAIGE;
                } else {
                    ap.pomDocument.sourceType = DocumentTypeInfo.DOC_TYPE_PROOF_OF_CELEBACY;
                }
            }
        }

        HashMap<String, LADM.Holding> parcelHoldings = new HashMap<>();
        HashMap<String, LADM.Parcel> parcelObjects = new HashMap<>();
        String list =null;
        
        if(tran.parcels.size()==0)
            throw new IllegalArgumentException(session.text("Select at least one parcel"));
        HashSet<String> notDone=new HashSet<String>();
        
        for (String p : tran.parcels) {
            notDone.add(p);
            parcelObjects.put(p, repo.getParcel(con, p, LADM.CONTENT_FULL));           
            list = INTAPSLangUtils.appendStringList(list, ",", "'" + p + "'");
        }
        LADM.Parcel templateParcel = parcelObjects.get(tran.parcels.get(0));

        List<String> traverse=new ArrayList<String>();
        traverse.add(templateParcel.parcelUID);
        while(!traverse.isEmpty())
        {
            String p=traverse.remove(0);
            notDone.remove(p);
            List<LADM.Parcel> adj=repo.getAdjacentParcels(con,p,LADM.CONTENT_FULL);
            for(LADM.Parcel a:adj)
            {
                if(!parcelObjects.containsKey(a.parcelUID))
                    continue;
                if(!notDone.contains(a.parcelUID))
                    continue;
                traverse.add(a.parcelUID);
            }
        }
        if(notDone.size()>0)
            throw new IllegalArgumentException(session.text("One or more of the parcels are not adjacent to each other"));
        

        
        for (int i = 0; i <tran.landCertDocs.size(); i++) {
            SourceDocument srcDoc = tran.landCertDocs.get(i);
            if (srcDoc == null) {
                throw new IllegalArgumentException(session.text("Land holding certificate is not providef for parcel ") + parcelObjects.get(tran.parcels.get(i)).upid);
            }
            srcDoc.uid = UUID.randomUUID().toString();
            srcDoc.sourceType = DocumentTypeInfo.DOC_TYPE_LAND_HOLDING_CERTIFICATE;
            tranRepo.saveDocument(con, theTransaction, srcDoc);
        }

        
        lrm.lrStartTransaction(con, theTransaction, tran.holdingUID);
        //create the consolidated parcel
        LADM.Parcel consolidatedParcel = templateParcel.clonedObjectNoRR();
        consolidatedParcel.restrictions.addAll(templateParcel.restrictions);
        consolidatedParcel.rights.addAll(holders);
        lrm.lrAddParcel(con, theTransaction, holding.holdingUID, consolidatedParcel);

        //union the geometry
        String unionSql = "Update nrlais_transaction.t_parcels set geometry=(Select ST_Multi(ST_Union(geometry)) from nrlais_transaction.t_parcels where uid in ($list)) where uid='$upid'; Update nrlais_transaction.t_parcels set areageom=ST_Area(geometry) where uid='$upid';";
        unionSql = StringUtils.replaceEach(unionSql, new String[]{"$list", "$upid"}, new String[]{list, consolidatedParcel.parcelUID});
        con.prepareStatement(unionSql).execute();
        for (LADM.Parcel parcel : parcelObjects.values()) {
            lrm.lrRemoveAndDeleteParcel(con, theTransaction, tran.holdingUID, parcel.parcelUID);
        }
        lrm.finishTransaction(con, theTransaction);

    }

    @Override
    public void saveTransaction(UserSession session, Connection con, LATransaction theTransaction, boolean register) throws Exception {
        try {
            ConsolidationTransactionData tran = (ConsolidationTransactionData) theTransaction.data;
            if (!register) {
                return;
            }
            GetLADM g = new GetLADM(session, "nrlais_inventory");
            LADM ladm = g.get(con, tran.holdingUID, LADM.CONTENT_FULL);
            super.transferLandRecordToTransaction(session, con, theTransaction.transactionUID, ladm);
            registerTransaction(session, con, theTransaction, tran);
        } catch (CloneNotSupportedException | IOException ex) {
            throw new SQLException(session.text("Can not Excute Transaction"), ex);
        }
    }
}
