/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.model.tran;

import com.intaps.nrlais.model.LADM.Party;
import com.intaps.nrlais.model.SourceDocument;
import com.intaps.nrlais.util.RationalNum;

/**
 *
 * @author Tewelde
 */
public class Applicant {
    public static int INHERITANCE_ROLE_DECEASED=1;
    public static int INHERITANCE_ROLE_BENEFICIARY=2;
    public static int INHERITANCE_ROLE_NEUTRAL=3;
    public String selfPartyUID=null;
    public Party representative=null;
    public SourceDocument idDocument=null;
    public SourceDocument letterOfAttorneyDocument=null;
    public int relationWithParty=0;
    public SourceDocument pomDocument=null;
    public RationalNum share=null;
    public int inheritancRole=INHERITANCE_ROLE_NEUTRAL;
}
