/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.repo.lrop;

import com.intaps.nrlais.UserSession;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.tran.ManualTransactionData;
import com.intaps.nrlais.repo.GetLADM;
import com.intaps.nrlais.repo.LRManipulator;
import com.intaps.nrlais.repo.ManualLRManipulatorFacade;
import java.sql.Connection;

/**
 *
 * @author Tewelde
 */
public class LrOpExecDeleteRestriction implements ManualLRManipulatorFacade.LROPExecutor<ManualTransactionData.LrOpDeleteRestriction> {

    @Override
    public Object doOperation(UserSession session, Connection con, LATransaction tran, ManualTransactionData.LrOpDeleteRestriction op) throws Exception {
        LRManipulator lrm = new LRManipulator(session);
        LADM.Restriction restriction=new GetLADM(session, "nrlais_transaction").getRestriction(con, op.restrictionUID);
        lrm.lrStartTransaction(con, tran, restriction.holdingUID);
        lrm.lrRemoveRestriction(con, tran, restriction.holdingUID, restriction.parcelUID, restriction);
        lrm.finishTransaction(con, tran,false);
        return null;
    }

}
