/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.replication;

/**
 *
 * @author Tewelde
 */
public class ReplicationAction {
    public int id;
    public long time;
    public String op;
    public String user;
    public String data;

    @Override
    public String toString() {
        return new java.util.Date()+": "+ op+" "+data;
    }
    
}
