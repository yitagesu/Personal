/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.tran;

import com.intaps.nrlais.UserSession;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.CMSSParcelSplitTask;
import com.intaps.nrlais.model.DocumentTypeInfo;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LADM.Parcel;
import com.intaps.nrlais.model.LADM.Party;
import com.intaps.nrlais.model.SourceDocument;
import com.intaps.nrlais.model.tran.Applicant;
import com.intaps.nrlais.model.tran.DivorceTransactionData;
import com.intaps.nrlais.model.tran.ParcelTransfer;
import com.intaps.nrlais.model.tran.Partition;
import com.intaps.nrlais.model.tran.PartyItem;
import com.intaps.nrlais.model.tran.PartyParcelShare;
import com.intaps.nrlais.model.tran.RentTransactionData;
import com.intaps.nrlais.model.tran.RestrictionTransactionData;
import com.intaps.nrlais.model.tran.RestrictiveInterestTransactionData;
import com.intaps.nrlais.repo.CMSSTaskManager;
import com.intaps.nrlais.repo.DocumentTypeRepo;
import com.intaps.nrlais.repo.GetLADM;
import com.intaps.nrlais.repo.GetTransaction;
import com.intaps.nrlais.repo.LRManipulator;
import com.intaps.nrlais.repo.TransactionRepo;
import com.intaps.nrlais.util.EthiopianCalendar;
import com.intaps.nrlais.util.RationalNum;
import com.intaps.nrlais.worlais.WORLAISClient;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.AbstractList;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.Mac;

/**
 *
 * @author Tewelde
 */
public abstract class RestrictionTransactionHandler<T extends RestrictionTransactionData> extends TransactionHandlerBase implements TransactionHandler {

    protected abstract SourceDocument[] sourceDocs(T data);

    protected abstract int[] sourceDocTypes(T data);

    /**
     *
     * @param con
     * @param theTransaction
     * @throws SQLException
     */
    //the logic for modifying the land register goes here.
    void registerTransaction(UserSession session, Connection con, LATransaction theTransaction, T tran) throws SQLException, Exception {
        LRManipulator lrm = new LRManipulator(session);
        TransactionRepo tranRep = new TransactionRepo(session);
        GetLADM repo = new GetLADM(session, "nrlais_transaction");
        LADM ladm = repo.get(con, tran.holdingUID, LADM.CONTENT_FULL);
        LADM.Holding holding = ladm.holding;

        //check applicants
        HashMap<String, Applicant> applicants = new HashMap<>();
        for (Applicant ap : tran.applicants) {
            if (applicants.containsKey(ap.selfPartyUID)) {
                throw new IllegalArgumentException("Duplicate applicant " + ap.selfPartyUID);
            }
            applicants.put(ap.selfPartyUID, ap);

        }
        for (LADM.Right r : holding.getHolders()) {
            //check and save applicant document
            if (!applicants.containsKey(r.partyUID)) {
                throw new IllegalArgumentException("Please provide applicant information for " + r.party.getFullName());
            }

            Applicant ap = applicants.get(r.partyUID);
            if (ap.idDocument == null) {
                throw new IllegalArgumentException("ID document not provided for " + r.party.getFullName());
            } else {
                ap.idDocument.uid = UUID.randomUUID().toString();
                tranRep.saveDocument(con, theTransaction, ap.idDocument);
            }
            if (ap.representative != null) {
                lrm.createParty(con, theTransaction, ap.representative);
                if (ap.letterOfAttorneyDocument == null) {
                    throw new IllegalArgumentException("ID document not provided for " + ap.representative.getFullName());
                }
                ap.letterOfAttorneyDocument.uid = UUID.randomUUID().toString();
                ap.letterOfAttorneyDocument.sourceType = DocumentTypeInfo.DOC_TYPE_LETTER_OF_ATTORNEY;
                tranRep.saveDocument(con, theTransaction, ap.letterOfAttorneyDocument);
            }
            if (ap.pomDocument != null) {
                ap.pomDocument.uid = UUID.randomUUID().toString();
                if (r.party.maritalstatus == LADM.Party.MARITAL_STATUS_MARRIED) {
                    ap.pomDocument.sourceType = DocumentTypeInfo.DOC_TYPE_PROOF_OF_MARRAIGE;
                } else {
                    ap.pomDocument.sourceType = DocumentTypeInfo.DOC_TYPE_PROOF_OF_CELEBACY;
                }
            }
        }
        //save documents
        SourceDocument[] allDocs = sourceDocs(tran);
        int[] allTypes = sourceDocTypes(tran);
        DocumentTypeRepo docTypeRepo = new DocumentTypeRepo(session);
        for (int i = 0; i < allDocs.length; i++) {
            SourceDocument srcDoc = allDocs[i];
            if (srcDoc == null) {
                throw new IllegalArgumentException("Source document of type:" + docTypeRepo.getDocumentType(con, srcDoc.sourceType) + " isnot provided");
            }
            srcDoc.uid = UUID.randomUUID().toString();
            srcDoc.sourceType = allTypes[i];
            tranRep.saveDocument(con, theTransaction, srcDoc);
        }

        HashMap<String, ParcelTransfer> partitions = new HashMap<String, ParcelTransfer>();
        tran.restrictedParcels.forEach((p) -> {
            partitions.put(p.parcelUID, p);
        });

        lrm.lrStartTransaction(con, theTransaction, holding.holdingUID);

        Iterator<Parcel> parcelIter = holding.parcels.iterator();
        if (tran.restrictionParties.size() == 0) {
            throw new IllegalArgumentException("Tenants are not registered");
        }
        for (PartyItem restrictor : tran.restrictionParties) {
            if (restrictor.idDoc != null) {
                restrictor.idDoc.uid = UUID.randomUUID().toString();
                tranRep.saveDocument(con, theTransaction, restrictor.idDoc);
            }
            if (restrictor.proofOfMaritalStatus != null) {
                restrictor.proofOfMaritalStatus.uid = UUID.randomUUID().toString();
                tranRep.saveDocument(con, theTransaction, restrictor.proofOfMaritalStatus);
            }
            if (restrictor.letterOfAttorneyDocument != null) {
                restrictor.letterOfAttorneyDocument.uid = UUID.randomUUID().toString();
                tranRep.saveDocument(con, theTransaction, restrictor.letterOfAttorneyDocument);
            }
        }

        while (parcelIter.hasNext()) {
            LADM.Parcel parcel = parcelIter.next();
            if (!partitions.containsKey(parcel.parcelUID)) {
                continue;
            }
            ParcelTransfer transfer = partitions.get(parcel.parcelUID);

            if (transfer.area > parcel.areaGeom || transfer.area < 0) {
                throw new IllegalArgumentException("Invalid rent/lease size");
            }
            List<LADM.Right> tenants = parcel.getTenants();
            if (tenants.size() > 0) {
                throw new IllegalArgumentException("Parcel " + parcel.upid + " is already rented");
            }
            for (PartyItem restrictor : tran.restrictionParties) {
                LADM.Restriction r = new LADM.Restriction();
                r.party = restrictor.party;
                r.restrictionType = tran.restrictionType;
                r.startDate = tran.startDate;
                r.endDate = tran.endDate;
                lrm.lrAddRestriction(con, theTransaction, holding.holdingUID, parcel.parcelUID, r);
            }
            //add cmss split parcel task if necessary
        }//parcels iterator

        lrm.finishTransaction(con, theTransaction);
    }

    @Override
    public void saveTransaction(UserSession session, Connection con, LATransaction theTransaction, boolean register) throws Exception {
        try {
            T tran = (T) theTransaction.data;
            LADM existing = new GetLADM(session, "nrlais_inventory").get(con, tran.holdingUID, LADM.CONTENT_FULL);

            String err = existing.getRuleErrorsAsString(theTransaction.time);
            if (!org.apache.commons.lang3.StringUtils.isEmpty(err)) {
                throw new IllegalStateException(err);
            }

            if (!register) {
                return;
            }

            super.transferLandRecordToTransaction(session, con, theTransaction.transactionUID, existing);
            registerTransaction(session, con, theTransaction, tran);

        } catch (CloneNotSupportedException | IOException ex) {
            throw new SQLException("Can't execute transaction", ex);
        }
    }

}
