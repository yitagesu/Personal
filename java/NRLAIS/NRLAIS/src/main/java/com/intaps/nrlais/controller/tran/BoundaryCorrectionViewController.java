/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.controller.tran;

import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.tran.Applicant;
import com.intaps.nrlais.model.tran.BoundaryCorrectionTransactionData;
import com.intaps.nrlais.model.tran.SpatialOperationTransactionData.HoldingParcel;
import com.intaps.nrlais.util.INTAPSLangUtils;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Tewelde
 */
public class BoundaryCorrectionViewController extends SpatialOperationViewController<BoundaryCorrectionTransactionData> {

    public BoundaryCorrectionViewController(HttpServletRequest request, HttpServletResponse response) throws Exception {
        super(request, response, BoundaryCorrectionTransactionData.class, LATransaction.TRAN_TYPE_BOUNDARY_CORRECTION);
    }

    public BoundaryCorrectionViewController(HttpServletRequest request, HttpServletResponse response, boolean loadData) throws Exception {
        super(request, response, BoundaryCorrectionTransactionData.class, LATransaction.TRAN_TYPE_BOUNDARY_CORRECTION, loadData);
    }

    public String landHoldingCertifcateText() {
        if (this.data.landHoldingCertificateDoc == null) {
            return "";
        }
        return this.data.landHoldingCertificateDoc.refText;
    }

    public String landHoldingCertifcateLink() {
        return showDocumentLink(this.data.landHoldingCertificateDoc);
    }

//    @Override
//    public List<CertificateLink> certficateLinks() throws SQLException, IOException {
//
//        List<TransactionViewController.CertificateLink> ret = new ArrayList<TransactionViewController.CertificateLink>();
//        ChangeSummary summary1 = new ChangeSummary();
//        try {
//            TransactionContainerViewController containerController = new TransactionContainerViewController(request, response);
//            summary1 = containerController.typedController.summary();
//        } catch (Exception ex) {
//            Logger.getLogger(ExOfficioViewController.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        if (!summary1.updatedHoldings.isEmpty() && summary.updatedHoldings != null) {
//            Map<String, UpdateItem> uniqueItems = new HashMap<>();
//            for (UpdateItem up : summary1.updatedHoldings) {
//                if (!uniqueItems.containsKey(up.holdingUID)) {
//                    uniqueItems.put(up.holdingUID, up);
//                }
//            }
//
//            for (Map.Entry<String, UpdateItem> entry : uniqueItems.entrySet()) {
//
//                LADM before = mainFacade.getFromHistoryByArchiveTx(this.tran.transactionUID, entry.getValue().holdingUID);
//                LADM after = mainFacade.getByTx(this.tran.transactionUID, entry.getValue().holdingUID);
//
//                List<String> par = new ArrayList<>();
//                par = newParcels(before, after);
//                if (par.size() > 0) {
//                    LADM ladm = mainFacade.getLADM("nrlais_inventory", after.holding.holdingUID);
//
//                    par = newParcels(before, after);
//                    for (int i = 0; i < after.holding.parcels.size(); i++) {
//
//                        if (par.contains(after.holding.parcels.get(i).parcelUID)) {
//                            LADM.Parcel l = ladm.holding.parcels.get(i);
//                            CertificateLink link = new CertificateLink();
//                            link.upid = l.upid;
//                            for (LADM.Right r : l.getHolders()) {
//                                link.owner = INTAPSLangUtils.appendStringList(link.owner, ", ", r.party.getFullName());
//                            }
//                            link.url = "javascript:transcation_showCertiricate('" + tran.transactionUID + "','" + l.rights.get(0).holdingUID + "','" + l.parcelUID + "')";
//                            ret.add(link);
//                        }
//
//                    }
//
//                }
//
//            }
//        }
//
//        return ret;
//    }

    private List<String> newParcels(LADM before, LADM after) {
        List<String> lp = new ArrayList<>();
        List<String> pUID = new ArrayList<>();
        List<String> pGeom = new ArrayList<>();
        for (LADM.Parcel p : before.holding.parcels) {
            pUID.add(p.parcelUID);
            pGeom.add(p.geometry);
        }
        for (LADM.Parcel p : after.holding.parcels) {
            if (!pUID.contains(p.parcelUID) || !pGeom.contains(p.geometry)) {
                lp.add(p.parcelUID);
            }
        }
        return lp;
    }
}
