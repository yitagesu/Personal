/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais;

import com.intaps.nrlais.util.GSONUtil;
import java.util.ArrayList;

/**
 *
 * @author Teweldemedhin Aberra
 */
public class NRLAISWorker {
    public static final int STATUS_IDLE = 0;
    public static final int STATUS_INITIALIZING = 1;
    public static final int STATUS_RUNNING = 2;
    public static final int STATUS_DONE = 3;
    public static final int STATUS_ERROR = 4;
    protected Thread thread = null;
    protected int status = 0;
    protected double progress = 0;
    protected String status_message = "idle";
    protected boolean stopRequested = false;
    protected ArrayList<String> errorList = new ArrayList<>();
    public boolean isIdle()
    {
        return status!=STATUS_RUNNING&&status!=STATUS_INITIALIZING;
    }
    void updateStatus(int status, String message) {
        this.status = status;
        this.status_message = message;
    }

    void updateStatus(int status, String message, double prog) {
        this.status = status;
        this.status_message = message;
        this.progress = prog;
    }

    public static class StatusInfo {

        public int status;
        public double progress;
        public String status_message = "idle";
        public String[] errorList = null;
        public int errorCount = 0;

    }

    public static class StatusInfoRet extends GSONUtil.JSONRet<NRLAISWorker.StatusInfo> {

    }


    public StatusInfo getStatus(int errorIndex, int errorCount) {
        StatusInfo ret;
        ret = new StatusInfo();
        ret.status = status;
        ret.progress = progress;
        ret.status_message = status_message;
        if (stopRequested) {
            ret.status_message += " (Stop requested)";
        }
        if (errorList == null) {
            ret.errorList = null;
                    
        } else {
            if (errorIndex != -1) {
                if (errorCount == -1) {
                    ret.errorList = errorList.toArray(new String[0]);
                } else {
                    ret.errorList = errorList.subList(errorIndex, Math.min(errorList.size() - errorIndex, errorCount)).toArray(new String[0]);
                }
            }
        }
        ret.errorCount = errorList == null ? 0 : errorList.size();
        return ret;
    }

    public void stop() {
        stopRequested = true;
    }

}
