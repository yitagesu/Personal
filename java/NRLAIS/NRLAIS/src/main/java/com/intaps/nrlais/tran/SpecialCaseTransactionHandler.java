/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.tran;

import com.intaps.nrlais.UserSession;
import com.intaps.nrlais.model.CMSSCreateParcelsTask;
import com.intaps.nrlais.model.CMSSEditParcelTask;
import com.intaps.nrlais.model.DocumentTypeInfo;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.SourceDocument;
import com.intaps.nrlais.model.tran.Applicant;
import com.intaps.nrlais.model.tran.BoundaryCorrectionTransactionData;
import com.intaps.nrlais.model.tran.ParcelItem;
import com.intaps.nrlais.model.tran.PartyItem;
import com.intaps.nrlais.model.tran.SpecialCaseTransactionData;
import com.intaps.nrlais.repo.CMSSTaskManager;
import com.intaps.nrlais.repo.DocumentTypeRepo;
import com.intaps.nrlais.repo.GetLADM;
import com.intaps.nrlais.repo.LRManipulator;
import com.intaps.nrlais.repo.TransactionRepo;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.UUID;

/**
 *
 * @author Tewelde
 */
public class SpecialCaseTransactionHandler extends TransactionHandlerBase implements TransactionHandler {

    void registerTransaction(UserSession session, Connection con, LATransaction theTransaction, SpecialCaseTransactionData tran) throws SQLException, Exception {
        LRManipulator lrm = new LRManipulator(session);
        CMSSTaskManager cmss = new CMSSTaskManager(session);
        TransactionRepo tranRepo = new TransactionRepo(session);

        HashMap<String, PartyItem> applicants = new HashMap<>();
        HashMap<LADM.Party, String> originalPartyUIDs = new HashMap<>();
        for(PartyItem p:tran.applicants)
            originalPartyUIDs.put(p.party, p.party.partyUID);

        for (PartyItem ap : tran.applicants) {
            if (applicants.containsKey(ap.party.partyUID)) {
                throw new IllegalArgumentException("Duplicate applicant" + ap.party.partyUID);
            }
            applicants.put(ap.party.partyUID, ap);
//CAUTION: don't delete. Commented until party picker id and represenative features are implemented
//            if (ap.idDoc == null) {
//                throw new IllegalArgumentException("ID Document not provided for" + ap.party.getFullName());
//            } else {
//                ap.idDoc.uid = UUID.randomUUID().toString();
//                tranRepo.saveDocument(con, theTransaction, ap.idDoc);
//            }
//            if (ap.representative != null) {
//                lrm.createParty(con, theTransaction, ap.representative);
//                if (ap.letterOfAttorneyDocument == null) {
//                    throw new IllegalArgumentException("Autorized Representation Document not provided for" + ap.representative.getFullName());
//
//                }
//                ap.letterOfAttorneyDocument.uid = UUID.randomUUID().toString();
//                ap.letterOfAttorneyDocument.sourceType = DocumentTypeInfo.DOC_TYPE_LETTER_OF_ATTORNEY;
//                tranRepo.saveDocument(con, theTransaction, ap.letterOfAttorneyDocument);
//            }
//            if (ap.proofOfMaritalStatus != null) {
//                ap.proofOfMaritalStatus.uid = UUID.randomUUID().toString();
//                if (ap.party.maritalstatus == LADM.Party.MARITAL_STATUS_MARRIED) {
//                    ap.proofOfMaritalStatus.sourceType = DocumentTypeInfo.DOC_TYPE_PROOF_OF_MARRAIGE;
//                } else {
//                    ap.proofOfMaritalStatus.sourceType = DocumentTypeInfo.DOC_TYPE_PROOF_OF_CELEBACY;
//                }
//            }
        }
        double totalShare = 0.0;
        for(PartyItem pi : tran.applicants){
            
           totalShare += pi.share.sharesInNumber();
        }
         if(totalShare != 1)
                throw new IllegalArgumentException("total share must be 100%");

        SourceDocument[] allDocs = new SourceDocument[]{tran.claimResolutionDoc, tran.firtLevelLandHoldingBook};
        int[] allTypes = new int[]{DocumentTypeInfo.DOC_TYPE_DIVORCE_CLAIM_RESOLUTION, DocumentTypeInfo.DOC_TYPE_FIRST_LEVEL_CERTIFICATE};
        DocumentTypeRepo docTypeRepo = new DocumentTypeRepo(session);
        for (int i = 0; i < allDocs.length; i++) {
            SourceDocument srcDoc = allDocs[i];
            if (srcDoc == null) {
                throw new IllegalArgumentException("Required Document of " + docTypeRepo.getDocumentType(con, srcDoc.sourceType) + " is not provided");
            }
            srcDoc.uid = UUID.randomUUID().toString();
            srcDoc.sourceType = allTypes[i];
            tranRepo.saveDocument(con, theTransaction, srcDoc);
        }

        lrm.lrStartTransaction(con, theTransaction, new ArrayList<String>());
        tran.holding.nrlais_kebeleid = theTransaction.nrlais_kebeleid;
        String holdingUID = lrm.lrCreateNewHolding(con, theTransaction, tran.holding);
        for (ParcelItem parcel : tran.parcels) {
            parcel.parcel.rights=new ArrayList<>();
            for (PartyItem h : tran.applicants) {
                h.party.partyUID=originalPartyUIDs.get(h.party);
                LADM.Right right=new LADM.Right();
                right.party=h.party;
                if(h.party.partyType==LADM.Party.PARTY_TYPE_STATE)
                    right.rightType=LADM.Right.RIGHT_SUR;
                else
                    right.rightType=LADM.Right.RIGHT_PUR;
                right.share=h.share;
                parcel.parcel.rights.add(right);
            }
            lrm.lrAddParcel(con, theTransaction, holdingUID, parcel.parcel);
        }
        lrm.finishTransaction(con, theTransaction);
        
        CMSSCreateParcelsTask task = new CMSSCreateParcelsTask();
        task.transactionUID = theTransaction.transactionUID;
        for (ParcelItem parcel : tran.parcels) {
            task.newParcelUIDs.add(parcel.parcel.parcelUID);
        }
        cmss.addCreateParcelsTask(con, task);
    }

    @Override
    public void saveTransaction(UserSession session, Connection con, LATransaction theTransaction, boolean register) throws Exception {
        try {
            SpecialCaseTransactionData tran = (SpecialCaseTransactionData) theTransaction.data;
            if (!register) {
                return;
            }
            if (tran.parcels.size() == 0) {
                throw new IllegalArgumentException("At least one parcel should be addded");
            }
            if (tran.applicants.size() == 0) {
                throw new IllegalArgumentException("At least one holder should be specified");
            }

            registerTransaction(session, con, theTransaction, tran);
        } catch (CloneNotSupportedException | IOException ex) {
            throw new SQLException("Cant Excute Transaction", ex);
        }
    }
}
