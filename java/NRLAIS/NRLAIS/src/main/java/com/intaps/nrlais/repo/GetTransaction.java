/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.repo;

import com.intaps.nrlais.Startup;
import com.intaps.nrlais.UserSession;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.SourceDocument;
import com.intaps.nrlais.tran.*;
import com.intaps.nrlais.tran.TransactionHandler;
import com.intaps.nrlais.util.EthiopianCalendar;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Tewelde
 */
public class GetTransaction extends RepoBase {

    public final static double OVERLAP_TOL=0.0001;
    public final static double GRID_TOL=0.01;
    public GetTransaction(UserSession session) {
        super(session);
    }

    public static TransactionHandler createTransactionHandler(int transactionType) {
        switch (transactionType) {
            case LATransaction.TRAN_TYPE_DIVORCE:
                return new DivorceTransactionHandler();
            case LATransaction.TRAN_TYPE_RENT:
                return new RentTransactionHandler();
            case LATransaction.TRAN_TYPE_GIFT:
                return new GiftTransactionHandler();
            case LATransaction.TRAN_TYPE_REALLOCATION:
                return new ReallocationTransactionHandler();
            case LATransaction.TRAN_TYPE_INHERITANCE:
                return new InheritanceTransactionHandler();
            case LATransaction.TRAN_TYPE_INHERITANCE_WITHWILL:
                return new InheritanceTransactionHandler();
            case LATransaction.TRAN_TYPE_DATA_MIGRATION:
                return new DataMigrationTransactionHandler();
            case LATransaction.TRAN_TYPE_EXPROPRIATION:
                return new ExpropriationTransactionHandler();
            case LATransaction.TRAN_TYPE_BOUNDARY_CORRECTION:
                return new BoundaryCorrectionTransactionHandler();
            case LATransaction.TRAN_TYPE_SPECIAL_CASE:
                return new SpecialCaseTransactionHandler();
            case LATransaction.TRAN_TYPE_CONSOLIDATION:
                return new ConsolidationTransactionHandler();
            case LATransaction.TRAN_TYPE_EX_OFFICIO:
                return new ExOfficioTransactionHandler();
            case LATransaction.TRAN_TYPE_EXCHANGE:
                return new ExchangeTransactionHandler();
            case LATransaction.TRAN_TYPE_REPLACE_CERTIFICATE:
                return new CertificateTransactionHandler();
            case LATransaction.TRAN_TYPE_RESTRICTIVE_INTEREST:
                return new RestrictiveInterestTransactionHandler();
            case LATransaction.TRAN_TYPE_SERVITUDE:
                return new ServitudeTransactionHandler();       
             case LATransaction.TRAN_TYPE_SIMPLE_CORRECTION:
                return new SimpleCorrectionHandler();
             case LATransaction.TRAN_TYPE_SPLIT:
                 return new SplitTransactionHandler();
            default:
                throw new IllegalArgumentException("Transaction handler not defined for transaction type:" + transactionType);
        }
    }

    public LATransaction get(final Connection con, String uid, boolean includeDetail) throws SQLException {
        LATransaction ret = null;
        String sql="Select * from nrlais_inventory.t_transaction where uid='" + uid + "'";
        try (ResultSet rs = con.prepareStatement(sql).executeQuery()) {
            if (rs.next()) {
                ret = new LATransaction();
                loadTranInfoFromResultSet(rs, ret);
            } else {
                return ret;
            }
        }
        if (includeDetail) {
            ret.data = createTransactionHandler(ret.transactionType).getData(super.session, con, uid);
        }
        return ret;
    }

    public SourceDocument getDocument(Connection con, String docuid, boolean includeImage) throws SQLException, IOException {
        try (ResultSet rs = con.prepareStatement("Select * from nrlais_inventory.t_adminsource where uid='" + docuid + "'").executeQuery()) {
            if (rs.next()) {
                SourceDocument ret = new SourceDocument();
                ret.uid = rs.getString("uid");
                super.populateNRLAISEntityFields(rs, ret);
                ret.notes = rs.getString("notes");
                ret.sourceType = rs.getInt("adminsourcetype");
                ret.refText = rs.getString("adminsourceref");
                ret.archiveType = rs.getInt("adminsourcearchivetype");
                ret.description = rs.getString("adminsourcedescription");
                ret.txuid = rs.getString("txuid");
                ret.mimeType = rs.getString("mimetype");
                ret.voidStatus = rs.getBoolean("voidstatus");
                if (includeImage) {
                    String folder = Startup.getAppConfig("docFolder", "data/docFolder");
                    File f = new File(folder + "/" + docuid);
                    if (f.exists()) {
                        ret.fileImage = org.apache.commons.io.FileUtils.readFileToByteArray(f);
                    }
                }
                return ret;
            } else {
                return null;
            }
        }
    }

    private void loadTranInfoFromResultSet(final ResultSet rs, LATransaction ret) throws SQLException {
        super.populateNRLAISEntityFields(rs, ret);
        ret.transactionUID = rs.getString("uid");
        ret.notes = rs.getString("notes");
        ret.transactionType = rs.getInt("transactiontype");
        ret.status = rs.getInt("txstatus");
        ret.year = rs.getInt("txyear");
        ret.seqNo = rs.getInt("txseqnr");
        ret.time = rs.getDate("txdate").getTime();
        ret.syssessionid = rs.getString("syssessionid");
        ret.spatialTask = rs.getInt("spatialtask");
    }

    public LATransaction getBySeqNo(final Connection con, String kebeleID, int seqNo, boolean includeDetail) throws SQLException {
        LATransaction ret = null;
        try (ResultSet rs = con.prepareStatement("Select * from nrlais_inventory.t_transaction where nrlais_kebeleid='" + kebeleID + "' seqnr=" + seqNo).executeQuery()) {
            if (rs.next()) {
                ret = new LATransaction();
                loadTranInfoFromResultSet(rs, ret);
            } else {
                return ret;
            }
        }
        if (includeDetail) {
            ret.data = createTransactionHandler(ret.transactionType).getData(super.session, con, ret.transactionUID);
        }
        return ret;
    }
    static SimpleDateFormat flowDateF = new SimpleDateFormat("MM/dd/yyyy");
    List<LATransaction.TranactionFlow> getTransactionFlow(Connection con, String txuid) throws SQLException {
        List<LATransaction.TranactionFlow> ret = new ArrayList<LATransaction.TranactionFlow>();
        try (ResultSet rs = con.prepareStatement("Select * from nrlais_inventory.t_transaction_flow where txuid='" + txuid + "' order by flowseq desc").executeQuery()) {
            while (rs.next()) {
                LATransaction.TranactionFlow f = new LATransaction.TranactionFlow();
                RepoBase.populateNRLAISEntityFields(rs, f);
                f.uid = rs.getString("uid");
                f.notes = rs.getString("notes");
                f.oldState = rs.getInt("oldstatus");
                f.newState = rs.getInt("newstatus");
                f.timeStr=EthiopianCalendar.ToEth(f.syscreatedate).toString()+" (EC)";                        
                ret.add(f);
            }
        }
        return ret;
    }
    
    public List<LATransaction> getHoldingHistory(Connection con,String tranUID,String holdingUID) throws SQLException, IOException
    {
        
        LADM h=null;
        if(tranUID==null)
        {
            h=new GetLADM(session, "nrlais_inventory").get(con, holdingUID, LADM.CONTENT_HOLDING);
        }
        else
            h=new GetLADM(session,null).getByTx(con, tranUID,holdingUID);
        if(h==null)
            return null;
        List<LATransaction> ret=new ArrayList<LATransaction>();
        LATransaction tran=get(con, h.holding.sourcetxuid, false);
        ret.add(tran);
        while(true)
        {
            try(ResultSet rs=con.prepareStatement("Select sourcetx from nrlais_historic.t_holdings where archivetx='"+tran.transactionUID+"'").executeQuery())
            {
                int nrec=0;
                String sourceTx=null;
                while(rs.next())
                {
                    sourceTx=rs.getString(1);
                    nrec++;
                    if(nrec>1)
                        break;
                }
                if(nrec>0)
                {
                    tran=get(con,sourceTx,false);
                    ret.add(tran);
                    if(nrec>1)
                        break;
                }
                else
                    break;
                
            }
        }
        return ret;        
    }
    List<String> getTransactionInputHoldings(Connection con, String transactionUID) throws SQLException {
        LATransaction tran = get(con, transactionUID, false);
        List<String> ret = new ArrayList<>();
        if (tran.isCommited()) {
            try (ResultSet rs = con.prepareStatement("Select inventoryid from nrlais_historic.t_holdings where archivetx='" + transactionUID + "'").executeQuery()) {
                while (rs.next()) {
                    ret.add(rs.getString(1));
                }
            }
        }
        else
        {
            try (ResultSet rs = con.prepareStatement("Select uid from nrlais_inventory.t_holdings where currenttxuid='" + transactionUID + "'").executeQuery()) {
                while (rs.next()) {
                    ret.add(rs.getString(1));
                }
            }
        }
        return ret;
    }

    List<String> getTransactionOutputHoldings(Connection con, String transactionUID) throws SQLException {
        LATransaction tran = get(con, transactionUID, false);
        List<String> ret = new ArrayList<>();
        if (tran.isCommited()) {
            try (ResultSet rs = con.prepareStatement("Select uid from nrlais_inventory.t_holdings where sourcetxuid='" + transactionUID + "'").executeQuery()) {
                while (rs.next()) {
                    ret.add(rs.getString(1));
                }
            }
            
            try (ResultSet rs = con.prepareStatement("Select inventoryid from nrlais_historic.t_holdings where sourcetx='" + transactionUID + "'").executeQuery()) {
                while (rs.next()) {
                    ret.add(rs.getString(1));
                }
            }            
        }
        else
        {
            try (ResultSet rs = con.prepareStatement("Select uid from nrlais_transaction.t_holdings where editstatus in ('n','u','i') and currenttxuid='" + transactionUID + "'").executeQuery()) {
                while (rs.next()) {
                    ret.add(rs.getString(1));
                }
            }
        }
        return ret;
    }
    
}
