/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.model;

import java.util.List;

/**
 *
 * @author Tewelde
 */
public class CMSSTask {
    public static final  int TASK_TYPE_CREATE=1;
    public static final  int TASK_TYPE_EDIT=2;
    public static final  int TASK_TYPE_SPLIT=3;
    
    public static final int TASK_STATUS_PENDING = 1;
    public static final int TASK_STATUS_COMMITED = 2;
    
    public static class TaskParcelGeom
    {
        public String geom;
        public String parcel_uid;
    }
    public static class TaskGeomAssignment
    {
        public String task_uid;
        public TaskParcelGeom[] assignments;
    }
    public String taskUID;
    public String transactionUID;
    public int status=TASK_STATUS_PENDING;
    public int taskType;
    public String getTypeString()
    {
        if(taskType==TASK_TYPE_SPLIT)
            return "Split Parcel";
        if(taskType==TASK_TYPE_CREATE)
            return "Crate Parcels";
        if(taskType==TASK_TYPE_EDIT)
            return "Boundary Correction";
        return "Unknown";
    }
    public CMSSTask()
    {
        
    }
    public CMSSTask(String uid,String txuid,int status,int taskType)
    {
        this.taskUID=uid;
        this.transactionUID=txuid;
        this.status=status;
        this.taskType=taskType;
    }
}
