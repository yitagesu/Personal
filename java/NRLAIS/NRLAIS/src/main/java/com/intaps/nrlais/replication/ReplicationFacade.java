/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.replication;

import com.intaps.nrlais.Startup;
import com.intaps.nrlais.UserSession;
import com.intaps.nrlais.repo.GetLADM;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author Tewelde
 */
public class ReplicationFacade {

    public static final String LEVEL_WORLAIS = "WORLAIS";
    public static final String LEVEL_ZONELAIS = "ZONELAIS";
    public static final String LEVEL_REGLAIS = "REGLAIS";
    public static final String LEVEL_CENLAIS = "CENLAIS";

    UserSession session;

    String level;

    public boolean isLowestLevel() {
        return LEVEL_WORLAIS.equals(level);
    }

    public boolean isTopLevel() {
        return LEVEL_CENLAIS.equals(level);
    }

    public boolean isMidLevel() {
        return LEVEL_ZONELAIS.equals(level) || LEVEL_REGLAIS.equals(level);
    }

    public ReplicationFacade(UserSession session) throws SQLException {
        this.session = session;
        try (Connection con = Startup.getNRLAISConnection()) {

            level = this.session.worlaisSession.getLevel(con);
        }
    }

    public List<ReplicationData> getReplicationData(String woredaID, int repNo, int setSize) throws SQLException {
        try (Connection con = Startup.getNRLAISConnection()) {
            List<ReplicationData> ret = new ArrayList<>();
            try (ResultSet rs = con.prepareStatement("SELECT * FROM nrlais_inventory.t_replicationlog where woredaid='" + woredaID + "' and repno>" + repNo + " order by repno").executeQuery()) {
                ReplicationData current = null;
                while (rs.next()) {
                    String dml = rs.getString("dml");
                    if (dml == null) {
                        break;
                    }
                    int thisRepNo = rs.getInt("repno");
                    ReplicationData.Item item = new ReplicationData.Item();
                    item.dml = dml;
                    item.repNo = thisRepNo;
                    if (current == null || current.data.size() == setSize) {
                        current = new ReplicationData();
                        current.woredaID = woredaID;
                        current.data = new ArrayList<>();
                        ret.add(current);
                    }
                    current.data.add(item);
                }
            }
            return ret;
        }
    }

    public int uploadReplicationData(ReplicationData data) throws SQLException {
        if (this.isLowestLevel()) {
            throw new IllegalStateException("WORLAIS dooesn't accept incomming replication request");
        }
        Connection con = Startup.getWriteConnection();
        synchronized (con) {
            int lastPoint = getReplicationNoInternal(con, data.woredaID);
            PreparedStatement insertLog = con.prepareStatement("INSERT INTO nrlais_inventory.t_replicationlog(\n"
                    + "	woredaid, repno, objuid, objecttype, operation, dml,log_time)\n"
                    + "	VALUES (?, ?, ?::uuid, ?, ?, ?);");
            con.setAutoCommit(false);
            try {                
                for (ReplicationData.Item item : data.data) {
                   
                    if (item.repNo <= lastPoint) {
                        continue;
                    }
                    if (item.repNo != lastPoint + 1) {
                        throw new IllegalArgumentException("Replication no " + item.repNo + " not expected");
                    }
                    insertLog.setString(1, data.woredaID);
                    insertLog.setInt(2, item.repNo);
                    insertLog.setString(3, UUID.randomUUID().toString());
                    insertLog.setInt(4, -1);
                    insertLog.setString(5, "x");
                    insertLog.setString(6, item.dml);
                    insertLog.setLong(7, new java.util.Date().getTime());
                    insertLog.execute();
                    con.prepareStatement(item.dml).execute();
                    lastPoint = item.repNo;
                }
                logReplicationAction("Incoming replication","Woreda: "+data.woredaID);
                con.commit();
                return lastPoint;
            } catch (Exception ex) {
                con.rollback();
                throw ex;
            }
        }
    }

    public Integer getReplicationNoInternal(Connection con, String woredaID) throws SQLException {

        try (ResultSet rs = con.prepareStatement("SELECT max(repno) FROM nrlais_inventory.t_replicationlog where woredaid='" + woredaID + "'").executeQuery()) {
            if (rs.next()) {
                return rs.getInt(1);
            }
            return 0;
        }

    }

    public Integer getReplicationNo(String woredaID) throws SQLException {
        try (Connection con = Startup.getNRLAISConnection()) {
            return getReplicationNoInternal(con, woredaID);
        }
    }

    public List<WoredaItem> getAllWoredas() throws SQLException {
        try (Connection con = Startup.getNRLAISConnection()) {
            List<WoredaItem> ret = new ArrayList<>();
            try (ResultSet rs = con.prepareStatement("Select woredaid,max(repno) as maxn,max(log_time) as maxt from  nrlais_inventory.t_replicationlog group by woredaid").executeQuery()) {

                while (rs.next()) {
                    WoredaItem w = new WoredaItem();
                    w.woredaID = rs.getString("woredaid");
                    w.replicationNo = rs.getInt("maxn");
                    w.lastRepTime=rs.getLong("maxt");
                    if(rs.wasNull())
                        w.lastRepTime=-1;
                    ret.add(w);
                }
            }
            return ret;
        }
    }

    public int logReplicationAction(String op, String data) throws SQLException {
        Connection con = Startup.getWriteConnection();
        synchronized (con) {
            String sql = "INSERT INTO nrlais_inventory.t_replication_actions(operation, op_time, op_user, op_data)\n"
                    + "VALUES (?, ?, ?, ?);";
            PreparedStatement p = con.prepareCall(sql);
            p.setString(1, op);
            p.setLong(2, new java.util.Date().getTime());
            p.setString(3, session.userName);
            p.setString(4, data);
            p.execute();
            try (ResultSet rs = con.prepareStatement("Select max(id) from nrlais_inventory.t_replication_actions").executeQuery()) {
                rs.next();
                return rs.getInt(1);
            }
        }
    }

    private ReplicationAction getReplicationAction(Connection con, int id) throws SQLException {
        try (ResultSet rs = con.prepareStatement("Select * from nrlais_inventory.t_replication_actions where id=" + id).executeQuery()) {
            if (rs.next()) {
                ReplicationAction ret = new ReplicationAction();
                ret.id = id;
                ret.data = rs.getString("op_data");
                ret.op = rs.getString("operation");
                ret.time = rs.getLong("op_time");
                ret.user = rs.getString("op_user");
                return ret;
            }
            return null;
        }
    }

    public ReplicationAction getLastReplicationAction() throws SQLException {
        try (Connection con = Startup.getNRLAISConnection()) {
            try (ResultSet rs = con.prepareStatement("Select max(id) from nrlais_inventory.t_replication_actions").executeQuery()) {
                if (rs.next()) {
                    return getReplicationAction(con, rs.getInt(1));
                }
                return null;
            }
        }
    }
}
