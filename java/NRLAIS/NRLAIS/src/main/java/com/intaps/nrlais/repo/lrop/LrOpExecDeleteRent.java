/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.repo.lrop;

import com.intaps.nrlais.UserSession;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.tran.ManualTransactionData;
import com.intaps.nrlais.repo.GetLADM;
import com.intaps.nrlais.repo.LRManipulator;
import com.intaps.nrlais.repo.ManualLRManipulatorFacade;
import java.io.IOException;
import java.sql.Connection;

/**
 *
 * @author Tewelde
 */
public class LrOpExecDeleteRent implements ManualLRManipulatorFacade.LROPExecutor<ManualTransactionData.LrOpDeleteRent> {

    @Override
    public Object doOperation(UserSession session, Connection con, LATransaction tran, ManualTransactionData.LrOpDeleteRent op) throws Exception {
        LRManipulator lrm = new LRManipulator(session);
        LADM.Right right=new GetLADM(session, "nrlais_transaction").getRight(con, op.rightUID);
        lrm.lrStartTransaction(con, tran, right.holdingUID);
        if(right.rightType!=LADM.Right.RIGHT_RENT && right.rightType!=LADM.Right.RIGHT_LEASE)
            throw new IllegalArgumentException("The right you are trying to delete is not rent right");
        lrm.lrRemoveRight(con, tran, right.holdingUID, right.parcelUID, right);
        lrm.finishTransaction(con, tran,false);
        return null;
    }

}
