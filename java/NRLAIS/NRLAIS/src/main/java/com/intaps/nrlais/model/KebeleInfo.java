/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.model;

import com.intaps.nrlais.UserSession;
import java.util.Calendar;

/**
 *
 * @author Tewelde
 */
public class KebeleInfo {

    public static String getKebeleCode(String kebele) {
        return kebele.substring(kebele.length() - 3, kebele.length());
    }

    public static String getRegionCode(String kebele) {
        return kebele.substring(0, 2);
    }

    public static String getRegionCodeAlpha(String code) {
        String region = code.substring(0, 2);
        switch (region) {
            case "07":
                region = "ደህ";
                break;
            case "04":
                region = "OR";
                break;
            case "01":
                region = "ትግ";
                break;
            case "03":
                region = "አማ";
                break;
        }
        return region;
    }

    public static String getZoneCode(String kebele) {
        return kebele.substring(2, 4);
    }

    public static String getWoredaCode(String kebele) {
        return kebele.substring(4, 6);
    }

    public static String getRegionID(String kebele) {
        return kebele.substring(0, 2);
    }

    public static String getZoneID(String kebele) {
        return kebele.substring(0, 4);
    }

    public static String getWoredaID(String kebele) {
        return kebele.substring(0, 6);
    }
    public String kebeleID;
    public MultiLangString name;
    public String geometry = "SRID=20137;" + new org.postgis.MultiPolygon().toString();

    public static void fixNRLAISEntityFileds(UserSession session, String kebele, NRLAISEntity e, boolean newRecord) {
        setAdminFields(e, kebele);
        e.syslastmodby = session.userName;
        e.syslastmoddate = Calendar.getInstance().getTimeInMillis();
        if (newRecord) {
            e.syscreateby = e.syslastmodby;
            e.syscreatedate = e.syslastmoddate;
        }
    }

    public static void setAdminFields(NRLAISHistoryEntity e, String kebele) {
        e.csaregionid = KebeleInfo.getRegionID(kebele);
        e.nrlais_zoneid = KebeleInfo.getZoneID(kebele);
        e.nrlais_woredaid = KebeleInfo.getWoredaID(kebele);
        e.nrlais_kebeleid = kebele;
    }

    public static void setAdminFields(NRLAISEntity e, String kebele) {
        e.csaregionid = KebeleInfo.getRegionID(kebele);
        e.nrlais_zoneid = KebeleInfo.getZoneID(kebele);
        e.nrlais_woredaid = KebeleInfo.getWoredaID(kebele);
        e.nrlais_kebeleid = kebele;
    }

    public CodeText codeText(String lang) {
        return new CodeText(this.kebeleID, this.name.text(lang));
    }
}
