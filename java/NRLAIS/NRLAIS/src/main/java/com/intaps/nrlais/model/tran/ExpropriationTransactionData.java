/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.model.tran;

import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.SourceDocument;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Tewelde
 */
public class ExpropriationTransactionData  extends LATransaction.TransactionData{
    public String holdingUID=null;
    public List<Applicant> applicants=new ArrayList<Applicant>();
    public List<PartyItem> wordaAdministrtion=new ArrayList<>();
    public List<ParcelTransfer> transfers=new ArrayList<ParcelTransfer>();
    public SourceDocument decisionOfWoredaAdministration=null;
    public SourceDocument proofOfpaymentOfCompensation=null;
    public SourceDocument landHoldingCertificate=null;
    public double payment;
}
