/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.controller.routes.api;

import com.google.gson.stream.JsonReader;
import com.intaps.nrlais.Startup;
import com.intaps.nrlais.controller.routes.RouteBase;
import com.intaps.nrlais.replication.ReplicationData;
import com.intaps.nrlais.replication.ReplicationFacade;
import com.intaps.nrlais.repo.GetMap;
import com.intaps.nrlais.util.GSONUtil;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Tewelde
 */
@WebServlet("/api/replication")
public class APIUploadReplication extends RouteBase<Integer, ReplicationData, Integer>{

    @Override
    public Integer createGetResponse(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        String woredaID=req.getParameter("wid");
        return new ReplicationFacade(Startup.getSessionByRequest(req)).getReplicationNo(woredaID);
    }

    @Override
    public ReplicationData deserializePostBody(JsonReader reader) throws Exception {
        return GSONUtil.getAdapter(ReplicationData.class).read(reader);
    }

    @Override
    public Integer processPost(HttpServletRequest req, HttpServletResponse resp, ReplicationData data) throws Exception {
        return new ReplicationFacade(Startup.getSessionByRequest(req)).uploadReplicationData(data);
    }
    
}
