/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.tran;

import com.intaps.nrlais.UserSession;
import com.intaps.nrlais.model.DocumentTypeInfo;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.SourceDocument;
import com.intaps.nrlais.model.tran.Applicant;
import com.intaps.nrlais.model.tran.ExchangeTransactionData;
import com.intaps.nrlais.model.tran.GiftTransactionData;
import com.intaps.nrlais.model.tran.ParcelTransfer;
import com.intaps.nrlais.model.tran.PartyItem;
import com.intaps.nrlais.model.tran.ReallocationTransactionData;
import com.intaps.nrlais.model.tran.TransferTransactionData;
import com.intaps.nrlais.repo.DocumentTypeRepo;
import com.intaps.nrlais.repo.GetLADM;
import com.intaps.nrlais.repo.LRManipulator;
import com.intaps.nrlais.repo.TransactionRepo;
import com.intaps.nrlais.util.EthiopianCalendar;
import com.intaps.nrlais.util.RationalNum;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 *
 * @author Tewelde
 */
public class ExchangeTransactionHandler extends TransactionHandlerBase implements TransactionHandler {

    void registerTransaction(UserSession session, Connection con, LATransaction theTransaction, ExchangeTransactionData tran) throws SQLException, Exception {
        LRManipulator lrm = new LRManipulator(session);
        TransactionRepo tranRep = new TransactionRepo(session);
        GetLADM repo = new GetLADM(session, "nrlais_transaction");
        LADM ladm = repo.get(con, tran.holdingUID1, LADM.CONTENT_FULL);
        LADM.Holding holding1 = ladm.holding;

        ladm = repo.get(con, tran.holdingUID2, LADM.CONTENT_FULL);
        LADM.Holding holding2 = ladm.holding;

        HashMap<String, Applicant> applicants = new HashMap<>();
        for (List<Applicant> apList : new List[]{tran.applicants1, tran.applicants2}) {
            for (Applicant ap : apList) {
                if (applicants.containsKey(ap.selfPartyUID)) {
                    throw new IllegalArgumentException(session.text("Duplicate applicant ") + ap.selfPartyUID);
                }
                applicants.put(ap.selfPartyUID, ap);
            }
        }
        List<LADM.Right> holders1 = holding1.getHolders();
        List<LADM.Right> holders2 = holding2.getHolders();
        for (List<LADM.Right> h : new List[]{holders1, holders2}) {
            for (LADM.Right r : h) {

                if (!applicants.containsKey(r.partyUID)) {
                    throw new IllegalArgumentException(session.text("Please provide applicant information for ") + r.party.getFullName());
                }

                Applicant ap = applicants.get(r.partyUID);
                if (ap.idDocument == null) {
                    throw new IllegalArgumentException(session.text("ID document not provided for ") + r.party.getFullName());
                } else {
                    ap.idDocument.uid = UUID.randomUUID().toString();
                    tranRep.saveDocument(con, theTransaction, ap.idDocument);
                }
                if (ap.representative != null) {
                    lrm.createParty(con, theTransaction, ap.representative);
                    if (ap.letterOfAttorneyDocument == null) {
                        throw new IllegalArgumentException(session.text("ID document not provided for ") + ap.representative.getFullName());
                    }
                    ap.letterOfAttorneyDocument.uid = UUID.randomUUID().toString();
                    ap.letterOfAttorneyDocument.sourceType = DocumentTypeInfo.DOC_TYPE_LETTER_OF_ATTORNEY;
                    tranRep.saveDocument(con, theTransaction, ap.letterOfAttorneyDocument);
                }

            }
        }

        //save documents
        SourceDocument[] allDocs = new SourceDocument[]{tran.claimResolutionDocument, tran.landHoldingCertifcateDocument1, tran.landHoldingCertifcateDocument2};
        int[] allTypes = new int[]{DocumentTypeInfo.DOC_TYPE_DIVORCE_CLAIM_RESOLUTION, DocumentTypeInfo.DOC_TYPE_LAND_HOLDING_CERTIFICATE, DocumentTypeInfo.DOC_TYPE_LAND_HOLDING_CERTIFICATE};
        DocumentTypeRepo docTypeRepo = new DocumentTypeRepo(session);
        for (int i = 0; i < allDocs.length; i++) {
            SourceDocument srcDoc = allDocs[i];
            if (srcDoc == null) {
                throw new IllegalArgumentException(session.text("Source document of type") + docTypeRepo.getDocumentType(con, allTypes[i]).name.textEn + session.text(" is not provided"));
            }
            srcDoc.uid = UUID.randomUUID().toString();
            srcDoc.sourceType = allTypes[i];
            tranRep.saveDocument(con, theTransaction, srcDoc);
        }

        ArrayList<String> inholding = new ArrayList<String>();
        inholding.add(holding1.holdingUID);
        inholding.add(holding2.holdingUID);

        lrm.lrStartTransaction(con, theTransaction, inholding);

        LADM.Holding holding1Dest;
        LADM.Holding holding2Dest;
        if (holding1.nrlais_kebeleid.equals(holding2.nrlais_kebeleid)) {
            holding1Dest = holding2;
            holding2Dest = holding1;
        } else {
            holding1Dest = holding2.cloneHolding();
            holding1Dest.nrlais_kebeleid = holding1.nrlais_kebeleid;
            holding1Dest.holdingUID = lrm.lrCreateNewHolding(con, theTransaction, holding1Dest);

            holding2Dest = holding1.cloneHolding();
            holding2Dest.nrlais_kebeleid = holding2.nrlais_kebeleid;
            holding2Dest.holdingUID = lrm.lrCreateNewHolding(con, theTransaction, holding2Dest);
        }
        for (String p : tran.parcelUID1) {
            LADM.Parcel parcel = holding1.getParcel(p);
            if (parcel == null) {
                throw new IllegalArgumentException(session.text("Invalid parcel from holding 1. uid")+":" + p);
            }
            lrm.lrTransferParcel(con, theTransaction, p, holding1.holdingUID, holding1Dest.holdingUID, LADM.Parcel.AC_TYPE_EXCHANGE, EthiopianCalendar.ToEth(theTransaction.time).Year, holders2);
        }
        for (String p : tran.parcelUID2) {
            LADM.Parcel parcel = holding2.getParcel(p);
            if (parcel == null) {
                throw new IllegalArgumentException(session.text("Invalid parcel from holding 2. uid")+":" + p);
            }
            lrm.lrTransferParcel(con, theTransaction, p, holding2.holdingUID, holding2Dest.holdingUID, LADM.Parcel.AC_TYPE_EXCHANGE, EthiopianCalendar.ToEth(theTransaction.time).Year, holders1);
        }
        lrm.finishTransaction(con, theTransaction);
    }

    public void saveTransaction(UserSession session, Connection con,
            LATransaction theTransaction, boolean register) throws Exception {
        try {
            ExchangeTransactionData tran = (ExchangeTransactionData) theTransaction.data;
            LADM existing = new GetLADM(session, "nrlais_inventory").get(con, tran.holdingUID1, LADM.CONTENT_FULL);

            String err = existing.getRuleErrorsAsString(theTransaction.time);
            if (!org.apache.commons.lang3.StringUtils.isEmpty(err)) {
                throw new IllegalStateException(err);
            }

            if (!register) {
                return;
            }

            super.transferLandRecordToTransaction(session, con, theTransaction.transactionUID, existing);

            LADM holding2 = new GetLADM(session, "nrlais_inventory").get(con, tran.holdingUID2, LADM.CONTENT_FULL);
            if (holding2 == null) {
                throw new IllegalArgumentException(session.text("Second holding not found. uid ")+":" + tran.holdingUID2);
            }
            super.transferLandRecordToTransaction(session, con, theTransaction.transactionUID, holding2);
            if (tran.parcelUID1.size() == 0 || tran.parcelUID2.size() == 0) {
                throw new IllegalArgumentException(session.text("Parcels not completly specified"));
            }

            registerTransaction(session, con, theTransaction, tran);

        } catch (CloneNotSupportedException | IOException ex) {
            throw new SQLException(session.text("Can not execute transaction"), ex);
        }
    }
}
