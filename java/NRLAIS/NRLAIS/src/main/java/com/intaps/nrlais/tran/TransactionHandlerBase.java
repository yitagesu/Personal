/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.tran;

import com.intaps.nrlais.Startup;
import com.intaps.nrlais.UserSession;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.repo.GetLADM;
import com.intaps.nrlais.repo.GetTransaction;
import com.intaps.nrlais.repo.LRManipulator;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Tewelde
 */
class TransactionHandlerBase {
    void transferLandRecordToTransaction(UserSession session, Connection con, String txuid,LADM rec) throws SQLException, IOException
    {
        /*String sql="SELECT nrlais_tx_pkg.lock_parcels(?, ARRAY[?], ?)";
        PreparedStatement p=con.prepareStatement(sql);
        p.setString(1, txuid);
        String[] parcels=new String[rec.holding.parcels.size()];
        for(int i=0;i<parcels.length;i++)
            parcels[i]=rec.holding.parcels.get(i).parcelUID;        
        p.setArray(2, con.createArrayOf("varchar", parcels));
        p.setBoolean(3, true);
        p.execute();
        */
        LRManipulator man=new LRManipulator(session);
           
        
        man.lockLandRecord(con,rec,txuid);
       
           
        String sql="SELECT nrlais_tx_pkg.copy_inv2tx(?::uuid)";
        PreparedStatement p=con.prepareStatement(sql);
        p.setString(1, txuid);
        
        p.execute();
    }
    public LATransaction.TransactionData getData(UserSession session, Connection con,String transactionUID) throws SQLException 
    {
        try(ResultSet rs=con.prepareStatement("SELECT tx_data FROM nrlais_inventory.t_transaction_data where tx_uid='"+transactionUID+"'").executeQuery())
        {
            if(rs.next())
            {
                String json=rs.getString(1);
                return Startup.readTransaction(json).data;
            }
            else
                return null;
        }
    }
     public void filterStateChange(UserSession session, Connection con, String transactionUID, String stateChange) throws SQLException 
     {
         
     }
}
