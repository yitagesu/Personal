/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.controller.routes.map;

import com.google.gson.stream.JsonReader;
import com.intaps.nrlais.Startup;
import com.intaps.nrlais.controller.routes.RouteBase;
import com.intaps.nrlais.controller.routes.api.APIBase;
import com.intaps.nrlais.model.map.MapData;
import com.intaps.nrlais.repo.MainFacade;
import com.intaps.nrlais.util.GSONUtil;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.velocity.util.MapFactory;

/**
 *
 * @author Tewelde
 */
@WebServlet("/api/map/get_parcels")
public class GetParcels extends RouteBase<MapData, GSONUtil.EmptyReturn, GSONUtil.EmptyReturn>{

    @Override
    public MapData createGetResponse(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        String type=req.getParameter("type");
        switch(type)
        {
            case "holding":
               return new MainFacade(Startup.getSessionByRequest(req)).getHoldingParcels(req.getParameter("schema"), req.getParameter("huid"));
               case "parcel":
               return new MainFacade(Startup.getSessionByRequest(req)).getSingleParcel(req.getParameter("schema"), req.getParameter("puid"));
               case "kebele":
                return new MainFacade(Startup.getSessionByRequest(req)).getSingleKebele(req.getParameter("schema"), req.getParameter("kcode"));    
            case "tran":
               return new MainFacade(Startup.getSessionByRequest(req)).getTranscationParcels(req.getParameter("tran_uid"));
            default:
                   throw new IllegalArgumentException("Invalid map request");
               
        }
    }

    @Override
    public GSONUtil.EmptyReturn deserializePostBody(JsonReader reader) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public GSONUtil.EmptyReturn processPost(HttpServletRequest req, HttpServletResponse resp, GSONUtil.EmptyReturn data) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
}
