/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.controller;

import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.map.GeoJSON;
import com.intaps.nrlais.util.EthiopianCalendar;
import com.vividsolutions.jts.io.ParseException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Tewelde
 */
public class HoldingViewController extends ViewControllerBase {

    public LADM.Holding holding;
    public boolean showMapLink=false;
    public HoldingViewController(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
        super(request, response);
        String holdingUID = request.getParameter("holding_uid");
        String tranUID = request.getParameter("tran_uid");
        String schema = request.getParameter("schema");
        showMapLink="true".equals(request.getParameter("show_map_link"));
        LADM l = null;
        if (schema == null && tranUID != null) {
            l = mainFacade.getByTx(tranUID, holdingUID);
        } else if ("nrlais_transaction".equals(schema)) {
            l = mainFacade.getLADM("nrlais_transaction", holdingUID, LADM.CONTENT_FULL);
        } else if (schema == null || schema == "nrlais_inventory") {
            l = mainFacade.getLADM("nrlais_inventory", holdingUID, LADM.CONTENT_FULL);
        } else if ("nrlais_historic".equals(schema)) {
            l = mainFacade.getFromHistoryByArchiveTx(tranUID, holdingUID);
        }
        if (l == null) {
            holding = null;
        } else {
            holding = l.holding;
        }
    }

    public static class HoldingHistory {

        public String date;
        public String tranType;
        public String tranID;
        public String url;
        public String note;
        public String tranUID;
        public int tranTypeID;
    }

    public static class Rent {

        public List<LADM.Right> renter = new ArrayList<>();
        public List<LADM.Parcel> parcels = new ArrayList<>();
        public GeoJSON.BBox box() throws ParseException
        {
            GeoJSON gj = new GeoJSON();
            List<GeoJSON.GJFeature> features = new ArrayList<>();
            for (LADM.Parcel p : this.parcels) {
                features.add(new GeoJSON.GJFeature(null, p.geometry, new GeoJSON.GJProperty[0]));
            }
            gj.features = features;
            return gj.box();
        }
        private boolean rentersEqual(List<LADM.Right> tenants) {
            if (tenants.size() != tenants.size()) {
                return false;
            }
            for (LADM.Right x : this.renter) {
                boolean found = false;
                for (LADM.Right y : tenants) {
                    if (x.partyUID.equals(y.partyUID)
                            && x.startLeaseDate == y.startLeaseDate
                            && x.endLeaseDate == y.endLeaseDate) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    return false;
                }
            }
            return true;
        }
    }
    
    public static class Restriction {
        public List<LADM.Restriction> restrict=new ArrayList<>();
        public List<LADM.Parcel> parcels=new ArrayList<>();
        public GeoJSON.BBox box() throws ParseException{
            GeoJSON gj = new GeoJSON();
            List<GeoJSON.GJFeature> features=new ArrayList<>();
            for (LADM.Parcel p: this.parcels){
                features.add(new GeoJSON.GJFeature(null, p.geometry, new GeoJSON.GJProperty[0]));
            }
            gj.features=features;
            return gj.box();
        }
        private boolean restrectorEqual(List<LADM.Restriction> restrict){
            if(restrict.size() != restrict.size()){
                return false;
            }
            for(LADM.Restriction y: this.restrict){
                boolean found =false;
                for(LADM.Restriction x: restrict){
                    if (y.partyUID.equals(x.partyUID)
                            && y.startDate == x.startDate
                            && y.endDate == x.endDate) {
                        found = true;
                        break;
                    } 
                }
                if(!found){
                    return false;
                }
            }
            return true;
        }
    }
    
    public static class Servitude{
        public List<LADM.Restriction> servitude=new ArrayList<>();
        public List<LADM.Parcel> parcels=new ArrayList<>();
        public GeoJSON.BBox box() throws ParseException{
            GeoJSON gj = new GeoJSON();
            List<GeoJSON.GJFeature> features=new ArrayList<>();
            for (LADM.Parcel p: this.parcels){
                features.add(new GeoJSON.GJFeature(null, p.geometry, new GeoJSON.GJProperty[0]));
            }
            gj.features=features;
            return gj.box();
        }
        private boolean servitudeEqual(List<LADM.Restriction> servitude){
            if(servitude.size() != servitude.size()){
                return false;
            }
            for(LADM.Restriction y: this.servitude){
                boolean found =false;
                for(LADM.Restriction x: servitude){
                    if (y.partyUID.equals(x.partyUID)
                            && y.startDate == x.startDate
                            && y.endDate == x.endDate) {
                        found = true;
                        break;
                    } 
                }
                if(!found){
                    return false;
                }
            }
            return true;
        }
    }
    public List<HoldingHistory> holdingHistory() throws SQLException, IOException {
        List<HoldingHistory> hist = new ArrayList<HoldingHistory>();
        for (LATransaction tran : mainFacade.getHoldingHistory(holding.currenttxuid, holding.holdingUID)) {
            HoldingHistory h = new HoldingHistory();
            h.date = EthiopianCalendar.ToEth(tran.time).toString() + " EC";
            h.tranType = super.lookupText("nrlais_sys.t_cl_transactiontype", tran.transactionType);
            h.tranID = tran.transactionID();
            h.url = "/view_transaction_detail.jsp?tranUID=" + tran.transactionUID;
            h.note = tran.notes == null ? "" : tran.notes;
            h.tranTypeID = tran.transactionType;
            h.tranUID = tran.transactionUID;
            hist.add(h);
        }
        return hist;
    }

    public List<Rent> rents() throws Exception {
        final ArrayList<Rent> ret = new ArrayList<>();
        for (LADM.Parcel p : holding.parcels) {
            List<LADM.Right> tentants = p.getTenants();
            if (tentants.size() == 0) {
                continue;
            }
            Rent theRent = null;
            for (Rent r : ret) {
                if (r.rentersEqual(tentants)) {
                    theRent = r;
                    break;
                }
            }
            if (theRent == null) {
                theRent = new Rent();
                theRent.renter.addAll(tentants);
                ret.add(theRent);
            }
            theRent.parcels.add(p);
        }
        return ret;
    }
    public String rentArea(Rent rent,LADM.Parcel parcel)
    {
        for(LADM.Right r:rent.renter)
            if(r.parcelUID.equals(parcel.parcelUID))
                return Double.toString(r.rentSize);
        return "";
    }
    
    public List<Restriction> restrictions() throws Exception{
        final ArrayList<Restriction> res=new ArrayList<>();
        for(LADM.Parcel p : holding.parcels){
            List<LADM.Restriction> restrictions=p.getRestriction();
            if(restrictions.size()==0){
                continue;
            }
            Restriction theRestriction=null;
            for(Restriction r:res){
                if(r.restrectorEqual(restrictions)){
                    theRestriction=r;
                            break;
                }
            }
            if(theRestriction==null){
                theRestriction= new Restriction();
                theRestriction.restrict.addAll(restrictions);
                res.add(theRestriction);
            }
            theRestriction.parcels.add(p);
        }
        return res;
    }
    
    public List<Servitude> servitudes() throws Exception{
        final ArrayList<Servitude> res=new ArrayList<>();
        for(LADM.Parcel p : holding.parcels){
            List<LADM.Restriction> servitudes=p.getServitude();
            if(servitudes.size()==0){
                continue;
            }
            Servitude theServitude=null;
            for(Servitude r:res){
                if(r.servitudeEqual(servitudes)){
                    theServitude=r;
                            break;
                }
            }
            if(theServitude==null){
                theServitude= new Servitude();
                theServitude.servitude.addAll(servitudes);
                res.add(theServitude);
            }
            theServitude.parcels.add(p);
        }
        return res;
    }
     
    public String parcelMapLink(LADM.Parcel parcel) throws ParseException {
        GeoJSON.BBox box = parcel.box();
        return StringUtils.replaceEach("javascript:mapview_zoomBox($x,$y,$w,$h)",
                new String[]{"$x", "$y", "$w", "$h"}, new String[]{Double.toString(box.centerX()),
                    Double.toString(box.centerY()), Double.toString(box.width()), Double.toString(box.height())});
    }

    public String holdingMapLink() throws ParseException {
        GeoJSON.BBox box = this.holding.box();
        return StringUtils.replaceEach("javascript:mapview_zoomBox($x,$y,$w,$h)",
                new String[]{"$x", "$y", "$w", "$h"}, new String[]{Double.toString(box.centerX()),
                    Double.toString(box.centerY()), Double.toString(box.width()), Double.toString(box.height())});
    }
    public String rentMapLink(Rent r) throws ParseException {
        GeoJSON.BBox box = r.box();
        return StringUtils.replaceEach("javascript:mapview_zoomBox($x,$y,$w,$h)",
                new String[]{"$x", "$y", "$w", "$h"}, new String[]{Double.toString(box.centerX()),
                    Double.toString(box.centerY()), Double.toString(box.width()), Double.toString(box.height())});
    }
    public String servitudeMapLink(Servitude r)throws ParseException{
        GeoJSON.BBox box = r.box();
        return StringUtils.replaceEach("javascript:mapview_zoomBox($x,$y,$w,$h)",
                new String[]{"$x", "$y", "$w", "$h"}, new String[]{Double.toString(box.centerX()),
                    Double.toString(box.centerY()), Double.toString(box.width()), Double.toString(box.height())});
    }
    public String restrictionMapLink(Restriction r) throws ParseException{
        GeoJSON.BBox box = r.box();
        return StringUtils.replaceEach("javascript:mapview_zoomBox($x,$y,$w,$h)",
                new String[]{"$x", "$y", "$w", "$h"}, new String[]{Double.toString(box.centerX()),
                    Double.toString(box.centerY()), Double.toString(box.width()), Double.toString(box.height())});
    }
    public String rentFrom(Rent r)
    {
        if(r==null || r.renter.size()==0)
            return "";
        return EthiopianCalendar.ToEth(r.renter.get(0).startRentLeaseDate()).toString();
    }
        
    public String rentTo(Rent r)
    {
        if(r==null || r.renter.size()==0)
            return "";
        return EthiopianCalendar.ToEth(r.renter.get(0).endRentLeaseDate()).toString();
    }
    public String rentAmount(Rent r)
    {
        if(r==null || r.renter.size()==0)
            return "";
        return Double.toString(r.renter.get(0).rentLeaseAmount());
    }
    public String rentArea(Rent r,String parcelUID)
    {
        return "Not known";
    }
    public String parcelNo(LADM.Parcel parcel)
    {
        return LADM.Parcel.formatParcelSeqNo(parcel.seqNo);
    }
    
    public String restrictionFrom(Restriction r){
        if(r==null || r.restrict.size()==0){
            return "";
        }
        return EthiopianCalendar.ToEth(r.restrict.get(0).startRestrictionDate()).toString();
    }
    public String restrictionTo(Restriction r){
        if(r==null || r.restrict.size()==0)
            return "";
        return EthiopianCalendar.ToEth(r.restrict.get(0).endRestrictionDate()).toString();
    }
    
    public String servitudeFrom(Servitude r){
        if(r==null || r.servitude.size()==0 ){
            return "";
        }
        return EthiopianCalendar.ToEth(r.servitude.get(0).startRestrictionDate()).toString();
    }
    public String servitudeTo(Servitude r){
        if(r==null || r.servitude.size()==0)
            return "";
        return EthiopianCalendar.ToEth(r.servitude.get(0).endRestrictionDate()).toString();
    }
}
