/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.controller.tran;

import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.SourceDocument;
import com.intaps.nrlais.model.tran.Applicant;
import com.intaps.nrlais.model.tran.GiftTransactionData;
import com.intaps.nrlais.model.tran.ParcelTransfer;
import com.intaps.nrlais.model.tran.PartyItem;
import com.intaps.nrlais.model.tran.ReallocationTransactionData;
import com.intaps.nrlais.model.tran.TransferTransactionData;
import com.intaps.nrlais.util.GSONUtil;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Tewelde
 */
public class TransferViewController<T extends TransferTransactionData> extends HoldingTransactionViewController<T> {

    public LADM.Holding otherHolding = null;
    public List<PartyItem> beneficiaries = null;

    public TransferViewController(HttpServletRequest request, HttpServletResponse response, Class c, int transactionType, boolean loadData) throws Exception {
        super(request, response, c, transactionType, loadData);
        if (loadData) {
            if (data.holdingUID == null) {
                holding = new LADM.Holding();
            } else {
                if (tran.isCommited()) {
                    holding = mainFacade.getFromHistoryByArchiveTx(this.tran.transactionUID, data.holdingUID).holding;
                } else {
                    holding = mainFacade.getLADM("nrlais_inventory", data.holdingUID).holding;
                }
            }
            if (this.data.tranferType == GiftTransactionData.TRANSFER_TYPE_OTHER_HOLDING && data.otherHoldingUID != null) {
                if (tran.isCommited()) {
                    this.otherHolding = mainFacade.getFromHistoryByArchiveTx(this.tran.transactionUID, data.otherHoldingUID).holding;
                } else {
                    this.otherHolding = mainFacade.getLADM("nrlais_inventory", data.otherHoldingUID).holding;
                }

            }

            if (this.otherHolding != null) {
                {
                    for (PartyItem pi : this.data.beneficiaries) {
                        if (pi.party == null) {
                            pi.party = this.otherHolding.getParty(pi.existingPartyUID);
                        }
                    }
                }
            } else {
                this.beneficiaries = this.data.beneficiaries;
            }
            kebele = mainFacade.getKebele(holding.nrlais_kebeleid);
            woreda = mainFacade.getWoreda(holding.nrlais_woredaid);
        }
    }

    public Applicant partyApplicant(String partyUID) {
        for (Applicant ap : this.data.applicants) {
            if (partyUID.equals(ap.selfPartyUID)) {
                return ap;
            }
        }
        return null;
    }

    public String letterOfAttroneyText(Applicant ap) {
        if (ap == null) {
            return "";
        }

        if (ap.letterOfAttorneyDocument == null) {
            return "";
        }
        return ap.letterOfAttorneyDocument.refText;
    }

    public String letterOfAttroneyLink(Applicant ap) {
        if (ap == null) {
            return "";
        }
        return showDocumentLink(ap.letterOfAttorneyDocument);
    }

    public ParcelTransfer transfer(String parcelUID) {
        for (ParcelTransfer t : this.data.transfers) {
            if (t.parcelUID.equals(parcelUID)) {
                return t;
            }
        }

        return null;
    }

    public String documentText(SourceDocument doc) {
        if (doc == null) {
            return "";
        }
        return doc.refText;
    }

    public String holdingJson() {
        return GSONUtil.toJson(this.holding);
    }

    public List<PartyItem> beneficiaries() {
        return this.data.beneficiaries;
    }

    public List<ParcelTransfer> transfers() {
        return this.data.transfers;
    }

    public LADM.Parcel transferParcel(ParcelTransfer t) {
        return holding.getParcel(t.parcelUID);
    }

}
