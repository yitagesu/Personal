/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.tran;

import com.intaps.nrlais.UserSession;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.CMSSParcelSplitTask;
import com.intaps.nrlais.model.DocumentTypeInfo;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LADM.Parcel;
import com.intaps.nrlais.model.LADM.Party;
import com.intaps.nrlais.model.SourceDocument;
import com.intaps.nrlais.model.tran.Applicant;
import com.intaps.nrlais.model.tran.DivorceTransactionData;
import com.intaps.nrlais.model.tran.Partition;
import com.intaps.nrlais.model.tran.PartyParcelShare;
import com.intaps.nrlais.repo.CMSSTaskManager;
import com.intaps.nrlais.repo.DocumentTypeRepo;
import com.intaps.nrlais.repo.GetLADM;
import com.intaps.nrlais.repo.GetTransaction;
import com.intaps.nrlais.repo.LRManipulator;
import com.intaps.nrlais.repo.TransactionRepo;
import com.intaps.nrlais.util.EthiopianCalendar;
import com.intaps.nrlais.util.RationalNum;
import com.intaps.nrlais.worlais.WORLAISClient;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.AbstractList;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.Mac;

/**
 *
 * @author Tewelde
 */
public class DivorceTransactionHandler extends TransactionHandlerBase implements TransactionHandler {

    /**
     *
     * @param con
     * @param theTransaction
     * @throws SQLException
     */
    //the logic for modifying the land register goes here.
    void registerTransaction(UserSession session, Connection con, LATransaction theTransaction, DivorceTransactionData tran) throws SQLException, Exception {
        LRManipulator lrm = new LRManipulator(session);       
        TransactionRepo tranRep = new TransactionRepo(session);
        GetLADM repo = new GetLADM(session, "nrlais_transaction");
        LADM ladm = repo.get(con, tran.holdingUID, LADM.CONTENT_FULL);
        LADM.Holding holding = ladm.holding;

        //check applicants
        HashMap<String, Applicant> applicants = new HashMap<>();
        for (Applicant ap : tran.applicants) {
            if (applicants.containsKey(ap.selfPartyUID)) {
                throw new IllegalArgumentException(session.text("Duplicate applicant") + ap.selfPartyUID);
            }
            applicants.put(ap.selfPartyUID, ap);

        }
        HashSet<String> husbandAndWives = new HashSet<>();
        for (LADM.Right r : holding.getHolders()) {
            boolean husbandOrWife = false;
            switch (r.party.mreg_familyrole) {
                case Party.FAMILY_ROLE_HUSBAND:
                    husbandOrWife = true;
                    husbandAndWives.add(r.partyUID);
                    break;
                case Party.FAMILY_ROLE_WIFE:
                    husbandOrWife = true;
                    husbandAndWives.add(r.partyUID);
                    break;
            }
            if (husbandOrWife) {
                //check and save applicant document
                if (!applicants.containsKey(r.partyUID)) {
                    throw new IllegalArgumentException(session.text("Please provide applicant information for")+" " + r.party.getFullName());
                }

                Applicant ap = applicants.get(r.partyUID);
                if (ap.idDocument == null) {
                    throw new IllegalArgumentException(session.text("ID document not provided for")+" " + r.party.getFullName());
                } else {
                    ap.idDocument.uid = UUID.randomUUID().toString();
                    tranRep.saveDocument(con, theTransaction, ap.idDocument);
                }
                if (ap.representative != null) {
                    lrm.createParty(con, theTransaction, ap.representative);
                    if (ap.letterOfAttorneyDocument == null) {
                        throw new IllegalArgumentException(session.text("ID document not provided for")+" " + ap.representative.getFullName());
                    }
                    ap.letterOfAttorneyDocument.uid = UUID.randomUUID().toString();
                    ap.letterOfAttorneyDocument.sourceType = DocumentTypeInfo.DOC_TYPE_LETTER_OF_ATTORNEY;
                    tranRep.saveDocument(con, theTransaction, ap.letterOfAttorneyDocument);
                }
            }
        }
        //save documents
        SourceDocument[] allDocs = new SourceDocument[]{tran.proofOfDivorceDocument, tran.claimResolutionDocument, tran.landHoldingCertifcateDocument};
        int[] allTypes = new int[]{DocumentTypeInfo.DOC_TYPE_PROOF_OF_DIVORCE, DocumentTypeInfo.DOC_TYPE_DIVORCE_CLAIM_RESOLUTION, DocumentTypeInfo.DOC_TYPE_LAND_HOLDING_CERTIFICATE};
        DocumentTypeRepo docTypeRepo = new DocumentTypeRepo(session);
        for (int i = 0; i < allDocs.length; i++) {
            SourceDocument srcDoc = allDocs[i];
            if (srcDoc == null) {
                throw new IllegalArgumentException(session.text("Source document of type")+":" + docTypeRepo.getDocumentType(con, srcDoc.sourceType) + session.text("is not provided"));
            }
            srcDoc.uid = UUID.randomUUID().toString();
            srcDoc.sourceType = allTypes[i];
            tranRep.saveDocument(con, theTransaction, srcDoc);
        }

        HashMap<String, Partition> partitions = new HashMap<String, Partition>();
        tran.partitions.forEach((p) -> {
            partitions.put(p.parcelUID, p);
        });

        lrm.lrStartTransaction(con, theTransaction, holding.holdingUID);

        HashMap<String, String> splitHoldings = new HashMap<>();

        Iterator<Parcel> parcelIter = holding.parcels.iterator();

        while (parcelIter.hasNext()) {
            LADM.Parcel parcel = parcelIter.next();
            if (!partitions.containsKey(parcel.parcelUID)) {
                throw new IllegalArgumentException(session.text("Partition information for parcel") + parcel.upid + session.text("is not set")+".");
            }
            Partition parition = partitions.get(parcel.parcelUID);
            HashMap<String, PartyParcelShare> shares = new HashMap<>();
            parition.shares.forEach((s)
                    -> {
                shares.put(s.partyUID, s);
            });
            if (parition.hasShared() && !parition.is100Percent()) {
                throw new IllegalArgumentException(session.text("Total share of shard parcels for the divorcing partners should be")+"100%");
            }
            double minSize = session.worlaisSession.getMinAreaConfig(con,ladm);
            boolean splitable = parcel.areaGeom > minSize;
            List<LADM.Right> holders = parcel.getHolders();

            Iterator<LADM.Right> iter = holders.iterator();
            ArrayList<Map.Entry<String, LADM.Parcel>> splitParcels = new ArrayList<>();
            boolean transfered = false;
            HashMap<LADM.Parcel,PartyParcelShare> parcelToShare=new HashMap<>();
            while (iter.hasNext()) {
                LADM.Right r = iter.next();
                switch (r.party.mreg_familyrole) {
                    case Party.FAMILY_ROLE_HUSBAND:
                    case Party.FAMILY_ROLE_WIFE:
                        if (shares.containsKey(r.partyUID)) {
                            r.party.maritalstatus = LADM.Party.MARITAL_STATUS_DIVORCED;
                            if (r.party.sex == LADM.Party.SEX_MALE) {
                                r.party.mreg_familyrole = LADM.Party.FAMILY_ROLE_MALE_HOUSEHOLD_HEAD;
                            } else {
                                r.party.mreg_familyrole = LADM.Party.FAMILY_ROLE_FEMALE_HOUSEHOLD_HEAD;
                            }
                            PartyParcelShare share = shares.get(r.partyUID);
                            
                            if (share.splitGeom) {
                                if (!splitable) {
                                    throw new IllegalArgumentException(session.text("This parcel can not be split because the parcel size is less than the threshold value"));
                                }
                                String sh;
                                if (splitHoldings.containsKey(r.partyUID)) {
                                    sh = splitHoldings.get(r.partyUID);
                                } else {
                                    LADM.Holding h = holding.cloneHolding();
                                    sh = lrm.lrCreateNewHolding(con, theTransaction, h);
                                    splitHoldings.put(r.partyUID, sh);
                                }
                                LADM.Parcel splitParcel = parcel.clonedObjectNoRR();
                                splitParcel.resetGeom();
                                splitParcel.mreg_actype = Parcel.AC_TYPE_DIVORCE;
                                splitParcel.mreg_acyear = EthiopianCalendar.ToEth(theTransaction.time).Year;
                                LADM.Right clonedRight = r.clonedObject(false);
                                clonedRight.share.set(1, 1);
                                splitParcel.rights.add(clonedRight);
                                splitParcels.add(new AbstractMap.SimpleEntry<>(sh, splitParcel));
                                parcelToShare.put(splitParcel,share);
                                iter.remove();

                            } else if (share.share.isOne()) {   //if one of the partners get the whole of this parcel

                                String sh;
                                if (splitHoldings.containsKey(r.partyUID)) {
                                    sh = splitHoldings.get(r.partyUID);
                                } else {
                                    LADM.Holding h = holding.cloneHolding();
                                    sh = lrm.lrCreateNewHolding(con, theTransaction, h);
                                    splitHoldings.put(r.partyUID, sh);
                                }
                                List<LADM.Right> transferedRights = new ArrayList<>();
                                LADM.Right clonedRight = r.clonedObject(false);
                                clonedRight.share.set(1, 1);
                                transferedRights.add(clonedRight);
                                lrm.lrTransferParcel(con, theTransaction, parcel.parcelUID, holding.holdingUID, sh, 
                                        LADM.Parcel.AC_TYPE_DIVORCE, EthiopianCalendar.ToEth(theTransaction.time).Year, transferedRights);
                                share.parcelUID=parcel.parcelUID;
                                transfered = true;
                                iter.remove();
                                break;

                            } else if (share.share.num == 0) { //if one of the partners loses the whole of this parcel
                                iter.remove();
                                share.parcelUID=null;
                                continue;
                            } else {
                                r.share = share.share;
                                share.parcelUID=parcel.parcelUID;
                            }
                        }
                        break;
                }
            }//rights of parcel iterator

            if (transfered) {
                if (splitParcels.size() > 0) {
                    throw new IllegalArgumentException(session.text("A transfered parcel can not be split"));
                }
                continue;
            }

            if (splitParcels.size() > 0) {
                if (!holders.isEmpty()) //if there are holders that hasn't split their share add one split item for them
                {
                    //verfy that their share adds up to 100%
                    RationalNum total = new RationalNum();
                    for (LADM.Right r : holders) {
                        total.add(r.share);
                    }
                    if (!total.isOne()) {
                        throw new IllegalArgumentException(session.text("The total share of the holders that are sharing")+" " + parcel.upid + session.text("is not")+" 100%");
                    }
                    lrm.lrUpateRights(con, theTransaction, holding.holdingUID, parcel.parcelUID, holders);
                    
                    splitParcels.add(new AbstractMap.SimpleEntry<>(holding.holdingUID, parcel));
                }
                lrm.lrSplitParcel(con, theTransaction, parcel.parcelUID, holding.holdingUID, splitParcels);
                for(Map.Entry<String,LADM.Parcel> s:splitParcels)
                {
                    parcelToShare.get(s.getValue()).parcelUID=s.getValue().parcelUID;
                }
                
            } else {
                lrm.lrUpateRights(con, theTransaction, holding.holdingUID, parcel.parcelUID, holders);
            }

            //add cmss split parcel task if necessary
        }//parcels iterator

        lrm.finishTransaction(con, theTransaction);
    }

    /*
    //backup of previos algorithm
                    if (shares.containsKey(r.partyUID)) {
                            PartyParcelShare share = shares.get(r.partyUID);
                            if (share.splitGeom) {
                                if(!splitable)
                                    throw new IllegalArgumentException("This parcel can't be split because ");
                                iter.remove();
                                SplitHoldingData sdata = splitHoldings.get(r.partyUID);

                                Parcel clonedParcel = parcel.clonedObjectNoRR();
                                clonedParcel.parcelUID = lrm.getParcelUID();
                                clonedParcel.mreg_actype = Parcel.AC_TYPE_DIVORCE;
                                clonedParcel.mreg_acyear = EthiopianCalendar.ToEth(new java.util.Date()).Year;

                                splitParcel.add(clonedParcel.parcelUID);

                                LADM.Right clonedRight;
                                if (sdata.partyMap.containsKey(r.partyUID)) {
                                    clonedRight = r.clonedObject(false);
                                    clonedRight.party = sdata.partyMap.get(r.partyUID);
                                    clonedRight.partyUID = clonedRight.party.partyUID;
                                } else {
                                    clonedRight = r.clonedObject(true);
                                    clonedRight.partyUID = (clonedRight.party.partyUID = lrm.getPartyUID());
                                    sdata.partyMap.put(r.partyUID, clonedRight.party);
                                }
                                clonedRight.rightUID = lrm.getRightUID();
                                clonedRight.share.num = 1;
                                clonedRight.share.denum = 1;
                                clonedParcel.rights.add(clonedRight);

                                clonedRight.party.mreg_familyrole = clonedRight.party.sex == Party.SEX_MALE ? Party.FAMILY_ROLE_MALE_HOUSEHOLD_HEAD : Party.FAMILY_ROLE_FEMALE_HOUSEHOLD_HEAD;
                                for (LADM.Restriction restr : parcel.restrictions) {
                                    LADM.Restriction clonedRestr;
                                    if (sdata.partyMap.containsKey(r.partyUID)) {
                                        clonedRestr = restr.clonedObject(false);
                                        clonedRestr.party = sdata.partyMap.get(r.partyUID);
                                        clonedRestr.partyUID = clonedRestr.party.partyUID;
                                    } else {
                                        clonedRestr = restr.clonedObject(true);
                                        clonedRestr.restrictionUID = clonedRestr.party.partyUID = lrm.getRestrictionUID();
                                        sdata.partyMap.put(restr.partyUID, clonedRestr.party);
                                    }
                                    clonedParcel.restrictions.add(clonedRestr);
                                }
                                sdata.ladm.holding.parcels.add(clonedParcel);
                            } else if (share.share.isOne()) {   //if one of the partners get the whole of this parcel
                                iter.remove();
                                SplitHoldingData sdata = splitHoldings.get(r.partyUID);

                                Parcel clonedParcel = parcel.clonedObjectNoRR();
                                
                                clonedParcel.parcelUID = parcel.parcelUID;
                                clonedParcel.mreg_actype = Parcel.AC_TYPE_DIVORCE;
                                clonedParcel.mreg_acyear = EthiopianCalendar.ToEth(new java.util.Date()).Year;
                                clonedParcel.seqNo = parcel.seqNo;//retain sequence number and UPID
                                clonedParcel.upid = parcel.upid;
                                
                                sdata.existingParcel.add(parcel.parcelUID);
                                
                                LADM.Right clonedRight;
                                if (sdata.partyMap.containsKey(r.partyUID)) {
                                    clonedRight = r.clonedObject(false);
                                    clonedRight.party = sdata.partyMap.get(r.partyUID);
                                    clonedRight.partyUID = clonedRight.party.partyUID;
                                } else {
                                    clonedRight = r.clonedObject(true);
                                    clonedRight.partyUID = (clonedRight.party.partyUID = lrm.getPartyUID());
                                    sdata.partyMap.put(r.partyUID, clonedRight.party);
                                }
                                clonedRight.rightUID = lrm.getRightUID();
                                clonedRight.share.num = 1;
                                clonedRight.share.denum = 1;
                                clonedParcel.rights.add(clonedRight);

                                clonedRight.party.mreg_familyrole = clonedRight.party.sex == Party.SEX_MALE ? Party.FAMILY_ROLE_MALE_HOUSEHOLD_HEAD : Party.FAMILY_ROLE_FEMALE_HOUSEHOLD_HEAD;
                                for (LADM.Restriction restr : parcel.restrictions) {
                                    LADM.Restriction clonedRestr;
                                    if (sdata.partyMap.containsKey(restr.partyUID)) {
                                        clonedRestr = restr.clonedObject(false);
                                        clonedRestr.party = sdata.partyMap.get(restr.partyUID);
                                        clonedRestr.partyUID = clonedRestr.party.partyUID;
                                    } else {
                                        clonedRestr = restr.clonedObject(true);
                                        clonedRestr.partyUID = clonedRestr.party.partyUID = lrm.getRestrictionUID();
                                        sdata.partyMap.put(restr.partyUID, clonedRestr.party);
                                    }
                                    clonedParcel.restrictions.add(clonedRestr);
                                }
                                sdata.ladm.holding.parcels.add(clonedParcel);

                            } else if(share.share.num==0){ //if one of the partners loses the whole of this parcel
                                iter.remove();
                            } else {
                                r.share = share.share;
                            }
                        }
                        break;
                }
            }//rights iterator

            //add cmss split parcel task if necessary
            if (holders.isEmpty()) {    //the parcel is completly split among the divorcing partners
                parcelIter.remove();
                if(splitParcel.size()>0)
                {
                    CMSSParcelSplitTask task = new CMSSParcelSplitTask();
                    task.transactionUID = theTransaction.transactionUID;
                    task.parcelUID = parcel.parcelUID;
                    task.newParcelUIDs = splitParcel;
                    cmss.addSplitParcelTask(con, task);
                }
            } else {
                if (splitParcel.size() > 0) //the parcel is partially split among the divorcing partners
                {
                    splitParcel.add(parcel.parcelUID);
                    CMSSParcelSplitTask task = new CMSSParcelSplitTask();
                    task.transactionUID = theTransaction.transactionUID;
                    task.parcelUID = parcel.parcelUID;
                    task.newParcelUIDs = splitParcel;
                    cmss.addSplitParcelTask(con, task);
                }
            }

        }//parcels iterator

        //save changes
        if (holding.parcels.isEmpty()) //all parcels are fully split, delete the original holder
        {
            lrm.deleteHolding(con, theTransaction, holding.holdingUID);
        } else {
            for (LADM.Right holder : ladm.holding.getHolders()) {
                holder.party.maritalstatus = LADM.Party.MARITAL_STATUS_DIVORCED;
                if (holder.party.sex == LADM.Party.SEX_MALE) {
                    holder.party.mreg_familyrole = LADM.Party.FAMILY_ROLE_MALE_HOUSEHOLD_HEAD;
                } else {
                    holder.party.mreg_familyrole = LADM.Party.FAMILY_ROLE_FEMALE_HOUSEHOLD_HEAD;
                }
            }
            lrm.updateHolding(con, theTransaction, ladm); //keep the existing holding but update it to reflect changes if any
        }
            for (SplitHoldingData s : splitHoldings.values()) {
            //s.holding.clearPartyUIDs();
            if (!s.ladm.holding.parcels.isEmpty()) {
                ladm.holding.holdingUID = lrm.createHolding(con, theTransaction, s.ladm,s.existingParcel);
            }
        }
     */
    @Override
    public void saveTransaction(UserSession session, Connection con, LATransaction theTransaction, boolean register) throws Exception {
        try {
            DivorceTransactionData tran = (DivorceTransactionData) theTransaction.data;
            LADM existing = new GetLADM(session, "nrlais_inventory").get(con, tran.holdingUID, LADM.CONTENT_FULL);

            String err = existing.getRuleErrorsAsString(theTransaction.time);
            if (!org.apache.commons.lang3.StringUtils.isEmpty(err)) {
                throw new IllegalStateException(err);
            }
            //reject hodling that has none holding right
            if (existing.holding.hasNoneHoldingRight()) {
                throw new IllegalArgumentException(session.text("Divorce transaction can not be executed on a holding with none-holding rights"));
            }
            //reject state land 
            if (existing.holding.isStateLand()) {
                throw new IllegalArgumentException(session.text("Divorce transaction can not be exectued on state land"));
            }
            boolean couples=true;
            List<LADM.Right> holders=existing.holding.getHolders();
            if(holders.size()<2)
            {
                for(LADM.Right r:holders)
                {
                    if(r.party.maritalstatus!=LADM.Party.MARITAL_STATUS_MARRIED)
                    {
                        couples=false;
                        break;
                    }
                    if(r.party.mreg_familyrole!=LADM.Party.FAMILY_ROLE_HUSBAND && r.party.mreg_familyrole!=LADM.Party.FAMILY_ROLE_WIFE)
                    {
                        couples=false;
                        break;
                    }
                }
            }
            if(!couples)
                throw new IllegalArgumentException(session.text("The holding must be owned by married couples for divorce transaction")+".\n"+session.text("If there is recording problem, please periform correction transaciton first"));
            
            if (!register) {
                return;
            }
            
            super.transferLandRecordToTransaction(session, con, theTransaction.transactionUID, existing);
            registerTransaction(session, con, theTransaction, tran);

        } catch (CloneNotSupportedException | IOException ex) {
            throw new SQLException(session.text("Can not execute transaction"), ex);
        }
    }

}
