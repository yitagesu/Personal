/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.repo;

import com.intaps.nrlais.UserSession;
import com.intaps.nrlais.model.CMSSParcelSplitTask;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.KebeleInfo;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.NRLAISEntity;
import com.intaps.nrlais.model.NRLAISTransactionEntity;
import com.intaps.nrlais.util.DiffUtility;
import com.intaps.nrlais.util.DiffUtility.Diff;
import com.intaps.nrlais.util.NamedPreparedStatement;
import com.intaps.nrlais.util.RationalNum;
import com.intaps.nrlais.util.ResourceUtils;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Tewelde
 */
public class LRManipulator extends RepoBase {

    public LRManipulator(UserSession session) {
        super(session);
    }

    public String getRightUID() {
        return UUID.randomUUID().toString();
    }

    public String getParcelUID() {
        return UUID.randomUUID().toString();
    }

    public String getRestrictionUID() {
        return UUID.randomUUID().toString();
    }

    public String createHolding(Connection con, LATransaction tran, LADM s, HashSet<String> existingParcels) throws IOException, SQLException, Exception {
        insertHolding(con, tran, s);
        insertParcels(con, tran, s.holding.holdingUID, s.holding.parcels, existingParcels);
        s.holding.forEachParty(new LADM.Holding.ProcessItem<LADM.Party>() {
            HashSet<String> done = new HashSet<>();

            @Override
            public boolean process(LADM.Party party) throws Exception {
                if (party.partyUID != null && done.contains(party.partyUID)) {
                    return true;
                }
                party.partyUID = insertParty(con, tran, party);
                done.add(party.partyUID);
                return true;
            }
        });
        for (LADM.Parcel p : s.holding.parcels) {
            insertRights(con, tran, s.holding.holdingUID, p, false);
            insertRestrictions(con, tran, s.holding.holdingUID, p, false);
        }
        return s.holding.holdingUID;
    }

    private String addTransactionContent(Connection con, String transactionUID, NRLAISEntity e, String objectuid, int objectType, String notes) throws SQLException, IOException {
        NamedPreparedStatement insertTranContent = ResourceUtils.getPreparedStatementFromResource(session, con, "insert_transaction_content.sql");
        String ret = UUID.randomUUID().toString();
        insertTranContent.setString("@uid", ret);
        setNRLAISEntityFields(insertTranContent, e);
        insertTranContent.setString("@notes", notes);
        insertTranContent.setString("@txuid", transactionUID);
        insertTranContent.setInt("@inputobjecttype", objectType);
        insertTranContent.setString("@inputobjectuid", objectuid);
        insertTranContent.setString("@outputobjectuid", null);
        insertTranContent.setString("@task", "ATWSS Task");
        insertTranContent.setBoolean("@isprimary", true);
        insertTranContent.setInt("@status", 2);
        insertTranContent.preparedStatement().execute();
        return ret;
    }

    public void updatePartyRecord(Connection con, LATransaction tran, LADM.Party p) throws IOException, SQLException {
        NamedPreparedStatement insertParty = ResourceUtils.getPreparedStatementFromResource(session,con, "update_party.sql");
        setPartyRecord(p, insertParty, tran);
        LADM.Party existing=new GetLADM(session, "nrlais_transaction").getParty(con, p.partyUID, LADM.CONTENT_FULL);
        deletePartyChilds(existing, con, tran);
        p.clearUIDForChilds();
        insertPartyChilds(con, p, tran);
    }

    private String insertParty(Connection con, LATransaction tran, LADM.Party p) throws IOException, SQLException {
        NamedPreparedStatement insertParty = ResourceUtils.getPreparedStatementFromResource(session,con, "insert_party.sql");
        setPartyRecord(p, insertParty, tran);

        insertPartyChilds(con, p, tran);
        return p.partyUID;
    }

    private void insertPartyChilds(Connection con, LADM.Party p, LATransaction tran) throws SQLException, IOException {
        NamedPreparedStatement insertPartyMember = ResourceUtils.getPreparedStatementFromResource(session,con, "insert_group_member.sql");
        RationalNum totalShare = new RationalNum(0, 0);
        for (LADM.GroupPartyMembership m : p.members) {
            if (m.membershipUID == null) {
                m.membershipUID = UUID.randomUUID().toString();
            }
            m.memberPartyUID = insertParty(con, tran, m.member);
            insertPartyMember.setString("@uid", m.membershipUID);

            setNRLAISTransactionEntityFields(insertPartyMember, p);
            insertPartyMember.setString("@notes", m.notes);
            insertPartyMember.setString("@grouppartyuid", p.partyUID);
            insertPartyMember.setString("@memberpartyuid", m.memberPartyUID);
            insertPartyMember.setInt("@sharenumerator", m.share.num);
            insertPartyMember.setInt("@sharedenominator", m.share.denum);
            totalShare.add(m.share);
            insertPartyMember.preparedStatement().execute();
        }
        if (p.members.size() > 0 && !totalShare.isOne()) {
            throw new IllegalArgumentException("Sum of total share should be 100%");
        }

        NamedPreparedStatement insertPartyContact = ResourceUtils.getPreparedStatementFromResource(session,con, "insert_contact.sql");
        for (LADM.PartyContact contact : p.contacts) {
            contact.uid = UUID.randomUUID().toString();
            insertPartyContact.setString("@uid", contact.uid);
            insertPartyContact.setString("@partyuid", p.partyUID);
            setNRLAISTransactionEntityFields(insertPartyContact, p);
            insertPartyContact.setString("@notes", contact.notes);
            insertPartyContact.setString("@contactdata", contact.contactData);
            insertPartyContact.setInt("@contactdatatype", contact.contactdataType);
            insertPartyContact.setString("@contactdatatypeother", contact.contactDataTypeOther);
            insertPartyContact.preparedStatement().execute();
        }

        NamedPreparedStatement insertPartyRole = ResourceUtils.getPreparedStatementFromResource(session,con, "insert_party_role.sql");
        for (LADM.PartyRole role : p.roles) {
            role.uid = UUID.randomUUID().toString();
            insertPartyRole.setString("@uid", role.uid);
            insertPartyRole.setString("@partyuid", p.partyUID);
            setNRLAISTransactionEntityFields(insertPartyRole, p);
            insertPartyRole.setString("@notes", role.notes);
            insertPartyRole.setInt("@roletype", role.roleType);
            insertPartyRole.setString("@roletypeother", role.roleTypeOther);
            insertPartyRole.preparedStatement().execute();
        }
        if (p.guardian != null) {
            String guardianID = insertParty(con, tran, p.guardian);
            NamedPreparedStatement insertPartyRelation = ResourceUtils.getPreparedStatementFromResource(session,con, "insert_party_relation.sql");

            insertPartyRelation.setString("@uid", UUID.randomUUID().toString());
            setNRLAISTransactionEntityFields(insertPartyRelation, p);
            insertPartyRelation.setString("@notes", "guradian");
            insertPartyRelation.setString("@frompartyuid", p.partyUID);
            insertPartyRelation.setString("@topartyuid", guardianID);
            insertPartyRelation.setInt("@relationshiptype", LADM.Party.RELATION_GUARDIAN);
            insertPartyRelation.preparedStatement().execute();

        }
    }

    private void setPartyRecord(LADM.Party p, NamedPreparedStatement insertParty, LATransaction tran) throws SQLException {
        if (p.partyUID == null) {
            p.partyUID = UUID.randomUUID().toString();
        }
        insertParty.setString("@uid", p.partyUID);

        KebeleInfo.fixNRLAISEntityFileds(session, tran.nrlais_kebeleid, p, true);
        p.currenttxuid = tran.transactionUID;
        setNRLAISTransactionEntityFields(insertParty, p);

        insertParty.setString("@notes", p.notes);
        insertParty.setInt("@partytype", p.partyType);
        insertParty.setString("@name", p.name1);
        insertParty.setString("@fathersname", p.name2);
        insertParty.setString("@grandfathersname", p.name3);
        insertParty.setString("@gender", RepoBase.reverseLookupSex(p.sex));
        insertParty.setDate("@dateofbirth", new java.sql.Date(p.dateOfBirth));
        insertParty.setBytes("@photo", p.photo);
        insertParty.setString("@extident", p.idText);
        insertParty.setInt("@extidenttype", p.idType);
        insertParty.setInt("@maritalstatus", p.maritalstatus);
        insertParty.setInt("@isorphan", p.isorphan);
        insertParty.setInt("@disability", p.disability);

        if (p.mreg_familyrole < 1) {
            insertParty.setNull("@mreg_familyrole", java.sql.Types.INTEGER);
        } else {
            insertParty.setInt("@mreg_familyrole", p.mreg_familyrole);
        }

        if (p.orgatype < 1) {
            insertParty.setNull("@orgatype", java.sql.Types.INTEGER);
        } else {
            insertParty.setInt("@orgatype", p.orgatype);
        }

        insertParty.preparedStatement().execute();
    }

    private void updateRight(Connection con, LATransaction tran, LADM.Right r) throws SQLException, IOException {
        deleteRight(con, tran, r, false);
        r.rightUID = UUID.randomUUID().toString();
        insertRight(con, tran, r.holdingUID, r.parcelUID, r, false);
    }

    private void insertRights(Connection con, LATransaction tran, String holdingUID, LADM.Parcel p, boolean insertParty) throws IOException, SQLException {
        for (LADM.Right r : p.rights) {
            insertRight(con, tran, holdingUID, p.parcelUID, r, insertParty);
        }
    }

    private void insertRight(Connection con, LATransaction tran, String holdingUID, String parcelUID, LADM.Right r, boolean insertParty) throws IOException, SQLException {
        NamedPreparedStatement insertRight = ResourceUtils.getPreparedStatementFromResource(session,con, "insert_right.sql");
        if (r.rightUID == null) {
            r.rightUID = UUID.randomUUID().toString();
        }

        insertRight.setString("@uid", r.rightUID);
        KebeleInfo.fixNRLAISEntityFileds(session, tran.nrlais_kebeleid, r, true);
        r.currenttxuid = tran.transactionUID;

        setNRLAISTransactionEntityFields(insertRight, r);

        insertRight.setString("@notes", r.notes);
        insertRight.setInt("@righttype", r.rightType);
        insertRight.setInt("@sharenumerator", r.share.num);
        insertRight.setInt("@sharedenominator", r.share.denum);
        insertRight.setDate("@startleasedate", new java.sql.Date(r.startLeaseDate));
        insertRight.setDate("@endleasedate", new java.sql.Date(r.endLeaseDate));
        insertRight.setDouble("@leaseamount", r.leaseAmount);
        insertRight.setString("@leaseref", r.leaseRef);
        insertRight.setDate("@startrentdate", new java.sql.Date(r.startRentDate));
        insertRight.setDate("@endrentdate", new java.sql.Date(r.endRentDate));
        insertRight.setDouble("@rentsize", r.rentSize);
        insertRight.setDate("@startcroppingdate", new java.sql.Date(r.startSharedCroppingDate));
        insertRight.setDate("@endcroppingdate", new java.sql.Date(r.endCroppingDate));
        insertRight.setString("@holdinguid", holdingUID);
        if (insertParty) {
            r.partyUID = insertParty(con, tran, r.party);
        }
        insertRight.setString("@partyuid", r.partyUID);
        insertRight.setString("@parceluid", parcelUID);
        insertRight.preparedStatement().execute();
    }

    private void updateRestriction(Connection con, LATransaction tran, LADM.Restriction r) throws SQLException, IOException {
        deleteRestriction(con, tran, r, false);
        r.restrictionUID = UUID.randomUUID().toString();
        insertRestriction(con, tran, r.holdingUID, r.parcelUID, r, false);
    }

    private void insertRestrictions(Connection con, LATransaction tran, String holdingUID, LADM.Parcel p, boolean insertParty) throws IOException, SQLException {
        for (LADM.Restriction r : p.restrictions) {
            insertRestriction(con, tran, holdingUID, p.parcelUID, r, insertParty);
        }
    }

    private void insertRestriction(Connection con, LATransaction tran, String holdingUID, String parcelUID, LADM.Restriction r, boolean insertParty) throws SQLException, IOException {
        NamedPreparedStatement insertRestriction = ResourceUtils.getPreparedStatementFromResource(session,con, "insert_restriction.sql");
        if (r.restrictionUID == null) {
            r.restrictionUID = UUID.randomUUID().toString();
        }
        insertRestriction.setString("@uid", r.restrictionUID);
        KebeleInfo.fixNRLAISEntityFileds(session, tran.nrlais_kebeleid, r, true);
        r.currenttxuid = tran.transactionUID;
        setNRLAISTransactionEntityFields(insertRestriction, r);

        insertRestriction.setString("@notes", r.notes);
        insertRestriction.setInt("@restrictiontype", r.restrictionType);
        insertRestriction.setString("@restrictiontypeother", r.restrictionTypeOther);
        insertRestriction.setString("@holdinguid", holdingUID);
        insertRestriction.setDate("@startdate", new java.sql.Date(r.startDate));
        insertRestriction.setDate("@enddate", new java.sql.Date(r.endDate));
        if (insertParty) {
            r.partyUID = insertParty(con, tran, r.party);
        }
        insertRestriction.setString("@partyuid", r.partyUID);
        insertRestriction.setString("@parceluid", parcelUID);
        insertRestriction.preparedStatement().execute();
    }

    private void insertParcels(Connection con, LATransaction tran, String holdingUID, Collection<LADM.Parcel> s, HashSet<String> existingParcels) throws IOException, SQLException {
        for (LADM.Parcel p : s) {
            if (p.parcelUID != null && existingParcels != null && existingParcels.contains(p.parcelUID)) {
                updateParcelRecord(con, tran, holdingUID, p);
            } else {
                insertParcel(con, tran, holdingUID, p);
            }
        }
    }

    private void insertParcel(Connection con, LATransaction tran, String holdingUID, LADM.Parcel p) throws SQLException, IOException {
        NamedPreparedStatement insertParcel = ResourceUtils.getPreparedStatementFromResource(session,con, "insert_parcel.sql");
        setParcelRecord(p, insertParcel, tran, con, holdingUID, true);
    }

    private void updateParcelRecord(Connection con, LATransaction tran, String holdingUID, LADM.Parcel p) throws SQLException, IOException {
        NamedPreparedStatement insertParcel = ResourceUtils.getPreparedStatementFromResource(session,con, "update_parcel.sql");
        setParcelRecord(p, insertParcel, tran, con, holdingUID, false);
    }

    private void setParcelRecord(LADM.Parcel p, NamedPreparedStatement insertParcel, LATransaction tran, Connection con, String holdingUID, boolean forInsert) throws SQLException {
        if (p.parcelUID == null) {
            p.parcelUID = UUID.randomUUID().toString();
        }
        insertParcel.setString("@uid", p.parcelUID);
        KebeleInfo.fixNRLAISEntityFileds(session, tran.nrlais_kebeleid, p, true);
        p.currenttxuid = tran.transactionUID;
        if (p.seqNo < 1) {
            p.setUPID(tran.nrlais_kebeleid, getSeqNo(con, tran.nrlais_kebeleid, "t_upid_seq"));
        }
        p.areaGeom = 0;
        insertParcel.setInt("@seqnr", p.seqNo);
        insertParcel.setString("@upid", p.upid);
        insertParcel.setDouble("@areageom", p.areaGeom);
        insertParcel.setDouble("@arealegal", p.areaLegal);
        insertParcel.setDouble("@areasurveyed", p.areaSurveyed);
        insertParcel.setString("@adjudicationid", p.adjudicationID);
        insertParcel.setInt("@adjudicatedby", p.adjudicatedBy);
        insertParcel.setString("@geometry", p.geometry);
        insertParcel.setString("@referencepoint", p.referencePoint);
        insertParcel.setInt("@landuse", p.landUse);
        insertParcel.setInt("@level", p.level);
        insertParcel.setString("@notes", p.notes);
        p.isPrimary = false;
        insertParcel.setBoolean("@isprimary", p.isPrimary);
        insertParcel.setInt("@mreg_teamid", p.mreg_teamid);
        insertParcel.setString("@mreg_mapsheet", p.mreg_mapsheetNo);
        insertParcel.setInt("@mreg_stage", p.mreg_stage);
        insertParcel.setInt("@mreg_actype", p.mreg_actype);
        insertParcel.setInt("@mreg_acyear", p.mreg_acyear);
        insertParcel.setInt("@soilfertilitytype", p.soilfertilityType);
        insertParcel.setDate("@mreg_surveydate", new java.sql.Date(p.mreg_surveyDate));

        if (forInsert) {
            insertParcel.setString("@fdc_uid", UUID.randomUUID().toString());
            insertParcel.setString("@sys_fs_h_uid", UUID.randomUUID().toString());
            insertParcel.setString("@holdinguid", holdingUID);
        }
        insertParcel.setString("@uid", p.parcelUID);

        setNRLAISTransactionEntityFields(insertParcel, p);
        insertParcel.preparedStatement().execute();
    }

    private void insertHolding(Connection con, LATransaction tran, LADM ladm) throws IOException, SQLException {
        insertHolding(con, tran, ladm.holding);
    }

    private void insertHolding(Connection con, LATransaction tran, LADM.Holding holding) throws IOException, SQLException {
        NamedPreparedStatement insertHolding = ResourceUtils.getPreparedStatementFromResource(session,con, "insert_holding.sql");
        setHoldingRecord(con, tran, holding, insertHolding);
    }

    private void updateHoldingRecord(Connection con, LATransaction tran, LADM.Holding holding) throws IOException, SQLException {
        NamedPreparedStatement insertHolding = ResourceUtils.getPreparedStatementFromResource(session,con, "update_holding.sql");
        setHoldingRecord(con, tran, holding, insertHolding);
    }

    private void setHoldingRecord(Connection con, LATransaction tran, LADM.Holding s, NamedPreparedStatement setStatement) throws SQLException {
        String kebele =s.nrlais_kebeleid==null? tran.nrlais_kebeleid:s.nrlais_kebeleid;
        //prepare standard fields
        if (s.holdingUID == null) {
            s.holdingUID = UUID.randomUUID().toString();
        }
        KebeleInfo.fixNRLAISEntityFileds(session, kebele, s, true);
        s.currenttxuid = tran.transactionUID;
        if (s.holdingSeqNo < 1) {
            s.setUHID(kebele, getSeqNo(con, kebele, "t_uhid_seq"));
        }

        setStatement.setString("@uid", s.holdingUID);
        setStatement.setInt("@seqnr", s.holdingSeqNo);
        setStatement.setString("@uhid", s.uhid);
        setStatement.setInt("@holdingtype", s.holdingType);
        setStatement.setString("@notes", s.notes);
        setNRLAISTransactionEntityFields(setStatement, s);
        setStatement.preparedStatement().execute();
    }

    public void deleteHolding(Connection con, LATransaction tran, String holdingUID) throws SQLException, IOException {
        LADM l = new GetLADM(session, "nrlais_transaction").get(con, holdingUID, LADM.CONTENT_FULL);
        for (LADM.Parcel p : l.holding.parcels) {
            deleteParcel(con, tran, p, true);
        }
        deletedObject(con, "t_holdings", holdingUID);
    }

    private void deleteParcel(Connection con, LATransaction tran, LADM.Parcel p, boolean deleteParties) throws SQLException {
        deleteParcel(con, tran, p, true, deleteParties);
    }

    private void deleteParcel(Connection con, LATransaction tran, LADM.Parcel p, boolean deleteRR, boolean deleteParties) throws SQLException {
        if (deleteRR) {
            for (LADM.Right r : p.rights) {
                deleteRight(con, tran, r, deleteParties);
            }
            for (LADM.Restriction r : p.restrictions) {
                deleteRestriction(con, tran, r, deleteParties);
            }
        }
        deletedObjectByFilter(con, "t_sys_fc_holding", "fdc_uid=(Select uid from nrlais_transaction.fdconnector where wfsid='" + p.parcelUID + "')");
        deletedObjectByFilter(con, "fdconnector", "wfsid='" + p.parcelUID + "'");
        deletedObject(con, "t_parcels", p.parcelUID);

    }

    void deleteParcelHoldingLink(Connection con, String parcelUID) throws SQLException {
        deletedObjectByFilter(con, "t_sys_fc_holding", "fdc_uid=(Select uid from nrlais_transaction.fdconnector where wfsid='" + parcelUID + "')");
        deletedObjectByFilter(con, "fdconnector", "wfsid='" + parcelUID + "'");

    }

    void linkParcelToHolding(Connection con, LATransaction tran, String holdingUID, String parcelUID) throws IOException, SQLException {
        NamedPreparedStatement insertParcel = ResourceUtils.getPreparedStatementFromResource(session,con, "insert_parcel_holding_link.sql");

        insertParcel.setString("@uid", parcelUID);
        NRLAISTransactionEntity p = new NRLAISTransactionEntity();
        KebeleInfo.fixNRLAISEntityFileds(session, tran.nrlais_kebeleid, p, true);
        p.currenttxuid = tran.transactionUID;
        insertParcel.setString("@notes", "");

        insertParcel.setString("@fdc_uid", UUID.randomUUID().toString());
        insertParcel.setString("@sys_fs_h_uid", UUID.randomUUID().toString());
        insertParcel.setString("@holdinguid", holdingUID);

        setNRLAISTransactionEntityFields(insertParcel, p);
        insertParcel.preparedStatement().execute();
    }

    void changeParcelHoldingLink(Connection con, LATransaction tran, String parcelUID, String fromHoldingUID, String toHoldingUID) throws IOException, SQLException {
        try (ResultSet rs = con.prepareStatement("Select uid from nrlais_transaction.fdconnector where wfsid='" + parcelUID + "'").executeQuery()) {
            if (rs.next()) {
                String fdcuid = rs.getString(1);
                con.prepareStatement("Update nrlais_transaction.t_sys_fc_holding set editstatus=(Select case when editstatus='n' then 'n' else 'u' end from nrlais_transaction.t_sys_fc_holding where fdc_uid='"+fdcuid+"'), holdinguid='" + toHoldingUID + "' where fdc_uid='" + fdcuid + "'").execute();
            } else {
                throw new IllegalStateException("Parcel " + parcelUID + " don't have fdconnector entry");
            }
        };
    }

    String getEditStatus(Connection con, String table, String uid) throws SQLException {
        try (ResultSet rs = con.prepareStatement("Select editStatus from nrlais_transaction." + table + " where uid='" + uid + "'").executeQuery()) {
            if (rs.next()) {
                return rs.getString(1);
            }
        }
        return null;
    }

    String getEditStatusByFilter(Connection con, String table, String filter) throws SQLException {
        try (ResultSet rs = con.prepareStatement("Select editStatus from nrlais_transaction." + table + " where "+filter).executeQuery()) {
            if (rs.next()) {
                return rs.getString(1);
            }
        }
        return "";
    }
    void deletedObject(Connection con, String table, String uid) throws SQLException {
        if (getEditStatus(con, table, uid).equals("n")) {
            con.prepareStatement("delete from nrlais_transaction." + table + " where uid='" + uid + "'").execute();
        } else {
            con.prepareStatement("Update nrlais_transaction."+table+" set editstatus='d' where uid='" + uid + "'").execute();
        }
    }
    void deletedObjectByFilter(Connection con, String table, String filter) throws SQLException {
        if (getEditStatusByFilter(con, table, filter).equals("n")) {
            con.prepareStatement("delete from nrlais_transaction." + table + " where "+filter).execute();
        } else {
            con.prepareStatement("Update nrlais_transaction."+table+" set editstatus='d' where "+filter).execute();
        }
    }


    private void deleteRight(Connection con, LATransaction tran, LADM.Right r, boolean deleteParties) throws SQLException {
        deletedObject(con, "t_rights", r.rightUID);
        if (deleteParties) {
            deleteParty(con, tran, r.party);
        }
    }

    private void deleteRestriction(Connection con, LATransaction tran, LADM.Restriction r, boolean deleteParties) throws SQLException {
        deletedObject(con, "t_restrictions", r.restrictionUID);
        if (deleteParties) {
            deleteParty(con, tran, r.party);
        }
    }

    private void deleteParty(Connection con, LATransaction tran, LADM.Party party) throws SQLException {
        deletePartyChilds(party, con, tran);
        deletedObject(con, "t_party", party.partyUID);
    }

    private void deletePartyChilds(LADM.Party party, Connection con, LATransaction tran) throws SQLException {
        
        for (LADM.GroupPartyMembership m : party.members) {
            deletedObject(con, "t_groupparty", m.membershipUID);
            deleteParty(con, tran, m.member);
        }
        deletedObjectByFilter(con,"t_partyrelationship","frompartyuid='" + party.partyUID + "'");
        if (party.guardian != null) {
            deleteParty(con, tran, party.guardian);
        }
        deletedObjectByFilter(con,"t_partyrole","partyuid='" + party.partyUID + "'");
        deletedObjectByFilter(con,"t_partycontactdata","partyuid='" + party.partyUID + "'");

    }

    public String getPartyUID() {
        return UUID.randomUUID().toString();
    }

    public String createParty(Connection con, LATransaction tran, LADM.Party party) throws IOException, SQLException {
        return insertParty(con, tran, party);
    }

    public void updateHolding(Connection con, LATransaction tran, LADM updatedHolding) throws SQLException, IOException, Exception {
        LADM existing = new GetLADM(session, "nrlais_transaction").get(con, updatedHolding.holding.holdingUID, LADM.CONTENT_FULL);
        if (existing == null) {
            throw new IllegalArgumentException("No holding with hodling uid:" + updatedHolding.holding.holdingUID + " in the database");
        }
        //Party diff
        HashMap<String, LADM.Party> oldParties = new HashMap<>();
        HashMap<String, LADM.Party> newParties = new HashMap<>();

        existing.holding.forEachParty((p) -> {
            if (!oldParties.containsKey(p.partyUID)) {
                oldParties.put(p.partyUID, p);

            }
            return true;
        });

        updatedHolding.holding.forEachParty((p) -> {
            if (p.partyUID == null) {
                p.partyUID = UUID.randomUUID().toString();
            }
            if (!newParties.containsKey(p.partyUID)) {
                newParties.put(p.partyUID, p);
            }
            return true;
        });

        Diff<LADM.Party, String> partyDiff = DiffUtility.calculateDiff(oldParties.values(), newParties.values(),
                new DiffUtility.DiffComparer<LADM.Party, String>() {
            @Override
            public boolean recordEqual(LADM.Party x, LADM.Party y) {
                return x.recordEquals(y);
            }

            @Override
            public String getKey(LADM.Party x) {
                return x.partyUID;
            }
        }
        );

        HashSet<String> insertedParties = new HashSet<>();

        //update holding reacord if needed
        if (!existing.holding.recordEquals(updatedHolding.holding)) {
            updateHolding(con, tran, updatedHolding);
        }
        updatedHolding.holding.parcels.forEach((p) -> {
            if (p.parcelUID == null) {
                p.parcelUID = UUID.randomUUID().toString();
            }
        });
        Diff<LADM.Parcel, String> parcelDiff = DiffUtility.calculateDiff(existing.holding.parcels, updatedHolding.holding.parcels,
                new DiffUtility.DiffComparer<LADM.Parcel, String>() {
            @Override
            public boolean recordEqual(LADM.Parcel x, LADM.Parcel y) {
                return x.recordEquals(y);
            }

            @Override
            public String getKey(LADM.Parcel x) {

                return x.parcelUID;
            }
        });

        //delete parcels
        for (LADM.Parcel parcel : parcelDiff.deletedItems.values()) {
            for (LADM.Right r : parcel.rights) {
                deleteRight(con, tran, r, false);
            }
            for (LADM.Restriction r : parcel.restrictions) {
                deleteRestriction(con, tran, r, false);
            }
            deleteParcel(con, tran, parcel, false);
        }
        //insert new parc[els
        insertParcels(con, tran, updatedHolding.holding.holdingUID, parcelDiff.newItems.values(), null);
        for (LADM.Parcel parcel : parcelDiff.newItems.values()) {
            for (LADM.Right r : parcel.rights) {
                insertNewRightsForUpdate(partyDiff, r, insertedParties, con, tran, updatedHolding);
            }
            for (LADM.Restriction r : parcel.restrictions) {
                insertNewRestrictionForUpdate(partyDiff, r, insertedParties, con, tran, updatedHolding);
            }
        }
        //updated parcels
        for (LADM.Parcel updatedParcel : updatedHolding.holding.parcels) {
            if (parcelDiff.updatedItems.containsKey(updatedParcel.parcelUID)) {
                updateParcelRecord(con, tran, existing.holding.holdingUID, updatedParcel);
            }

            LADM.Parcel existingParcel = existing.holding.getParcel(updatedParcel.parcelUID);
            if (existingParcel == null) {
                continue;
            }
            Diff<LADM.Right, String> rightDiff = DiffUtility.calculateDiff(existingParcel.rights, updatedParcel.rights,
                    new DiffUtility.DiffComparer<LADM.Right, String>() {
                @Override
                public boolean recordEqual(LADM.Right x, LADM.Right y) {
                    return x.recordEquals(y);
                }

                @Override
                public String getKey(LADM.Right x) {
                    return x.rightUID;
                }
            });

            Diff<LADM.Restriction, String> restrictionDiff = DiffUtility.calculateDiff(existingParcel.restrictions, updatedParcel.restrictions,
                    new DiffUtility.DiffComparer<LADM.Restriction, String>() {
                @Override
                public boolean recordEqual(LADM.Restriction x, LADM.Restriction y) {
                    return x.recordEquals(y);
                }

                @Override
                public String getKey(LADM.Restriction x) {
                    return x.restrictionUID;
                }
            });

            for (LADM.Right r : rightDiff.newItems.values()) {
                insertNewRightsForUpdate(partyDiff, r, insertedParties, con, tran, updatedHolding);
            }

            for (LADM.Restriction r : restrictionDiff.newItems.values()) {
                insertNewRestrictionForUpdate(partyDiff, r, insertedParties, con, tran, updatedHolding);
            }
            for (LADM.Right r : rightDiff.deletedItems.values()) {
                deleteRight(con, tran, r, false);
            }

            for (LADM.Restriction r : restrictionDiff.deletedItems.values()) {
                deleteRestriction(con, tran, r, false);
            }

            for (LADM.Right r : rightDiff.updatedItems.values()) {
                updateRight(con, tran, r);
            }

            for (LADM.Restriction r : restrictionDiff.updatedItems.values()) {
                updateRestriction(con, tran, r);
            }
        }
        for (LADM.Party party : partyDiff.deletedItems.values()) {
            deleteParty(con, tran, party);
        }

        for (LADM.Party party : partyDiff.updatedItems.values()) {
            updatePartyRecord(con, tran, party);
        }

    }

    private void insertNewRestrictionForUpdate(Diff<LADM.Party, String> partyDiff, LADM.Restriction r, HashSet<String> insertedParties, Connection con, LATransaction tran, LADM updatedHolding) throws IOException, SQLException {
        if (partyDiff.newItems.containsKey(r.party.partyUID)) {
            if (!insertedParties.contains(r.party.partyUID)) {
                insertParty(con, tran, r.party);
                insertedParties.add(r.party.partyUID);
            }
        }
        r.holdingUID = updatedHolding.holding.holdingUID;
        r.partyUID = r.party.partyUID;
        insertRestriction(con, tran, r.holdingUID, r.parcelUID, r, false);
    }

    private void insertNewRightsForUpdate(Diff<LADM.Party, String> partyDiff, LADM.Right r, HashSet<String> insertedParties, Connection con, LATransaction tran, LADM updatedHolding) throws SQLException, IOException {
        if (partyDiff.newItems.containsKey(r.party.partyUID)) {
            if (!insertedParties.contains(r.party.partyUID)) {
                insertParty(con, tran, r.party);
                insertedParties.add(r.party.partyUID);
            }
        }
        r.holdingUID = updatedHolding.holding.holdingUID;
        r.partyUID = r.party.partyUID;
        insertRight(con, tran, r.holdingUID, r.parcelUID, r, false);
    }

    public void purgeRecord(Connection con, LADM ladm) throws SQLException, Exception {
        for (LADM.Parcel parcel : ladm.holding.parcels) {
            purgeParcel(con, parcel);
        }
        con.prepareStatement("delete from nrlais_transaction.t_holdings where uid='" + ladm.holding.holdingUID + "'").execute();
        ladm.holding.forEachParty((p) -> {
            purgeParty(con, p);
            return true;
        });

    }

    private void purgeParcel(Connection con, LADM.Parcel parcel) throws SQLException {
        for (LADM.Right r : parcel.rights) {
            purgeRight(con, r);
        }
        for (LADM.Restriction r : parcel.restrictions) {
            purgeRestriction(con, r);
        }
        con.prepareStatement("delete from nrlais_transaction.t_sys_fc_holding where fdc_uid=(Select uid from nrlais_transaction.fdconnector where wfsid='" + parcel.parcelUID + "')").execute();
        con.prepareStatement("delete from nrlais_transaction.fdconnector where wfsid='" + parcel.parcelUID + "'").execute();
        con.prepareStatement("delete from nrlais_transaction.t_parcels where uid='" + parcel.parcelUID + "'").execute();
    }

    private void purgeRight(Connection con, LADM.Right r) throws SQLException {
        con.prepareStatement("delete from nrlais_transaction.t_rights where uid='" + r.rightUID + "'").execute();
    }

    private void purgeRestriction(Connection con, LADM.Restriction r) throws SQLException {
        con.prepareStatement("delete from nrlais_transaction.t_restrictions where uid='" + r.restrictionUID + "'").execute();
    }

    private void purgeParty(Connection con, LADM.Party p) throws SQLException {
        for (LADM.GroupPartyMembership m : p.members) {
            purgeParty(con, m.member);
        }

        con.prepareStatement("delete from nrlais_transaction.t_partycontactdata where partyuid='" + p.partyUID + "'").execute();
        con.prepareStatement("delete from nrlais_transaction.t_partyrole where partyuid='" + p.partyUID + "'").execute();
        con.prepareStatement("delete from nrlais_transaction.t_groupparty where grouppartyuid='" + p.partyUID + "'").execute();
        con.prepareStatement("delete from nrlais_transaction.t_party where uid='" + p.partyUID + "'").execute();
    }

    void assertTransactionEntityLock(NRLAISTransactionEntity e, String txuid, String objectName) {
        if (e.currenttxuid != null && !e.currenttxuid.equals(txuid)) {
            throw new IllegalStateException(objectName + " is already locked");
        }
    }
    static HashMap<String, Integer> tableToObjectIDMap = new HashMap<>();

    static {
        tableToObjectIDMap.put("fdconnector", 3);
        tableToObjectIDMap.put("fdmodel", 4);
        tableToObjectIDMap.put("t_party", 5);
        tableToObjectIDMap.put("t_partyrelationship", 6);
        tableToObjectIDMap.put("t_partyrole", 7);
        tableToObjectIDMap.put("t_parcels", 8);
        tableToObjectIDMap.put("t_restrictions", 9);
        tableToObjectIDMap.put("t_rights", 10);
        tableToObjectIDMap.put("t_holdings", 11);
        tableToObjectIDMap.put("t_sys_fc_holding", 12);
        tableToObjectIDMap.put("t_points", 13);
        tableToObjectIDMap.put("t_parcels2points", 14);
        tableToObjectIDMap.put("t_topoborder", 15);
        tableToObjectIDMap.put("t_parcels2topoborders", 16);
        tableToObjectIDMap.put("t_reqparcelrel", 17);
        tableToObjectIDMap.put("t_partycontactdata", 18);
        tableToObjectIDMap.put("t_groupparty", 19);
        tableToObjectIDMap.put("t_parcelpredecessor", 20);

    }

    void lockObject(Connection con, String txuid, String tableName, String kebele, String uid) throws SQLException, IOException {
        String sql = "Update nrlais_inventory." + tableName + " set currenttxuid='" + txuid + "' where uid='" + uid + "'";
        con.prepareStatement(sql).execute();
        sql = ResourceUtils.getSql("insert_tran_content.sql");
        NamedPreparedStatement st = new NamedPreparedStatement(session,con, sql);
        NRLAISEntity entityFields = new NRLAISEntity();
        KebeleInfo.fixNRLAISEntityFileds(session, kebele, entityFields, true);
        RepoBase.setNRLAISEntityFields(st, entityFields, true);
        st.setString("@uid", UUID.randomUUID().toString());
        st.setString("@notes", "ATWSS");
        st.setString("@txuid", txuid);
        st.setInt("@inputobjecttype", tableToObjectIDMap.get(tableName));
        st.setString("@inputobjectuid", uid);
        st.setNull("@outputobjecttype", java.sql.Types.INTEGER);
        st.setNull("@outputobjectuid", java.sql.Types.VARCHAR);
        st.setString("@task", "ATWSS Task");
        st.setBoolean("@isprimary", true);
        st.setInt("@status", 1);
        st.preparedStatement().execute();
    }

    public void lockLandRecord(Connection con, LADM rec, String txuid) throws SQLException, IOException {
        assertTransactionEntityLock(rec.holding, txuid, "Holding");
        lockObject(con, txuid, "t_holdings", rec.holding.nrlais_kebeleid, rec.holding.holdingUID);
        for (LADM.Parcel p : rec.holding.parcels) {
            lockParcel(con, p, txuid);
        }
    }

    private void lockParcel(Connection con, LADM.Parcel p, String txuid) throws SQLException, IOException {
        lockObject(con, txuid, "t_parcels", p.nrlais_kebeleid, p.parcelUID);
        String fdcuid = null;
        String sfdcuid = null;
        try (ResultSet rs = con.prepareStatement("Select uid from nrlais_inventory.fdconnector where wfsid='" + p.parcelUID + "'").executeQuery()) {
            if (rs.next()) {
                fdcuid = rs.getString(1);
            }
        }
        if (fdcuid == null) {
            throw new IllegalStateException("Parcel " + p.parcelUID + " has no related entry in fdconnector");
        }
        lockObject(con, txuid, "fdconnector", p.nrlais_kebeleid, fdcuid);
        try (ResultSet rs = con.prepareStatement("Select uid from nrlais_inventory.t_sys_fc_holding where fdc_uid='" + fdcuid + "'").executeQuery()) {
            if (rs.next()) {
                sfdcuid = rs.getString(1);
            }
        }
        if (sfdcuid == null) {
            throw new IllegalStateException("fdConnector " + fdcuid + " has no related entry in t_sys_fc_holding");
        }
        lockObject(con, txuid, "t_sys_fc_holding", p.nrlais_kebeleid, sfdcuid);

        for (LADM.Right r : p.rights) {
            lockRights(con, r, txuid);
        }
        for (LADM.Restriction r : p.restrictions) {
            lockRestrictions(con, r, txuid);
        }
    }

    private void lockRights(Connection con, LADM.Right r, String txuid) throws SQLException, IOException {
        lockObject(con, txuid, "t_rights", r.nrlais_kebeleid, r.rightUID);
        lockParty(con, r.party, txuid);
    }

    private void lockRestrictions(Connection con, LADM.Restriction r, String txuid) throws SQLException, IOException {
        lockObject(con, txuid, "t_restrictions", r.nrlais_kebeleid, r.restrictionUID);
        lockParty(con, r.party, txuid);
    }

    private void lockParty(Connection con, LADM.Party party, String txuid) throws SQLException, IOException {
        lockObject(con, txuid, "t_party", party.nrlais_kebeleid, party.partyUID);
        for (LADM.GroupPartyMembership m : party.members) {
            lockObject(con, txuid, "t_groupparty", party.nrlais_kebeleid, m.membershipUID);
            lockParty(con, m.member, txuid);
        }
        if (party.guardian != null) {
            String relUID = null;
            try (ResultSet rs = con.prepareStatement("Select uid from nrlais_inventory.t_partyrelationship where frompartyuid='" + party.partyUID + "' and topartyuid='" + party.guardian.partyUID + "'").executeQuery()) {
                if (rs.next()) {
                    relUID = rs.getString(1);
                }
            }
            lockObject(con, txuid, "t_partyrelationship", party.nrlais_kebeleid, relUID);
            lockParty(con, party.guardian, txuid);
        }
        for (LADM.PartyRole role : party.roles) {
            lockObject(con, txuid, "t_partyrole", party.nrlais_kebeleid, role.uid);
        }
        for (LADM.PartyContact c : party.contacts) {
            lockObject(con, txuid, "t_partycontactdata", party.nrlais_kebeleid, c.uid);
        }
    }

    public void lrUpdateRent(Connection con, LATransaction tran, LADM.Right rent) throws SQLException, IOException {
        LADM.Right right = new GetLADM(session, "nrlais_transaction").getRight(con, rent.rightUID);
        right.rightType = rent.rightType;
        if (right.rightType != LADM.Right.RIGHT_RENT && right.rightType != LADM.Right.RIGHT_LEASE) {
            throw new IllegalArgumentException("Only rent or lease right expected");
        }
        right.startRentDate = rent.startRentDate;
        right.startLeaseDate = rent.startLeaseDate;
        right.endRentDate = rent.endRentDate;
        right.endLeaseDate = rent.endLeaseDate;
        right.leaseAmount = rent.leaseAmount;
        right.rentSize = rent.rentSize;
        right.parcelUID=rent.parcelUID;
        right.partyUID=rent.partyUID;
        updateRight(con, tran, right);
        updateOutputHoldingCache(con, right.holdingUID);
    }

    public void lrUpdateRestriction(Connection con, LATransaction tran, LADM.Restriction restriction) throws  SQLException, IOException, SQLException 
    {
        LADM.Restriction existing = new GetLADM(session, "nrlais_transaction").getRestriction(con, restriction.restrictionUID);
        existing.restrictionType = restriction.restrictionType;
        existing.startDate = restriction.startDate;
        existing.endDate = restriction.endDate;
        existing.parcelUID=restriction.parcelUID;
        existing.partyUID=restriction.partyUID;
        updateRestriction(con, tran, existing);
        updateOutputHoldingCache(con, existing.holdingUID);
    }

    public void clearHoldingFromTranSchema(Connection con, String txuid) throws IOException, SQLException {
        NamedPreparedStatement p=new NamedPreparedStatement(session, con, ResourceUtils.getSql("clear_holding.sql"));
        p.setString("@txuid", txuid);
        p.preparedStatement().execute();
    }

    static class InputHolding {

        public LADM.Holding holding;

        private InputHolding(LADM.Holding h) {
            this.holding = h;
        }

    }

    static class OutputHolding {

        HashMap<String, String> transferedParties = new HashMap<>();
        public LADM.Holding holding;

        private OutputHolding(LADM.Holding h) {
            this.holding = h;
        }
    }
    HashMap<String, InputHolding> inputHoldings = null;
    HashMap<String, OutputHolding> outputHoldings = null;

    public void lrStartTransaction(Connection con, LATransaction tran, String inputHolding) throws SQLException, IOException {
        ArrayList<String> arr = new ArrayList<String>();
        arr.add(inputHolding);
        lrStartTransaction(con, tran, arr);
    }

    public void lrStartTransaction(Connection con, LATransaction tran, Collection<String> inputHoldings) throws SQLException, IOException {
        this.inputHoldings = new HashMap<String, InputHolding>();
        this.outputHoldings = new HashMap<String, OutputHolding>();
        for (String h : inputHoldings) {
            this.inputHoldings.put(h, new InputHolding(new GetLADM(session, "nrlais_transaction").get(con, h, LADM.CONTENT_FULL).holding));
        }
    }

    public void finishTransaction(Connection con, LATransaction tran) throws SQLException, IOException, Exception 
    {
        this.finishTransaction(con, tran,true);
    }
    public void finishTransaction(Connection con, LATransaction tran,boolean validate) throws SQLException, IOException, Exception {
        if (outputHoldings == null) {
            throw new IllegalStateException("LR Transaction not started");
        }
        for (OutputHolding o : outputHoldings.values()) {
            if (o.holding.parcels.isEmpty()) {
                deleteHolding(con, tran, o.holding.holdingUID);
            } else {
                if(validate)
                {
                String err=new LADM(o.holding).getRuleErrorsAsString(tran.time);
                if(!org.apache.commons.lang3.StringUtils.isEmpty(err))
                    throw new IllegalStateException(err);
                }
                updateHoldingRecord(con, tran, o.holding);
            }
        }
        inputHoldings = null;
        outputHoldings = null;
    }

    public String lrCreateNewHolding(Connection con, LATransaction tran, LADM.Holding holding) throws IOException, SQLException {
        holding.holdingUID = UUID.randomUUID().toString();
        insertHolding(con, tran, holding);
        outputHoldings.put(holding.holdingUID, new OutputHolding(holding));
        return holding.holdingUID;
    }

    void updateOutputHoldingCache(Connection con, String holdingUID) throws SQLException, IOException {
        LADM ladm = new GetLADM(session, "nrlais_transaction").get(con, holdingUID, LADM.CONTENT_FULL);
        if (ladm == null) {
            throw new IllegalArgumentException("Holding " + holdingUID + " not in database");
        }
        LADM.Holding holding = ladm.holding;
        if (outputHoldings.containsKey(holdingUID)) {
            outputHoldings.get(holdingUID).holding = holding;
        } else {
            outputHoldings.put(holdingUID, new OutputHolding(holding));
        }
    }

    OutputHolding outputHolding(Connection con, String holdingUID) throws SQLException, IOException {
        if (outputHoldings.containsKey(holdingUID)) {
            return outputHoldings.get(holdingUID);
        }
        LADM ladm = new GetLADM(session, "nrlais_transaction").get(con, holdingUID, LADM.CONTENT_FULL);
        if (ladm == null) {
            throw new IllegalArgumentException("Holding " + holdingUID + " not in database");
        }
        outputHoldings.put(holdingUID, new OutputHolding(ladm.holding));
        return outputHoldings.get(holdingUID);
    }

    InputHolding inputHolding(Connection con, String holdingUID) throws SQLException, IOException {
        if (inputHoldings.containsKey(holdingUID)) {
            return inputHoldings.get(holdingUID);
        }
        LADM ladm = new GetLADM(session, "nrlais_transaction").get(con, holdingUID, LADM.CONTENT_FULL);
        if (ladm == null) {
            throw new IllegalArgumentException("Holding " + holdingUID + " not in database");
        }
        inputHoldings.put(holdingUID, new InputHolding(ladm.holding));
        return inputHoldings.get(holdingUID);
    }

    void updateInputHoldingCache(Connection con, String holdingUID) throws SQLException, IOException {
        LADM.Holding holding = new GetLADM(session, "nrlais_transaction").get(con, holdingUID, LADM.CONTENT_FULL).holding;
        if (inputHoldings.containsKey(holdingUID)) {
            inputHoldings.put(holdingUID, new InputHolding(holding));
        } else {
            inputHoldings.get(holdingUID).holding = holding;
        }
    }

    public String lrAddParcel(Connection con, LATransaction tran, String holdingUID, LADM.Parcel parcel) throws SQLException, IOException, Exception {
        //load to output to check assert that holding exists
        updateOutputHoldingCache(con, holdingUID);

        parcel.parcelUID = UUID.randomUUID().toString();
        insertParcel(con, tran, holdingUID, parcel);
        for (LADM.Right r : parcel.rights) {
            lrAddRight(con, tran, holdingUID, parcel.parcelUID, r);
        }
        for (LADM.Restriction r : parcel.restrictions) {
            lrAddRestriction(con, tran, holdingUID, parcel.parcelUID, r);
        }
        updateOutputHoldingCache(con, holdingUID);
        return parcel.parcelUID;
    }

    public void lrRemoveAndDeleteParcel(Connection con, LATransaction tran, String holdingUID, String parcelUID) throws SQLException, Exception {
        LADM ladm;
        ladm = assertAndGetHolding(con, holdingUID);
        LADM.Parcel parcel = assertGetParcel(ladm.holding, parcelUID, holdingUID);

        for (LADM.Right r : parcel.rights) {
            lrRemoveRight(con, tran, holdingUID, parcelUID, r);
        }
        for (LADM.Restriction r : parcel.restrictions) {
            lrRemoveRestriction(con, tran, holdingUID, parcelUID, r);
        }
        deleteParcel(con, tran, parcel, false, false);
        updateOutputHoldingCache(con, holdingUID);
    }

    void removeLastParty(Connection con, LATransaction tran, LADM.Holding holding, String partyUID) throws Exception {
        final LADM.Party[] lastParty = {null};

        holding.forEachParty(new LADM.Holding.ProcessItem<LADM.Party>() {
            @Override
            public boolean process(LADM.Party p) throws Exception {
                if (StringUtils.equals(partyUID, p.partyUID)) {
                    if (lastParty[0] == null) {
                        lastParty[0] = p;
                    } else {
                        lastParty[0] = null;
                        return false;
                    }
                }
                return true;
            }
        });
        if (lastParty[0] != null) {
            deleteParty(con, tran, lastParty[0]);
        }
    }

    public String lrAddOrUpdateParty(Connection con, LATransaction tran, String holdingUID, LADM.Party party) throws SQLException, Exception {
        final LADM.Party[] existing = {null};
        final OutputHolding outputHolding = outputHolding(con, holdingUID);
        if (party.partyUID != null) {
            final String tansferdTo = outputHolding.transferedParties.containsKey(party.partyUID) ? outputHolding.transferedParties.get(party.partyUID) : null;
            outputHolding.holding.forEachParty(new LADM.Holding.ProcessItem<LADM.Party>() {
                @Override
                public boolean process(LADM.Party p) throws Exception {
                    if (tansferdTo == null && StringUtils.equals(party.partyUID, p.partyUID)) {
                        existing[0] = p;
                        return false;
                    } else if (tansferdTo != null && StringUtils.equals(tansferdTo, p.partyUID)) {
                        existing[0] = p;
                        party.partyUID = tansferdTo;
                        return false;
                    }
                    return true;
                }
            });

        }
        if (existing[0] == null) {
            String oldPartyUID = party.partyUID;
            party.partyUID = UUID.randomUUID().toString();
            if (oldPartyUID != null) {
                outputHolding.transferedParties.put(oldPartyUID, party.partyUID);
            }
            insertParty(con, tran, party);
        } else {
            if (!existing[0].recordEquals(party)) {
                updatePartyRecord(con, tran, party);
            }
        }
        updateOutputHoldingCache(con, holdingUID);
        return party.partyUID;
    }

    LADM assertAndGetHolding(Connection con, String holdingUID) throws SQLException, IOException {
        LADM ladm = new GetLADM(session, "nrlais_transaction").get(con, holdingUID, LADM.CONTENT_FULL);
        if (ladm == null) {
            throw new IllegalArgumentException("Invalid holding uid:" + holdingUID);
        }
        return ladm;
    }

    public String lrAddRight(Connection con, LATransaction tran, String holdingUID, String parcelUID, LADM.Right right) throws Exception {
        right.rightUID = UUID.randomUUID().toString();
        right.partyUID = lrAddOrUpdateParty(con, tran, holdingUID, right.party);
        right.holdingUID = holdingUID;
        right.parcelUID = parcelUID;
        insertRight(con, tran, holdingUID, parcelUID, right, false);
        updateOutputHoldingCache(con, holdingUID);
        return right.rightUID;
    }

    public void lrRemoveRight(Connection con, LATransaction tran, String holdingUID, String parcelUID, LADM.Right right) throws Exception {
        LADM.Holding holding = outputHolding(con, holdingUID).holding;
        assertGetParcel(holding, parcelUID, holdingUID);
        deleteRight(con, tran, right, false);
        removeLastParty(con, tran, holding, right.partyUID);
        updateOutputHoldingCache(con, holdingUID);
    }

    public String lrAddRestriction(Connection con, LATransaction tran, String holdingUID, String parcelUID, LADM.Restriction restriction) throws Exception {
        restriction.restrictionUID = UUID.randomUUID().toString();
        restriction.partyUID = lrAddOrUpdateParty(con, tran, holdingUID, restriction.party);
        restriction.holdingUID = holdingUID;
        restriction.parcelUID = parcelUID;
        insertRestriction(con, tran, holdingUID, parcelUID, restriction, false);
        updateOutputHoldingCache(con, holdingUID);
        return restriction.restrictionUID;
    }

    public void lrRemoveRestriction(Connection con, LATransaction tran, String holdingUID, String parcelUID, LADM.Restriction restriction) throws Exception {
        LADM.Holding holding = outputHolding(con, holdingUID).holding;
        deleteRestriction(con, tran, restriction, false);
        removeLastParty(con, tran, holding, restriction.partyUID);
        updateOutputHoldingCache(con, holdingUID);
    }

    public void lrDeleteParty(Connection con, LATransaction tran, String holdingUID, String partyUID) throws Exception {
        LADM.Holding holding = outputHolding(con, holdingUID).holding;
        if (holding == null) {
            throw new IllegalArgumentException("Invalid holding uid: " + holdingUID);
        }
        holding.forEachParty((party) -> {
            if (party.partyUID.equals(partyUID)) {
                throw new IllegalStateException("This party can't be deleted as it is used in the land record");
            }
            return true;
        });
        deleteParty(con, tran, new GetLADM(session, "nrlais_transaction").getParty(con, partyUID, LADM.CONTENT_FULL));
    }

    public void lrTransferParcel(Connection con, LATransaction tran, String parcelUID, String fromHoldingUID, String toHoldingUID, int actype, int acyear, Collection<LADM.Right> rights) throws SQLException, Exception {
        LADM.Holding holdingFrom = outputHolding(con, fromHoldingUID).holding;
        LADM.Parcel parcel = assertGetParcel(holdingFrom, parcelUID, fromHoldingUID);

        for (LADM.Right r : parcel.rights) {
            lrRemoveRight(con, tran, fromHoldingUID, parcelUID, r);
        }
        for (LADM.Restriction r : parcel.restrictions) {
            lrRemoveRestriction(con, tran, fromHoldingUID, parcelUID, r);
        }
        changeParcelHoldingLink(con, tran, parcelUID, fromHoldingUID, toHoldingUID);
        parcel.rights.clear();
        parcel.rights.addAll(rights);

        parcel.mreg_actype = actype;
        parcel.mreg_actype = acyear;

        for (LADM.Right r : parcel.rights) {
            lrAddRight(con, tran, toHoldingUID, parcelUID, r);
        }
        for (LADM.Restriction r : parcel.restrictions) {
            lrAddRestriction(con, tran, toHoldingUID, parcelUID, r);
        }

        updateOutputHoldingCache(con, toHoldingUID);
        updateOutputHoldingCache(con, fromHoldingUID);
    }

    public void lrSplitParcel(Connection con, LATransaction tran, String parcelUID, String fromHoldingUID,
            List<Map.Entry<String, LADM.Parcel>> toHoldings) throws SQLException, Exception {
        LADM.Holding holding = outputHolding(con, fromHoldingUID).holding;
        assertGetParcel(holding, parcelUID, fromHoldingUID);

        CMSSParcelSplitTask task = new CMSSParcelSplitTask();
        task.transactionUID = tran.transactionUID;
        task.parcelUID = parcelUID;
        task.newParcelUIDs = new ArrayList<>();

        for (Map.Entry<String, LADM.Parcel> th : toHoldings) {
            th.getValue().parcelUID = lrAddParcel(con, tran, th.getKey(), th.getValue());
            task.newParcelUIDs.add(th.getValue().parcelUID);
        }
        new CMSSTaskManager(session).addSplitParcelTask(con, task);
        lrRemoveAndDeleteParcel(con, tran, fromHoldingUID, parcelUID);

        updateOutputHoldingCache(con, fromHoldingUID);
        for (Map.Entry<String, LADM.Parcel> th : toHoldings) {
            updateOutputHoldingCache(con, th.getKey());
        }
    }

    public void lrUpateParcel(Connection con, LATransaction tran, String holdingUID, LADM.Parcel parcel) throws SQLException, IOException {
        LADM.Holding holding = outputHolding(con, holdingUID).holding;
        LADM.Parcel existing = assertGetParcel(holding, parcel.parcelUID, holdingUID);
        parcel.seqNo = existing.seqNo;
        parcel.upid = existing.upid;
        parcel.geometry = existing.geometry;
        parcel.referencePoint = existing.referencePoint;
        parcel.areaGeom = existing.areaGeom;
        this.updateParcelRecord(con, tran, holdingUID, parcel);
        updateOutputHoldingCache(con, holdingUID);
    }

    public void lrUpateRights(Connection con, LATransaction theTransaction, String holdingUID, String parcelUID, Iterable<LADM.Right> rights) throws SQLException, Exception {
        RationalNum totalHoldingShare = new RationalNum(0, 1);
        for (LADM.Right r : rights) {
            if (r.rightType == LADM.Right.RIGHT_SUR || r.rightType == LADM.Right.RIGHT_PUR) {
                totalHoldingShare.add(r.share);
            }
        }
        //if(!totalHoldingShare.isOne())
        //  throw new IllegalArgumentException("The total share of all holders should be exactly 100%");
        LADM.Holding holding = outputHolding(con, holdingUID).holding;
        LADM.Parcel parcel = assertGetParcel(holding, parcelUID, holdingUID);

        for (LADM.Right r : rights) {
            lrAddRight(con, theTransaction, holdingUID, parcelUID, r);
        }

        for (LADM.Right r : parcel.rights) {
            lrRemoveRight(con, theTransaction, holdingUID, parcelUID, r);
        }

        updateOutputHoldingCache(con, holdingUID);
    }

    private LADM.Parcel assertGetParcel(LADM.Holding holding, String parcelUID, String holdingUID) throws IllegalArgumentException {
        LADM.Parcel parcel = holding.getParcel(parcelUID);
        if (parcel == null) {
            throw new IllegalArgumentException("Parcel " + parcelUID + " is not in holding " + holdingUID);
        }
        return parcel;
    }

    private String histInsertHolding(Connection con, LATransaction tran, LADM.Holding holding) throws IOException, SQLException {
        NamedPreparedStatement st = ResourceUtils.getPreparedStatementFromResource(session,con, "insert_hist_holding.sql");
        String kebele = tran.nrlais_kebeleid;
        LADM.HistHolding hist = new LADM.HistHolding();
        hist.data = holding;
        KebeleInfo.setAdminFields(hist, kebele);
        RepoBase.setNRLAISHistoryEntityFields(st, tran.transactionUID, hist, holding.holdingUID);
        st.setString("@uhid", holding.uhid);
        st.setInt("@holdingtype", holding.holdingType);
        st.preparedStatement().execute();
        return hist.id;
    }

    private void histInsertParcel(Connection con, LATransaction tran, LADM.Parcel parcel, String histHoldingUID) throws IOException, SQLException {
        NamedPreparedStatement st = ResourceUtils.getPreparedStatementFromResource(session,con, "insert_hist_parcel.sql");
        String kebele = tran.nrlais_kebeleid;
        LADM.HistParcel hist = new LADM.HistParcel();
        hist.data = parcel;
        KebeleInfo.setAdminFields(hist, kebele);
        RepoBase.setNRLAISHistoryEntityFields(st, tran.transactionUID, hist, parcel.parcelUID);
        st.setString("@upid", parcel.upid);
        st.setString("@holdingid", histHoldingUID);
        st.setString("@geometry", parcel.geometry);
        st.preparedStatement().execute();
    }

    private String histInsertParty(Connection con, LATransaction tran, LADM.Party party) throws IOException, SQLException {
        NamedPreparedStatement st = ResourceUtils.getPreparedStatementFromResource(session,con, "insert_hist_party.sql");
        String kebele = tran.nrlais_kebeleid;
        LADM.HistParty hist = new LADM.HistParty();
        hist.data = party;
        KebeleInfo.setAdminFields(hist, kebele);
        RepoBase.setNRLAISHistoryEntityFields(st, tran.transactionUID, hist, party.partyUID);
        st.setInt("@partytype", party.partyType);
        st.setInt("@isorphan", party.isorphan);
        st.setInt("@disability", party.disability);
        st.setString("@name_concat", party.getFullName());
        st.preparedStatement().execute();
        return hist.id;
    }

    private void histInsertRight(Connection con, LATransaction tran, LADM.Right right, String histHoldingUID, String histPartyUID) throws IOException, SQLException {
        NamedPreparedStatement st = ResourceUtils.getPreparedStatementFromResource(session,con, "insert_hist_right.sql");
        String kebele = tran.nrlais_kebeleid;
        LADM.HistRight hist = new LADM.HistRight();
        hist.data = right;
        KebeleInfo.setAdminFields(hist, kebele);
        RepoBase.setNRLAISHistoryEntityFields(st, tran.transactionUID, hist, right.partyUID);
        st.setInt("@type", right.rightType);
        st.setString("@holdingid", histHoldingUID);
        st.setString("@partyid", histPartyUID);
        st.preparedStatement().execute();
    }

    private void histInsertRestriction(Connection con, LATransaction tran, LADM.Restriction restriction, String histHoldingUID, String histPartyUID) throws IOException, SQLException {
        NamedPreparedStatement st = ResourceUtils.getPreparedStatementFromResource(session,con, "insert_hist_right.sql");
        String kebele = tran.nrlais_kebeleid;
        LADM.HistRestriction hist = new LADM.HistRestriction();
        hist.data = restriction;
        KebeleInfo.setAdminFields(hist, kebele);
        RepoBase.setNRLAISHistoryEntityFields(st, tran.transactionUID, hist, restriction.partyUID);
        st.setInt("@type", restriction.restrictionType);
        if (restriction.hasDate) {
            st.setDate("@startdate", new java.sql.Date(restriction.startDate));
            st.setDate("@enddate", new java.sql.Date(restriction.endDate));
        } else {
            st.setNull("@startdate", java.sql.Types.DATE);
            st.setNull("@enddate", java.sql.Types.DATE);
        }
        st.setString("@holdingid", histHoldingUID);
        st.setString("@partyid", histPartyUID);

        st.preparedStatement().execute();
    }

    void logToHistory(Connection con, LATransaction tran, String holdingUID) throws SQLException, IOException {
        LADM l = new GetLADM(session, "nrlais_inventory").get(con, holdingUID, LADM.CONTENT_FULL);
        List<LADM.Parcel> parcels = l.holding.parcels;
        l.holding.parcels = null;

        String histHoldingUID = histInsertHolding(con, tran, l.holding);
        HashMap<String, String> insertedParty = new HashMap<>();
        for (LADM.Parcel p : parcels) {
            List<LADM.Right> rights = p.rights;
            List<LADM.Restriction> restrictios = p.restrictions;
            p.rights = null;
            p.restrictions = null;
            histInsertParcel(con, tran, p, histHoldingUID);
            for (LADM.Right r : rights) {
                LADM.Party party = r.party;
                r.party = null;
                if (!insertedParty.containsKey(party.partyUID)) {
                    String huid = histInsertParty(con, tran, party);
                    insertedParty.put(party.partyUID, huid);
                }
                histInsertRight(con, tran, r, histHoldingUID, insertedParty.get(party.partyUID));
            }
            for (LADM.Restriction r : restrictios) {
                LADM.Party party = r.party;
                r.party = null;
                if (!insertedParty.containsKey(party.partyUID)) {
                    String huid = histInsertParty(con, tran, party);
                    insertedParty.put(party.partyUID, huid);
                }
                histInsertRestriction(con, tran, r, histHoldingUID, insertedParty.get(party.partyUID));
            }
        }
    }
}
