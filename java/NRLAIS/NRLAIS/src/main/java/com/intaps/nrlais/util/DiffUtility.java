/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 *
 * @author Tewelde
 */
public class DiffUtility  {
    public static class Diff<T,K>
    {
        public HashMap<K,T> deletedItems=new HashMap<>();
        public HashMap<K,T> updatedItems=new HashMap<>();
        public HashMap<K,T> newItems=new HashMap<>();
    }
    public static interface DiffComparer<T,K>
    {
        public boolean recordEqual(T x,T y);
        public K getKey(T x);
    }
    public static <T,K> Diff<T,K> calculateDiff(Collection<T> orgingalList,Collection<T> newList,DiffComparer<T,K> comparer)
    {
        Diff<T,K> ret=new Diff<T,K>();
        HashMap<K,T> newKeys=new HashMap<>();
        newList.forEach((x)->newKeys.put(comparer.getKey(x), x));
        for(T o:orgingalList)
        {
            if(!newKeys.containsKey(comparer.getKey(o)))
                ret.deletedItems.put(comparer.getKey(o),o);
            else 
            {
                T newRecord=newKeys.get(comparer.getKey(o));
                if(!comparer.recordEqual(o, newRecord))
                {
                    ret.updatedItems.put(comparer.getKey(newRecord),newRecord);
                }
            }
        }
        HashMap<K,T> oldKeys=new HashMap<>();
        orgingalList.forEach((x)->oldKeys.put(comparer.getKey(x),x));
        for(T n:newList)
        {
           if(!oldKeys.containsKey(comparer.getKey(n)))
               ret.newItems.put(comparer.getKey(n),n);
        }
        return ret;        
    }
}
