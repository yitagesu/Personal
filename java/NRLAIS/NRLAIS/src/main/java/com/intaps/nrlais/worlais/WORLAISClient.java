/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.worlais;

import com.intaps.nrlais.Startup;
import com.intaps.nrlais.UserSession;
import com.intaps.nrlais.util.GSONUtil;
import com.intaps.nrlais.util.GSONUtil.JSONRet;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.repo.RepoBase;
import com.intaps.nrlais.worlais.model.WSearchPars;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.Proxy;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

/**
 *
 * @author Tewelde
 */
public class WORLAISClient {

    public String getSesionID() {
        return this.sessionID;
    }

    public int getRole() {
        return this.role;
    }

    public boolean isOffier() {
        return (this.role & ROLE_OFFICER) == ROLE_OFFICER;
    }

    public boolean isExpert() {
        return (this.role & ROLE_EXPERT) == ROLE_EXPERT;
    }

    public boolean isSupper() {
        return (this.role & ROLE_SUPPER) == ROLE_SUPPER;
    }
    public boolean isAdmin() {
        return (this.role & ROLE_ADMIN) == ROLE_ADMIN;    
    }
    public String getUserFullName(String syscreateby) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    

    static class DBConfigRepo extends RepoBase {

        public DBConfigRepo(UserSession session) {
            super(session);
        }

        public String getConfigValue(Connection con, String key) throws SQLException {
            try (ResultSet rs = con.prepareStatement("Select configvalue from nrlais_sys.t_config where configkey='" + key + "'").executeQuery()) {
                if (rs.next()) {
                    return rs.getString("configvalue");
                } else {
                    return null;
                }
            }

        }
    }

    public double getMinAreaConfig(Connection con,LADM ladm) {
        try {
            DBConfigRepo repo = new DBConfigRepo(null);
            String val=repo.getConfigValue(con, "cmss.parcelsize.min");
            if(StringUtils.isEmpty(val))
                return 0;
            return Double.parseDouble(val);
            
        } catch (Exception ex) {
            throw new IllegalStateException("Minimum parcel size couldn't not be rerieved", ex);
        }
    }

    public String getWoredaID(Connection con) {
        try {
            DBConfigRepo repo = new DBConfigRepo(null);
            return repo.getConfigValue(con, "system.region") + repo.getConfigValue(con, "system.zone") + repo.getConfigValue(con, "system.woreda");
        } catch (Exception ex) {
            throw new IllegalStateException("Installation woreda configuration couldn't be retreived", ex);
        }
    }
    public String getLevel(Connection con) {
        try {
            DBConfigRepo repo = new DBConfigRepo(null);
            return repo.getConfigValue(con, "system.level");
        } catch (Exception ex) {
            throw new IllegalStateException("Installation woreda configuration couldn't be retreived", ex);
        }
    }

    public enum ConnectionStatus {
        DISCONNECTED,
        CONNECTED,
        ERROR,
    }

    public static class UP {

        public String username;
        public String password;
        public long clientDate;

    }

    public static class LoginRes {

        public static class LoginResult {

            public String[] roles;
        }
        public boolean success;
        public String message;
        public String[] roles;
        public LoginResult result;
    }
    public static final int ROLE_UNKNOWN = 0;
    public static final int ROLE_OFFICER = 1;
    public static final int ROLE_EXPERT = 2;
    public static final int ROLE_SUPPER = 4;
    public static final int ROLE_ADMIN=8;
    String _url;
    String sessionID = null;
    int role = ROLE_UNKNOWN;

    public WORLAISClient() {
        _url = Startup.getAppConfig("worlais-url");
    }

    public String login(String un, String pw) throws MalformedURLException, IOException {
        UP up = new UP();
        up.username = un;
        up.password = pw;
        up.clientDate = new java.util.Date().getTime();
        LoginRes res = invokeJSON("/nrlais/cmss/login", up, LoginRes.class);
        if (!res.success) {
            throw new IllegalArgumentException(res.message == null ? "Login failed" : res.message);
        }
        int role = ROLE_UNKNOWN;
        if (res.result != null && res.result.roles != null) {
            for (String r : res.result.roles) {
                switch (r) {
                    case "Land Administration Expert":
                        role |= ROLE_EXPERT;
                        break;
                    case "Land Administration Officer":
                        role |= ROLE_OFFICER;
                        break;
                    case "Supervisor":
                        role |= ROLE_SUPPER;
                        break;
                    case "admin":
                        role |= ROLE_ADMIN;
                        break;
                }
            }
        }
        this.role = role;
        return this.sessionID;
    }

    public <RetType> RetType invokeJSON(String pathAndQuery, Object post, Class<RetType> c) throws MalformedURLException, IOException {
        URL url = new URL(_url + pathAndQuery);
        return invokeJSON(url, post, c, post==null?"GET":"POST");
    }

    public <RetType> RetType invokeJSON(String pathAndQuery, Object post, Class<RetType> c, String postMethod) throws MalformedURLException, IOException {
        URL url = new URL(_url + pathAndQuery);
        return invokeJSON(url, post, c, postMethod);
    }

    void readSessionKey(CloseableHttpResponse response) {
        Header h = response.getFirstHeader("set-cookie");
        if (h != null) {
            HeaderElement[] vals = h.getElements();
            for (HeaderElement nv : vals) {
                if(nv.getName().equalsIgnoreCase("connect.sid"))
                {
                    this.sessionID=nv.getValue();
                }
            }
        }
    }

    <RetType> RetType readJSONResponse(CloseableHttpResponse response, Class<RetType> c) throws IOException {
        HttpEntity entity = response.getEntity();
        try (InputStream in = new BufferedInputStream(entity.getContent());) {
            String retStr = new String(org.apache.commons.io.IOUtils.toByteArray(in));
            RetType ret = GSONUtil.fromJson(c, retStr);
            readSessionKey(response);
            return ret;
        }
    }
    void setRequestCookies(HttpRequestBase request)
    {
        if (sessionID != null) 
        {
            request.setHeader("Cookie", "connect.sid=" + sessionID);
        }
    }

    public <RetType> RetType invokeJSON(URL url, Object post, Class<RetType> c, String postMethod) throws IOException {

        CloseableHttpClient httpclient = HttpClients.createDefault();
        if (postMethod.equals("GET")) {
            HttpGet httpGet = new HttpGet(url.toString());
            setRequestCookies(httpGet);
            CloseableHttpResponse response = httpclient.execute(httpGet);
            try {
                return readJSONResponse(response, c);
            } finally {
                response.close();
            }
        } else {
            
            HttpEntityEnclosingRequestBase httpPost;
            if(postMethod.equals("PATCH"))
                httpPost=new HttpPatch(url.toString());
            else if(postMethod.equals("POST"))
                httpPost= new HttpPost(url.toString());
            else
                throw new IllegalArgumentException("HTTP Method "+postMethod+" not supported");
            setRequestCookies(httpPost);
            String jsonString = GSONUtil.toJson(post);
            StringEntity requestEntity = new StringEntity(jsonString, ContentType.APPLICATION_JSON);
            httpPost.setEntity(requestEntity);
            CloseableHttpResponse response = httpclient.execute(httpPost);            
            try 
            {
                return readJSONResponse(response, c);

            } finally {
                response.close();
            }
        }
    }

    static class WSearchParsRes extends JSONRet<WSearchPars> {

    }

    static class CheckResult {

        public static class Data {

            public String success;
        }
        public boolean success;
        public Data result;

    }

    static class StatusChangeResult {

        public boolean success;
        public int result;
    }
    static class StatusChangeRequest
    {
        public String statusnotes;
        
    }
    public void transactionChangeSate(String txuid, String cmd,String note) throws Exception
    {
        transactionChangeSate(txuid, cmd, note,true);
    }
    public void transactionChangeSate(String txuid, String cmd,String note,boolean runCheck) throws Exception {
        if(runCheck)
            runCheck(txuid);
        StatusChangeRequest snote=new StatusChangeRequest();
        snote.statusnotes=note;
        StatusChangeResult sres = invokeJSON("/nrlais/pss/transaction/status/" + txuid + "/" + cmd, snote, StatusChangeResult.class, "PATCH");
        if (!sres.success) {
            throw new Exception("Sorry, we failed to change the status of the transaction");
        }
    }

    public void runCheck(String txuid) throws IOException, Exception {
        CheckResult res = invokeJSON("/nrlais/ladm/checks/execute/" + txuid, null, CheckResult.class);
        if (!res.success || res.result == null) {
            throw new Exception("Failed to run data checking");
        }
        if (!"passed".equals(res.result.success)) {
            throw new Exception("Data is invalid. Please check your transaction");
        }
    }

}
