/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.repo;

import com.intaps.nrlais.UserSession;
import com.intaps.nrlais.model.KebeleInfo;
import com.intaps.nrlais.model.MultiLangString;
import com.intaps.nrlais.worlais.WORLAISClient;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Tewelde
 */
public class KebeleRepo extends RepoBase {

    public KebeleRepo(UserSession session) {
        super(session);
    }
 
    public List<KebeleInfo> getAllKebeles(final Connection con) throws SQLException {
        String woredaID = super.session.worlaisSession.getWoredaID(con);
        List<KebeleInfo> ret = new ArrayList<>();
        try (ResultSet rs = con.prepareStatement("Select nrlais_kebeleid,kebelenameeng,kebelenameamharic,kebelenametigrinya,kebelenameoromifya from nrlais_sys.t_kebeles where nrlais_woredaid='" + woredaID + "' order by nrlais_kebeleid").executeQuery()) {
            while (rs.next()) {
                KebeleInfo k = new KebeleInfo();
                k.kebeleID = rs.getString("nrlais_kebeleid");
                k.name = new MultiLangString(rs.getString("kebelenameeng"), rs.getString("kebelenameamharic"), rs.getString("kebelenameoromifya"),
                        rs.getString("kebelenametigrinya"));
                ret.add(k);
            }
        }
        return ret;
    }

   
    public KebeleInfo get(final Connection con, String kebeleid) throws SQLException {
        try (ResultSet rs = con.prepareStatement("Select nrlais_kebeleid,kebelenameeng,kebelenameamharic,kebelenametigrinya,kebelenameoromifya from nrlais_sys.t_kebeles where nrlais_kebeleid='" + kebeleid + "'").executeQuery()) {
            if (rs.next()) {
                KebeleInfo k = new KebeleInfo();
                k.kebeleID = rs.getString("nrlais_kebeleid");
                k.name = new MultiLangString(rs.getString("kebelenameeng"), rs.getString("kebelenameamharic"), rs.getString("kebelenameoromifya"),
                        rs.getString("kebelenametigrinya"));
                return k;
            }
            return null;
        }
    }
}
