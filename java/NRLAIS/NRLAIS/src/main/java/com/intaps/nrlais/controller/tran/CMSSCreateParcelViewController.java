/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.controller.tran;

import com.intaps.nrlais.controller.ViewControllerBase;
import com.intaps.nrlais.model.CMSSCreateParcelsTask;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.util.INTAPSLangUtils;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Tewelde
 */
public class CMSSCreateParcelViewController extends ViewControllerBase {    
    public List<LADM.Parcel> parcels;
    public CMSSCreateParcelsTask task;
    
    public CMSSCreateParcelViewController(HttpServletRequest request, HttpServletResponse response) throws SQLException {
        super(request, response);
        
        task=(CMSSCreateParcelsTask)mainFacade.getTask(request.getParameter("task_uid"),true);
        parcels=new ArrayList<LADM.Parcel>();
        for(String p:task.newParcelUIDs)
        {
            parcels.add(mainFacade.getParcel("nrlais_transaction",p));            
        }
    }
    public String parcelOwner(LADM.Parcel parcel)
    {
        String ret="";
        for(LADM.Right r:parcel.getHolders())
        {
            ret=INTAPSLangUtils.appendStringList(ret, "</br>", r.party.getFullName());
        }
        return ret;                
    }   
   
    
}