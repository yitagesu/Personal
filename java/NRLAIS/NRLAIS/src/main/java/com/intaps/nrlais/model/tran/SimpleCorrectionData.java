/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.model.tran;

import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.SourceDocument;
import com.intaps.nrlais.util.RationalNum;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author yitagesu
 */
public class SimpleCorrectionData extends HoldingTranscationData {
    public List<Applicant> applicant = new ArrayList<Applicant>();
    public SourceDocument landHoldingCertifcateDocument = null;
}
