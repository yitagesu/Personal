/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.model.tran;

import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.SourceDocument;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Tewelde
 */
public class TransferTransactionData extends HoldingTranscationData{
    public int tranferType=GiftTransactionData.TRANSFER_TYPE_NEW_HOLDING;
    public String otherHoldingUID=null;
    public List<PartyItem> beneficiaries=new ArrayList<>();
    public List<ParcelTransfer> transfers=new ArrayList<ParcelTransfer>();
    public List<Applicant> applicants=new ArrayList<Applicant>();
    public SourceDocument landHoldingCertificateDoc=null;

}
