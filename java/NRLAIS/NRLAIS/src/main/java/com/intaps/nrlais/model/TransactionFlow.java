/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.model;

/**
 *
 * @author Tewelde
 */
public class TransactionFlow extends NRLAISEntity{
    public String flowUID;
    public String notes;
    public String transactionUID;
    public int oldStatus;
    public int newStatus;
    public int flowSeqNo;
}
