/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.repo;

import com.intaps.nrlais.Startup;
import com.intaps.nrlais.UserSession;
import com.intaps.nrlais.model.CMSSTask;
import com.intaps.nrlais.model.KebeleInfo;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.tran.DataMigrationTransactionData;
import com.intaps.nrlais.tran.TransactionHandler;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

/**
 *
 * @author Tewelde
 */
public class TransactionFacade extends RepoBase {

    public TransactionFacade(UserSession session) {
        super(session);
    }

    public String saveTransaction(LATransaction tran, boolean register) throws SQLException, IOException, Exception {
        if (!session.worlaisSession.isOffier()) {
            throw new IllegalStateException("Only officer can register transactions");
        }
        Connection con = Startup.getWriteConnection();
        synchronized (con) {
            assertKebele(con, tran.nrlais_kebeleid);
            con.setAutoCommit(false);
            try {
                TransactionRepo repo = new TransactionRepo(session);
                String txuid;
                if (tran.transactionUID != null) {
                    LATransaction existing = new GetTransaction(session).get(con, tran.transactionUID, false);
                    if (existing == null) {
                        throw new IllegalArgumentException(tran.transactionUID + " is not valid trancation uid");
                    }
                    if (existing.status != LATransaction.TRAN_STATUS_INITIATED) {
                        throw new IllegalArgumentException(tran.transactionUID + " can't be edited");
                    }
                    if (existing.transactionType != tran.transactionType) {
                        throw new IllegalArgumentException("Transaction type can't be changed once saved");
                    }
                    txuid = existing.transactionUID;
                    repo.updateTransaction(con, tran);
                } else {
                    txuid = repo.createTransaction(con, tran.nrlais_kebeleid, tran.transactionType, tran.notes, tran.time, true);
                }
                tran.transactionUID = txuid;
                TransactionHandler handler = GetTransaction.createTransactionHandler(tran.transactionType);
                handler.saveTransaction(session, con, tran, register);
                repo.updateTransactionData(con, tran);
                if (register) {
                    repo.changeTransactionState(con, txuid, LATransaction.TRAN_STATUS_INITIATED, LATransaction.TRAN_STATUS_REGISTERD, tran.notes);
                }
                con.commit();
                return txuid;
            } catch (Exception ex) {
                con.rollback();
                throw ex;
            }
        }
    }

    public void assignGeometry(CMSSTask.TaskGeomAssignment a, boolean commit) throws SQLException, Exception {
        Connection con = Startup.getWriteConnection();
        synchronized (con) {
            con.setAutoCommit(false);
            try {
                CMSSTaskManager man = new CMSSTaskManager(session);
                man.assignGeometry(con, a);
                if (commit) {
                    man.commitTask(con, a.task_uid);
                }
                con.commit();
            } catch (Exception ex) {
                con.rollback();
                throw ex;
            }
        }
    }

    public void commitTask(String task_uid) throws SQLException {
        Connection con = Startup.getWriteConnection();
        synchronized (con) {
            con.setAutoCommit(false);
            try {
                CMSSTaskManager man = new CMSSTaskManager(session);
                man.commitTask(con, task_uid);

                con.commit();
            } catch (Exception ex) {
                con.rollback();
                throw ex;
            }
        }
    }

    void assertKebele(Connection con, String kebeleid) throws SQLException {
        KebeleInfo kebele = new KebeleRepo(session).get(con, kebeleid);
        if (kebele == null) {
            throw new IllegalArgumentException(kebeleid + " doesn't exist in the database");
        }
        if (!session.worlaisSession.getWoredaID(con).equals(KebeleInfo.getWoredaID(kebeleid))) {
            throw new IllegalArgumentException(kebeleid + " is not within the woreda of this WORLAIS installation");
        }
    }

    public String dumpMigrationData(LADM.Holding holding) throws SQLException, Exception {
        Connection con = Startup.getWriteConnection();
        synchronized (con) {
            con.setAutoCommit(false);
            try {
                LATransaction tran=new LATransaction();
                tran.transactionType=LATransaction.TRAN_TYPE_DATA_MIGRATION;
                tran.time=new java.util.Date().getTime();
                tran.notes="Data Migration";
                tran.nrlais_kebeleid=holding.nrlais_kebeleid;
                DataMigrationTransactionData data=new DataMigrationTransactionData();
                data.holding=holding;
                tran.data=data;
                
                TransactionRepo repo = new TransactionRepo(session);                
                tran.transactionUID = repo.createTransaction(con, tran.nrlais_kebeleid, tran.transactionType, tran.notes, tran.time, true);
                TransactionHandler handler = GetTransaction.createTransactionHandler(tran.transactionType);
                handler.saveTransaction(session, con, tran,true);                
                repo.changeTransactionState(con, tran.transactionUID, LATransaction.TRAN_STATUS_INITIATED, LATransaction.TRAN_STATUS_ACCEPT_REQUESTED, tran.notes);
                repo.updateTransactionData(con, tran);
                repo.acceptDataMigrationTranscation(con, tran, "Data Migration");
                
                con.commit();
                return holding.holdingUID;
            } catch (Exception ex) {
                con.rollback();
                throw ex;
            }
        }
    }

}
