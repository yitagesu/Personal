/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.controller.routes;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.intaps.nrlais.Startup;
import com.intaps.nrlais.controller.routes.api.APIBase;
import com.intaps.nrlais.controller.tran.DivorceViewController;
import com.intaps.nrlais.controller.tran.RentViewController;
import com.intaps.nrlais.controller.tran.TransactionViewController;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.tran.DivorceTransactionData;
import com.intaps.nrlais.model.tran.RentTransactionData;
import com.intaps.nrlais.repo.MainFacade;
import com.intaps.nrlais.repo.TransactionFacade;
import com.intaps.nrlais.util.GSONUtil;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Tewelde
 */
@WebServlet("/api/transaction")
public class Transaction extends RouteBase<LATransaction,LATransaction,String> {

    @Override
    public LATransaction createGetResponse(HttpServletRequest req,HttpServletResponse resp) throws Exception {
        String txuid = req.getParameter("txuid");
        return new MainFacade(Startup.getSessionByRequest(req)).getTransaction(txuid, true);
    }

    @Override
    public String processPost(HttpServletRequest req,HttpServletResponse resp, LATransaction data) throws Exception {
        if(data.data==null)
            throw new IllegalArgumentException("Data is not provided for the transaction");
        return TransactionViewController.createController(req, resp, data.data.getClass(), false).saveData(data);
    }

    @Override
    public LATransaction deserializePostBody(JsonReader reader) throws IOException {
        return Startup.readTransaction(reader);
    }    
}

