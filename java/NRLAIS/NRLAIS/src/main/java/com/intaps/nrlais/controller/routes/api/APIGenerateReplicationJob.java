/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.controller.routes.api;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.intaps.nrlais.NRLAISWorker;
import com.intaps.nrlais.Startup;
import com.intaps.nrlais.replication.ReplicationFileGeneratorWorker;
import com.intaps.nrlais.util.GSONUtil;
import java.io.File;
import java.io.FileInputStream;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.codec.binary.StringUtils;

/**
 *
 * @author Teweldemedhin Aberra
 */
@WebServlet("/api/replication_job")
public class APIGenerateReplicationJob extends APIBase {

    static class StatusInfoRet extends GSONUtil.JSONRet<ReplicationFileGeneratorWorker.ReplicationFileGeneratorStatusInfo> {

    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        GSONUtil.VoidRet ret = new GSONUtil.VoidRet();
        try {
            assertLogedInUser(req, resp);

            synchronized (ReplicationFileGeneratorWorker.worker) {
                ReplicationFileGeneratorWorker.worker.stop();
                ret.error = null;
            }

        } catch (Exception ex) {
            Logger.getLogger(APIGenerateReplicationJob.class.getName()).log(Level.SEVERE, null, ex);
            ret.error = ex.getMessage();
        }
        try (ServletOutputStream str = resp.getOutputStream()) {
            resp.setContentType("application/json");
            JsonWriter writer = new JsonWriter(new OutputStreamWriter(str, "UTF-8"));
            GSONUtil.getAdapter(GSONUtil.VoidRet.class).write(writer, ret);
            writer.close();
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ;
        if ("true".equals(req.getParameter("get_file"))) {
            try {
                synchronized (ReplicationFileGeneratorWorker.worker) {
                    String file="data/tmp/" + ReplicationFileGeneratorWorker.worker.getFileName();
                    try (FileInputStream input = new FileInputStream(file);
                            ServletOutputStream out = resp.getOutputStream()) {
                        resp.setContentType("application/zip, application/octet-stream");
                        resp.setHeader("content-disposition", "attachment; filename=\"" + new File(file).getName() + "\"");
                        byte[] buffer = new byte[1024 * 4];
                        int len;
                        while ((len = input.read(buffer)) > 0) {
                            out.write(buffer, 0, len);
                        }
                    }
                    return;
                }
            } catch (Exception ex) {
                throw new ServletException(ex);
            }
        }
        StatusInfoRet ret = new StatusInfoRet();
        try {
            assertLogedInUser(req, resp);
            String ind = req.getParameter("err_index");
            String cnt = req.getParameter("err_count");
            int errIndex = ind == null ? -1 : Integer.parseInt(ind);
            int errCount = cnt == null ? -1 : Integer.parseInt(cnt);

            synchronized (ReplicationFileGeneratorWorker.worker) {
                ret.res = (ReplicationFileGeneratorWorker.ReplicationFileGeneratorStatusInfo) ReplicationFileGeneratorWorker.worker.getStatus(errIndex, errCount);
                ret.error = null;
            }

        } catch (Exception ex) {
            Logger.getLogger(APIGenerateReplicationJob.class.getName()).log(Level.SEVERE, null, ex);
            ret.error = ex.getMessage();
            ret.res = null;
        }
        try (ServletOutputStream str = resp.getOutputStream()) {
            resp.setContentType("application/json");
            JsonWriter writer = new JsonWriter(new OutputStreamWriter(str, "UTF-8"));
            GSONUtil.getAdapter(StatusInfoRet.class).write(writer, ret);
            writer.close();
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        GSONUtil.VoidRet ret = new GSONUtil.VoidRet();
        try (
                ServletInputStream ins = req.getInputStream();
                JsonReader reader = new JsonReader(new InputStreamReader(ins, "UTF-8"));) {
            try {
                String userName = assertLogedInUser(req, resp);
                ReplicationFileGeneratorWorker.ReplicationFileGeneratorPar par = GSONUtil.getAdapter(ReplicationFileGeneratorWorker.ReplicationFileGeneratorPar.class).read(reader);
                synchronized (ReplicationFileGeneratorWorker.worker) {
                    ReplicationFileGeneratorWorker.worker.run(Startup.getSessionByRequest(req), par);
                }
                ret.error = null;
            } catch (Exception ex) {
                Logger.getLogger(APIGenerateReplicationJob.class.getName()).log(Level.SEVERE, null, ex);
                ret.error = "Error: " + ex.getMessage();
            }
        }
        try (ServletOutputStream str = resp.getOutputStream()) {
            resp.setContentType("application/json");
            JsonWriter writer = new JsonWriter(new OutputStreamWriter(str, "UTF-8"));
            GSONUtil.getAdapter(GSONUtil.VoidRet.class).write(writer, ret);
            writer.close();
        }
    }
}
