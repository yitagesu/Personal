/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.controller.routes;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonReader;
import com.intaps.nrlais.Startup;
import com.intaps.nrlais.controller.tran.ExOfficioViewController;
import com.intaps.nrlais.controller.tran.ManualLRManipulatorViewController;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.tran.ManualTransactionData;
import com.intaps.nrlais.util.GSONUtil;
import java.io.IOException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Tewelde
 */
@WebServlet("/api/lr_operation")
public class LRManipulatorRoute extends RouteBase<ManualTransactionData.LrOperation, ManualTransactionData.LrOperation, Object> {

    @Override
    public ManualTransactionData.LrOperation createGetResponse(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
     @Override
    public Object processPost(HttpServletRequest req, HttpServletResponse resp, ManualTransactionData.LrOperation data) throws Exception {
        return new ManualLRManipulatorViewController(req, resp).processCommand(data);
    }

    @Override
    public ManualTransactionData.LrOperation deserializePostBody(JsonReader reader) throws IOException, ClassNotFoundException {
        JsonElement je = GSONUtil.parse(reader);
        JsonObject obj = je.getAsJsonObject();
        String tname = obj.getAsJsonPrimitive("opType").getAsString();
        Class c = Class.forName(tname);
        Object ret = GSONUtil.gson.getAdapter(c).fromJsonTree(je);
        return (ManualTransactionData.LrOperation) ret;
    }
}
