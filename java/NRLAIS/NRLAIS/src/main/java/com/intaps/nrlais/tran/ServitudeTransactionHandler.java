/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.tran;

import com.intaps.nrlais.UserSession;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.CMSSParcelSplitTask;
import com.intaps.nrlais.model.DocumentTypeInfo;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LADM.Parcel;
import com.intaps.nrlais.model.LADM.Party;
import com.intaps.nrlais.model.SourceDocument;
import com.intaps.nrlais.model.tran.Applicant;
import com.intaps.nrlais.model.tran.DivorceTransactionData;
import com.intaps.nrlais.model.tran.ParcelTransfer;
import com.intaps.nrlais.model.tran.Partition;
import com.intaps.nrlais.model.tran.PartyItem;
import com.intaps.nrlais.model.tran.PartyParcelShare;
import com.intaps.nrlais.model.tran.RentTransactionData;
import com.intaps.nrlais.model.tran.RestrictionTransactionData;
import com.intaps.nrlais.model.tran.RestrictiveInterestTransactionData;
import com.intaps.nrlais.model.tran.ServitudeTransactionData;
import com.intaps.nrlais.repo.CMSSTaskManager;
import com.intaps.nrlais.repo.DocumentTypeRepo;
import com.intaps.nrlais.repo.GetLADM;
import com.intaps.nrlais.repo.GetTransaction;
import com.intaps.nrlais.repo.LRManipulator;
import com.intaps.nrlais.repo.TransactionRepo;
import com.intaps.nrlais.util.EthiopianCalendar;
import com.intaps.nrlais.util.RationalNum;
import com.intaps.nrlais.worlais.WORLAISClient;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.AbstractList;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.Mac;

/**
 *
 * @author Tewelde
 */
public class ServitudeTransactionHandler extends RestrictionTransactionHandler<ServitudeTransactionData> implements TransactionHandler {

    @Override
    protected SourceDocument[] sourceDocs(ServitudeTransactionData tran) {
        return new SourceDocument[]{tran.landHoldingCertificateDoc};
    }

    @Override
    protected int[] sourceDocTypes(ServitudeTransactionData tran) {
        return new int[]{DocumentTypeInfo.DOC_TYPE_LAND_HOLDING_CERTIFICATE};
    }

}
