/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.repo.lrop;

import com.intaps.nrlais.UserSession;
import com.intaps.nrlais.model.CMSSCreateParcelsTask;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.tran.ManualTransactionData;
import com.intaps.nrlais.repo.CMSSTaskManager;
import com.intaps.nrlais.repo.GetLADM;
import com.intaps.nrlais.repo.LRManipulator;
import com.intaps.nrlais.repo.ManualLRManipulatorFacade;
import java.io.IOException;
import java.sql.Connection;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Tewelde
 */
public class LrOpExecUpdateParcel implements ManualLRManipulatorFacade.LROPExecutor<ManualTransactionData.LrOpUpateParcel> {

    @Override
    public Object doOperation(UserSession session, Connection con, LATransaction tran, ManualTransactionData.LrOpUpateParcel op) throws Exception {
        LRManipulator lrm = new LRManipulator(session);
        GetLADM ladm = new GetLADM(session, "nrlais_transaction");
        LADM.Holding holding;
        if (op.parcel.parcelUID == null) {
            holding = ladm.get(con, op.holdingUID, LADM.CONTENT_FULL).holding;
            lrm.lrStartTransaction(con, tran, holding.holdingUID);
            op.parcel.rights = new ArrayList<>();
            op.parcel.rights.addAll(holding.getHolders());
            String parcelUID = lrm.lrAddParcel(con, tran, holding.holdingUID, op.parcel);
            CMSSCreateParcelsTask cp = new CMSSCreateParcelsTask();
            cp.transactionUID = tran.transactionUID;
            cp.newParcelUIDs.add(parcelUID);
            new CMSSTaskManager(session).addCreateParcelsTask(con, cp);
            lrm.finishTransaction(con, tran,false);
        } else {
            holding = ladm.getByParcel(con, op.parcel.parcelUID, LADM.CONTENT_FULL).holding;
            lrm.lrStartTransaction(con, tran, holding.holdingUID);
            lrm.lrUpateParcel(con, tran, holding.holdingUID, op.parcel);
            lrm.finishTransaction(con, tran,false);
        }
        return op.parcel.parcelUID;

    }

}
