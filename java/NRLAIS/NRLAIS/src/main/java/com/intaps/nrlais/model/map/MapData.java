/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.model.map;

/**
 *
 * @author Tewelde
 */
public class MapData {
    public String description;
    public GeoJSON gj;
    public double x;
    public double y;
    public double width;
    public double height;
}
