/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.tran;

import com.intaps.nrlais.UserSession;
import com.intaps.nrlais.model.DocumentTypeInfo;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.SourceDocument;
import com.intaps.nrlais.model.tran.Applicant;
import com.intaps.nrlais.model.tran.Partition;
import com.intaps.nrlais.model.tran.PartyParcelShare;
import com.intaps.nrlais.model.tran.SplitHoldingTransactionData;
import com.intaps.nrlais.repo.DocumentTypeRepo;
import com.intaps.nrlais.repo.GetLADM;
import com.intaps.nrlais.repo.LRManipulator;
import com.intaps.nrlais.repo.TransactionRepo;
import com.intaps.nrlais.util.EthiopianCalendar;
import com.intaps.nrlais.util.RationalNum;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 *
 * @author user
 */
public class SplitTransactionHandler extends TransactionHandlerBase implements TransactionHandler {
    
        void registerTransaction(UserSession session, Connection con, LATransaction theTransaction, SplitHoldingTransactionData tran) throws SQLException, Exception {
        LRManipulator lrm = new LRManipulator(session);       
        TransactionRepo tranRep = new TransactionRepo(session);
        GetLADM repo = new GetLADM(session, "nrlais_transaction");
        LADM ladm = repo.get(con, tran.holdingUID, LADM.CONTENT_FULL);
        LADM.Holding holding = ladm.holding;

        //check applicants
        HashMap<String, Applicant> applicants = new HashMap<>();
        for (Applicant ap : tran.applicants) {
            if (applicants.containsKey(ap.selfPartyUID)) {
                throw new IllegalArgumentException(session.text("Duplicate applicant ") + ap.selfPartyUID);
            }
            applicants.put(ap.selfPartyUID, ap);

        }
        //save documents
        SourceDocument[] allDocs = new SourceDocument[]{tran.landHoldingCertifcateDocument};
        int[] allTypes = new int[]{DocumentTypeInfo.DOC_TYPE_LAND_HOLDING_CERTIFICATE};
        DocumentTypeRepo docTypeRepo = new DocumentTypeRepo(session);
        for (int i = 0; i < allDocs.length; i++) {
            SourceDocument srcDoc = allDocs[i];
            if (srcDoc == null) {
                throw new IllegalArgumentException(session.text("Source document of type")+":" + docTypeRepo.getDocumentType(con, srcDoc.sourceType) + session.text(" isnot provided"));
            }
            srcDoc.uid = UUID.randomUUID().toString();
            srcDoc.sourceType = allTypes[i];
            tranRep.saveDocument(con, theTransaction, srcDoc);
        }
        
        if(!holding.isFullyOwnedByCouples() && holding.getHolders().size() > 1)
        {


        HashMap<String, Partition> partitions = new HashMap<String, Partition>();
        tran.partitions.forEach((p) -> {
            partitions.put(p.parcelUID, p);
        });

        lrm.lrStartTransaction(con, theTransaction, holding.holdingUID);

        HashMap<String, String> splitHoldings = new HashMap<>();

        Iterator<LADM.Parcel> parcelIter = holding.parcels.iterator();

        while (parcelIter.hasNext()) {
            LADM.Parcel parcel = parcelIter.next();
            if (!partitions.containsKey(parcel.parcelUID)) {
                throw new IllegalArgumentException(session.text("Partition information for parcel ") + parcel.upid + session.text(" is not set")+".");
            }
            Partition parition = partitions.get(parcel.parcelUID);
            HashMap<String, PartyParcelShare> shares = new HashMap<>();
            parition.shares.forEach((s)
                    -> {
                shares.put(s.partyUID, s);
            });
            if (parition.hasShared() && !parition.is100Percent()) {
                throw new IllegalArgumentException(session.text("Total share of shard parcels should be 100%"));
            }
            double minSize = session.worlaisSession.getMinAreaConfig(con,ladm);
            boolean splitable = parcel.areaGeom > minSize;
            List<LADM.Right> holders = parcel.getHolders();

            Iterator<LADM.Right> iter = holders.iterator();
            ArrayList<Map.Entry<String, LADM.Parcel>> splitParcels = new ArrayList<>();
            boolean transfered = false;
            HashMap<LADM.Parcel,PartyParcelShare> parcelToShare=new HashMap<>();
            while (iter.hasNext()) {
                LADM.Right r = iter.next();
                        if (shares.containsKey(r.partyUID)) {
                            PartyParcelShare share = shares.get(r.partyUID);
                            if (share.splitGeom) {
                                if (!splitable) {
                                    throw new IllegalArgumentException(session.text("This parcel can not be split because "));
                                }
                               
                                String sh;
                                if (splitHoldings.containsKey(r.partyUID)) {
                                    sh = splitHoldings.get(r.partyUID);
                                } else {
                                    LADM.Holding h = holding.cloneHolding();
                                    sh = lrm.lrCreateNewHolding(con, theTransaction, h);
                                    splitHoldings.put(r.partyUID, sh);
                                }
                                LADM.Parcel splitParcel = parcel.clonedObjectNoRR();
                                splitParcel.resetGeom();
                                splitParcel.mreg_actype = LADM.Parcel.AC_TYPE_SPLIT;
                                splitParcel.mreg_acyear = EthiopianCalendar.ToEth(theTransaction.time).Year;
                                LADM.Right clonedRight = r.clonedObject(false);
                                clonedRight.share.set(1, 1);
                                splitParcel.rights.add(clonedRight);
                                splitParcels.add(new AbstractMap.SimpleEntry<>(sh, splitParcel));
                                parcelToShare.put(splitParcel,share);
                                iter.remove();

                            }
                            
                             else if(share.share.isOne())
                                    throw new IllegalArgumentException(session.text("100% share is not allowed"));
                            else {
                                r.share = share.share;
                                share.parcelUID=parcel.parcelUID;
                            }
                        }
               
            }//rights of parcel iterator

            if (transfered) {
                if (splitParcels.size() > 0) {
                    throw new IllegalArgumentException(session.text("A transfered parcel can not be split"));
                }
                continue;
            }

            if (splitParcels.size() > 0) {
                if (!holders.isEmpty()) //if there are holders that hasn't split their share add one split item for them
                {
                    //verfy that their share adds up to 100%
                    RationalNum total = new RationalNum();
                    for (LADM.Right r : holders) {
                        total.add(r.share);
                    }
                    if (!total.isOne()) {
                        throw new IllegalArgumentException(session.text("The total share of the holders that are sharing ") + parcel.upid + session.text(" is not 100%"));
                    }
                    lrm.lrUpateRights(con, theTransaction, holding.holdingUID, parcel.parcelUID, holders);
                    
                    splitParcels.add(new AbstractMap.SimpleEntry<>(holding.holdingUID, parcel));
                }
                lrm.lrSplitParcel(con, theTransaction, parcel.parcelUID, holding.holdingUID, splitParcels);
                for(Map.Entry<String,LADM.Parcel> s:splitParcels)
                {
                    parcelToShare.get(s.getValue()).parcelUID=s.getValue().parcelUID;
                }
                
            } else {
                lrm.lrUpateRights(con, theTransaction, holding.holdingUID, parcel.parcelUID, holders);
            }

            //add cmss split parcel task if necessary
        }//parcels iterator
        }
        else
            throw new IllegalArgumentException(session.text("number of holding must be greater than 1 or must not be married"));

        lrm.finishTransaction(con, theTransaction);
    }

    
    

    @Override
    public void saveTransaction(UserSession session, Connection con, LATransaction theTransaction, boolean register) throws Exception {
        
         try {
            SplitHoldingTransactionData tran = (SplitHoldingTransactionData) theTransaction.data;
            LADM existing = new GetLADM(session, "nrlais_inventory").get(con, tran.holdingUID, LADM.CONTENT_FULL);

            String err = existing.getRuleErrorsAsString(theTransaction.time);
            if (!org.apache.commons.lang3.StringUtils.isEmpty(err)) {
                throw new IllegalStateException(err);
            }
            //reject hodling that has none holding right
            if (existing.holding.hasNoneHoldingRight()) {
                throw new IllegalArgumentException(session.text("Inheritance transaction can not be execute on a holding with none-holding rights"));
            }
            //reject state land 
            if (existing.holding.isStateLand()) {
                throw new IllegalArgumentException(session.text("Inheritance transaction can not be exectued on state land"));
            }

            if (!register) {
                return;
            }

            super.transferLandRecordToTransaction(session, con, theTransaction.transactionUID, existing);
            registerTransaction(session, con, theTransaction, tran);

        } catch (CloneNotSupportedException | IOException ex) {
            throw new SQLException(session.text("Can not execute transaction"), ex);
        }
        
        
    }
    
}
