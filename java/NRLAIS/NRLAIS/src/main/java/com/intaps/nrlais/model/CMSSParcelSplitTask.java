/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Tewelde
 */
public class CMSSParcelSplitTask extends CMSSTask{
    public String parcelUID;
    public List<String> newParcelUIDs=new ArrayList<>(); 

    public CMSSParcelSplitTask()
    {
        
    }
    public CMSSParcelSplitTask(CMSSTask baseRet)
    {
        this.taskUID=baseRet.taskUID;
        this.transactionUID=baseRet.transactionUID;
        this.status=baseRet.status;
        this.taskType=TASK_TYPE_SPLIT;    
    }
}
