/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.worlais.model;

/**
 *
 * @author Tewelde
 */
public class WSearchPars {
    public static class WhereSpec
    {
        
    }
    private String schema;
    private String entity;
    private WhereSpec where=null;
    private String [] orderBy=new String[0];
    private String[] attributes;
    private int offset=0;
    private int limit=100;

    /**
     * @return the schema
     */
    public String getSchema() {
        return schema;
    }

    /**
     * @param schema the schema to set
     */
    public void setSchema(String schema) {
        this.schema = schema;
    }

    /**
     * @return the entity
     */
    public String getEntity() {
        return entity;
    }

    /**
     * @param entity the entity to set
     */
    public void setEntity(String entity) {
        this.entity = entity;
    }

    /**
     * @return the where
     */
    public WhereSpec getWhere() {
        return where;
    }

    /**
     * @param where the where to set
     */
    public void setWhere(WhereSpec where) {
        this.where = where;
    }

    /**
     * @return the orderBy
     */
    public String[] getOrderBy() {
        return orderBy;
    }

    /**
     * @param orderBy the orderBy to set
     */
    public void setOrderBy(String[] orderBy) {
        this.orderBy = orderBy;
    }

    /**
     * @return the attributes
     */
    public String[] getAttributes() {
        return attributes;
    }

    /**
     * @param attributes the attributes to set
     */
    public void setAttributes(String[] attributes) {
        this.attributes = attributes;
    }

    /**
     * @return the offset
     */
    public int getOffset() {
        return offset;
    }

    /**
     * @param offset the offset to set
     */
    public void setOffset(int offset) {
        this.offset = offset;
    }

    /**
     * @return the limit
     */
    public int getLimit() {
        return limit;
    }

    /**
     * @param limit the limit to set
     */
    public void setLimit(int limit) {
        this.limit = limit;
    }
}
