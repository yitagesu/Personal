/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.repo;

import com.intaps.nrlais.UserSession;
import com.intaps.nrlais.model.CMSSCreateParcelsTask;
import com.intaps.nrlais.model.CMSSEditParcelTask;
import com.intaps.nrlais.model.CMSSParcelSplitTask;
import com.intaps.nrlais.model.CMSSTask;
import com.intaps.nrlais.model.CMSSTaskGeom;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.repo.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Tewelde
 */
public class GetCMSSTask extends RepoBase {

    public GetCMSSTask(UserSession session) {
        super(session);
    }

    void populateBaseFields(ResultSet rs, CMSSTask baseRet) throws SQLException {
        baseRet.taskUID = rs.getString("uid");
        baseRet.taskType = rs.getInt("task_type");
        baseRet.status = rs.getInt("status");
        baseRet.transactionUID = rs.getString("transaction_uid");
    }

    public CMSSTask getTask(Connection con,String taskUID, boolean includeDetail) throws SQLException {
            CMSSTask baseRet = null;
            try (ResultSet rs = con.prepareStatement("Select * from nrlais_transaction.cmss_task where uid='" + taskUID + "'").executeQuery()) {
                if (rs.next()) {
                    baseRet = new CMSSTask();
                    populateBaseFields(rs, baseRet);
                    if (!includeDetail) {
                        return baseRet;
                    }
                } else {
                    return null;
                }
            }
            return populateTaskDetail(con, baseRet);
    }

    CMSSTask populateTaskDetail(Connection con, CMSSTask baseRet) throws SQLException {
        switch (baseRet.taskType) {
            case CMSSTask.TASK_TYPE_CREATE:
                CMSSCreateParcelsTask create = new CMSSCreateParcelsTask(baseRet);
                try (ResultSet rs = con.prepareStatement("Select target_parcel_uid from nrlais_transaction.cmss_create_task_parcels where task_uid='" + baseRet.taskUID + "'").executeQuery()) {
                    while (rs.next()) {
                        create.newParcelUIDs.add(rs.getString(1));
                    } 
                }
                return create;
            case CMSSTask.TASK_TYPE_EDIT:
                CMSSEditParcelTask edit = new CMSSEditParcelTask(baseRet);
                try (ResultSet rs = con.prepareStatement("Select parcel_uid from nrlais_transaction.cmss_edit_task_parcels where task_uid='" + baseRet.taskUID + "'").executeQuery()) {
                    while (rs.next()) {
                        edit.parcels.add(rs.getString(1));
                    } 
                }
                return edit;
            case CMSSTask.TASK_TYPE_SPLIT:
                CMSSParcelSplitTask split = new CMSSParcelSplitTask(baseRet);
                try (ResultSet rs = con.prepareStatement("Select parcel_uid from nrlais_transaction.cmss_split_task where task_uid='" + baseRet.taskUID + "'").executeQuery()) {
                    if (rs.next()) {
                        split.parcelUID = rs.getString(1);
                    } else {
                        throw new IllegalStateException("Split parcel detail not found in database for task id:" + baseRet.taskUID);
                    }
                }
                try (ResultSet rs = con.prepareStatement("Select target_parcel_uid from nrlais_transaction.cmss_split_task_parcels where task_uid='" + baseRet.taskUID + "'").executeQuery()) {
                    while (rs.next()) {
                        split.newParcelUIDs.add(rs.getString(1));
                    } 
                }
                return split;
            default:
                throw new IllegalStateException("Invalid task type:" + baseRet.taskType);
        }
    }

    public boolean isTaskDone(Connection con,String taskID) throws SQLException {
        return getTask(con,taskID, false).status == CMSSTask.TASK_STATUS_COMMITED;
    }

    public List<CMSSTask> getTaskforTransaction(final Connection con, String transactionUID, boolean includeDetail) throws SQLException {
        List<CMSSTask> ret = new ArrayList<CMSSTask>();
        try (ResultSet rs = con.prepareStatement("Select * from nrlais_transaction.cmss_task where transaction_uid='" + transactionUID + "'").executeQuery()) {
            while (rs.next()) {
                CMSSTask baseRet = new CMSSTask();
                populateBaseFields(rs, baseRet);
                ret.add(baseRet);
            }
            if (includeDetail) {
                for (int i = 0; i < ret.size(); i++) {
                    ret.set(i, populateTaskDetail(con, ret.get(i)));
                }
            }
            return ret;
        }
    }
    public List<CMSSTask> getPendingTasks(final Connection con,boolean includeDetail) throws SQLException {
        List<CMSSTask> ret = new ArrayList<CMSSTask>();
        try (ResultSet rs = con.prepareStatement("Select cmss_task.* from nrlais_transaction.cmss_task  inner join nrlais_inventory.t_transaction on t_transaction.uid=cmss_task.transaction_uid \n" +
            "where t_transaction.txstatus="+LATransaction.TRAN_STATUS_REGISTERD).executeQuery()) {
            while (rs.next()) {
                CMSSTask baseRet = new CMSSTask();
                populateBaseFields(rs, baseRet);
                ret.add(baseRet);
            }
            if (includeDetail) {
                for (int i = 0; i < ret.size(); i++) {
                    ret.set(i, populateTaskDetail(con, ret.get(i)));
                }
            }
            return ret;
        }
    }
    public List<CMSSTaskGeom> getTaskGeometries(final Connection con,String taskuid) throws SQLException
    {
        List<CMSSTaskGeom> ret=new ArrayList<CMSSTaskGeom>();
        try (ResultSet rs = con.prepareStatement("Select *,ST_Area(geom) as area from nrlais_transaction.cmss_task_geom where task_uid='" + taskuid + "'").executeQuery()) {
            while (rs.next()) {
                CMSSTaskGeom geom = new CMSSTaskGeom();
                geom.id=rs.getInt("id");
                geom.geom=rs.getObject("geom").toString();
                geom.parcelUID=rs.getString("parcel_uid");
                geom.label=rs.getString("label");
                geom.area=rs.getDouble("area");
                ret.add(geom);
            }            
            return ret;
        }
    }

    boolean isTransactionDone(Connection con, String txuid) throws SQLException {
        for(CMSSTask task:getTaskforTransaction(con, txuid, false))
            if(task.status==CMSSTask.TASK_STATUS_PENDING)
                return false;
        return true;
}

    
}
