/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.controller.tran;

import com.intaps.nrlais.controller.ViewControllerBase;
import com.intaps.nrlais.Startup;
import com.intaps.nrlais.model.IDName;
import com.intaps.nrlais.model.IDNameText;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.tran.PartyItem;
import com.intaps.nrlais.util.GSONUtil;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.net.URLCodec;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Tewelde
 */
public class PartyViewController extends ViewControllerBase {

    public PartyItem party;

    public static class Settings {

        public boolean share = false;
        public boolean idDoc = false;
        public int partyType = -1;
        public String containerEl = "";
        public boolean pom = false;
        public boolean representative = false;
        public boolean member = false;
        public boolean guardian = false;
    }
    public Settings setting;

    public PartyViewController(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException, DecoderException {
        super(request, response);
        String s = request.getParameter("setting");
        if (StringUtils.isEmpty(s)) {
            this.setting = new Settings();
        } else {
            this.setting = GSONUtil.getAdapter(Settings.class).fromJson(s);
        }
        if ("POST".equals(request.getMethod())) {
            this.party = Startup.readPostedObject(request, PartyItem.class);
        } else {
            String partyUID = request.getParameter("partyUID");
            String schema = request.getParameter("schema");
            if (StringUtils.isEmpty(schema)) {
                schema = "nrlais_transaction";
            }
            if (partyUID != null) {
                party = new PartyItem(super.mainFacade.getParty(schema, partyUID));
            } else {
                party = new PartyItem();
            }
        }
    }

    public String fullName() {
        if (party.party == null) {
            return "";
        }
        return party.party.getFullName();
    }

    public String name1() {
        if (party.party == null) {
            return "";
        }
        return party.party.name1;
    }

    public String name2() {
        if (party.party == null) {
            return "";
        }
        return party.party.name2;
    }

    public String name3() {
        if (party.party == null) {
            return "";
        }
        return party.party.name3;
    }

    public int sex() {
        if (party.party == null) {
            return LADM.Party.SEX_MALE;
        }
        return party.party.sex;
    }

    public String birthDate() {
        if (party.party == null) {
            return LADM.Party.formatBirthDate(new java.util.Date().getTime());
        }
        return LADM.Party.formatBirthDate(party.party.dateOfBirth);
    }

    public int orgType() throws SQLException {
        if (this.party == null || this.party.party == null) {
            return LADM.Party.ORG_TYPE_NA;
        }
        return this.party.party.orgatype;
    }

    public String sexText() throws SQLException {
        return super.sexText(party.party.sex);
    }

    public String roleText() throws SQLException {
        return super.roleText(party.party.mreg_familyrole);
    }

    public List<IDNameText> familyRoleList() throws SQLException {
        List<IDNameText> ret = new ArrayList<>();
        for (IDName name : this.mainFacade.getAllLookup("static.family_role")) {
            ret.add(name.idNameText(session.lang()));
        }
        return ret;
    }

    public List<IDNameText> maritalStatusList() throws SQLException {
        List<IDNameText> ret = new ArrayList<>();
        for (IDName name : this.mainFacade.getAllLookup("nrlais_sys.t_cl_maritalstatus")) {
            ret.add(name.idNameText(session.lang()));
        }
        return ret;
    }

    public List<IDNameText> partyTypeList() throws SQLException {
        List<IDNameText> ret = new ArrayList<>();
        for (IDName name : this.mainFacade.getAllLookup("nrlais_sys.t_cl_partytype")) {
            ret.add(name.idNameText(session.lang()));
        }
        return ret;
    }

    public List<IDNameText> holdingPurposeList() throws SQLException {
        List<IDNameText> ret = new ArrayList<>();
        for (IDName name : this.mainFacade.getAllLookup("nrlais_sys.t_cl_organizationtype")) {
            ret.add(name.idNameText(session.lang()));
        }
        return ret;
    }

    public String martialStatusText() throws SQLException {
        return super.maritalStatusText(party.party.maritalstatus);
    }

    public int partyType() throws SQLException {
        if (this.party == null || this.party.party == null) {
            return LADM.Party.PARTY_TYPE_NATURAL_PERSON;
        }
        return this.party.party.partyType;
    }

    public String partyTypeText() throws SQLException {
        if (this.party == null || this.party.party == null) {
            return "";
        }
        return super.partyTypeText(this.party.party.partyType);
    }

    public int martialStatus() {
        if (party.party == null) {
            return LADM.Party.MARITAL_STATUS_MARRIED;
        }
        return party.party.maritalstatus;
    }

    public int familyRole() {
        if (party.party == null) {
            return LADM.Party.FAMILY_ROLE_HUSBAND;
        }
        return party.party.mreg_familyrole;
    }

    public String address() {
        if (party.party == null) {
            return "";
        }
        LADM.PartyContact c = party.party.contact(LADM.PartyContact.CONTACT_TYPE_ADDRESS);
        if (c == null) {
            return "";
        }
        return c.contactData;
    }

    public boolean hasDisablity() {
        if (party.party == null) {
            return false;
        }
        return party.party.disability == 1;
    }

    public boolean isOrphan() {
        if (party.party == null) {
            return false;
        }
        return party.party.isorphan == 1;
    }

    public String share() {
        if (party.share == null) {
            return "";
        }
        return party.share.toString();
    }

    public boolean hasGuardian() {
        return party.party != null && party.party.guardian != null;
    }

    public String gName1() {
        if (!hasGuardian()) {
            return "";
        }
        return party.party.guardian.name1;
    }

    public String gName2() {
        if (!hasGuardian()) {
            return "";
        }
        return party.party.guardian.name2;
    }

    public String gName3() {
        if (!hasGuardian()) {
            return "";
        }
        return party.party.guardian.name3;
    }

    public int gSex() {
        if (!hasGuardian()) {
            return LADM.Party.SEX_MALE;
        }
        return party.party.guardian.sex;
    }

    public String gBirthDate() {
        if (!hasGuardian()) {
            return LADM.Party.formatBirthDate(new java.util.Date().getTime());
        }
        return LADM.Party.formatBirthDate(party.party.guardian.dateOfBirth);
    }

    public String gAddress() {
        if (!hasGuardian()) {
            return "";
        }
        LADM.PartyContact c = party.party.guardian.contact(LADM.PartyContact.CONTACT_TYPE_ADDRESS);
        if (c == null) {
            return "";
        }
        return c.contactData;
    }
    public String idRef()
    {
        return super.showDocumentText(party.idDoc);
    }
    public String pomRef(){
        return super.showDocumentText(party.proofOfMaritalStatus);
    }
    public String laterOfAutorizationRef(){
        return super.showDocumentText(party.letterOfAttorneyDocument);
    }
    
    public String rName1() {
        if (party.representative==null) {
            return "";
        }
        return party.representative.name1;
    }

    public String rName2() {
        if (party.representative==null) {
            return "";
        }
        return party.representative.name2;
    }

    public String rName3() {
        if (party.representative==null) {
            return "";
        }
        return party.representative.name3;
    }

    public int rSex() {
        if (party.representative==null) {
            return LADM.Party.SEX_MALE;
        }
        return party.representative.sex;
    }

    public String rBirthDate() {
        if (party.representative==null) {
            return LADM.Party.formatBirthDate(new java.util.Date().getTime());
        }
        return LADM.Party.formatBirthDate(party.representative.dateOfBirth);
    }

    public String rAddress() {
        if (party.representative==null) {
            return "";
        }
        LADM.PartyContact c = party.representative.contact(LADM.PartyContact.CONTACT_TYPE_ADDRESS);
        if (c == null) {
            return "";
        }
        return c.contactData;
    }
    public String rPhone() {
        if (party.representative==null) {
            return "";
        }
        LADM.PartyContact c = party.representative.contact(LADM.PartyContact.CONTACT_TYPE_OTHER);
        if (c == null) {
            return "";
        }
        return c.contactData;
    }
}
