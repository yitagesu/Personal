/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.controller.tran;

import com.intaps.nrlais.Startup;
import com.intaps.nrlais.model.IDName;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LADM.Parcel;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.PartyShare;
import com.intaps.nrlais.model.SourceDocument;
import com.intaps.nrlais.model.tran.Applicant;
import com.intaps.nrlais.model.tran.SimpleCorrectionData;
import com.intaps.nrlais.model.tran.ManualTransactionData;
import com.intaps.nrlais.model.tran.ParcelTransfer;
import com.intaps.nrlais.model.tran.Partition;
import com.intaps.nrlais.model.tran.PartyItem;
import com.intaps.nrlais.model.tran.PartyParcelShare;
import com.intaps.nrlais.repo.GetLADM;
import com.intaps.nrlais.repo.MainFacade;
import com.intaps.nrlais.repo.ManualLRManipulatorFacade;
import com.intaps.nrlais.repo.RepoBase;
import com.intaps.nrlais.repo.TransactionFacade;
import com.intaps.nrlais.util.GSONUtil;
import com.intaps.nrlais.util.INTAPSDateUtils;
import com.intaps.nrlais.util.INTAPSLangUtils;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author yitagesu
 */
/**
 *
 * @author yitagesu
 */
public class SimpleCorrectionViewController extends HoldingTransactionViewController<SimpleCorrectionData> {

    public SimpleCorrectionViewController(HttpServletRequest request, HttpServletResponse response, boolean loadData) throws Exception {
        super(request, response, SimpleCorrectionData.class, LATransaction.TRAN_TYPE_SIMPLE_CORRECTION, loadData);
        if (loadData) {
            if (data.holdingUID == null) {
                holding = new LADM.Holding();
            } else {
                if (tran.isCommited()) {
                    holding = mainFacade.getFromHistoryByArchiveTx(tran.transactionUID, data.holdingUID).holding;
                } else {
                    holding = mainFacade.getLADM("nrlais_inventory", data.holdingUID).holding;
                }

            }
            kebele = mainFacade.getKebele(holding.nrlais_kebeleid);
            woreda = mainFacade.getWoreda(holding.nrlais_woredaid);
        }
    }

    public Applicant applicant(String partyUID) {
        for (Applicant ap : this.data.applicant) {
            if (partyUID.equals(ap.selfPartyUID)) {
                return ap;
            }
        }
        return null;
    }

    public String letterOfAttroneyText(Applicant ap) {
        if (ap == null) {
            return "";
        }

        if (ap.letterOfAttorneyDocument == null) {
            return "";
        }
        return ap.letterOfAttorneyDocument.refText;
    }

    public String letterOfAttroneyLink(Applicant ap) {
        if (ap == null) {
            return "";
        }
        return showDocumentLink(ap.letterOfAttorneyDocument);
    }

    public String landHoldingCertificateText() {
        if (this.data.landHoldingCertifcateDocument == null) {
            return "";
        }
        return this.data.landHoldingCertifcateDocument.refText;
    }

    public String landHoldingCertificateLink() {
        return showDocumentLink(this.data.landHoldingCertifcateDocument);
    }

}
