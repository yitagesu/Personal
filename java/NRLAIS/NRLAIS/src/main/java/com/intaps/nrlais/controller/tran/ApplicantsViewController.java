/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.controller.tran;

import com.intaps.nrlais.controller.ViewControllerBase;
import com.google.gson.reflect.TypeToken;
import com.intaps.nrlais.Startup;
import com.intaps.nrlais.UserSession;
import com.intaps.nrlais.model.*;
import com.intaps.nrlais.model.LADM.*;
import com.intaps.nrlais.model.tran.Applicant;
import com.intaps.nrlais.repo.*;
import com.intaps.nrlais.util.GSONUtil;
import com.intaps.nrlais.util.RationalNum;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Tewelde
 */
public class ApplicantsViewController extends ViewControllerBase {
    public static class Settings
    {
        public boolean pickID=true;
        public boolean pickRepresentative=true;
        public boolean pickPOM=false;
        public boolean pickShare=false;
        public String shareLabel="";
        public boolean selectable=false;
        public boolean inheritance=false;
        
    }
    public String holdingID;
    public Holding holding;
    Applicant[] applicants=null;
    public Settings settings=new Settings();
   
    public ApplicantsViewController(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
        super(request,response);
        holdingID = request.getParameter("holding_uid");
        holding= mainFacade.getLADM("nrlais_inventory", holdingID, LADM.CONTENT_FULL).holding;
        if("POST".equals(request.getMethod()))
        {
           this.applicants=Startup.readPostedArrayObject(request, new TypeToken<Applicant>(){}.getType());
        }
        String json=request.getParameter("setting");
        if(json!=null)
            this.settings=GSONUtil.getAdapter(Settings.class).fromJson(json);
        
    }
    public Applicant applicant(String partyUID)
    {
        if(applicants==null)
            return null;
        for(Applicant ap:applicants)
            if(StringUtils.equals(ap.selfPartyUID,partyUID))
                return ap;
        return null;
    }
    public String repName(Applicant ap)
    {
        if(ap!=null && ap.representative!=null)
            return ap.representative.getFullName();
        return "";
    }
    public String repSex(Applicant ap) throws SQLException
    {
        if(ap!=null && ap.representative!=null)
        {
            return sexText(ap.representative.sex);
        }
        return "";
    }
    public String repAge(Applicant ap)
    {
        if(ap!=null && ap.representative!=null)
        {
            return Integer.toString(super.age(ap.representative.dateOfBirth));
        }
        return "";
    }
    public String idType(Applicant ap) throws SQLException
    {
        if(ap!=null && ap.idDocument!=null)
        {
            return adminSourceText(ap.idDocument.sourceType);
        }
        return "";
    }
    public String idRef(Applicant ap) throws SQLException
    {
        if(ap!=null && ap.idDocument!=null)
        {
            return ap.idDocument.refText;
        }
        return "";
    }
    
    public String pomRef(Applicant ap) throws SQLException
    {
        if(ap!=null && ap.pomDocument!=null)
        {
            return ap.pomDocument.refText;
        }
        return "";
    }
    public String pomText(Applicant ap) throws SQLException
    {
        if(ap!=null && ap.pomDocument!=null && ap.pomDocument.description!=null)
        {
            return ap.pomDocument.description;
        }
        return "";
    }
    public String share(Applicant ap) throws SQLException
    {
        if(ap!=null && ap.pomDocument!=null && ap.pomDocument.description!=null)
        {
            return ap.share.toString();
        }
        return "1";
    }
    public boolean selected(String partyUID)
    {
        return applicant(partyUID)!=null;
    }
}
