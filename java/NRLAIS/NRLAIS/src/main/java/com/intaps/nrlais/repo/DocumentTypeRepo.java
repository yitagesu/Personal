/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.repo;

import com.intaps.nrlais.UserSession;
import com.intaps.nrlais.model.DocumentTypeInfo;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Tewelde
 */
public class DocumentTypeRepo extends RepoBase {
    public static final int IDENTITY_DOCUMENT = 1;
    public static final int DIVORCE_EVIDENCE = 2;
    public static final int COURT_DECISION = 3;
    public static final int LETTER_OF_ATHORNEY = 4;

    public DocumentTypeRepo(UserSession session)
    {
        super(session);
    }
    public List<DocumentTypeInfo> getAllDocumentTypes(Connection con) throws SQLException
    {
       
            return super.loadIDDescriptionRecord(con, "nrlais_sys.t_cl_adminsourcetype",DocumentTypeInfo.class);
        
    }
    public DocumentTypeInfo getDocumentType(Connection con,int id) throws SQLException
    {
        return super.getIDName(con, "nrlais_sys.t_cl_adminsourcetype",DocumentTypeInfo.class,id);        
    }
}
