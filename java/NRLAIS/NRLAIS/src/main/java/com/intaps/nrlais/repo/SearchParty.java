/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.repo;

import com.intaps.nrlais.UserSession;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LADM.Party;
import com.intaps.nrlais.model.SearchHoldingPars;
import com.intaps.nrlais.model.SearchPartyPars;
import com.intaps.nrlais.util.INTAPSLangUtils;
import com.intaps.nrlais.util.ResourceUtils;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Tewelde
 */
public class SearchParty extends RepoBase {
    public SearchParty(UserSession session)
    {
        super(session);
    }
    String getSql(SearchPartyPars pars,boolean count) throws IOException
    {
        String cr=null;
        if(!StringUtils.isEmpty(pars.name1))
        {
            cr=INTAPSLangUtils.appendStringList(cr, " and ", "(t_party.name like  '"+pars.name1+"%' or gparty.name like '"+pars.name1+"')");
        }
        if(!StringUtils.isEmpty(pars.name2))
        {
            cr=INTAPSLangUtils.appendStringList(cr, " and ", "(t_party.fathersname like  '"+pars.name2+"%' or gparty.fathersname like '"+pars.name2+"')");
        }
        if(!StringUtils.isEmpty(pars.name3))
        {
            cr=INTAPSLangUtils.appendStringList(cr, " and ", "t_party.grandfathersname like  '"+pars.name3+"%' or gparty.grandfathersname like '"+pars.name3+"')");
        }
        if(!StringUtils.isEmpty(pars.kebele))
        {
            cr=INTAPSLangUtils.appendStringList(cr, " and ", "t_party.nrlais_kebeleid like  '"+pars.kebele+"%'");
        }
        if(!StringUtils.isEmpty(pars.idNumber))
        {
            cr=INTAPSLangUtils.appendStringList(cr, " and ", "t_party.extident like  '%"+pars.idNumber+"%'");
            if(pars.idType>0)
            {
                cr=INTAPSLangUtils.appendStringList(cr, " and ", "t_party.extidenttype="+pars.idType);
            }
        }

        String sql=ResourceUtils.getSql("search_party.sql");
        sql=StringUtils.replaceEach(sql, new String[]{"@select","@schema","@cr"}, new String[]{
            count?"count(distinct t_party.uid)":"distinct t_party.uid","nrlais_inventory",cr
        });
        return sql;
    }
    public int count(final Connection con,SearchPartyPars pars) throws SQLException, IOException
    {
        String sql=getSql(pars, true);
            try(ResultSet rs=con.prepareStatement(sql).executeQuery())
            {
                if(rs.next())
                    return rs.getInt(1);
                return 0;
            }
    }
    public List<Party> searchParty(final Connection con,SearchPartyPars pars,int pageOffset,int pageSize) throws IOException, SQLException
    {
       List<LADM.Party> ret=new ArrayList<>();
            String sql=getSql(pars, false)+" offset "+pageOffset+" limit "+pageSize;
            try(ResultSet rs=con.prepareStatement(sql).executeQuery())
            {
                while(rs.next())
                {
                    LADM.Party p=new GetLADM(super.session, "nrlais_inventory").getParty(con,rs.getString("uid"), LADM.CONTENT_FULL);
                    ret.add(p);
                }
            }
            return ret;
        
    }
}
