/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.controller.routes.api;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import com.intaps.nrlais.Startup;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.tran.DivorceTransactionData;
import com.intaps.nrlais.util.GSONUtil;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Teweldemedhin Aberra
 */
public abstract class APIBase extends HttpServlet {

    protected String assertLogedInUser(HttpServletRequest req, HttpServletResponse resp) throws IOException, IllegalAccessException {
        return Startup.getSessionByRequest(req).userName;
    }
    HashMap<String,String> getLowerCasePars(HttpServletRequest req)
    {
        HashMap<String,String> ret=new HashMap<>();
        Enumeration<String> pars=req.getParameterNames();
        while(pars.hasMoreElements())
        {
            String par=pars.nextElement();
            ret.put(par.toLowerCase(),req.getParameter(par));
        }
        return ret;
    }
}
