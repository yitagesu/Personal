/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.controller.routes.api;

import com.google.gson.stream.JsonWriter;
import com.intaps.nrlais.Startup;
import com.intaps.nrlais.util.GSONUtil;
import com.intaps.nrlais.worlais.WORLAISClient;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Teweldemedhin Aberra
 */
@WebServlet("/api/login")
public class APILogin extends APIBase {

    public static class UserNamePasword {

        public String userName;
        public String password;
        public String lang="en-us";
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        GSONUtil.StringRet ret = new GSONUtil.StringRet();
        try {
            String lo = req.getParameter("logout");
            if (!org.apache.commons.lang3.StringUtils.isEmpty(lo)) {
                Startup.removeSession(req);
            } else {
                UserNamePasword up= Startup.readPostedObject(req, UserNamePasword.class);
                
                WORLAISClient w = new WORLAISClient();
                String sessionID = w.login(up.userName, up.password);
                Startup.addSession(req, up.userName,up.lang, w);
                ret.res = sessionID;
                ret.error = null;
            }
        } catch (Exception ex) {
            Logger.getLogger(APILogin.class.getName()).log(Level.SEVERE, null, ex);
            ret.error = "Error: " + (ex.getMessage() == null ? "" : ex.getMessage());
            ret.res = null;
        }

        try (ServletOutputStream str = resp.getOutputStream()) {
            resp.setContentType("application/json");
            JsonWriter writer = new JsonWriter(new OutputStreamWriter(str, "UTF-8"));
            GSONUtil.getAdapter(GSONUtil.StringRet.class).write(writer, ret);
            writer.close();
        }
    }
}
