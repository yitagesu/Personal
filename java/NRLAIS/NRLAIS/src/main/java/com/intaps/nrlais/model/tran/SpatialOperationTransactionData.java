/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.model.tran;

import com.intaps.nrlais.model.LATransaction;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Tewelde
 */
public class SpatialOperationTransactionData extends LATransaction.TransactionData {
    public static class HoldingParcel
    {
        public String holdingUID;
        public String parcelUID;
    }
    public HoldingParcel parcel=null;
    public ArrayList<HoldingParcel> adjucentParcels=new ArrayList<HoldingParcel>();
    public List<Applicant> applicants=new ArrayList<Applicant>();
}
