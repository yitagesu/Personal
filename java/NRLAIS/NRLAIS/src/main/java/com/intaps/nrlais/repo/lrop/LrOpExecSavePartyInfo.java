/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.repo.lrop;

import com.intaps.nrlais.UserSession;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.tran.ManualTransactionData;
import com.intaps.nrlais.repo.GetLADM;
import com.intaps.nrlais.repo.LRManipulator;
import com.intaps.nrlais.repo.ManualLRManipulatorFacade;
import java.io.IOException;
import java.sql.Connection;

/**
 *
 * @author Tewelde
 */
public class LrOpExecSavePartyInfo implements ManualLRManipulatorFacade.LROPExecutor<ManualTransactionData.LrOpSavePartyInfo> {

    @Override
    public Object doOperation(UserSession session, Connection con, LATransaction tran, ManualTransactionData.LrOpSavePartyInfo op) throws Exception {
        LRManipulator lrm = new LRManipulator(session);
        if(op.party.partyUID==null)
            lrm.createParty(con, tran, op.party);
        else
            lrm.updatePartyRecord(con,tran,op.party);
        return op.party.partyUID;
    }

}
