/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.camisweb.vmcontroller;
    import com.intaps.camisweb.viewmodel.ProjectInfo;
import java.util.ArrayList;
import java.util.Date;
/**
 *
 * @author user
 */
public class Project {
    public static ProjectInfo.project getproject(){
        ProjectInfo.project pr = new ProjectInfo.project();
        pr.pm = new ArrayList();
        
        ProjectInfo.project.project_mgmt lbv = new ProjectInfo.project.project_mgmt();
        lbv.project_id= 001;
        lbv.project_name="Agro Project";
        lbv.project_type="Agricultural";
        lbv.project_location="Tigray";
        lbv.zone="04";
        lbv.woreda="12";
        lbv.kebele="06";
        lbv.city="mekele";
        lbv.landmark="mountain"; 
        lbv.perquisite_activites="none";
        lbv.briefdesc_work="plant and distribute fruits all over the country";
        lbv.briefdesc_psubactivites="fruits such as bananas oranges and the like will be planted.";
        lbv.start_date="Jan/12/2018";
        lbv.end_date="Jan/12/2019";
        lbv.milestones="Aug/12/2018";
        
        pr.pm.add(lbv);
        return pr;
     }
}
