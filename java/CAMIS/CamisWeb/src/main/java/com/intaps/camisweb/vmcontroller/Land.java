package com.intaps.camisweb.vmcontroller;
import com.intaps.camisweb.viewmodel.LandInfo;
import java.util.ArrayList;

public class Land {
    public static LandInfo.LandBank getLandValue(){
        LandInfo.LandBank lv = new LandInfo.LandBank();
        lv.values = new ArrayList();
        
        LandInfo.LandBank.LandValue lbv = new LandInfo.LandBank.LandValue();
        lbv.investorName = "Debebe Dawit";
        lbv.upi = "010601004/00221";
        lbv.location = "Gambela";
        lbv.area = 4322.54;
        lbv.status = true;
        lv.values.add(lbv);
        return lv;
    }
    public static LandInfo.CommInvestor getCommInvestor(){
        LandInfo.CommInvestor lv = new LandInfo.CommInvestor();
        lv.values = new ArrayList();
        
        LandInfo.CommInvestor.CommInverstorData lbv = new LandInfo.CommInvestor.CommInverstorData();
        lbv.investorName = "Ermias Surafeal";
        lbv.area = 455555.32;
        lbv.capital = 10000000;
        lv.values.add(lbv);
        return lv;
    }
    public static LandInfo.Investor getInvestorData(){
    LandInfo.Investor lv = new LandInfo.Investor();
    lv.data = new ArrayList();
    
    LandInfo.Investor.InvestorData lbv  = new LandInfo.Investor.InvestorData();
    lbv.inverstorName ="Ermias Surafeal";
    lbv.area = 45555.54;
    
    lv.data.add(lbv);
    return lv;
}
    public static LandInfo.LandBankDashBoard getLandBankDashBoardData(){
        LandInfo.LandBankDashBoard lv = new LandInfo.LandBankDashBoard();
        lv.data = new ArrayList();
        LandInfo.LandBankDashBoard.LandBankDashBoardData lbv = new LandInfo.LandBankDashBoard.LandBankDashBoardData();
         lbv.upi = "010601004/00221";
         lbv.investmentType = "Horticulture";
          lbv.area = 45555.54;
           lv.data.add(lbv);
           
           return lv;
    }
    public static LandInfo.Evaluator getEvaluatorData(){
        LandInfo.Evaluator lv = new LandInfo.Evaluator();
        lv.data = new ArrayList();
        LandInfo.Evaluator.EvaluatorData lbv = new LandInfo.Evaluator.EvaluatorData();
        lbv.evaluatorName  ="Esayas Kebede";
        lbv.evaluatorEmailAddress="esayasman2010@gmail.com";
        lbv.evaluatorPhoneNumber =3233;
       lv.data.add(lbv);
        return lv;
    }
    public static LandInfo.LandTransferProcess getLandTransferProcess(){
        LandInfo.LandTransferProcess lv = new LandInfo.LandTransferProcess();
        lv.data = new ArrayList();
        LandInfo.LandTransferProcess.LandTransferProcessData lbv = new LandInfo.LandTransferProcess.LandTransferProcessData();
        lbv.landId = "GA0045";
        lbv.landType = "Cultivation";
        lbv.area = 3455.455;
        lbv.status ="Bid Process";
        lv.data.add(lbv);
        return lv;
                
    }
    
        public static LandInfo.LandBankProfile getLandBankProfile(){
         LandInfo.LandBankProfile lv = new LandInfo.LandBankProfile();
        lv.data = new ArrayList();
        LandInfo.LandBankProfile.LandBankProfileData lbv = new LandInfo.LandBankProfile.LandBankProfileData();
        lbv.parcelId = "070106001/00001";
        lbv.address="Gurage zone,Meseretewegeramo";
        lbv.area = 566.43;
        lbv.investmentType = "Horticulture";
        lbv.landUse = "Cultivation";
        
        LandInfo.LandBankProfile.LandBankProfileData lbv1 = new LandInfo.LandBankProfile.LandBankProfileData();
        lbv1.parcelId = "070106001/00002";
        lbv1.address="Gurage zone,Meseretewegeramo";
        lbv1.area = 78999.43;
        lbv1.investmentType = "Api-culture";
        lbv1.landUse = "Cultivation";
        
         LandInfo.LandBankProfile.LandBankProfileData lbv2 = new LandInfo.LandBankProfile.LandBankProfileData();
        lbv2.parcelId = "070106001/00003";
        lbv2.address="Gurage zone,Meseretewegeramo";
        lbv2.area = 23345.43;
        lbv2.investmentType = "Livestock";
        lbv2.landUse = "Cultivation";
        
         LandInfo.LandBankProfile.LandBankProfileData lbv3 = new LandInfo.LandBankProfile.LandBankProfileData();
        lbv3.parcelId = "070106001/00004";
        lbv3.address="Meseretewegeramo";
        lbv3.area = 12.222;
        lbv3.investmentType = "Large scale farming";
        lbv3.landUse = "Cultivation";
        
         LandInfo.LandBankProfile.LandBankProfileData lbv4 = new LandInfo.LandBankProfile.LandBankProfileData();
        lbv4.parcelId = "070106001/00005";
        lbv4.address="Gurage zone,Meseretewegeramo";
        lbv4.area = 456788.21;
        lbv4.investmentType = "Large scale farming";
        lbv4.landUse = "Cultivation";
        
        LandInfo.LandBankProfile.LandBankProfileData lbv5 = new LandInfo.LandBankProfile.LandBankProfileData();
        lbv5.parcelId = "070106001/00006";
        lbv5.address="Gurage zone,Meseretewegeramo";
        lbv5.area = 123449;
        lbv5.investmentType = "Large scale farming";
        lbv5.landUse = "Cultivation";
        
        LandInfo.LandBankProfile.LandBankProfileData lbv6 = new LandInfo.LandBankProfile.LandBankProfileData();
        lbv6.parcelId = "070106001/00007";
        lbv6.address="Gurage zone,Meseretewegeramo";
        lbv6.area = 1454353.11;
        lbv6.investmentType = "Horticulture";
        lbv6.landUse = "Cultivation";

        LandInfo.LandBankProfile.LandBankProfileData lbv7 = new LandInfo.LandBankProfile.LandBankProfileData();
        lbv7.parcelId = "070106001/00008";
        lbv7.address="Gurage zone,Meseretewegeramo";
        lbv7.area = 10000.99;
        lbv7.investmentType = "api-culture";
        lbv7.landUse = "Cultivation";
        
        lv.data.add(lbv);
        lv.data.add(lbv1);
        lv.data.add(lbv2);
        lv.data.add(lbv3);
        lv.data.add(lbv4);
        lv.data.add(lbv5);
        lv.data.add(lbv6);
        lv.data.add(lbv7);
        
        return lv;
    }
        public static LandInfo.LandBankProfile getLandBankProfileData(){
        LandInfo.LandBankProfile lv = new LandInfo.LandBankProfile();
        lv.data = new ArrayList();
        LandInfo.LandBankProfile.LandBankProfileData lbv = new LandInfo.LandBankProfile.LandBankProfileData();
        lbv.parcelId = "070106001/00001";
        lbv.kebele = "Meseretewegeramo";
        lbv.woreda="Meskan";
        lbv.zone="Gurage";
        lbv.region="Southern People";
        lbv.city="Meseretewegeramo";
        lbv.address="Gurage zone,Meseretewegeramo";
        lbv.centroid = "2.2";
        lbv.xCordinate =8.062945;
        lbv.yCordinate=37.974300;
        lbv.area = 566.43;
        lbv.investmentType = "Horticulture";
        lbv.landUse = "Cultivation";
        lbv.statusInfraRoadElec = "Yes";
        lbv.statusInfraTele = "Yes";
        lbv.statusInfraHealth="No";
        lbv.statusInfraEducation = "No";
        lbv.statusInfraAdmin ="Yes";
        lbv.conditionExpansion = "Great";
        lbv.socioEconomy="Great";
        lbv.accessibilityAir = "Yes";
        lbv.accessibilityWater = "No";
        lbv.accessibilityRoad = "Yes";
        lbv.accessibilityRail ="No";
        lbv.distance = 455555.45;
        lbv.ageroEcology = "Great";
        lbv.surUnderWaterPote = "Great";
        lbv.waterPote = "Great";
        lbv.leasePerHectare =34555.54;
        lbv.leaseType ="Governmetal";
        lbv.leaseDuration = "25 years";
        lbv.neighrbourFromNorth = "Meskan1";
        lbv.neighrbourFromSouth="Meskan2";
        lbv.neighrbourFromEast="Meskan3";
        lbv.neighrbourFromWest="Meskan4";
        lbv.height = 3455.55;
        lbv.windDirecSpeed = "from north to south";
        lbv.rainfallDist ="Great";
        lbv.humudity ="Great";
        lbv.coverOfHerbs="Great";
        lbv.landscape ="Great";
        lbv.others="Great";

        lv.data.add(lbv);
        return lv;
}

    
    
    
    public static LandInfo.AuthenticationForm getAuthenticationForm(String companyName){
        LandInfo.AuthenticationForm af = new LandInfo.AuthenticationForm();
        af.data = new ArrayList<>();
        LandInfo.AuthenticationForm.Form aff = new LandInfo.AuthenticationForm.Form();
        aff.companyName = "INTAPS";
        aff.east = "To east";
        aff.email ="info@intaps.com";
        aff.email = "35454";
        aff.kebele = "07";
        aff.landBelongs = "Oromia";
        aff.landLeaseRate =0.6;
        aff.landNeigbours = "Ambo Hotel";
        aff.landPerHec = 3.0;
        aff.leaseContractDetail ="details of the contract";
        aff.north = "to North";
        aff.phone = "25456456";
        aff.south = "to south";
        aff.west = " to west";
        aff.woreda = "11";
        aff.zone = "Addis Ababa";
        aff.region = "Addis Ababa";
        af.data.add(aff);
        return af;
        
    }
}
