/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.camisweb;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import org.apache.catalina.WebResourceRoot;
import org.apache.catalina.core.StandardContext;
import org.apache.catalina.startup.Tomcat;
import org.apache.catalina.webresources.DirResourceSet;
import org.apache.catalina.webresources.StandardRoot;

/**
 *
 * @author Tewelde
 */
public class Startup {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            System.out.println("Starting Tomcat embeded server");
            Tomcat tomcat = new Tomcat();

            //configure temp directory temporary java classes Tomcat generates
            Path tempPath = Files.createTempDirectory("tomcat-base-dir");
            System.out.println("Tomcat-base-dir: " + tempPath.toString());
            tomcat.setBaseDir(tempPath.toString());

            //configure listening port for Tomcat
            tomcat.setPort(8086);

            //configure web content folder
            File webContentFolder = new File("res/web");
            if (!webContentFolder.exists()) {
                throw new IllegalStateException(webContentFolder.getAbsolutePath() + " not found");
            }

            StandardContext ctx = (StandardContext) tomcat.addWebapp("", webContentFolder.getAbsolutePath());

            //Set execution independent of current thread context classloader (compatibility with exec:java mojo)
            ctx.setParentClassLoader(Startup.class.getClassLoader());

            //declare an alternate location for your "WEB-INF/classes" dir:     
            File additionWebInfClasses = new File("target/classes");
            WebResourceRoot resources = new StandardRoot(ctx);
            resources.addPreResources(new DirResourceSet(resources, "/WEB-INF/classes", additionWebInfClasses.getAbsolutePath(), "/"));
            ctx.setResources(resources);

            tomcat.start();
            tomcat.getServer().await();
        } catch (Exception ex) {
            System.out.println("CAMIS Webapp Crashed");
            ex.printStackTrace();
        }
    }

}
