/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.camisweb.model;

/**
 *
 * @author user
 */
public class FinancialInformation {
    private String budgetCode;
    private String accountName;
    private double budgetAmount;
    
    public FinancialInformation(String name,double amount)
    {
       setAccountName(name);
       setBudgetAmount(amount);
       
    }

    /**
     * @return the budgetCode
     */
    public String getBudgetCode() {
        return budgetCode;
    }

    /**
     * @param budgetCode the budgetCode to set
     */
    public void setBudgetCode(String budgetCode) {
        this.budgetCode = budgetCode;
    }

    /**
     * @return the accountName
     */
    public String getAccountName() {
        return accountName;
    }

    /**
     * @param accountName the accountName to set
     */
    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    /**
     * @return the budgetAmount
     */
    public double getBudgetAmount() {
        return budgetAmount;
    }

    /**
     * @param budgetAmount the budgetAmount to set
     */
    public void setBudgetAmount(double budgetAmount) {
        this.budgetAmount = budgetAmount;
    }
}
