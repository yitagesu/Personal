
<!DOCTYPE html>
<%@page import="com.intaps.camisweb.*"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>CAMIS</title>
        <link rel="stylesheet" href="/assets/css/external.css"/>
        <link rel="stylesheet" href="/assets/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="/assets/css/custom.min.css"/>
        <link rel="stylesheet" href="/assets/css/popup.css"/>
            </head>
    <body class="nav-sm">
        
        <div class="container body">
            <div class="main_container">

           <%@include file="all.jsp" %>
                <div class="right_col tab-content" role="main"  id ="content">
                    <div>
                        <div class="row">
                           
                            
                            
                            
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="x_panel" id="content">
                                    <h2>Land Profile</h2>
                                     
                                      <div class="col-md-8">
                                    <div class="form-group">
                                        <label for="sel1"><strong>Zone:</strong></label>
                                        <select class="form-control" id="sel1">
                                            <option>Zone 1</option>
                                            <option>Zone 2</option>
                                            <option>Zone 3</option>
                                            <option>Zone 4</option>
                                        </select>
                                    </div>
                                       <div class="form-group" >
                                           <label><strong>Woreda:</strong></label>
                                               <select class="form-control">
                                                        <option>Woreda 1</option>
                                                        <option>Woreda 2</option>
                                                        <option>Woreda 3</option>
                                                         <option>Woreda 4</option>
                                            </select>
                                            </div>
                                      <div class="form-group">
                                          <label><strong>Kebele:</strong></label>
                                              <select class="form-control">
                                                        <option>Kebele 1</option>
                                                        <option>Kebele 2</option>
                                                        <option>Kebele 3</option>
                                                        <option>Kebele 4</option>
                                            </select>
                                          </div>
                                            </div>
                                        <div class="form-group align-right col-md-12">
                                                <button id="search_button" class="btn btn-success "><i class="fa fa-search"></i> Search</button>
                                            </div>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                            <table class="table table-striped jambo_table bulk_action" id="tclick">
                                                <thead>
                                                    <tr class="bg-primary">
                                                        <th class="column-title">UPID</th>
                                                        <th class="column-title">Investment Type</th>
                                                        <th class="column-title">Area</th>
                                                        <th class="column-title">Land Use</th>
                                                         <th class="column-title">Address</th>
                                                       
                                                       
                                                    </tr>
                                                </thead>
  <%
                                                LandInfo.LandBankProfile retLandPro = Land.getLandBankProfile();
                                                for(LandInfo.LandBankProfile.LandBankProfileData val: retLandPro.data){
                                                %>
                                                <tbody>
                                                    <tr class="even pointer">
                                                          <td class=" "><%=val.parcelId%></td>
                                                        <td class=" "><%=val.investmentType%></td>
                                                        <td class=" "><%=val.area%></td>
                                                        <td class=" "><%=val.landUse%></td>
                                                         <td><%=val.address%></td>
                                                    </tr>
                                                  <%}%>
                                                </tbody>
                                            </table>
                                       
                                   
                                </div>
                            </div>
                                                
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>Thematic</h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                            </li>
                                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>

                                    <div class="x_content">
                                        <div class="map has-scroll" id="map">
                                            <img src="assets/images/map.jpg" class="img-responsive"/>
                                        </div>
                                    </div
                                    
                                </div>
                            </div>
                                            
                        </div>
                               
                                                
                                                
                                                
                    </div>
                   
                </div>
            </div>
            </div>
         </div>
            <script src="/assets/js/jquery.min.js"></script>
        <script src="/assets/js/external.js"></script>
        <script src="/assets/js/Char.min.js"></script>
        <script src="/assets/js/bootstrap-progressbar.min.js"></script>
        <script src="/assets/js/bootstrap.min.js"></script>
        <script src="/assets/js/custom.min.js"></script>
        <script>
            
          $('#tclick').on('click', 'tr', function() {
              var currentRow = $(this).closest("tr"); 
               var comapany_name=currentRow.find("td:eq(1)").html();
              
                    $.ajax({
                        url: '/land_profile_data1.jsp',
                        data: {'cname':comapany_name},
                        dataType: 'xml',
                        complete : function(){
                           // var stateObj = { foo: "bar" };
                            //history.pushState(stateObj, "page 2", this.url);
                            window.location.href = this.url;
                            
                            
                        },
                        success: function(xml){
                        }
                    });
          // 
            });  
            
        </script>
    </body>
</html>
