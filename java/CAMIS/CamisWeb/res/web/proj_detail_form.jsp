<%-- 
    Document   : proj_detail_form
    Created on : Jan 22, 2018, 11:20:53 AM
    Author     : user
--%>

<!DOCTYPE html>
<%@page import="com.intaps.camisweb.*"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>CAMIS</title>
        <link rel="stylesheet" href="/assets/css/external.css"/>
        <link rel="stylesheet" href="/assets/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="/assets/css/custom.min.css"/>
        <link rel="stylesheet" href="/assets/css/popup.css"/>
        <link rel="stylesheet" href="/assets/css/jquery-ui.css"/>
        <script src="/assets/js/dhtmlxscheduler.js"></script>
        <link rel="stylesheet" href="/assets/css/dhtmlxscheduler.css"/>
        <style>
            .form-control{
                border-radius: 4px; 
            }
            
                    .file-field.medium .file-path-wrapper {
  height: 3rem; }
  .file-field.medium .file-path-wrapper .file-path {
    height: 2.8rem; }

.file-field.big-2 .file-path-wrapper {
  height: 3.7rem; }
  .file-field.big-2 .file-path-wrapper .file-path {
    height: 3.5rem; }
                
            
            
        </style>
            </head>
    <body class="nav-sm">
        
        <div class="container body">
            <div class="main_container">

           <%@include file="all.jsp" %>
           
               <div class="right_col tab-content" role="main">
                    
                        <div class="row" >
                             <div class="col-md-12 col-sm-12 col-xs-12" > 
                            
                             <div id="tabs">
                                <ul>
                                <li><a href="#fragment-1"><span>Project Detail</span></a></li>
                                <li><a href="#fragment-2"><span>Project Financial Information</span></a></li>
                                <li><a href="#fragment-3"><span>Project HR Requirement</span></a></li>
                                <li><a href="#fragment-4"><span>Project Inventory Information</span></a></li>
                                <li><a href="#fragment-5"><span>Project Files</span></a></li>
                                <li><a href="#fragment-6"><span>Project Schedule</span></a></li>
                              </ul>
                                   <form action="projectDetails" class="form-horizontal" role="form" method="post">
                   <div id="fragment-1">
          
               <p><i>${message}</i></p>
         
                 
            
          
                 
                <div class ="row">
              <div class="col-md-6">
              <div class="form-group">
                  
            <input type="hidden" name="action" value="add" >
            
            <label for="id" class="col-lg-2 control-label col-sm-2 col-md-2 col-xs-2">Project ID</label>
            <div class="col-lg-8 col-sm-2 col-md-8 col-xs-2">
            <input class="form-control"  type="text" id = "id" name="id" value ="${project.id}" placeholder="Project Id"/>
            </div>
                </div>
         
            <div class="form-group">
            <label for="pname" class="col-lg-2 control-label col-sm-2 col-md-2 col-xs-2">Project Name</label>
              <div class="col-lg-8 col-sm-2 col-md-8 ol-xs-2">  
            <input class="form-control" maxlength="255" type="text" id ="pname" name="name" value ="${project.name}" placeholder="Project Name"/>
              </div>
    </div>
            <div class="form-group">
            <label for="ptype"  class="col-lg-2 control-label col-sm-2 col-md-2 col-xs-2">Project Type</label>
            <div class="col-lg-8 col-sm-2 col-md-8 ol-xs-2"> 
            <input class="form-control" type="text" id ="ptype" name="type" value ="${project.type}"  placeholder="Project Type"/>
            </div>
            </div>
            <div lass="form-group">
             <label for="plocation" class="col-lg-2 control-label col-sm-2 col-md-2 col-xs-2">Project Location</label>
             <div class="col-lg-8 col-sm-2 col-md-8 ol-xs-2">
            <input class="form-control" type="text" id ="plocation" name="location" value ="${project.location}" placeholder="Project Location"/>
            </div>
            </div>
                    </div>
            <div class ="col-md-6"> 
            <div class="form-group">
            <label for="zone" class="col-lg-2 control-label col-sm-2 col-md-2 col-xs-2">Zone</label>
            <div class="col-lg-8 col-sm-2 col-md-8 ol-xs-2">
                <select class="form-control" id ="zone" name="zone">
                    <option>Zone_1</option>
                    <option>Zone_2</option>
                    <option>Zone_3</option>
                </select>
            </div>
                </div>
                 <div class="form-group">
             <label for="woreda" class="col-lg-2 control-label col-sm-2 col-md-2 col-xs-2">Woreda</label>
             <div class="col-lg-8 col-sm-2 col-md-8 ol-xs-2">
                <select class="form-control" id ="woreda" name="woreda">
                    <option>woreda_1</option>
                    <option>woreda_2</option>
                    <option>woreda_3</option>
                </select>
                 </div>
                 </div>
                 <div class="form-group">
              <label for="keb" class="col-lg-2 control-label col-sm-2 col-md-2 col-xs-2">Kebele</label>
              <div class="col-lg-8 col-sm-2 col-md-8 ol-xs-2">
                <select  class="form-control" id="keb" name ="kebele">
                    <option>kebele_1</option>
                    <option>kebele_2</option>
                    <option>kebele_3</option>
                </select></div>
                 </div>
             <div class="form-group">
               <label for="city" class="col-lg-2 control-label col-sm-2 col-md-2 col-xs-2">City</label>
               <div class="col-lg-8 col-sm-2 col-md-8 ol-xs-2">
                <select class="form-control" id="city" name ="city">
                    <option>city_1</option>
                    <option>city_2</option>
                    <option>city_3</option>
                </select>
             </div>
             </div>
                 <div class="form-group">
                    <label for="nei" class="col-lg-2 control-label col-sm-2 col-md-2 col-xs-2">Neighborhood Landmark Feature</label>
                    <div class="col-lg-8 col-sm-2 col-md-8 ol-xs-2">
                <select class="form-control" id="nei"name ="neighborhood">
                    <option>Mountain</option>
                    <option>River</option>
                    <option>Lake</option>
                </select>
                </div>
                 </div>


            </div>
                 </div>
            
              
      
                <div class="col-md-12 col-sm-12 col-xs-12" > 
                <div class="form-row align-items-center">
           <div class="form-group">
            <label for="prereq">prerequisite activities:</label>
            <textarea class="form-control"  name="prerequisite" id ="prereq" value = "${project.prerequisite}" rows="8"></textarea>
           </div>
           <div class="form-group">
               <label for="dis">Brief Description of works:</label>
            <textarea class="form-control" name="description" id ="dis "value ="${project.description}" rows="8"></textarea>
           </div>
           <div class="form-group">
            <label for="activities">Brief Description of Project Sub Activities:</label>
            <textarea class="form-control" name="sub_activities" id ="activities" value ="${project.subActivityDescription}" rows="8"></textarea>
           </div>
            </div>
            </div>
                      

                   </div>
           
           <div id="fragment-2">
             

               <fieldset>
                   <legend>Budget Information </legend>
             <div class="form-group">
            <label for="aname" class="col-lg-2 control-label col-sm-2 col-md-2 col-xs-2">Account Name</label>
             <div class="col-lg-3 col-sm-2 col-md-3 ol-xs-2">
            <input class="form-control" type="text" id = "aname" name="account_name" value ="${finance.accountName}" placeholder="Account Name"/>
            </div>
             </div>
            <div class="form-group">
            <label for="amount" class="col-lg-2 control-label col-sm-2 col-md-2 col-xs-2">Budget Amount</label>
             <div class="col-lg-3 col-sm-2 col-md-3 ol-xs-2">
            <input class="form-control" type="text" id = "amount" name="budget_amount" value ="${finance.budgetAmount}" placeholder="Budget Amount"/>
            </div>  

           </div>
               </fieldset>
            <fieldset>
                <legend>Project Bank Account</legend>
                    <div class="form-group">
            <label for="bname" class="col-lg-2 control-label col-sm-2 col-md-2 col-xs-2">Bank Name</label>
             <div class="col-lg-3 col-sm-2 col-md-3 ol-xs-2">
            <input class="form-control" type="text" id = "bname" name="bank_name" value ="${finance.accountName}" placeholder="Bank Name"/>
            </div>
             </div>
            <div class="form-group">
            <label for="bbranch" class="col-lg-2 control-label col-sm-2 col-md-2 col-xs-2">Bank Branch</label>
             <div class="col-lg-3 col-sm-2 col-md-3 ol-xs-2">
            <input class="form-control" type="text" id = "bbranch" name="bank_branch" value ="${finance.accountName}" placeholder="Bank Branch"/>
            </div>
             </div>
            
            <div class="form-group">
            <label for="account_no" class="col-lg-2 control-label col-sm-2 col-md-2 col-xs-2">Bank Account Number</label>
             <div class="col-lg-3 col-sm-2 col-md-3 ol-xs-2">
            <input class="form-control" type="text" id = "account_no" name="account_number" value ="${finance.accountName}" placeholder="Bank Account Number"/>
            </div>
             </div>
                
                
            </fieldset>
            <fieldset>
                <legend>Project Sponsor</legend>
               <div class="form-group">
            <label for="sponsor_name" class="col-lg-2 control-label col-sm-2 col-md-2 col-xs-2">Sponsor Name</label>
             <div class="col-lg-3 col-sm-2 col-md-3 ol-xs-2">
            <input class="form-control" type="text" id = "sponsor_name" name="sponsor_name" value ="${finance.accountName}" placeholder="Sponsor Name"/>
            </div>
             </div>
                     <div class="form-group">
                    <label for="sponsor_type" class="col-lg-2 control-label col-sm-2 col-md-2 col-xs-2">Sponsor Type</label>
                    <div class="col-lg-3 col-sm-2 col-md-3 ol-xs-2">
                <select class="form-control" id="sponsor_type"name ="sponsor_type">
                    <option>Financial Sponsors</option>
                    <option>In-Kind Sponsors</option>
                    <option>Media Sponsors</option>
                </select>
                </div>
                 </div>
            
               <div class="form-group">
            <label for="quantity" class="col-lg-2 control-label col-sm-2 col-md-2 col-xs-2">Quantity</label>
             <div class="col-lg-3 col-sm-2 col-md-3 ol-xs-2">
            <input class="form-control" type="text" id = "quantity" name="quantity" value ="${finance.accountName}" placeholder="Quantity"/>
            </div>
             </div>
            
            <div class="form-group">
            <label for="amount_birr" class="col-lg-2 control-label col-sm-2 col-md-2 col-xs-2">Amount In Birr</label>
             <div class="col-lg-3 col-sm-2 col-md-3 ol-xs-2">
            <input class="form-control" type="text" id = "amount_birr" name="amount_birr" value ="${finance.accountName}" placeholder="Amount In Birr"/>
            </div>
             </div>
                
            </fieldset>
           </div>
            
            <div id="fragment-3">
                <p></p>
             <div class="form-group">
            <label for="emp_id" class="col-lg-2 control-label col-sm-2 col-md-2 col-xs-2">Employee ID</label>
             <div class="col-lg-3 col-sm-2 col-md-3 ol-xs-2">
            <input class="form-control" type="text" id = "emp_id" name="emp_id" value ="${finance.accountName}" placeholder="Employee ID"/>
            </div>
             </div>
            
            <div class="form-group">
            <label for="full_name" class="col-lg-2 control-label col-sm-2 col-md-2 col-xs-2">Full Name</label>
             <div class="col-lg-3 col-sm-2 col-md-3 ol-xs-2">
            <input class="form-control" type="text" id = "full_name" name="full_name" value ="${finance.accountName}" placeholder="Full Name"/>
            </div>
             </div>
            
                <div class="form-group">
            <label  class="col-lg-2 control-label col-sm-2 col-md-2 col-xs-2">Gender</label>
            <label class="radio-inline"><input type="radio" name="optradio">Male</label>
            <label class="radio-inline"><input type="radio" name="optradio">Female</label>
             </div>
                 <div class="form-group">
                    <label for="dept" class="col-lg-2 control-label col-sm-2 col-md-2 col-xs-2">Department</label>
                    <div class="col-lg-3 col-sm-2 col-md-3 ol-xs-2">
                <select class="form-control" id="dept"name ="dept">
                    <option>Department 1</option>
                    <option>Department 2</option>
                    <option>Department 3</option>
                </select>
                </div>
                 </div>
            
               <div class="form-group">
            <label for="specialization" class="col-lg-2 control-label col-sm-2 col-md-2 col-xs-2">Specialization</label>
             <div class="col-lg-3 col-sm-2 col-md-3 ol-xs-2">
            <input class="form-control" type="text" id = "specialization" name="specialization" value ="${finance.accountName}" placeholder="Specialization"/>
            </div>
             </div>
            <div class="form-group">
            <label for="task" class="col-lg-2 control-label col-sm-2 col-md-2 col-xs-2">Task Assignment</label>
             <div class="col-lg-3 col-sm-2 col-md-3 ol-xs-2">
            <input class="form-control" type="text" id = "task" name="task" value ="${finance.accountName}" placeholder="Task Assignment"/>
            </div>
             </div>

            </div>
            <div id="fragment-4">
                <p></p>
              <div class="form-group">
            <label for="item_code" class="col-lg-2 control-label col-sm-2 col-md-2 col-xs-2">Item Code</label>
             <div class="col-lg-3 col-sm-2 col-md-3 ol-xs-2">
            <input class="form-control" type="text" id = "item_code" name="item_code" value ="${finance.accountName}" placeholder="Item Code"/>
            </div>
             </div>
            
               <div class="form-group">
            <label for="item_name" class="col-lg-2 control-label col-sm-2 col-md-2 col-xs-2">Item Name</label>
             <div class="col-lg-3 col-sm-2 col-md-3 ol-xs-2">
            <input class="form-control" type="text" id = "item_code" name="item_name" value ="${finance.accountName}" placeholder="Item Name"/>
            </div>
             </div>
                  <div class="form-group">
                    <label for="item_type" class="col-lg-2 control-label col-sm-2 col-md-2 col-xs-2">Item Type</label>
                    <div class="col-lg-3 col-sm-2 col-md-3 ol-xs-2">
                <select class="form-control" id="item_type"name ="item_type">
                    <option>Item 1</option>
                    <option>Item 2</option>
                    <option>Item 3</option>
                </select>
                </div>
                 </div>
            
            <div class="form-group">
                    <label for="item_state" class="col-lg-2 control-label col-sm-2 col-md-2 col-xs-2">Item State</label>
                    <div class="col-lg-3 col-sm-2 col-md-3 ol-xs-2">
                <select class="form-control" id="item_state"name ="item_state">
                    <option>Type 1</option>
                    <option>Type 2</option>
                    <option>Type 3</option>
                </select>
                </div>
                 </div>
            <div class="form-group">
            <label for="quantity" class="col-lg-2 control-label col-sm-2 col-md-2 col-xs-2">Quantity</label>
             <div class="col-lg-3 col-sm-2 col-md-3 ol-xs-2">
            <input class="form-control" type="text" id = "quantity" name="quantity" value ="${finance.accountName}" placeholder="Quantity"/>
            </div>
             </div>
            
             <div class="form-group">
            <label for="unit_price" class="col-lg-2 control-label col-sm-2 col-md-2 col-xs-2">Unit Price</label>
             <div class="col-lg-3 col-sm-2 col-md-3 ol-xs-2">
            <input class="form-control" type="text" id = "unit_price" name="unit_price" value ="${finance.accountName}" placeholder="Unit Price"/>
            </div>
             </div>
            
             <div class="form-group">
            <label for="total" class="col-lg-2 control-label col-sm-2 col-md-2 col-xs-2">Total</label>
             <div class="col-lg-3 col-sm-2 col-md-3 ol-xs-2">
            <input class="form-control" type="text" id = "total" name="total" value ="${finance.accountName}" placeholder="Total"/>
            </div>
             </div>
            
            <div class="form-group">
                    <label for="store" class="col-lg-2 control-label col-sm-2 col-md-2 col-xs-2">Store</label>
                    <div class="col-lg-3 col-sm-2 col-md-3 ol-xs-2">
                <select class="form-control" id="store"name ="store">
                    <option>Type 1</option>
                    <option>Type 2</option>
                    <option>Type 3</option>
                </select>
                </div>
                 </div>
            
             <div class="form-group">
            <label for="issue_date" class="col-lg-2 control-label col-sm-2 col-md-2 col-xs-2">Issue Date</label>
             <div class="col-lg-3 col-sm-2 col-md-3 ol-xs-2">
            <input class="form-control" type="text" id = "issue_date" name="issue_date" value ="${finance.accountName}" placeholder="Issue Date"/>
            </div>
             </div>
            
            <div class="form-group">
            <label for="issued_by" class="col-lg-2 control-label col-sm-2 col-md-2 col-xs-2">Issue By</label>
             <div class="col-lg-3 col-sm-2 col-md-3 ol-xs-2">
            <input class="form-control" type="text" id = "issued_by" name="issued_by" value ="${finance.accountName}" placeholder="Issue By"/>
            </div>
             </div>
                
            </div>
            <div id="fragment-5">
                <p></p>
              <div class="form-group">
            <label for="file_name" class="col-lg-2 control-label col-sm-2 col-md-2 col-xs-2">File Name</label>
             <div class="col-lg-3 col-sm-2 col-md-3 ol-xs-2">
            <input class="form-control" type="text" id = "file_name" name="file_name" value ="${finance.accountName}" placeholder="File Name"/>
            </div>
             </div>
            
            <div class="form-group">
            <label for="file_name" class="col-lg-2 control-label col-sm-2 col-md-2 col-xs-2">File Name</label>
             <div class="col-lg-3 col-sm-2 col-md-3 ol-xs-2">
            <input class="form-control" type="text" id = "file_name" name="file_name" value ="${finance.accountName}" placeholder="File Name"/>
            </div>
             </div>
            
            <div class="file-field">
        <div class="btn btn-outline-success btn-rounded waves-effect btn-sm">
            <span>Choose file</span>
            <input type="file">
        </div>
        <div class="file-path-wrapper">
           <input class="file-path validate" type="text" placeholder="Upload your file">
        </div>
    </div>
           

	</div>
            <div id="fragment-6">
                        <div id="scheduler_here" class="dhx_cal_container" style='width:100%; height:100%; padding:20%;'>
    <div class="dhx_cal_navline">
        <div class="dhx_cal_prev_button">&nbsp;</div>
        <div class="dhx_cal_next_button">&nbsp;</div>
        <div class="dhx_cal_today_button"></div>
        <div class="dhx_cal_date"></div>
        <div class="dhx_cal_tab" name="day_tab" style="right:204px;"></div>
        <div class="dhx_cal_tab" name="week_tab" style="right:140px;"></div>
        <div class="dhx_cal_tab" name="month_tab" style="right:76px;"></div>
    </div>
    <div class="dhx_cal_header"></div>
    <div class="dhx_cal_data"></div>       
</div>
                
            </div>
                           

                
            
            </form>
           
 

             
                       

            </div>
            
                            </div>
                             </div>

           </div>
                  </div>   
            
                      <div class="col-md-6 col-sm-12 col-xs-12" > 

                  </div> 
                 
            </div>
            </div>
         <script src="/assets/js/jquery.js"></script>
         <script src="/assets/js/jquery-ui.js"></script>
        <script src="/assets/js/external.js"></script>
        <script src="/assets/js/Char.min.js"></script>
        <script src="/assets/js/bootstrap-progressbar.min.js"></script>
        <script src="/assets/js/bootstrap.min.js"></script>
        <script src="/assets/js/custom.min.js"></script> 
        
  <script>
      scheduler.init('scheduler_here', new Date(),"month");
$( "#tabs" ).tabs();

</script>


    </body>
</html>
