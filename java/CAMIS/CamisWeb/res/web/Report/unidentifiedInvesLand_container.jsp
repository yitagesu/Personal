<%-- 
    Document   : listLandInvestment_container
    Created on : Jan 8, 2018, 11:22:45 AM
    Author     : meda
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
    <html>
        <head>
            <link href="/assets/images/launcher.png" rel="icon" type="image/png"  >
        <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="/assets/css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">
        <link href="/assets/css/plugins/dataTables.bootstrap.css" rel="stylesheet">

        <link href="/assets/css/bootstrap-select.css" rel="stylesheet" type="text/css">
        <link href="/assets/css/jquery-ui.css" rel="stylesheet" type="text/css">
        <link href="/assets/css/ethdate/jquery.calendars.picker.css" rel="stylesheet" type="text/css">
        <link href="/assets/css/bootstrap.vertical-tabs.css" type="text/css" rel="stylesheet">
        <link href="/assets/css/report.css" type="text/css" rel="stylesheet">
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
            <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
        </head>
        <body>
            <div id="report_top" class ="row">
                 
                <br>
                       <div class="col-md-4 col-sm-4 col-xs-4">
                           
                         <div class="input-group" id="land-use-content">
                        <span class="input-group-addon">Region</span>
                        <select id="lease_invest" class="form-control">
                        <option>Southern People</option>    
                        <option>Amhara</option>
                        <option>Oromia</option>
                        <option>Tigray</option>
                        
                        </select>
                         </div>
                           <br>
                        <div class="input-group" id="land-use-content">
                        <span class="input-group-addon">Woreda</span>
                        <select id="lease_invest" class="form-control">
                        <option>Meskan</option>
                        <option>Sodo Woreda</option>
                        <option>Enidguagn</option>
                        <option>Gumer</option>
                        <option>Cheha</option>
                        <option>Enemor Ener</option>
                         <option>Muhur Na Aklil</option>
                        <option>Geta</option>
                         <option>Kebena</option>
                        <option>Ezha</option>
                          <option>Abeshege</option>
                         <option>Kokir Gedebano</option>
                        
                        </select>
                         </div>
                      
                 </div>
                      
                      <div class="col-md-4 col-sm-4 col-xs-4">
                         <div class="input-group" id="land-use-content">
                        <span class="input-group-addon">Zone</span>
                        <select id="lease_invest" class="form-control">
                        <option>Gurage</option>
                        <option>Hadiya</option>
                        <option>Kembata Timbaro</option>
                        <option>Sidama</option>
                         <option>Gedeo</option>
                        <option>Wolayita</option>
                        <option>South Omo</option>
                        <option>Sheka</option>
                         <option>Kefa</option>
                        <option>Gamo Gofa</option>
                        <option>Bench Maji</option>
                        <option>Yem</option>
                         <option>Amaro Special</option>
                         <option>Burdi Special</option>
                        <option>Konso Special</option>
                        <option>Derashe Lyiu Woreda</option>
                        <option>Dawuro</option>
                        </select>
                         </div>
                          <br>
                        
                 </div>
                      
                        <div class="col-md-4 col-sm-4 col-xs-4 align-right">
                    <div class="btn-group" style="display: flex;">
                        <button class="btn btn-success" type="button">Generate</button>
                    </div>
                </div>
                      <br>
                       <br>
                     
                       
          
                
            </div>
             <div id="report_bottom" class="col-md-12 col-sm-12 col-xs-12">
            </div>
        </body>
        <script>    
               $(document).ready(function(){
                   $("button").click(function(){
                       $("#report_bottom").load("/Report/unidentifiedInvesLand_content.jsp");
                       });
                       });
        </script>
    </html>
