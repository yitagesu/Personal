<%-- 
    Document   : listLandInvestment_container
    Created on : Jan 8, 2018, 11:22:45 AM
    Author     : meda
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
    <html>
        <head>
            <link href="/assets/images/launcher.png" rel="icon" type="image/png"  >
        <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="/assets/css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">
        <link href="/assets/css/plugins/dataTables.bootstrap.css" rel="stylesheet">

        <link href="/assets/css/bootstrap-select.css" rel="stylesheet" type="text/css">
        <link href="/assets/css/jquery-ui.css" rel="stylesheet" type="text/css">
        <link href="/assets/css/ethdate/jquery.calendars.picker.css" rel="stylesheet" type="text/css">
        <link href="/assets/css/bootstrap.vertical-tabs.css" type="text/css" rel="stylesheet">
        <link href="/assets/css/report.css" type="text/css" rel="stylesheet">
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
            <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
        </head>
        <body>
            <div class ="row">
                  <div id="report_top" class="col-md-12 col-sm-12 col-xs-12">
                      <br>
                      
                        
                <div class="col-md-4 col-sm-4 col-xs-4">
                         <div class="input-group" id="land-use-content">
                        <span class="input-group-addon">Investor Type</span>
                        <select id="lease_invest" class="form-control">
                        <option>Local</option>    
                        <option>Diaspora</option>
                        <option>Foreign</option>
                        <option>Dual Citizenship</option>
                        
                        </select>
                         </div>
                   
                 </div>
                      
                        <div class="col-md-4 col-sm-4 col-xs-4">
                         <div class="input-group" id="land-use-content">
                        <span class="input-group-addon">Investment Type</span>
                        <select id="lease_invest" class="form-control">
                        <option>Horticulture</option>
                        <option>Api-culture</option>
                        <option>Livestock</option>
                        <option>Large scale farming</option>
                        </select>
                         </div>
                 </div>
                      
                        <div class="col-md-4 col-sm-4 col-xs-4">
                    <div class="btn-group" style="display: flex;">
                        <button class="btn btn-success" type="button">Generate</button>
                    </div>
                </div>
                      <br>
                       <br>
                      <div class="col-md-4 col-sm-4 col-xs-4">
                        
                 </div>
                
                     <div id="report_bottom" class="col-md-12 col-sm-12 col-xs-12">
            </div>
            </div>
        </body>
        <script>
               $(document).ready(function(){
                   $("button").click(function(){
                       $("#report_bottom").load("/Report/investorAwarded_content.jsp");
                       });
                       });
        </script>
    </html>
