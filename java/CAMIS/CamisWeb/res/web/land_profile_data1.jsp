<%-- 
    Document   : fetch_data
    Created on : Dec 22, 2017, 2:50:27 PM
    Author     : user
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.intaps.camisweb.*"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>CAMIS</title>
        <link rel="stylesheet" href="assets/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="assets/css/external.css"/>

        <link rel="stylesheet" href="assets/css/custom.min.css"/>
        <style>
            .table-borderless > tbody > tr > td,
.table-borderless > tbody > tr > th,
.table-borderless > tfoot > tr > td,
.table-borderless > tfoot > tr > th,
.table-borderless > thead > tr > td,
.table-borderless > thead > tr > th {
    border: none;
}
        </style>
    
    </head>
  
        <body class="nav-sm">
            <div class="container body">
            <div class="main_container">
                 <%@include file="all.jsp" %>
                <div class="right_col tab-content" role="main">
                    <div>
                        <div class="row">
                            
                            <div class="col-md-12 col-sm-6 col-xs-12">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="x_panel">
                                    
                                      <div class="form-group align-left">
                                                <button id="return" class="btn btn-success "> Back</button>
                                                <button id="return" class="btn btn-success ">Update</button>
                                                <button id="return" class="btn btn-success "> Edit</button>
                                               
                                            </div>
                                    
                                        <table class="table table-borderless">
                                            <thead>
                                            <th></th>
                                            <th></th>
                                            </thead>
                                            <tbody>
                                                <%
                                                LandInfo.LandBankProfile retLandPro = Land.getLandBankProfileData();
                                                for(LandInfo.LandBankProfile.LandBankProfileData val: retLandPro.data){
                                                %>
                                                <tr class="bg-primary"><td>Parcel ID/Block ID/Farm ID (UPID)</td><td></td></tr>
                                                <tr ><td ><strong>Parcel Id</strong></td>
                                                    <td><b><%=val.parcelId%></b></td>
                                                </tr>
                                                 <tr class="bg-primary"><td>Address</td><td></td></tr>
                                                <tr><td><strong>Kebele</strong></td>
                                                    <td ><b><%=val.kebele%></b></td>
                                                    </tr>
                                                <tr><td><strong>Woreda</strong></td>
                                                    <td ><b><%=val.woreda%></b></td></tr>
                                                
                                                <tr><td><strong>Zone</strong></td>
                                                <td ><b><%=val.zone%></td></tr>
                                                
                                                <tr><td><strong>Region</strong></td>
                                                <td><b><%=val.region%></td></tr>
                                                
                                                <tr><td><strong>City</strong></td>
                                                <td><b><%=val.city%></td></tr>
                                                
                                                <tr><td><strong>Address</strong></td>
                                               	<td><b><%=val.address%></td></tr>
                                                 <tr><td><strong>Distance from point of center(region capital)</strong></td>
                                                     <td><b><%=val.distance%></td></tr>
                                                 <tr><td><strong>Centroid</strong></td>
                                                 <td><%=val.centroid%></td></tr>
                                                 <tr><td><strong>X Coordinate</strong></td>
                                                 <td><%=val.xCordinate%></td></tr>
                                                 <tr><td><strong>Y Coordinate</strong></td>
                                                   <td><b><%=val.yCordinate%></td></tr>
                                                
                                                 <tr class="bg-primary"><td>Neighbor</td><td></td></tr>
                                                  <tr><td><strong>From North---</strong></td>
                                                    <td><b><%=val.neighrbourFromNorth%></td></tr>
                                                   <tr><td><strong>From South--</strong></td>
                                                    <td><b><%=val.neighrbourFromSouth%></td></tr>
                                                    <tr><td><strong>From East--</strong></td>
                                                    <td><b><%=val.neighrbourFromWest%></td></tr>
                                                     <tr><td><strong>From West--</strong></td>
                                                    <td><b><%=val.neighrbourFromEast%></td></tr>
                                                  
                                                     <tr class="bg-primary"><td>Accessibility</td><td></td></tr>
                                                    <tr><td><strong>Air</strong></td>
                                                    <td><b><%=val.accessibilityAir%></td></tr>
                                                     <tr><td><strong>Road</strong></td>
                                                    <td><b><%=val.accessibilityRoad%></td></tr>
                                                    <tr><td><strong>Rail</strong></td>
                                                    <td><b><%=val.accessibilityRail%></td></tr>
                                                     <tr><td><strong>Water</strong></td>
                                                    <td><b><%=val.accessibilityWater%></td></tr>
                                                 
                                                  <tr class="bg-primary"><td>Area</td><td></td></tr>
                                                   <tr><td><strong>Area(hectare)</strong></td>
                                                    <td><b><%=val.area%></td></tr>
                                                     <tr><td><strong>condition of the expansion spots</strong></td>
                                                    <td><b><%=val.conditionExpansion%></td></tr>
                                                    
                                                    <tr class="bg-primary"><td>Status of infrastructure and services</td><td></td></tr>
                                                   <tr><td><strong>Road-Asphalt and Electric Line</strong></td>
                                                    <td><b><%=val.statusInfraRoadElec%></td></tr>
                                                     <tr><td><strong>Telecommunication Service</strong></td>
                                                    <td><b><%=val.statusInfraTele%></td></tr>
                                                      <tr><td><strong>Health</strong></td>
                                                    <td><b><%=val.statusInfraHealth%></td></tr>
                                                       <tr><td><strong>education</strong></td>
                                                    <td><b><%=val.statusInfraEducation%></td></tr>
                                                       <tr><td><strong>Closest administration office</strong></td>
                                                    <td><b><%=val.statusInfraAdmin%></td></tr>
                                                    
                                                  <%
                                                        }
                                                        %>
                                            </tbody>
                                        </table>
                                                        
                          
                        </div>
                                                        
                    </div>
                                              
                       <div class="col-md-6 col-sm-18 col-xs-18">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>Thematic</h2>
                                       
                                        
                                    </div>

                                    <div class="x_content">
                                        <div class="map has-scroll" id="map">
                                            <img src="assets/images/map.jpg" class="img-responsive"/>
                                        </div>
                                    </div
                                    
                                </div>
                           
                                           <table class="table table-borderless">
                                            <thead>
                                            <th></th>
                                            <th></th>
                                            </thead>
                                            <tbody>
                                                <%
                                                LandInfo.LandBankProfile retLandPro1 = Land.getLandBankProfileData();
                                                for(LandInfo.LandBankProfile.LandBankProfileData val: retLandPro1.data){
                                                %>
                                            
                                                <tr><td></td><td></td></tr>
                                                 <tr class="bg-primary"><td>Land Use</td><td></td></tr>
                                                     <tr><td><strong>Land use</strong></td>
                                                    <td><%=val.landUse%></td></tr>
                                                    <tr><td><strong>Investment Type</strong></td>
                                                    <td><%=val.investmentType%></td></tr>
                                                    
                                                     <tr class="bg-primary"><td>Soil Status</td><td></td></tr>
                                                    <tr><td><strong>Soil fertility test result</strong></td>
                                                    <td>testResult.docx</td></tr>
                                                   
                                                  <tr class="bg-primary"><td>Agro-Ecology</td><td></td></tr>
                                                    <tr><td><strong>Socio-economic study</strong></td>
                                                    <td><%=val.socioEconomy%></td></tr>
                                                    <tr><td><strong>Agero-ecology</strong></td>
                                                    <td><%=val.ageroEcology%></td></tr>
                                                    
                                                     <tr class="bg-primary"><td>Lease Information</td><td></td></tr>
                                                      <tr><td><strong>Lease Type</strong></td>
                                                    <td><%=val.leaseType%></td></tr>
                                                      <tr><td><strong>Lease Duration</strong></td>
                                                    <td><%=val.leaseDuration%></td></tr>
                                                      <tr><td><strong>Lease amount per hectare</strong></td>
                                                    <td><%=val.leasePerHectare%></td></tr>
                                                     
                                                     <tr class="bg-primary"><td>Weather Condition and Others</td><td></td></tr>
                                                     <tr><td><strong>Surface and underground water potential</strong></td>
                                                    <td><%=val.surUnderWaterPote%></td></tr>
                                                     
                                                     <tr><td><strong>Water potential for irrigation services</strong></td>
                                                    <td><%=val.waterPote%></td></tr>
                                                      <tr><td><strong>Wind directions and speed</strong></td>
                                                    <td><%=val.windDirecSpeed%></td></tr>
                                                     <tr><td><strong>Rainfall distribution</strong></td>
                                                    <td><%=val.rainfallDist%></td></tr>
                                                     
                                                     <tr><td><strong>Atmospheric moisture content(Humidity)</strong></td>
                                                    <td><%=val.humudity%></td></tr>
                                                     
                                                      <tr><td><strong>The height above the sea level</strong></td>
                                                    <td><%=val.height%></td></tr>
                                                    
                                                     <tr><td><strong>Cover of herbs</strong></td>
                                                    <td><%=val.coverOfHerbs%></td></tr>
                                                     <tr><td><strong>Landscape</strong></td>
                                                    <td><%=val.landscape%></td></tr>
                                                     <tr><td><strong>Others</strong></td>
                                                    <td><%=val.others%></td></tr>
                                                <%}%>
                                            </tbody>
                                            </table>
                            </div>
                            
                                            
                        </div>                                  
                
                    
                    
                    
                    
                    </div>
                                            </div>
            </div>
             </div>                                  
                </div>
            </div>

    <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/external.js"></script>
        <script src="assets/js/Char.min.js"></script>
        <script src="assets/js/bootstrap-progressbar.min.js"></script>
        <script src="assets/js/icheck.min.js"></script>
        <script src="assets/js/custom.min.js"></script>
        <script>
            
          $('#return').on('click',  function() {
                history.go(-1);
            });  
            
        </script>
</body>
</html>
