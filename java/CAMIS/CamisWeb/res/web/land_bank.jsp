<%-- 
    Document   : login.jsp
    Created on : Nov 9, 2017, 4:30:56 PM
    Author     : Yitagesu
--%>



<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>CAMIS</title>
        <link rel="stylesheet" href="/assets/css/external.css"/>
        <link rel="stylesheet" href="/assets/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="/assets/css/custom.min.css"/>
        <link rel="stylesheet" href="/assets/css/ol.css"/>
        <style>
        #map_preview {
            clear: both;
            position: relative;
            width: 768px;
            height: 595px;
            border: 1px solid black;
        }
           #map_preview2 {
            clear: both;
            position: relative;
            width: 350px;
            height: 200px;
            border: 1px solid black;
        }

         .ol-popup {
        position: absolute;
        background-color: white;
        -webkit-filter: drop-shadow(0 1px 4px rgba(0,0,0,0.2));
        filter: drop-shadow(0 1px 4px rgba(0,0,0,0.2));
        padding: 15px;
        border-radius: 10px;
        border: 1px solid #cccccc;
        bottom: 12px;
        left: -50px;
        min-width: 280px;
      }
      .ol-popup:after, .ol-popup:before {
        top: 100%;
        border: solid transparent;
        content: " ";
        height: 0;
        width: 0;
        position: absolute;
        pointer-events: none;
      }
      .ol-popup:after {
        border-top-color: white;
        border-width: 10px;
        left: 48px;
        margin-left: -10px;
      }
      .ol-popup:before {
        border-top-color: #cccccc;
        border-width: 11px;
        left: 48px;
        margin-left: -11px;
      }
      .ol-popup-closer {
        text-decoration: none;
        position: absolute;
        top: 2px;
        right: 8px;
      }
      .ol-popup-closer:after {
        content: "X";
      }
      .buttons{
          float: right;
      }
      td a {
    display:block;
    width:100%;
  
}


 a.view {
       color: #0000ff;
  }
 a.details {
       color: #00ff7f;
  }
    </style>
        

    </head>

    <body class="nav-sm">
        <div class="container body">
            <div class="main_container">
<div class="col-md-3 left_col menu_fixed">
                    <div class="left_col scroll-view">
                        <div class="navbar nav_title" style="border: 0;">
                            <a href="#" class="site_title"><i class="fa fa-map-marker"></i> <span>CAMiS</span></a>
                        </div>

                        <div id="sidebar-menu" class="main_menu_side main_menu">
                            <div class="menu_section">
                                <h3 style="visibility:hidden;">general</h3>
                                <ul class="nav side-menu">
                                    <li class="active"><a href=".dashboard" data-toggle="tab" ><i class="fa fa-table"></i> Dashboard </a>
                                    </li>
                                    <li><a href="#user" data-toggle="tab" ><i class="fa fa-user"></i> User Management</a>
                                    </li>
                                    <li><a><i class="fa fa-gears"></i> Setting</a>
                                    </li>
                                    <li><a><i class="fa fa-gear"></i> Other</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="top_nav">
                    <div class="nav_menu">
                        <nav>
                            <div class="nav toggle">
                                <a id=""><i class="fa fa-map-marker"></i> <span>CAMiS</span></a>
                                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                            </div>
                            

                            <ul class="nav navbar-nav navbar-right">
                                <li class="">
                                    <a href="" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        Welcome yite
                                        <span class=" fa fa-angle-down"></span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                                        <li><a href="#"> Change Password</a></li>
                                        <li><a href="#">Help</a></li>
                                        <li><a href="#"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="right_col tab-content" role="main">
                    <div class=" right_col_content col-md-12" style=" margin-top: -50pt">
                        <div class="row ">
                            <div class="col-md-6 col-sm-6 col-xs-12" id="map-tab">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>Map</h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                            </li>
                                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>

                                    <div class="x_content">
                                        <div class="" id="map_preview">
                                            
                                        </div>
                                        
                                                <div id="location"></div>
                                                            <div id="scale">
                                                        </div>
                                                        <div id="label">
                                                            <em>Click on the map to get feature info</em>
                                                        </div>
                                        <div id="popup" class="ol-popup">
                                        <a href="#" id="popup-closer" class="ol-popup-closer"></a>
                                        <div id="popup-content"></div>
                                      </div>

                                    </div>
                                    <div class="x_footer"></div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12" id="map-legend-tab">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>Map Legend</h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                            </li>
                                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="map-legend">
                                        <div class="col-md-6 col-ms-6 col-xs-6" id = "map_preview2">
                                            
                                        </div>
                                        <div class="col-md-4 col-ms-4 col-xs-4 table-responsive">
                                            <table class="table table-striped">
                                                <tbody>
                                                    <tr class="even pointer">
                                                        <td class=" " style="color:greenyellow;"><i class="fa fa-square"></i> </td>
                                                        <td class=" ">Land Bank
                                                        </td>
                                                        <td><input type="checkbox" id="in_land_bank" name="land_type"value="in_land_bank" checked ="checked"/> </td>
                                                    </tr>
                                                    <tr class="even pointer">
                                                        <td class=" " style="color:fuchsia;"><i class="fa fa-square"></i></td>
                                                        <td class=" ">Transfered Land</td>
                                                        <td><input type="checkbox" id ="transfered" name="land_type"value="transfered" checked ="checked"/> </td>
                                                    </tr>
                                                    <tr class="even pointer">
                                                        <td class=" " style="color:firebrick;"><i class="fa fa-square"></i></td>
                                                        <td class=" " >Developed land</td>
                                                        <td><input type="checkbox" id ="unidentified" name="land_type"value="unidentified" checked ="checked"/> </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div>
                                        <input type="text" class="form-control mb-2 mr-sm-2 mb-sm-0" id="inlineFormInput" placeholder="Search Away" style="width:300px;margin-top:250px"/>
                                        <hr />
                                    </div>
                                    <div class="x_content">

                                        <div id ="nodelist" class="map-legend-detail">
                                            <table class="table table-striped jambo_table bulk_action" id="tclick">
                                                <caption><h3>Transfered Land</h3></caption>
                                                <thead>
                                                    <tr class="headings">
                                                        <th class="column-title">UPID</th>
                                                        <th class="column-title">Owner</th>
                                                        <th class="column-title">Zone </th>
                                                        <th class="column-title">Woreda</th>
                                                        <th class="column-title">Kebele</th>
                                                        <th class="column-title">Area</th>
                                                        <th class="column-title">Land Use</th>
                                                        <th class="column-title"></th>
                                                        <th class="column-title"></th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    <tr class="even pointer" >
                                                        <td>0001</td>
                                                        <td>Intaps</td>
                                                        <td>Zone 1</td>
                                                        <td>Woreda 01</td>
                                                        <td>Kebele 02</td>
                                                        <td>1000</td>
                                                        <td>Horticulture</td>
                                                        <td><a href ="#view">View</a></td>
                                                        <td><a href ="#details">Details</a></td>
                                                        

                                                    </tr>
                                                       <tr class="even pointer" >
                                                        <td>0002</td>
                                                        <td>diaspora</td>
                                                        <td>Zone 2</td>
                                                        <td>Woreda 01</td>
                                                        <td>Kebele 03</td>
                                                        <td>2000</td>
                                                        <td>Horticulture</td>
                                                        <td><a href ="#view">View</a></td>
                                                        <td><a href ="#details">Details</a></td>
                                                    </tr>
                                                    <tr class="even pointer" >
                                                        <td>0003</td>
                                                        <td>spatni</td>
                                                        <td>Zone 4</td>
                                                        <td>Woreda 01</td>
                                                        <td>Kebele 03</td>
                                                        <td>6000</td>
                                                        <td>Irrigation</td>
                                                        <td><a href ="#view">View</a></td>
                                                        <td><a href ="#details">Details</a></td>
                                                        
                                                    </tr>
                                                    
                                                     <tr class="even pointer" >
                                                        <td>0004</td>
                                                        <td>diaspora2</td>
                                                        <td>Zone 14</td>
                                                        <td>Woreda 22</td>
                                                        <td>Kebele 11</td>
                                                        <td>4000</td>
                                                        <td>Live Stock</td>
                                                        <td><a href ="#view">View</a></td>
                                                        <td><a href ="#details">Details</a></td>
                                                    </tr>
                                                     <tr class="even pointer" >
                                                        <td>0005</td>
                                                        <td>investor</td>
                                                        <td>Zone 8</td>
                                                        <td>Woreda 2</td>
                                                        <td>Kebele 19</td>
                                                        <td>7000</td>
                                                        <td>Live Stock</td>
                                                        <td><a href ="#view">View</a></td>
                                                        <td><a href ="#details">Details</a></td>
                                                    </tr>
                                                    <tr class="even pointer" >
                                                        <td>0006</td>
                                                        <td>Alamudin</td>
                                                        <td>Zone 30</td>
                                                        <td>Woreda 11</td>
                                                        <td>Kebele 7</td>
                                                        <td>10000</td>
                                                        <td>Apiculture</td>
                                                        <td><a href ="#view">View</a></td>
                                                        <td><a href ="#details">Details</a></td>
                                                    </tr>
                                                    <tr class="even pointer" >
                                                        <td>0007</td>
                                                        <td>Abebe Integrated plc</td>
                                                        <td>Zone 4</td>
                                                        <td>Woreda 09</td>
                                                        <td>Kebele 22</td>
                                                        <td>4500</td>
                                                        <td>Apiculture</td>
                                                        <td><a href ="#view">View</a></td>
                                                        <td><a href ="#details">Details</a></td>
                                                    </tr> 
                                                    
                                                          <tr class="even pointer" >
                                                        <td>0007</td>
                                                        <td>Abebe Integrated plc</td>
                                                        <td>Zone 4</td>
                                                        <td>Woreda 09</td>
                                                        <td>Kebele 22</td>
                                                        <td>4500</td>
                                                        <td>Apiculture</td>
                                                        <td><a href ="#view">View</a></td>
                                                        <td><a href ="#details">Details</a></td>
                                                    </tr> 
                                                          <tr class="even pointer" >
                                                        <td>0007</td>
                                                        <td>Abebe Integrated plc</td>
                                                        <td>Zone 4</td>
                                                        <td>Woreda 09</td>
                                                        <td>Kebele 22</td>
                                                        <td>4500</td>
                                                        <td>Apiculture</td>
                                                        <td><a href ="#view">View</a></td>
                                                        <td><a href ="#details">Details</a></td>
                                                    </tr> 
                                                          <tr class="even pointer" >
                                                        <td>0007</td>
                                                        <td>Abebe Integrated plc</td>
                                                        <td>Zone 4</td>
                                                        <td>Woreda 09</td>
                                                        <td>Kebele 22</td>
                                                        <td>4500</td>
                                                        <td>Apiculture</td>
                                                        <td><a href ="#view">View</a></td>
                                                        <td><a href ="#details">Details</a></td>
                                                    </tr> 
                                                          <tr class="even pointer" >
                                                        <td>0007</td>
                                                        <td>Abebe Integrated plc</td>
                                                        <td>Zone 4</td>
                                                        <td>Woreda 09</td>
                                                        <td>Kebele 22</td>
                                                        <td>4500</td>
                                                        <td>Apiculture</td>
                                                        <td><a href ="#view">View</a></td>
                                                        <td><a href ="#details">Details</a></td>
                                                    </tr> 
                                                          <tr class="even pointer" >
                                                        <td>0007</td>
                                                        <td>Abebe Integrated plc</td>
                                                        <td>Zone 4</td>
                                                        <td>Woreda 09</td>
                                                        <td>Kebele 22</td>
                                                        <td>4500</td>
                                                        <td>Apiculture</td>
                                                        <td><a href ="#view">View</a></td>
                                                        <td><a href ="#details">Details</a></td>
                                                    </tr> 
                                                          <tr class="even pointer" >
                                                        <td>0007</td>
                                                        <td>Abebe Integrated plc</td>
                                                        <td>Zone 4</td>
                                                        <td>Woreda 09</td>
                                                        <td>Kebele 22</td>
                                                        <td>4500</td>
                                                        <td>Apiculture</td>
                                                        <td><a href ="#view">View</a></td>
                                                        <td><a href ="#details">Details</a></td>
                                                    </tr> 
                                                          <tr class="even pointer" >
                                                        <td>0007</td>
                                                        <td>Abebe Integrated plc</td>
                                                        <td>Zone 4</td>
                                                        <td>Woreda 09</td>
                                                        <td>Kebele 22</td>
                                                        <td>4500</td>
                                                        <td>Apiculture</td>
                                                        <td><a href ="#view">View</a></td>
                                                        <td><a href ="#details">Details</a></td>
                                                    </tr> 
                                            
                                                </tbody>
                                            </table>
                                            
                                            <button id="nextt" class="buttons">Next</button>
                                            <button id = "prevt" class="buttons">Previous</button>
                                        </div>
                                        
                                        
                                             <div  id = "nodelist1"class="map-legend-detail">
                                            <table class="table table-striped jambo_table bulk_action" id="tclick1">
                                                <caption><h3>Land bank</h3></caption>
                                                <thead>
                                                    <tr class="headings">
                                                        <th class="column-title">UPID</th>
                                                        <th class="column-title">Zone </th>
                                                        <th class="column-title">Woreda</th>
                                                        <th class="column-title">Kebele</th>
                                                        <th class="column-title">Area</th>
                                                        <th class="column-title">Land Use</th>
                                                        <th class="column-title"></th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                     <tr class="even pointer" >
                                                        <td>00034</td>
                                                        <td>Zone 4</td>
                                                        <td>Woreda 09</td>
                                                        <td>Kebele 22</td>
                                                        <td>4500</td>
                                                        <td>Apiculture</td>
                                                        <td>Details</td>
                                                    </tr> 
                                                       <tr class="even pointer" >
                                                        <td>0007</td>
                                                        <td>Zone 11</td>
                                                        <td>Woreda 06</td>
                                                        <td>Kebele 2</td>
                                                        <td>7500</td>
                                                        <td>Apiculture</td>
                                                        <td>Details</td>
                                                    </tr>
                                                    <tr class="even pointer" >
                                                        <td>0039</td>
                                                        <td>Zone 8</td>
                                                        <td>Woreda 03</td>
                                                        <td>Kebele 08</td>
                                                        <td>5500</td>
                                                        <td>Horticulture</td>
                                                        <td>Details</td>
                                                    </tr>
                                                     <tr class="even pointer" >
                                                        <td>0075</td>
                                                        <td>Zone 10</td>
                                                        <td>Woreda 22</td>
                                                        <td>Kebele 34</td>
                                                        <td>5500</td>
                                                        <td>Horticulture</td>
                                                        <td>Details</td>
                                                    </tr>
                                                     <tr class="even pointer" >
                                                        <td>0015</td>
                                                        <td>Zone 14</td>
                                                        <td>Woreda 13</td>
                                                        <td>Kebele 06</td>
                                                        <td>1500</td>
                                                        <td>Live Stock</td>
                                                        <td>Details</td>
                                                    </tr>
                                                    <tr class="even pointer" >
                                                        <td>0115</td>
                                                        <td>Zone 2</td>
                                                        <td>Woreda 03</td>
                                                        <td>Kebele 06</td>
                                                        <td>1900</td>
                                                        <td>Live Stock</td>
                                                        <td>Details</td>
                                                    </tr> 
                                            
                                                </tbody>
                                            </table>
                                        </div>
                                        
                                        
                                        <div  id = "nodelist2"class="map-legend-detail">
                                            <table class="table table-striped jambo_table bulk_action" id="tclick2">
                                                <caption><h3>Developed land</h3></caption>
                                                <thead>
                                                    <tr class="headings">
                                                        <th class="column-title">UPID</th>
                                                        <th class="column-title">Zone </th>
                                                        <th class="column-title">Woreda</th>
                                                        <th class="column-title">Kebele</th>
                                                        <th class="column-title">Area</th>
                                                        <th class="column-title">Land Use</th>
                                                        <th class="column-title"></th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    <tr class="even pointer" >
                                                    
                                                        <td>Intaps</td>
                                                        <td>Gambela</td>
                                                        <td>100&emsp;&emsp;<a class ="select">&emsp;&emsp;&emsp;Details</a></td>
                                                        

                                                    </tr>
                                                       <tr class="even pointer" >
                                                    
                                                        <td>diaspora</td>
                                                        <td>Gambela</td>
                                                        <td>2000</td>
                                                    </tr>
                                                    <tr class="even pointer" >
                                                    
                                                        <td>spatni</td>
                                                        <td>Amhara</td>
                                                        <td>6000</td>
                                                    </tr>
                                                    
                                                     <tr class="even pointer" >
                                                    
                                                        <td>diaspora2</td>
                                                        <td>Oromia</td>
                                                        <td>4000</td>
                                                    </tr>
                                                     <tr class="even pointer" >
                                                    
                                                        <td>investor</td>
                                                        <td>Oromia</td>
                                                        <td>7000</td>
                                                    </tr>
                                                    <tr class="even pointer" >
                                                    
                                                        <td>Alamudin</td>
                                                        <td>Somalia</td>
                                                        <td>10000</td>
                                                    </tr> 
                                            
                                                </tbody>
                                            </table>
                                        </div>
                                        

                                        
                                    </div>
                                    <div class="x_footer"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="right_col_footer">
                    </div>
                </div>
            </div>
            <script src="/assets/js/jquery.min.js"></script>
        <script src="/assets/js/jquery-ui.js"></script>
        <script src="/assets/js/external.js"></script>
        <script src="/assets/js/Chart.min.js"></script>
        <script src="/assets/js/bootstrap-progressbar.min.js"></script>
        <script src="/assets/js/bootstrap.min.js"></script>
        <script src="/assets/js/custom.min.js"></script>
        <script src="/assets/js/ol-debug.js"></script>
        <script src="/assets/js/proj4.js"></script>
        <script src="/assets/js/map.js"></script>
    </body>
</html>
