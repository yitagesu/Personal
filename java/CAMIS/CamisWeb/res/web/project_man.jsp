<%-- 
    Document   : project_man
    Created on : Jan 11, 2018, 7:18:45 AM
    Author     : Joseph
--%>
<%@page import="com.intaps.camisweb.*"%>
<%@page import="com.intaps.camisweb.viewmodel.*"%>
<%@page import="com.intaps.camisweb.vmcontroller.*"%>


<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>CAMIS</title>
        <link rel="stylesheet" href="/assets/css/external.css"/>
        <link rel="stylesheet" href="/assets/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="/assets/css/custom.min.css"/>
        
<style>
body {font-family: Arial;}

/* Style the tab */
.tab {
    overflow: hidden;
    border: 1px solid #ccc;
    background-color: #f1f1f1;
}

/* Style the buttons inside the tab */
.tab button {
    background-color: inherit;
    float: left;
    border: none;
    outline: none;
    cursor: pointer;
    padding: 14px 16px;
    transition: 0.3s;
    font-: 17px;
}

/* Change background color of buttons on hover */
.tab button:hover {
    background-color: #ddd;
}

/* Create an active/current tablink class */
.tab button.active {
    background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
    display: none;
    padding: 6px 12px;
    border: 1px solid #ccc;
    border-top: none;
}
.mtable th, .mtable td { 
     border-top: none !important; 
 }
</style>
    </head>

    <body class="nav-sm">
        
        <div class="container body">
            <div class="main_container">

           <%@include file="all.jsp" %>
                <div class="right_col tab-content" role="main"  id ="content">
                    <div>
                        <div class="row">
                           
                            
                            
                            
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel" id="content">
                                    <h2 style="color: black">Project Management</h2>
                                     
                                    <div class="panel panel-group">
                                        <div class="panel panel-primary">
                                            <div class="panel panel-heading"><i class="fa fa-inbox" aria-hidden="true"  style="color: goldenrod"></i> Project Detail</div>
                                            <div class="panel panel-body">
                                                <table>
                                                    <%
                                                ProjectInfo.project retDetails = Project.getproject();
                                                for(ProjectInfo.project.project_mgmt val:retDetails.pm){
                                                %>
                                                    <tr><td style="color: black"><strong>Project ID</strong></td><td style="padding-left:50pt"><%=val.project_id%></td></tr>
                                                    <tr><td style="color: black"><strong>Project Name</strong></td><td style="padding-left:50pt"><%=val.project_name%></td></tr>
                                                    <tr><td style="color: black"><strong>Project Type</strong></td><td style="padding-left:50pt"><%=val.project_type%></td></tr>
                                                    <tr><td style="color: black"><strong>Project Location</strong></td><td style="padding-left:50pt"><%=val.project_location%></td></tr>
                                                    <tr><td style="color: black"><strong>Zone</strong></td><td style="padding-left:50pt"><%=val.zone%></td></tr>
                                                    <tr><td style="color: black"><strong>Woreda</strong></td><td style="padding-left:50pt"><%=val.woreda%></td></tr>
                                                    <tr><td style="color: black"><strong>Kebele</strong></td><td style="padding-left:50pt"><%=val.kebele%></td></tr>
                                                    <tr><td style="color: black"><strong>City </strong></td><td style="padding-left:50pt"><%=val.city%></td></tr>
                                                    <tr><td style="color: black"><strong>Landmark feature in neighborhood to the project</strong></td><td style="padding-left:50pt"><%=val.landmark%></td></tr>
                                                    <tr><td style="color: black"><strong>prerequisite activities</strong></td><td style="padding-left:50pt"><%=val.perquisite_activites%></td></tr>
                                                    <tr><td style="color: black"><strong>Brief Description of Works</strong></td><td style="padding-left:50pt"><%=val.briefdesc_work%></td></tr>
                                                    <tr><td style="color: black"><strong>Brief Description of Project Sub Activities</strong></td><td style="padding-left:50pt"><%=val.briefdesc_psubactivites%></td></tr>
                                                    <%}%>
                                                </table>
                                                
                                                
                                            </div>
                                            
                                        </div>
                                        <div class="panel panel-primary">
                                            <div class="panel panel-heading"><i class="fa fa-clock-o" aria-hidden="true"  style="color: goldenrod"></i> Project Schedule</div>
                                            <div class="panel panel-body">
                                                <table>
                                                    <%
                                                ProjectInfo.project retDetails2 = Project.getproject();
                                                for(ProjectInfo.project.project_mgmt val:retDetails2.pm){
                                                %>
                                                    <tr><td style="color: black"><strong>Start Date</strong></td><td style="padding-left:50pt"><%=val.start_date%></td></tr>
                                                    <tr><td style="color: black"><strong>Project Sub activity(Milestone) dates</strong></td><td style="padding-left:50pt"><%=val.milestones%></td></tr>
                                                    <tr><td style="color: black"><strong>Finish Date</strong></td><td style="padding-left:50pt"><%=val.end_date%></td></tr>
                                                    <tr><td style="color: black"><strong>Project Calendar</strong></td><td style="padding-left:50pt">
                                                    <%}%>         
                                                            <image height="300px" width="600px" src="assets/images/Schedule.jpg"/>
                                                            
                                                        </td></tr>
                                                    
                                                </table>
                                                
                                                
                                            </div>
                                            
                                        </div>
                                        <div class="panel panel-primary">
                                            <div class="panel panel-heading"><i class="fa fa-users" aria-hidden="true"  style="color: goldenrod"></i> Project HR Requirement</div>
                                            <div class="panel panel-body">
                                                <table class="table table-striped jambo_table bulk_action table-bordered">
                                                    <thead>
                                                    <tr><td><strong>Employee ID</strong></td>
                                                    <td><strong>Full Name</strong></td>
                                                    <td><strong>Gender</strong></td>
                                                    <td><strong>Department</strong></td>
                                                    <td><strong>Specialization</strong></td>
                                                    <td><strong>Task Assigned</strong></td></tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>1234</td><td>Yonatan Abi</td><td>Male</td><td>Software Engineering</td><td>Programer</td><td>Project Manager</td>
                                                        </tr>
                                                         <tr>
                                                            <td>1235</td><td>Mulugeta Abi</td><td>Male</td><td>Computer  Engineering</td><td>Programer</td><td>Programing</td>
                                                        </tr>
                                                    </tbody>
                                                    
                                                </table>
                                                
                                                
                                            </div>
                                            
                                        </div>
                                        <div class="panel panel-primary">
                                            <div class="panel panel-heading"><i class="fa fa-dollar" aria-hidden="true"  style="color: goldenrod"></i> Project Financial Information</div>
                                            <div class="panel panel-body">
                                                <center><h2 style="color: black">Budget information</h2></center>
                                                <table class="table table-striped jambo_table bulk_action table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <td>Budget Code </td><td>Account Name</td><td>Budget amount</td>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr><td>167233412</td><td>EHAIA </td><td>10,000,000.00</td>
                                                    </tr>
                                                    </tbody>
                                                    
                                                </table>
                                                <center> <h2 style="color: black">Project Bank Account</h2></center>
                                                <table class="table table-striped jambo_table bulk_action table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <td>Bank name</td><td>Bank Branch</td><td>Bank Account</td>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr><td>Commercial bank of Ethiopia</td><td>CMC </td><td>1232100012</td>
                                                    </tr>
                                                    </tbody>
                                                    
                                                </table>
                                                <center><h2 style="color: black">Project Sponsor</h2></center>
                                                <table class="table table-striped jambo_table bulk_action table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <td rowspan="2">Sponsor name</td><td colspan="4">Sponsor Type</td><td rowspan="2">Quantity</td><td rowspan="2">Amount in birr</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Material</td><td>Knowledge</td><td>Money</td><td>Others</td>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>Yonatan</td><td>Gps Device </td><td></td><td></td><td> </td><td>1</td><td>5000.00</td>
                                                    </tr>
                                                    </tbody>
                                                    
                                                </table>
                                                
                                            </div>
                                            
                                        </div>
                                         <div class="panel panel-primary">
                                            <div class="panel panel-heading"><i class="fa fa-shopping-cart" aria-hidden="true"  style="color: goldenrod"></i> Project Inventory Information</div>
                                            <div class="panel panel-body">
                                                <table class="table table-striped jambo_table bulk_action table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <td>Item </td>
                                                            <td>Item Type</td>
                                                            <td>Item Status</td>
                                                            <td>Quantity</td>
                                                            <td>Unit Price</td>
                                                            <td>Price</td>
                                                            <td>Store</td>
                                                            <td>Issue Date</td>
                                                            <td>Issued by</td>
                                                            <td>Reason</td>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>GPS</td><td>Expense</td><td></td>
                                                            <td>1</td><td>5000</td><td>5000</td>
                                                            <td>Main Store</td><td>10/1/2018</td><td>Yonatan</td><td>For Demarcation</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="panel panel-primary">
                                            <div class="panel panel-heading"><i class="fa fa-files-o" aria-hidden="true"  style="color: goldenrod"></i> Project Files</div>
                                            <div class="panel panel-body">
                                                <table class="table table-striped jambo_table bulk_action table-bordered">
                                                    <thead>
                                                        <tr><td>File Name </td><td>File Type</td><td>Uploaded By</td><td>Date</td></tr>
                                                    </thead>
                                                    <tbody>
                                                         <tr><td>Project Report</td><td>Project report</td><td>Yonatan</td><td>Jan/11/2018</td></tr>
                                                         <tr><td>Project Progress </td><td>Progress report</td><td>Yonatan</td><td>Jan/6/2018</td></tr>
                                                         <tr><td>Project Difficulty </td><td>Project difficulty report</td><td>Yonatan</td><td>Jan/1/2018</td></tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                      
                                        
                                    </div>
                                       
                                   
                                </div>
                            </div>
                                                
                           
                               
                                                
                                                
                                                
                    </div>
                   
                </div>
            </div>
            </div>
         </div>
        <script src="/assets/js/jquery.js"></script>    
        <script src="/assets/js/jquery.min.js"></script>
        <script src="/assets/js/external.js"></script>
        <script src="/assets/js/Chart.min.js"></script>
        <script src="/assets/js/bootstrap-progressbar.min.js"></script>
        <script src="/assets/js/bootstrap.min.js"></script>
        <script src="/assets/js/custom.min.js"></script>
        
    </body>
</html>


