<%--
    Document   : bid_process
    Created on : Nov 25, 2017, 12:04:45 PM
    Author     : Isaac
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>CAMIS</title>
        <link rel="stylesheet" href="assets/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="assets/css/external.css"/>

        <link rel="stylesheet" href="assets/css/custom.min.css"/>
    </head>
        <body class="nav-sm">
            <div class="container body">
            <div class="main_container">

                <div class="col-md-3 left_col menu_fixed">
                    <div class="left_col scroll-view">
                        <div class="navbar nav_title" style="border: 0;">
                            <a href="#" class="site_title"><i class="fa fa-map-marker"></i> <span>CAMiS</span></a>
                        </div>

                        <div id="sidebar-menu" class="main_menu_side main_menu">
                            <div class="menu_section">
                                <h3 style="visibility:hidden;">general</h3>
                                <ul class="nav side-menu">
                                    <li class="active"><a href=".dashboard" data-toggle="tab" ><i class="fa fa-table"></i> Dashboard </a>
                                    </li>
                                    <li><a href="#user" data-toggle="tab" ><i class="fa fa-user"></i> User Management</a>
                                    </li>
                                    <li><a><i class="fa fa-gears"></i> Setting</a>
                                    </li>
                                    <li><a><i class="fa fa-gear"></i> Other</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="top_nav">
                    <div class="nav_menu">
                        <nav>
                            <div class="nav toggle">
                                <a id=""><i class="fa fa-map-marker"></i> <span>CAMiS</span></a>
                                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                            </div>

                            <ul class="nav navbar-nav navbar-right">
                                <li class="">
                                    <a href="" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        Welcome yite
                                        <span class=" fa fa-angle-down"></span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                                        <li><a href="#"> Change Password</a></li>
                                        <li><a href="#">Help</a></li>
                                        <li><a href="/login.jsp"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="right_col tab-content" role="main">
                    <div>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>Bid Process</h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                            </li>
                                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>

                                  <div class="x_content">
                                     <div class="row">
                                        <div class="col-md-4">
                                        <h3>Profile</h3>
                                        <table>
                                            <tr><td>Company's name : </td> <td>AGTECH TRADING PLC</td></tr>
                                            <tr><td>Company's owner name : </td> <td>Dawit Mulugeta</td></tr>
                                            <tr><td>Address : </td><td>Bole, Addis Ababa,Ethiopia</td></tr>
                                            <tr><td>Owner's status : </td><td>Local investor</td></tr>
                                            <tr><td>Company's started date : </td> <td> 23 Dec, 2000</td></tr>
                                            <tr><td>Work permit : </td> <td>Since 2002 the company got a work permit from federal government</td></tr>

                                        </table>
                                        </div>
                                         <div class="col-md-2 col-md-offset-6">
                                        <image height="140px" src="assets/images/mal.jpg"/>
                                        </div>
                                     </div>
                                   </div>
                                       <h4>Documents:</h4>
                                    <ul>
                                        <li>  Business plan : <a href="#">business_plan.docx</a></li>
                                        <li>  Evaluation Doc : <a href="#">evaluation.dcox</a></li>
                                    </ul>

                                    <h5>Evaluation</h5>
                                    <div class="search-result has-scroll col-md-6">
                                            <table class="table table-striped jambo_table bulk_action">            <thead>
                                                    <tr class="headings">
                                                        <th class="column-title">Evaluator Name </th>
                                                        <th class="column-title">Evaluation Area</th>
                                                        <th class="column-title">Evaluation</th>
                                                    </tr>
                                                </thead>

                                                <tbody>

                                                    <tr class="even pointer">
                                                        <td class=" ">yitagesu Kebede</td>
                                                        <td class=" ">business plan</td>
                                                        <td class=" ">Good</td>
                                                    </tr>
                                                    <tr class="even pointer">
                                                        <td class=" ">yitagesu Kebede</td>
                                                        <td class=" ">investment plan</td>
                                                        <td class=" ">Good</td>
                                                    </tr>

                                                </tbody>
                                            </table>
                                    </div>
                                        </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

    </body>
    <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/external.js"></script>
        <script src="assets/js/Char.min.js"></script>
        <script src="assets/js/bootstrap-progressbar.min.js"></script>
        <script src="assets/js/icheck.min.js"></script>
        <script src="assets/js/custom.min.js"></script>
</html>
